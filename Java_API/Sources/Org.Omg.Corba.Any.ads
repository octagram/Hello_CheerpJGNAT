pragma Extensions_Allowed (On);
limited with Java.Io.Serializable;
limited with Java.Lang.String;
limited with Java.Math.BigDecimal;
limited with Org.Omg.CORBA.Object;
limited with Org.Omg.CORBA.Portable.InputStream;
limited with Org.Omg.CORBA.Portable.OutputStream;
limited with Org.Omg.CORBA.Portable.Streamable;
limited with Org.Omg.CORBA.TypeCode;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.CORBA.Any is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Any (This : Ref := null)
                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Equal (This : access Typ;
                   P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class)
                   return Java.Boolean is abstract;

   function type_K (This : access Typ)
                    return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;

   procedure type_K (This : access Typ;
                     P1_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class) is abstract;

   procedure Read_value (This : access Typ;
                         P1_InputStream : access Standard.Org.Omg.CORBA.Portable.InputStream.Typ'Class;
                         P2_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class) is abstract;
   --  can raise Org.Omg.CORBA.MARSHAL.Except

   procedure Write_value (This : access Typ;
                          P1_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class) is abstract;

   function Create_output_stream (This : access Typ)
                                  return access Org.Omg.CORBA.Portable.OutputStream.Typ'Class is abstract;

   function Create_input_stream (This : access Typ)
                                 return access Org.Omg.CORBA.Portable.InputStream.Typ'Class is abstract;

   function Extract_short (This : access Typ)
                           return Java.Short is abstract;
   --  can raise Org.Omg.CORBA.BAD_OPERATION.Except

   procedure Insert_short (This : access Typ;
                           P1_Short : Java.Short) is abstract;

   function Extract_long (This : access Typ)
                          return Java.Int is abstract;
   --  can raise Org.Omg.CORBA.BAD_OPERATION.Except

   procedure Insert_long (This : access Typ;
                          P1_Int : Java.Int) is abstract;

   function Extract_longlong (This : access Typ)
                              return Java.Long is abstract;
   --  can raise Org.Omg.CORBA.BAD_OPERATION.Except

   procedure Insert_longlong (This : access Typ;
                              P1_Long : Java.Long) is abstract;

   function Extract_ushort (This : access Typ)
                            return Java.Short is abstract;
   --  can raise Org.Omg.CORBA.BAD_OPERATION.Except

   procedure Insert_ushort (This : access Typ;
                            P1_Short : Java.Short) is abstract;

   function Extract_ulong (This : access Typ)
                           return Java.Int is abstract;
   --  can raise Org.Omg.CORBA.BAD_OPERATION.Except

   procedure Insert_ulong (This : access Typ;
                           P1_Int : Java.Int) is abstract;

   function Extract_ulonglong (This : access Typ)
                               return Java.Long is abstract;
   --  can raise Org.Omg.CORBA.BAD_OPERATION.Except

   procedure Insert_ulonglong (This : access Typ;
                               P1_Long : Java.Long) is abstract;

   function Extract_float (This : access Typ)
                           return Java.Float is abstract;
   --  can raise Org.Omg.CORBA.BAD_OPERATION.Except

   procedure Insert_float (This : access Typ;
                           P1_Float : Java.Float) is abstract;

   function Extract_double (This : access Typ)
                            return Java.Double is abstract;
   --  can raise Org.Omg.CORBA.BAD_OPERATION.Except

   procedure Insert_double (This : access Typ;
                            P1_Double : Java.Double) is abstract;

   function Extract_boolean (This : access Typ)
                             return Java.Boolean is abstract;
   --  can raise Org.Omg.CORBA.BAD_OPERATION.Except

   procedure Insert_boolean (This : access Typ;
                             P1_Boolean : Java.Boolean) is abstract;

   function Extract_char (This : access Typ)
                          return Java.Char is abstract;
   --  can raise Org.Omg.CORBA.BAD_OPERATION.Except

   procedure Insert_char (This : access Typ;
                          P1_Char : Java.Char) is abstract;
   --  can raise Org.Omg.CORBA.DATA_CONVERSION.Except

   function Extract_wchar (This : access Typ)
                           return Java.Char is abstract;
   --  can raise Org.Omg.CORBA.BAD_OPERATION.Except

   procedure Insert_wchar (This : access Typ;
                           P1_Char : Java.Char) is abstract;

   function Extract_octet (This : access Typ)
                           return Java.Byte is abstract;
   --  can raise Org.Omg.CORBA.BAD_OPERATION.Except

   procedure Insert_octet (This : access Typ;
                           P1_Byte : Java.Byte) is abstract;

   function Extract_any (This : access Typ)
                         return access Org.Omg.CORBA.Any.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.BAD_OPERATION.Except

   procedure Insert_any (This : access Typ;
                         P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class) is abstract;

   function Extract_Object (This : access Typ)
                            return access Org.Omg.CORBA.Object.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.BAD_OPERATION.Except

   procedure Insert_Object (This : access Typ;
                            P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class) is abstract;

   function Extract_Value (This : access Typ)
                           return access Java.Io.Serializable.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.BAD_OPERATION.Except

   procedure Insert_Value (This : access Typ;
                           P1_Serializable : access Standard.Java.Io.Serializable.Typ'Class) is abstract;

   procedure Insert_Value (This : access Typ;
                           P1_Serializable : access Standard.Java.Io.Serializable.Typ'Class;
                           P2_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class) is abstract;
   --  can raise Org.Omg.CORBA.MARSHAL.Except

   procedure Insert_Object (This : access Typ;
                            P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class;
                            P2_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class) is abstract;
   --  can raise Org.Omg.CORBA.BAD_PARAM.Except

   function Extract_string (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.BAD_OPERATION.Except

   procedure Insert_string (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Omg.CORBA.DATA_CONVERSION.Except and
   --  Org.Omg.CORBA.MARSHAL.Except

   function Extract_wstring (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.BAD_OPERATION.Except

   procedure Insert_wstring (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Omg.CORBA.MARSHAL.Except

   function Extract_TypeCode (This : access Typ)
                              return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.BAD_OPERATION.Except

   procedure Insert_TypeCode (This : access Typ;
                              P1_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class) is abstract;

   function Extract_Streamable (This : access Typ)
                                return access Org.Omg.CORBA.Portable.Streamable.Typ'Class;
   --  can raise Org.Omg.CORBA.BAD_INV_ORDER.Except

   procedure Insert_Streamable (This : access Typ;
                                P1_Streamable : access Standard.Org.Omg.CORBA.Portable.Streamable.Typ'Class);

   function Extract_fixed (This : access Typ)
                           return access Java.Math.BigDecimal.Typ'Class;

   procedure Insert_fixed (This : access Typ;
                           P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class);

   procedure Insert_fixed (This : access Typ;
                           P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class;
                           P2_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class);
   --  can raise Org.Omg.CORBA.BAD_INV_ORDER.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Any);
   pragma Export (Java, Equal, "equal");
   pragma Export (Java, type_K, "type");
   pragma Export (Java, Read_value, "read_value");
   pragma Export (Java, Write_value, "write_value");
   pragma Export (Java, Create_output_stream, "create_output_stream");
   pragma Export (Java, Create_input_stream, "create_input_stream");
   pragma Export (Java, Extract_short, "extract_short");
   pragma Export (Java, Insert_short, "insert_short");
   pragma Export (Java, Extract_long, "extract_long");
   pragma Export (Java, Insert_long, "insert_long");
   pragma Export (Java, Extract_longlong, "extract_longlong");
   pragma Export (Java, Insert_longlong, "insert_longlong");
   pragma Export (Java, Extract_ushort, "extract_ushort");
   pragma Export (Java, Insert_ushort, "insert_ushort");
   pragma Export (Java, Extract_ulong, "extract_ulong");
   pragma Export (Java, Insert_ulong, "insert_ulong");
   pragma Export (Java, Extract_ulonglong, "extract_ulonglong");
   pragma Export (Java, Insert_ulonglong, "insert_ulonglong");
   pragma Export (Java, Extract_float, "extract_float");
   pragma Export (Java, Insert_float, "insert_float");
   pragma Export (Java, Extract_double, "extract_double");
   pragma Export (Java, Insert_double, "insert_double");
   pragma Export (Java, Extract_boolean, "extract_boolean");
   pragma Export (Java, Insert_boolean, "insert_boolean");
   pragma Export (Java, Extract_char, "extract_char");
   pragma Export (Java, Insert_char, "insert_char");
   pragma Export (Java, Extract_wchar, "extract_wchar");
   pragma Export (Java, Insert_wchar, "insert_wchar");
   pragma Export (Java, Extract_octet, "extract_octet");
   pragma Export (Java, Insert_octet, "insert_octet");
   pragma Export (Java, Extract_any, "extract_any");
   pragma Export (Java, Insert_any, "insert_any");
   pragma Export (Java, Extract_Object, "extract_Object");
   pragma Export (Java, Insert_Object, "insert_Object");
   pragma Export (Java, Extract_Value, "extract_Value");
   pragma Export (Java, Insert_Value, "insert_Value");
   pragma Export (Java, Extract_string, "extract_string");
   pragma Export (Java, Insert_string, "insert_string");
   pragma Export (Java, Extract_wstring, "extract_wstring");
   pragma Export (Java, Insert_wstring, "insert_wstring");
   pragma Export (Java, Extract_TypeCode, "extract_TypeCode");
   pragma Export (Java, Insert_TypeCode, "insert_TypeCode");
   pragma Export (Java, Extract_Streamable, "extract_Streamable");
   pragma Export (Java, Insert_Streamable, "insert_Streamable");
   pragma Export (Java, Extract_fixed, "extract_fixed");
   pragma Export (Java, Insert_fixed, "insert_fixed");

end Org.Omg.CORBA.Any;
pragma Import (Java, Org.Omg.CORBA.Any, "org.omg.CORBA.Any");
pragma Extensions_Allowed (Off);
