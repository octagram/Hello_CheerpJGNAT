pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.CompletionStatus;
with Java.Io.Serializable;
with Java.Lang.Object;
with Org.Omg.CORBA.SystemException;

package Org.Omg.CORBA.COMM_FAILURE is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Org.Omg.CORBA.SystemException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_COMM_FAILURE (This : Ref := null)
                              return Ref;

   function New_COMM_FAILURE (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_COMM_FAILURE (P1_Int : Java.Int;
                              P2_CompletionStatus : access Standard.Org.Omg.CORBA.CompletionStatus.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_COMM_FAILURE (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Int : Java.Int;
                              P3_CompletionStatus : access Standard.Org.Omg.CORBA.CompletionStatus.Typ'Class; 
                              This : Ref := null)
                              return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "org.omg.CORBA.COMM_FAILURE");
   pragma Java_Constructor (New_COMM_FAILURE);

end Org.Omg.CORBA.COMM_FAILURE;
pragma Import (Java, Org.Omg.CORBA.COMM_FAILURE, "org.omg.CORBA.COMM_FAILURE");
pragma Extensions_Allowed (Off);
