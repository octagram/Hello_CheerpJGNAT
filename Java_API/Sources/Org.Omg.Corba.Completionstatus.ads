pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.CORBA.CompletionStatus is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Value (This : access Typ)
                   return Java.Int;

   function From_int (P1_Int : Java.Int)
                      return access Org.Omg.CORBA.CompletionStatus.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   U_COMPLETED_YES : constant Java.Int;

   --  final
   U_COMPLETED_NO : constant Java.Int;

   --  final
   U_COMPLETED_MAYBE : constant Java.Int;

   --  final
   COMPLETED_YES : access Org.Omg.CORBA.CompletionStatus.Typ'Class;

   --  final
   COMPLETED_NO : access Org.Omg.CORBA.CompletionStatus.Typ'Class;

   --  final
   COMPLETED_MAYBE : access Org.Omg.CORBA.CompletionStatus.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Value, "value");
   pragma Import (Java, From_int, "from_int");
   pragma Import (Java, U_COMPLETED_YES, "_COMPLETED_YES");
   pragma Import (Java, U_COMPLETED_NO, "_COMPLETED_NO");
   pragma Import (Java, U_COMPLETED_MAYBE, "_COMPLETED_MAYBE");
   pragma Import (Java, COMPLETED_YES, "COMPLETED_YES");
   pragma Import (Java, COMPLETED_NO, "COMPLETED_NO");
   pragma Import (Java, COMPLETED_MAYBE, "COMPLETED_MAYBE");

end Org.Omg.CORBA.CompletionStatus;
pragma Import (Java, Org.Omg.CORBA.CompletionStatus, "org.omg.CORBA.CompletionStatus");
pragma Extensions_Allowed (Off);
