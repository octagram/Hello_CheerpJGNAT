pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.NVList;
with Java.Lang.Object;

package Org.Omg.CORBA.Context is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Context (This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Context_name (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   function Parent (This : access Typ)
                    return access Org.Omg.CORBA.Context.Typ'Class is abstract;

   function Create_child (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Org.Omg.CORBA.Context.Typ'Class is abstract;

   procedure Set_one_value (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class) is abstract;

   procedure Set_values (This : access Typ;
                         P1_NVList : access Standard.Org.Omg.CORBA.NVList.Typ'Class) is abstract;

   procedure Delete_values (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function Get_values (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Int : Java.Int;
                        P3_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Org.Omg.CORBA.NVList.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Context);
   pragma Export (Java, Context_name, "context_name");
   pragma Export (Java, Parent, "parent");
   pragma Export (Java, Create_child, "create_child");
   pragma Export (Java, Set_one_value, "set_one_value");
   pragma Export (Java, Set_values, "set_values");
   pragma Export (Java, Delete_values, "delete_values");
   pragma Export (Java, Get_values, "get_values");

end Org.Omg.CORBA.Context;
pragma Import (Java, Org.Omg.CORBA.Context, "org.omg.CORBA.Context");
pragma Extensions_Allowed (Off);
