pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.DataInputStream;
limited with Org.Omg.CORBA.DataOutputStream;
with Java.Lang.Object;

package Org.Omg.CORBA.CustomMarshal is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Marshal (This : access Typ;
                      P1_DataOutputStream : access Standard.Org.Omg.CORBA.DataOutputStream.Typ'Class) is abstract;

   procedure Unmarshal (This : access Typ;
                        P1_DataInputStream : access Standard.Org.Omg.CORBA.DataInputStream.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Marshal, "marshal");
   pragma Export (Java, Unmarshal, "unmarshal");

end Org.Omg.CORBA.CustomMarshal;
pragma Import (Java, Org.Omg.CORBA.CustomMarshal, "org.omg.CORBA.CustomMarshal");
pragma Extensions_Allowed (Off);
