pragma Extensions_Allowed (On);
limited with Java.Io.Serializable;
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.AnySeqHolder;
limited with Org.Omg.CORBA.BooleanSeqHolder;
limited with Org.Omg.CORBA.CharSeqHolder;
limited with Org.Omg.CORBA.DoubleSeqHolder;
limited with Org.Omg.CORBA.FloatSeqHolder;
limited with Org.Omg.CORBA.LongLongSeqHolder;
limited with Org.Omg.CORBA.LongSeqHolder;
limited with Org.Omg.CORBA.Object;
limited with Org.Omg.CORBA.OctetSeqHolder;
limited with Org.Omg.CORBA.ShortSeqHolder;
limited with Org.Omg.CORBA.TypeCode;
limited with Org.Omg.CORBA.ULongLongSeqHolder;
limited with Org.Omg.CORBA.ULongSeqHolder;
limited with Org.Omg.CORBA.UShortSeqHolder;
limited with Org.Omg.CORBA.WCharSeqHolder;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.ValueBase;

package Org.Omg.CORBA.DataInputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ValueBase_I : Org.Omg.CORBA.Portable.ValueBase.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read_any (This : access Typ)
                      return access Org.Omg.CORBA.Any.Typ'Class is abstract;

   function Read_boolean (This : access Typ)
                          return Java.Boolean is abstract;

   function Read_char (This : access Typ)
                       return Java.Char is abstract;

   function Read_wchar (This : access Typ)
                        return Java.Char is abstract;

   function Read_octet (This : access Typ)
                        return Java.Byte is abstract;

   function Read_short (This : access Typ)
                        return Java.Short is abstract;

   function Read_ushort (This : access Typ)
                         return Java.Short is abstract;

   function Read_long (This : access Typ)
                       return Java.Int is abstract;

   function Read_ulong (This : access Typ)
                        return Java.Int is abstract;

   function Read_longlong (This : access Typ)
                           return Java.Long is abstract;

   function Read_ulonglong (This : access Typ)
                            return Java.Long is abstract;

   function Read_float (This : access Typ)
                        return Java.Float is abstract;

   function Read_double (This : access Typ)
                         return Java.Double is abstract;

   function Read_string (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function Read_wstring (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   function Read_Object (This : access Typ)
                         return access Org.Omg.CORBA.Object.Typ'Class is abstract;

   function Read_Abstract (This : access Typ)
                           return access Java.Lang.Object.Typ'Class is abstract;

   function Read_Value (This : access Typ)
                        return access Java.Io.Serializable.Typ'Class is abstract;

   function Read_TypeCode (This : access Typ)
                           return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;

   procedure Read_any_array (This : access Typ;
                             P1_AnySeqHolder : access Standard.Org.Omg.CORBA.AnySeqHolder.Typ'Class;
                             P2_Int : Java.Int;
                             P3_Int : Java.Int) is abstract;

   procedure Read_boolean_array (This : access Typ;
                                 P1_BooleanSeqHolder : access Standard.Org.Omg.CORBA.BooleanSeqHolder.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int) is abstract;

   procedure Read_char_array (This : access Typ;
                              P1_CharSeqHolder : access Standard.Org.Omg.CORBA.CharSeqHolder.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int) is abstract;

   procedure Read_wchar_array (This : access Typ;
                               P1_WCharSeqHolder : access Standard.Org.Omg.CORBA.WCharSeqHolder.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int) is abstract;

   procedure Read_octet_array (This : access Typ;
                               P1_OctetSeqHolder : access Standard.Org.Omg.CORBA.OctetSeqHolder.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int) is abstract;

   procedure Read_short_array (This : access Typ;
                               P1_ShortSeqHolder : access Standard.Org.Omg.CORBA.ShortSeqHolder.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int) is abstract;

   procedure Read_ushort_array (This : access Typ;
                                P1_UShortSeqHolder : access Standard.Org.Omg.CORBA.UShortSeqHolder.Typ'Class;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int) is abstract;

   procedure Read_long_array (This : access Typ;
                              P1_LongSeqHolder : access Standard.Org.Omg.CORBA.LongSeqHolder.Typ'Class;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int) is abstract;

   procedure Read_ulong_array (This : access Typ;
                               P1_ULongSeqHolder : access Standard.Org.Omg.CORBA.ULongSeqHolder.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int) is abstract;

   procedure Read_ulonglong_array (This : access Typ;
                                   P1_ULongLongSeqHolder : access Standard.Org.Omg.CORBA.ULongLongSeqHolder.Typ'Class;
                                   P2_Int : Java.Int;
                                   P3_Int : Java.Int) is abstract;

   procedure Read_longlong_array (This : access Typ;
                                  P1_LongLongSeqHolder : access Standard.Org.Omg.CORBA.LongLongSeqHolder.Typ'Class;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int) is abstract;

   procedure Read_float_array (This : access Typ;
                               P1_FloatSeqHolder : access Standard.Org.Omg.CORBA.FloatSeqHolder.Typ'Class;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int) is abstract;

   procedure Read_double_array (This : access Typ;
                                P1_DoubleSeqHolder : access Standard.Org.Omg.CORBA.DoubleSeqHolder.Typ'Class;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Read_any, "read_any");
   pragma Export (Java, Read_boolean, "read_boolean");
   pragma Export (Java, Read_char, "read_char");
   pragma Export (Java, Read_wchar, "read_wchar");
   pragma Export (Java, Read_octet, "read_octet");
   pragma Export (Java, Read_short, "read_short");
   pragma Export (Java, Read_ushort, "read_ushort");
   pragma Export (Java, Read_long, "read_long");
   pragma Export (Java, Read_ulong, "read_ulong");
   pragma Export (Java, Read_longlong, "read_longlong");
   pragma Export (Java, Read_ulonglong, "read_ulonglong");
   pragma Export (Java, Read_float, "read_float");
   pragma Export (Java, Read_double, "read_double");
   pragma Export (Java, Read_string, "read_string");
   pragma Export (Java, Read_wstring, "read_wstring");
   pragma Export (Java, Read_Object, "read_Object");
   pragma Export (Java, Read_Abstract, "read_Abstract");
   pragma Export (Java, Read_Value, "read_Value");
   pragma Export (Java, Read_TypeCode, "read_TypeCode");
   pragma Export (Java, Read_any_array, "read_any_array");
   pragma Export (Java, Read_boolean_array, "read_boolean_array");
   pragma Export (Java, Read_char_array, "read_char_array");
   pragma Export (Java, Read_wchar_array, "read_wchar_array");
   pragma Export (Java, Read_octet_array, "read_octet_array");
   pragma Export (Java, Read_short_array, "read_short_array");
   pragma Export (Java, Read_ushort_array, "read_ushort_array");
   pragma Export (Java, Read_long_array, "read_long_array");
   pragma Export (Java, Read_ulong_array, "read_ulong_array");
   pragma Export (Java, Read_ulonglong_array, "read_ulonglong_array");
   pragma Export (Java, Read_longlong_array, "read_longlong_array");
   pragma Export (Java, Read_float_array, "read_float_array");
   pragma Export (Java, Read_double_array, "read_double_array");

end Org.Omg.CORBA.DataInputStream;
pragma Import (Java, Org.Omg.CORBA.DataInputStream, "org.omg.CORBA.DataInputStream");
pragma Extensions_Allowed (Off);
