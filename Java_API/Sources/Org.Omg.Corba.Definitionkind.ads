pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.CORBA.DefinitionKind is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Value (This : access Typ)
                   return Java.Int;

   function From_int (P1_Int : Java.Int)
                      return access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_DefinitionKind (P1_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   U_Dk_none : constant Java.Int;

   --  final
   U_Dk_all : constant Java.Int;

   --  final
   U_Dk_Attribute : constant Java.Int;

   --  final
   U_Dk_Constant : constant Java.Int;

   --  final
   U_Dk_Exception : constant Java.Int;

   --  final
   U_Dk_Interface : constant Java.Int;

   --  final
   U_Dk_Module : constant Java.Int;

   --  final
   U_Dk_Operation : constant Java.Int;

   --  final
   U_Dk_Typedef : constant Java.Int;

   --  final
   U_Dk_Alias : constant Java.Int;

   --  final
   U_Dk_Struct : constant Java.Int;

   --  final
   U_Dk_Union : constant Java.Int;

   --  final
   U_Dk_Enum : constant Java.Int;

   --  final
   U_Dk_Primitive : constant Java.Int;

   --  final
   U_Dk_String : constant Java.Int;

   --  final
   U_Dk_Sequence : constant Java.Int;

   --  final
   U_Dk_Array : constant Java.Int;

   --  final
   U_Dk_Repository : constant Java.Int;

   --  final
   U_Dk_Wstring : constant Java.Int;

   --  final
   U_Dk_Fixed : constant Java.Int;

   --  final
   U_Dk_Value : constant Java.Int;

   --  final
   U_Dk_ValueBox : constant Java.Int;

   --  final
   U_Dk_ValueMember : constant Java.Int;

   --  final
   U_Dk_Native : constant Java.Int;

   --  final
   U_Dk_AbstractInterface : constant Java.Int;

   --  final
   Dk_none : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_all : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Attribute : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Constant : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Exception : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Interface : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Module : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Operation : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Typedef : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Alias : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Struct : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Union : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Enum : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Primitive : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_String : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Sequence : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Array : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Repository : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Wstring : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Fixed : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Value : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_ValueBox : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_ValueMember : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_Native : access Org.Omg.CORBA.DefinitionKind.Typ'Class;

   --  final
   Dk_AbstractInterface : access Org.Omg.CORBA.DefinitionKind.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Value, "value");
   pragma Import (Java, From_int, "from_int");
   pragma Java_Constructor (New_DefinitionKind);
   pragma Import (Java, U_Dk_none, "_dk_none");
   pragma Import (Java, U_Dk_all, "_dk_all");
   pragma Import (Java, U_Dk_Attribute, "_dk_Attribute");
   pragma Import (Java, U_Dk_Constant, "_dk_Constant");
   pragma Import (Java, U_Dk_Exception, "_dk_Exception");
   pragma Import (Java, U_Dk_Interface, "_dk_Interface");
   pragma Import (Java, U_Dk_Module, "_dk_Module");
   pragma Import (Java, U_Dk_Operation, "_dk_Operation");
   pragma Import (Java, U_Dk_Typedef, "_dk_Typedef");
   pragma Import (Java, U_Dk_Alias, "_dk_Alias");
   pragma Import (Java, U_Dk_Struct, "_dk_Struct");
   pragma Import (Java, U_Dk_Union, "_dk_Union");
   pragma Import (Java, U_Dk_Enum, "_dk_Enum");
   pragma Import (Java, U_Dk_Primitive, "_dk_Primitive");
   pragma Import (Java, U_Dk_String, "_dk_String");
   pragma Import (Java, U_Dk_Sequence, "_dk_Sequence");
   pragma Import (Java, U_Dk_Array, "_dk_Array");
   pragma Import (Java, U_Dk_Repository, "_dk_Repository");
   pragma Import (Java, U_Dk_Wstring, "_dk_Wstring");
   pragma Import (Java, U_Dk_Fixed, "_dk_Fixed");
   pragma Import (Java, U_Dk_Value, "_dk_Value");
   pragma Import (Java, U_Dk_ValueBox, "_dk_ValueBox");
   pragma Import (Java, U_Dk_ValueMember, "_dk_ValueMember");
   pragma Import (Java, U_Dk_Native, "_dk_Native");
   pragma Import (Java, U_Dk_AbstractInterface, "_dk_AbstractInterface");
   pragma Import (Java, Dk_none, "dk_none");
   pragma Import (Java, Dk_all, "dk_all");
   pragma Import (Java, Dk_Attribute, "dk_Attribute");
   pragma Import (Java, Dk_Constant, "dk_Constant");
   pragma Import (Java, Dk_Exception, "dk_Exception");
   pragma Import (Java, Dk_Interface, "dk_Interface");
   pragma Import (Java, Dk_Module, "dk_Module");
   pragma Import (Java, Dk_Operation, "dk_Operation");
   pragma Import (Java, Dk_Typedef, "dk_Typedef");
   pragma Import (Java, Dk_Alias, "dk_Alias");
   pragma Import (Java, Dk_Struct, "dk_Struct");
   pragma Import (Java, Dk_Union, "dk_Union");
   pragma Import (Java, Dk_Enum, "dk_Enum");
   pragma Import (Java, Dk_Primitive, "dk_Primitive");
   pragma Import (Java, Dk_String, "dk_String");
   pragma Import (Java, Dk_Sequence, "dk_Sequence");
   pragma Import (Java, Dk_Array, "dk_Array");
   pragma Import (Java, Dk_Repository, "dk_Repository");
   pragma Import (Java, Dk_Wstring, "dk_Wstring");
   pragma Import (Java, Dk_Fixed, "dk_Fixed");
   pragma Import (Java, Dk_Value, "dk_Value");
   pragma Import (Java, Dk_ValueBox, "dk_ValueBox");
   pragma Import (Java, Dk_ValueMember, "dk_ValueMember");
   pragma Import (Java, Dk_Native, "dk_Native");
   pragma Import (Java, Dk_AbstractInterface, "dk_AbstractInterface");

end Org.Omg.CORBA.DefinitionKind;
pragma Import (Java, Org.Omg.CORBA.DefinitionKind, "org.omg.CORBA.DefinitionKind");
pragma Extensions_Allowed (Off);
