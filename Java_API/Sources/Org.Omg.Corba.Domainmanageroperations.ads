pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.Policy;
with Java.Lang.Object;

package Org.Omg.CORBA.DomainManagerOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get_domain_policy (This : access Typ;
                               P1_Int : Java.Int)
                               return access Org.Omg.CORBA.Policy.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Get_domain_policy, "get_domain_policy");

end Org.Omg.CORBA.DomainManagerOperations;
pragma Import (Java, Org.Omg.CORBA.DomainManagerOperations, "org.omg.CORBA.DomainManagerOperations");
pragma Extensions_Allowed (Off);
