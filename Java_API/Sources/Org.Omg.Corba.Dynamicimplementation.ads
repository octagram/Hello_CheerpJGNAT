pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.Omg.CORBA.Object;
with Org.Omg.CORBA.Portable.ObjectImpl;

package Org.Omg.CORBA.DynamicImplementation is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Object_I : Org.Omg.CORBA.Object.Ref)
    is new Org.Omg.CORBA.Portable.ObjectImpl.Typ(Object_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DynamicImplementation (This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function U_Ids (This : access Typ)
                   return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DynamicImplementation);
   pragma Import (Java, U_Ids, "_ids");

end Org.Omg.CORBA.DynamicImplementation;
pragma Import (Java, Org.Omg.CORBA.DynamicImplementation, "org.omg.CORBA.DynamicImplementation");
pragma Extensions_Allowed (Off);
