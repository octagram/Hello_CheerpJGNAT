pragma Extensions_Allowed (On);
package Org.Omg.CORBA.DynAnyPackage is
   pragma Preelaborate;
end Org.Omg.CORBA.DynAnyPackage;
pragma Import (Java, Org.Omg.CORBA.DynAnyPackage, "org.omg.CORBA.DynAnyPackage");
pragma Extensions_Allowed (Off);
