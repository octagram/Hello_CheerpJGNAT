pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.Any;
with Java.Lang.Object;
with Org.Omg.CORBA.DynAny;
with Org.Omg.CORBA.Object;

package Org.Omg.CORBA.DynArray is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DynAny_I : Org.Omg.CORBA.DynAny.Ref;
            Object_I : Org.Omg.CORBA.Object.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get_elements (This : access Typ)
                          return Standard.Java.Lang.Object.Ref is abstract;

   procedure Set_elements (This : access Typ;
                           P1_Any_Arr : access Org.Omg.CORBA.Any.Arr_Obj) is abstract;
   --  can raise Org.Omg.CORBA.DynAnyPackage.InvalidSeq.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Get_elements, "get_elements");
   pragma Export (Java, Set_elements, "set_elements");

end Org.Omg.CORBA.DynArray;
pragma Import (Java, Org.Omg.CORBA.DynArray, "org.omg.CORBA.DynArray");
pragma Extensions_Allowed (Off);
