pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.Omg.CORBA.DynAny;
with Org.Omg.CORBA.Object;

package Org.Omg.CORBA.DynEnum is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DynAny_I : Org.Omg.CORBA.DynAny.Ref;
            Object_I : Org.Omg.CORBA.Object.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Value_as_string (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   procedure Value_as_string (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function Value_as_ulong (This : access Typ)
                            return Java.Int is abstract;

   procedure Value_as_ulong (This : access Typ;
                             P1_Int : Java.Int) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Value_as_string, "value_as_string");
   pragma Export (Java, Value_as_ulong, "value_as_ulong");

end Org.Omg.CORBA.DynEnum;
pragma Import (Java, Org.Omg.CORBA.DynEnum, "org.omg.CORBA.DynEnum");
pragma Extensions_Allowed (Off);
