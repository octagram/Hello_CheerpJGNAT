pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.DynAny;
with Org.Omg.CORBA.Object;

package Org.Omg.CORBA.DynFixed is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DynAny_I : Org.Omg.CORBA.DynAny.Ref;
            Object_I : Org.Omg.CORBA.Object.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get_value (This : access Typ)
                       return Java.Byte_Arr is abstract;

   procedure Set_value (This : access Typ;
                        P1_Byte_Arr : Java.Byte_Arr) is abstract;
   --  can raise Org.Omg.CORBA.DynAnyPackage.InvalidValue.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Get_value, "get_value");
   pragma Export (Java, Set_value, "set_value");

end Org.Omg.CORBA.DynFixed;
pragma Import (Java, Org.Omg.CORBA.DynFixed, "org.omg.CORBA.DynFixed");
pragma Extensions_Allowed (Off);
