pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.NameValuePair;
limited with Org.Omg.CORBA.TCKind;
with Java.Lang.Object;
with Org.Omg.CORBA.DynAny;
with Org.Omg.CORBA.Object;

package Org.Omg.CORBA.DynStruct is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DynAny_I : Org.Omg.CORBA.DynAny.Ref;
            Object_I : Org.Omg.CORBA.Object.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Current_member_name (This : access Typ)
                                 return access Java.Lang.String.Typ'Class is abstract;

   function Current_member_kind (This : access Typ)
                                 return access Org.Omg.CORBA.TCKind.Typ'Class is abstract;

   function Get_members (This : access Typ)
                         return Standard.Java.Lang.Object.Ref is abstract;

   procedure Set_members (This : access Typ;
                          P1_NameValuePair_Arr : access Org.Omg.CORBA.NameValuePair.Arr_Obj) is abstract;
   --  can raise Org.Omg.CORBA.DynAnyPackage.InvalidSeq.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Current_member_name, "current_member_name");
   pragma Export (Java, Current_member_kind, "current_member_kind");
   pragma Export (Java, Get_members, "get_members");
   pragma Export (Java, Set_members, "set_members");

end Org.Omg.CORBA.DynStruct;
pragma Import (Java, Org.Omg.CORBA.DynStruct, "org.omg.CORBA.DynStruct");
pragma Extensions_Allowed (Off);
