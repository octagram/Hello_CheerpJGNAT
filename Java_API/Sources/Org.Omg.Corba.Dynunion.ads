pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.TCKind;
with Java.Lang.Object;
with Org.Omg.CORBA.DynAny;
with Org.Omg.CORBA.Object;

package Org.Omg.CORBA.DynUnion is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DynAny_I : Org.Omg.CORBA.DynAny.Ref;
            Object_I : Org.Omg.CORBA.Object.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Set_as_default (This : access Typ)
                            return Java.Boolean is abstract;

   procedure Set_as_default (This : access Typ;
                             P1_Boolean : Java.Boolean) is abstract;

   function Discriminator (This : access Typ)
                           return access Org.Omg.CORBA.DynAny.Typ'Class is abstract;

   function Discriminator_kind (This : access Typ)
                                return access Org.Omg.CORBA.TCKind.Typ'Class is abstract;

   function Member (This : access Typ)
                    return access Org.Omg.CORBA.DynAny.Typ'Class is abstract;

   function Member_name (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure Member_name (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function Member_kind (This : access Typ)
                         return access Org.Omg.CORBA.TCKind.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Set_as_default, "set_as_default");
   pragma Export (Java, Discriminator, "discriminator");
   pragma Export (Java, Discriminator_kind, "discriminator_kind");
   pragma Export (Java, Member, "member");
   pragma Export (Java, Member_name, "member_name");
   pragma Export (Java, Member_kind, "member_kind");

end Org.Omg.CORBA.DynUnion;
pragma Import (Java, Org.Omg.CORBA.DynUnion, "org.omg.CORBA.DynUnion");
pragma Extensions_Allowed (Off);
