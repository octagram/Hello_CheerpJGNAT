pragma Extensions_Allowed (On);
limited with Java.Lang.Exception_K;
with Java.Lang.Object;

package Org.Omg.CORBA.Environment is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Environment (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function exception_K (This : access Typ)
                         return access Java.Lang.Exception_K.Typ'Class is abstract;

   procedure exception_K (This : access Typ;
                          P1_Exception_K : access Standard.Java.Lang.Exception_K.Typ'Class) is abstract;

   procedure Clear (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Environment);
   pragma Export (Java, exception_K, "exception");
   pragma Export (Java, Clear, "clear");

end Org.Omg.CORBA.Environment;
pragma Import (Java, Org.Omg.CORBA.Environment, "org.omg.CORBA.Environment");
pragma Extensions_Allowed (Off);
