pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.TypeCode;
with Java.Lang.Object;

package Org.Omg.CORBA.ExceptionList is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_ExceptionList (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Count (This : access Typ)
                   return Java.Int is abstract;

   procedure Add (This : access Typ;
                  P1_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class) is abstract;

   function Item (This : access Typ;
                  P1_Int : Java.Int)
                  return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.Bounds.Except

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int) is abstract;
   --  can raise Org.Omg.CORBA.Bounds.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ExceptionList);
   pragma Export (Java, Count, "count");
   pragma Export (Java, Add, "add");
   pragma Export (Java, Item, "item");
   pragma Export (Java, Remove, "remove");

end Org.Omg.CORBA.ExceptionList;
pragma Import (Java, Org.Omg.CORBA.ExceptionList, "org.omg.CORBA.ExceptionList");
pragma Extensions_Allowed (Off);
