pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.TypeCode;
with Java.Lang.Object;
with Org.Omg.CORBA.IRObjectOperations;

package Org.Omg.CORBA.IDLTypeOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            IRObjectOperations_I : Org.Omg.CORBA.IRObjectOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function type_K (This : access Typ)
                    return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, type_K, "type");

end Org.Omg.CORBA.IDLTypeOperations;
pragma Import (Java, Org.Omg.CORBA.IDLTypeOperations, "org.omg.CORBA.IDLTypeOperations");
pragma Extensions_Allowed (Off);
