pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.DefinitionKind;
with Java.Lang.Object;

package Org.Omg.CORBA.IRObjectOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Def_kind (This : access Typ)
                      return access Org.Omg.CORBA.DefinitionKind.Typ'Class is abstract;

   procedure Destroy (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Def_kind, "def_kind");
   pragma Export (Java, Destroy, "destroy");

end Org.Omg.CORBA.IRObjectOperations;
pragma Import (Java, Org.Omg.CORBA.IRObjectOperations, "org.omg.CORBA.IRObjectOperations");
pragma Extensions_Allowed (Off);
