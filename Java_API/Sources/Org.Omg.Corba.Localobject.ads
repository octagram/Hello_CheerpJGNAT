pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Context;
limited with Org.Omg.CORBA.ContextList;
limited with Org.Omg.CORBA.DomainManager;
limited with Org.Omg.CORBA.ExceptionList;
limited with Org.Omg.CORBA.NVList;
limited with Org.Omg.CORBA.NamedValue;
limited with Org.Omg.CORBA.ORB;
limited with Org.Omg.CORBA.Policy;
limited with Org.Omg.CORBA.Portable.InputStream;
limited with Org.Omg.CORBA.Portable.OutputStream;
limited with Org.Omg.CORBA.Portable.ServantObject;
limited with Org.Omg.CORBA.Request;
limited with Org.Omg.CORBA.SetOverrideType;
with Java.Lang.Object;
with Org.Omg.CORBA.Object;

package Org.Omg.CORBA.LocalObject is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Object_I : Org.Omg.CORBA.Object.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LocalObject (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function U_Is_equivalent (This : access Typ;
                             P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class)
                             return Java.Boolean;

   function U_Non_existent (This : access Typ)
                            return Java.Boolean;

   function U_Hash (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Int;

   function U_Is_a (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return Java.Boolean;

   function U_Duplicate (This : access Typ)
                         return access Org.Omg.CORBA.Object.Typ'Class;

   procedure U_Release (This : access Typ);

   function U_Request (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Org.Omg.CORBA.Request.Typ'Class;

   function U_Create_request (This : access Typ;
                              P1_Context : access Standard.Org.Omg.CORBA.Context.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_NVList : access Standard.Org.Omg.CORBA.NVList.Typ'Class;
                              P4_NamedValue : access Standard.Org.Omg.CORBA.NamedValue.Typ'Class)
                              return access Org.Omg.CORBA.Request.Typ'Class;

   function U_Create_request (This : access Typ;
                              P1_Context : access Standard.Org.Omg.CORBA.Context.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_NVList : access Standard.Org.Omg.CORBA.NVList.Typ'Class;
                              P4_NamedValue : access Standard.Org.Omg.CORBA.NamedValue.Typ'Class;
                              P5_ExceptionList : access Standard.Org.Omg.CORBA.ExceptionList.Typ'Class;
                              P6_ContextList : access Standard.Org.Omg.CORBA.ContextList.Typ'Class)
                              return access Org.Omg.CORBA.Request.Typ'Class;

   function U_Get_interface (This : access Typ)
                             return access Org.Omg.CORBA.Object.Typ'Class;

   function U_Get_interface_def (This : access Typ)
                                 return access Org.Omg.CORBA.Object.Typ'Class;

   function U_Orb (This : access Typ)
                   return access Org.Omg.CORBA.ORB.Typ'Class;

   function U_Get_policy (This : access Typ;
                          P1_Int : Java.Int)
                          return access Org.Omg.CORBA.Policy.Typ'Class;

   function U_Get_domain_managers (This : access Typ)
                                   return Standard.Java.Lang.Object.Ref;

   function U_Set_policy_override (This : access Typ;
                                   P1_Policy_Arr : access Org.Omg.CORBA.Policy.Arr_Obj;
                                   P2_SetOverrideType : access Standard.Org.Omg.CORBA.SetOverrideType.Typ'Class)
                                   return access Org.Omg.CORBA.Object.Typ'Class;

   function U_Is_local (This : access Typ)
                        return Java.Boolean;

   function U_Servant_preinvoke (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Class : access Standard.Java.Lang.Class.Typ'Class)
                                 return access Org.Omg.CORBA.Portable.ServantObject.Typ'Class;

   procedure U_Servant_postinvoke (This : access Typ;
                                   P1_ServantObject : access Standard.Org.Omg.CORBA.Portable.ServantObject.Typ'Class);

   function U_Request (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_Boolean : Java.Boolean)
                       return access Org.Omg.CORBA.Portable.OutputStream.Typ'Class;

   function U_Invoke (This : access Typ;
                      P1_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class)
                      return access Org.Omg.CORBA.Portable.InputStream.Typ'Class;
   --  can raise Org.Omg.CORBA.Portable.ApplicationException.Except and
   --  Org.Omg.CORBA.Portable.RemarshalException.Except

   procedure U_ReleaseReply (This : access Typ;
                             P1_InputStream : access Standard.Org.Omg.CORBA.Portable.InputStream.Typ'Class);

   function Validate_connection (This : access Typ)
                                 return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LocalObject);
   pragma Import (Java, U_Is_equivalent, "_is_equivalent");
   pragma Import (Java, U_Non_existent, "_non_existent");
   pragma Import (Java, U_Hash, "_hash");
   pragma Import (Java, U_Is_a, "_is_a");
   pragma Import (Java, U_Duplicate, "_duplicate");
   pragma Import (Java, U_Release, "_release");
   pragma Import (Java, U_Request, "_request");
   pragma Import (Java, U_Create_request, "_create_request");
   pragma Import (Java, U_Get_interface, "_get_interface");
   pragma Import (Java, U_Get_interface_def, "_get_interface_def");
   pragma Import (Java, U_Orb, "_orb");
   pragma Import (Java, U_Get_policy, "_get_policy");
   pragma Import (Java, U_Get_domain_managers, "_get_domain_managers");
   pragma Import (Java, U_Set_policy_override, "_set_policy_override");
   pragma Import (Java, U_Is_local, "_is_local");
   pragma Import (Java, U_Servant_preinvoke, "_servant_preinvoke");
   pragma Import (Java, U_Servant_postinvoke, "_servant_postinvoke");
   pragma Import (Java, U_Invoke, "_invoke");
   pragma Import (Java, U_ReleaseReply, "_releaseReply");
   pragma Import (Java, Validate_connection, "validate_connection");

end Org.Omg.CORBA.LocalObject;
pragma Import (Java, Org.Omg.CORBA.LocalObject, "org.omg.CORBA.LocalObject");
pragma Extensions_Allowed (Off);
