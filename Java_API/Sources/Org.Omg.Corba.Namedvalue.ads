pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Any;
with Java.Lang.Object;

package Org.Omg.CORBA.NamedValue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_NamedValue (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Name (This : access Typ)
                  return access Java.Lang.String.Typ'Class is abstract;

   function Value (This : access Typ)
                   return access Org.Omg.CORBA.Any.Typ'Class is abstract;

   function Flags (This : access Typ)
                   return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NamedValue);
   pragma Export (Java, Name, "name");
   pragma Export (Java, Value, "value");
   pragma Export (Java, Flags, "flags");

end Org.Omg.CORBA.NamedValue;
pragma Import (Java, Org.Omg.CORBA.NamedValue, "org.omg.CORBA.NamedValue");
pragma Extensions_Allowed (Off);
