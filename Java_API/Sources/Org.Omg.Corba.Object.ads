pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Context;
limited with Org.Omg.CORBA.ContextList;
limited with Org.Omg.CORBA.DomainManager;
limited with Org.Omg.CORBA.ExceptionList;
limited with Org.Omg.CORBA.NVList;
limited with Org.Omg.CORBA.NamedValue;
limited with Org.Omg.CORBA.Policy;
limited with Org.Omg.CORBA.Request;
limited with Org.Omg.CORBA.SetOverrideType;
with Java.Lang.Object;

package Org.Omg.CORBA.Object is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function U_Is_a (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return Java.Boolean is abstract;

   function U_Is_equivalent (This : access Typ;
                             P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class)
                             return Java.Boolean is abstract;

   function U_Non_existent (This : access Typ)
                            return Java.Boolean is abstract;

   function U_Hash (This : access Typ;
                    P1_Int : Java.Int)
                    return Java.Int is abstract;

   function U_Duplicate (This : access Typ)
                         return access Org.Omg.CORBA.Object.Typ'Class is abstract;

   procedure U_Release (This : access Typ) is abstract;

   function U_Get_interface_def (This : access Typ)
                                 return access Org.Omg.CORBA.Object.Typ'Class is abstract;

   function U_Request (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Org.Omg.CORBA.Request.Typ'Class is abstract;

   function U_Create_request (This : access Typ;
                              P1_Context : access Standard.Org.Omg.CORBA.Context.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_NVList : access Standard.Org.Omg.CORBA.NVList.Typ'Class;
                              P4_NamedValue : access Standard.Org.Omg.CORBA.NamedValue.Typ'Class)
                              return access Org.Omg.CORBA.Request.Typ'Class is abstract;

   function U_Create_request (This : access Typ;
                              P1_Context : access Standard.Org.Omg.CORBA.Context.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_NVList : access Standard.Org.Omg.CORBA.NVList.Typ'Class;
                              P4_NamedValue : access Standard.Org.Omg.CORBA.NamedValue.Typ'Class;
                              P5_ExceptionList : access Standard.Org.Omg.CORBA.ExceptionList.Typ'Class;
                              P6_ContextList : access Standard.Org.Omg.CORBA.ContextList.Typ'Class)
                              return access Org.Omg.CORBA.Request.Typ'Class is abstract;

   function U_Get_policy (This : access Typ;
                          P1_Int : Java.Int)
                          return access Org.Omg.CORBA.Policy.Typ'Class is abstract;

   function U_Get_domain_managers (This : access Typ)
                                   return Standard.Java.Lang.Object.Ref is abstract;

   function U_Set_policy_override (This : access Typ;
                                   P1_Policy_Arr : access Org.Omg.CORBA.Policy.Arr_Obj;
                                   P2_SetOverrideType : access Standard.Org.Omg.CORBA.SetOverrideType.Typ'Class)
                                   return access Org.Omg.CORBA.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, U_Is_a, "_is_a");
   pragma Export (Java, U_Is_equivalent, "_is_equivalent");
   pragma Export (Java, U_Non_existent, "_non_existent");
   pragma Export (Java, U_Hash, "_hash");
   pragma Export (Java, U_Duplicate, "_duplicate");
   pragma Export (Java, U_Release, "_release");
   pragma Export (Java, U_Get_interface_def, "_get_interface_def");
   pragma Export (Java, U_Request, "_request");
   pragma Export (Java, U_Create_request, "_create_request");
   pragma Export (Java, U_Get_policy, "_get_policy");
   pragma Export (Java, U_Get_domain_managers, "_get_domain_managers");
   pragma Export (Java, U_Set_policy_override, "_set_policy_override");

end Org.Omg.CORBA.Object;
pragma Import (Java, Org.Omg.CORBA.Object, "org.omg.CORBA.Object");
pragma Extensions_Allowed (Off);
