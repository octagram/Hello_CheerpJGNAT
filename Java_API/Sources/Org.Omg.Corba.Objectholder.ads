pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.Object;
limited with Org.Omg.CORBA.Portable.InputStream;
limited with Org.Omg.CORBA.Portable.OutputStream;
limited with Org.Omg.CORBA.TypeCode;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.Streamable;

package Org.Omg.CORBA.ObjectHolder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Streamable_I : Org.Omg.CORBA.Portable.Streamable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Value : access Org.Omg.CORBA.Object.Typ'Class;
      pragma Import (Java, Value, "value");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ObjectHolder (This : Ref := null)
                              return Ref;

   function New_ObjectHolder (P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure U_Read (This : access Typ;
                     P1_InputStream : access Standard.Org.Omg.CORBA.Portable.InputStream.Typ'Class);

   procedure U_Write (This : access Typ;
                      P1_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class);

   function U_Type (This : access Typ)
                    return access Org.Omg.CORBA.TypeCode.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ObjectHolder);
   pragma Import (Java, U_Read, "_read");
   pragma Import (Java, U_Write, "_write");
   pragma Import (Java, U_Type, "_type");

end Org.Omg.CORBA.ObjectHolder;
pragma Import (Java, Org.Omg.CORBA.ObjectHolder, "org.omg.CORBA.ObjectHolder");
pragma Extensions_Allowed (Off);
