pragma Extensions_Allowed (On);
limited with Java.Applet.Applet;
limited with Java.Lang.String;
limited with Java.Util.Properties;
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.Context;
limited with Org.Omg.CORBA.ContextList;
limited with Org.Omg.CORBA.Environment;
limited with Org.Omg.CORBA.ExceptionList;
limited with Org.Omg.CORBA.NVList;
limited with Org.Omg.CORBA.NamedValue;
limited with Org.Omg.CORBA.Object;
limited with Org.Omg.CORBA.Policy;
limited with Org.Omg.CORBA.Portable.OutputStream;
limited with Org.Omg.CORBA.Request;
limited with Org.Omg.CORBA.ServiceInformationHolder;
limited with Org.Omg.CORBA.StructMember;
limited with Org.Omg.CORBA.TCKind;
limited with Org.Omg.CORBA.TypeCode;
limited with Org.Omg.CORBA.UnionMember;
limited with Org.Omg.CORBA.ValueMember;
with Java.Lang.Object;

package Org.Omg.CORBA.ORB is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_ORB (This : Ref := null)
                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  synchronized
   function Init return access Org.Omg.CORBA.ORB.Typ'Class;

   function Init (P1_String_Arr : access Java.Lang.String.Arr_Obj;
                  P2_Properties : access Standard.Java.Util.Properties.Typ'Class)
                  return access Org.Omg.CORBA.ORB.Typ'Class;

   function Init (P1_Applet : access Standard.Java.Applet.Applet.Typ'Class;
                  P2_Properties : access Standard.Java.Util.Properties.Typ'Class)
                  return access Org.Omg.CORBA.ORB.Typ'Class;

   --  protected
   procedure Set_parameters (This : access Typ;
                             P1_String_Arr : access Java.Lang.String.Arr_Obj;
                             P2_Properties : access Standard.Java.Util.Properties.Typ'Class) is abstract;

   --  protected
   procedure Set_parameters (This : access Typ;
                             P1_Applet : access Standard.Java.Applet.Applet.Typ'Class;
                             P2_Properties : access Standard.Java.Util.Properties.Typ'Class) is abstract;

   procedure Connect (This : access Typ;
                      P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class);

   procedure Destroy (This : access Typ);

   procedure Disconnect (This : access Typ;
                         P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class);

   function List_initial_services (This : access Typ)
                                   return Standard.Java.Lang.Object.Ref is abstract;

   function Resolve_initial_references (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                                        return access Org.Omg.CORBA.Object.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.ORBPackage.InvalidName.Except

   function Object_to_string (This : access Typ;
                              P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class)
                              return access Java.Lang.String.Typ'Class is abstract;

   function String_to_object (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Org.Omg.CORBA.Object.Typ'Class is abstract;

   function Create_list (This : access Typ;
                         P1_Int : Java.Int)
                         return access Org.Omg.CORBA.NVList.Typ'Class is abstract;

   function Create_operation_list (This : access Typ;
                                   P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class)
                                   return access Org.Omg.CORBA.NVList.Typ'Class;

   function Create_named_value (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class;
                                P3_Int : Java.Int)
                                return access Org.Omg.CORBA.NamedValue.Typ'Class is abstract;

   function Create_exception_list (This : access Typ)
                                   return access Org.Omg.CORBA.ExceptionList.Typ'Class is abstract;

   function Create_context_list (This : access Typ)
                                 return access Org.Omg.CORBA.ContextList.Typ'Class is abstract;

   function Get_default_context (This : access Typ)
                                 return access Org.Omg.CORBA.Context.Typ'Class is abstract;

   function Create_environment (This : access Typ)
                                return access Org.Omg.CORBA.Environment.Typ'Class is abstract;

   function Create_output_stream (This : access Typ)
                                  return access Org.Omg.CORBA.Portable.OutputStream.Typ'Class is abstract;

   procedure Send_multiple_requests_oneway (This : access Typ;
                                            P1_Request_Arr : access Org.Omg.CORBA.Request.Arr_Obj) is abstract;

   procedure Send_multiple_requests_deferred (This : access Typ;
                                              P1_Request_Arr : access Org.Omg.CORBA.Request.Arr_Obj) is abstract;

   function Poll_next_response (This : access Typ)
                                return Java.Boolean is abstract;

   function Get_next_response (This : access Typ)
                               return access Org.Omg.CORBA.Request.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.WrongTransaction.Except

   function Get_primitive_tc (This : access Typ;
                              P1_TCKind : access Standard.Org.Omg.CORBA.TCKind.Typ'Class)
                              return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;

   function Create_struct_tc (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class;
                              P3_StructMember_Arr : access Org.Omg.CORBA.StructMember.Arr_Obj)
                              return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;

   function Create_union_tc (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class;
                             P4_UnionMember_Arr : access Org.Omg.CORBA.UnionMember.Arr_Obj)
                             return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;

   function Create_enum_tc (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                            P3_String_Arr : access Java.Lang.String.Arr_Obj)
                            return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;

   function Create_alias_tc (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class)
                             return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;

   function Create_exception_tc (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                 P3_StructMember_Arr : access Org.Omg.CORBA.StructMember.Arr_Obj)
                                 return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;

   function Create_interface_tc (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;

   function Create_string_tc (This : access Typ;
                              P1_Int : Java.Int)
                              return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;

   function Create_wstring_tc (This : access Typ;
                               P1_Int : Java.Int)
                               return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;

   function Create_sequence_tc (This : access Typ;
                                P1_Int : Java.Int;
                                P2_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class)
                                return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;

   function Create_array_tc (This : access Typ;
                             P1_Int : Java.Int;
                             P2_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class)
                             return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;

   function Create_native_tc (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Org.Omg.CORBA.TypeCode.Typ'Class;

   function Create_abstract_interface_tc (This : access Typ;
                                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                                          P2_String : access Standard.Java.Lang.String.Typ'Class)
                                          return access Org.Omg.CORBA.TypeCode.Typ'Class;

   function Create_fixed_tc (This : access Typ;
                             P1_Short : Java.Short;
                             P2_Short : Java.Short)
                             return access Org.Omg.CORBA.TypeCode.Typ'Class;

   function Create_value_tc (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_Short : Java.Short;
                             P4_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class;
                             P5_ValueMember_Arr : access Org.Omg.CORBA.ValueMember.Arr_Obj)
                             return access Org.Omg.CORBA.TypeCode.Typ'Class;

   function Create_recursive_tc (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Org.Omg.CORBA.TypeCode.Typ'Class;

   function Create_value_box_tc (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                 P3_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class)
                                 return access Org.Omg.CORBA.TypeCode.Typ'Class;

   function Create_any (This : access Typ)
                        return access Org.Omg.CORBA.Any.Typ'Class is abstract;

   procedure Run (This : access Typ);

   procedure Shutdown (This : access Typ;
                       P1_Boolean : Java.Boolean);

   function Work_pending (This : access Typ)
                          return Java.Boolean;

   procedure Perform_work (This : access Typ);

   function Get_service_information (This : access Typ;
                                     P1_Short : Java.Short;
                                     P2_ServiceInformationHolder : access Standard.Org.Omg.CORBA.ServiceInformationHolder.Typ'Class)
                                     return Java.Boolean;

   function Create_policy (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class)
                           return access Org.Omg.CORBA.Policy.Typ'Class;
   --  can raise Org.Omg.CORBA.PolicyError.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ORB);
   pragma Export (Java, Init, "init");
   pragma Export (Java, Set_parameters, "set_parameters");
   pragma Export (Java, Connect, "connect");
   pragma Export (Java, Destroy, "destroy");
   pragma Export (Java, Disconnect, "disconnect");
   pragma Export (Java, List_initial_services, "list_initial_services");
   pragma Export (Java, Resolve_initial_references, "resolve_initial_references");
   pragma Export (Java, Object_to_string, "object_to_string");
   pragma Export (Java, String_to_object, "string_to_object");
   pragma Export (Java, Create_list, "create_list");
   pragma Export (Java, Create_operation_list, "create_operation_list");
   pragma Export (Java, Create_named_value, "create_named_value");
   pragma Export (Java, Create_exception_list, "create_exception_list");
   pragma Export (Java, Create_context_list, "create_context_list");
   pragma Export (Java, Get_default_context, "get_default_context");
   pragma Export (Java, Create_environment, "create_environment");
   pragma Export (Java, Create_output_stream, "create_output_stream");
   pragma Export (Java, Send_multiple_requests_oneway, "send_multiple_requests_oneway");
   pragma Export (Java, Send_multiple_requests_deferred, "send_multiple_requests_deferred");
   pragma Export (Java, Poll_next_response, "poll_next_response");
   pragma Export (Java, Get_next_response, "get_next_response");
   pragma Export (Java, Get_primitive_tc, "get_primitive_tc");
   pragma Export (Java, Create_struct_tc, "create_struct_tc");
   pragma Export (Java, Create_union_tc, "create_union_tc");
   pragma Export (Java, Create_enum_tc, "create_enum_tc");
   pragma Export (Java, Create_alias_tc, "create_alias_tc");
   pragma Export (Java, Create_exception_tc, "create_exception_tc");
   pragma Export (Java, Create_interface_tc, "create_interface_tc");
   pragma Export (Java, Create_string_tc, "create_string_tc");
   pragma Export (Java, Create_wstring_tc, "create_wstring_tc");
   pragma Export (Java, Create_sequence_tc, "create_sequence_tc");
   pragma Export (Java, Create_array_tc, "create_array_tc");
   pragma Export (Java, Create_native_tc, "create_native_tc");
   pragma Export (Java, Create_abstract_interface_tc, "create_abstract_interface_tc");
   pragma Export (Java, Create_fixed_tc, "create_fixed_tc");
   pragma Export (Java, Create_value_tc, "create_value_tc");
   pragma Export (Java, Create_recursive_tc, "create_recursive_tc");
   pragma Export (Java, Create_value_box_tc, "create_value_box_tc");
   pragma Export (Java, Create_any, "create_any");
   pragma Export (Java, Run, "run");
   pragma Export (Java, Shutdown, "shutdown");
   pragma Export (Java, Work_pending, "work_pending");
   pragma Export (Java, Perform_work, "perform_work");
   pragma Export (Java, Get_service_information, "get_service_information");
   pragma Export (Java, Create_policy, "create_policy");

end Org.Omg.CORBA.ORB;
pragma Import (Java, Org.Omg.CORBA.ORB, "org.omg.CORBA.ORB");
pragma Extensions_Allowed (Off);
