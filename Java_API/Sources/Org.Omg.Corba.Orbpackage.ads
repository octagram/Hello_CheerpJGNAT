pragma Extensions_Allowed (On);
package Org.Omg.CORBA.ORBPackage is
   pragma Preelaborate;
end Org.Omg.CORBA.ORBPackage;
pragma Import (Java, Org.Omg.CORBA.ORBPackage, "org.omg.CORBA.ORBPackage");
pragma Extensions_Allowed (Off);
