pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.CORBA.ParameterMode is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Value (This : access Typ)
                   return Java.Int;

   function From_int (P1_Int : Java.Int)
                      return access Org.Omg.CORBA.ParameterMode.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_ParameterMode (P1_Int : Java.Int; 
                               This : Ref := null)
                               return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   U_PARAM_IN : constant Java.Int;

   --  final
   PARAM_IN : access Org.Omg.CORBA.ParameterMode.Typ'Class;

   --  final
   U_PARAM_OUT : constant Java.Int;

   --  final
   PARAM_OUT : access Org.Omg.CORBA.ParameterMode.Typ'Class;

   --  final
   U_PARAM_INOUT : constant Java.Int;

   --  final
   PARAM_INOUT : access Org.Omg.CORBA.ParameterMode.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Value, "value");
   pragma Import (Java, From_int, "from_int");
   pragma Java_Constructor (New_ParameterMode);
   pragma Import (Java, U_PARAM_IN, "_PARAM_IN");
   pragma Import (Java, PARAM_IN, "PARAM_IN");
   pragma Import (Java, U_PARAM_OUT, "_PARAM_OUT");
   pragma Import (Java, PARAM_OUT, "PARAM_OUT");
   pragma Import (Java, U_PARAM_INOUT, "_PARAM_INOUT");
   pragma Import (Java, PARAM_INOUT, "PARAM_INOUT");

end Org.Omg.CORBA.ParameterMode;
pragma Import (Java, Org.Omg.CORBA.ParameterMode, "org.omg.CORBA.ParameterMode");
pragma Extensions_Allowed (Off);
