pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.CompletionStatus;
with Java.Io.Serializable;
with Java.Lang.Object;
with Org.Omg.CORBA.SystemException;

package Org.Omg.CORBA.PERSIST_STORE is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Org.Omg.CORBA.SystemException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_PERSIST_STORE (This : Ref := null)
                               return Ref;

   function New_PERSIST_STORE (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_PERSIST_STORE (P1_Int : Java.Int;
                               P2_CompletionStatus : access Standard.Org.Omg.CORBA.CompletionStatus.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   function New_PERSIST_STORE (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_Int : Java.Int;
                               P3_CompletionStatus : access Standard.Org.Omg.CORBA.CompletionStatus.Typ'Class; 
                               This : Ref := null)
                               return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "org.omg.CORBA.PERSIST_STORE");
   pragma Java_Constructor (New_PERSIST_STORE);

end Org.Omg.CORBA.PERSIST_STORE;
pragma Import (Java, Org.Omg.CORBA.PERSIST_STORE, "org.omg.CORBA.PERSIST_STORE");
pragma Extensions_Allowed (Off);
