pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Portable.InputStream;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Org.Omg.CORBA.Portable.ApplicationException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ApplicationException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                      P2_InputStream : access Standard.Org.Omg.CORBA.Portable.InputStream.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetId (This : access Typ)
                   return access Java.Lang.String.Typ'Class;

   function GetInputStream (This : access Typ)
                            return access Org.Omg.CORBA.Portable.InputStream.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "org.omg.CORBA.portable.ApplicationException");
   pragma Java_Constructor (New_ApplicationException);
   pragma Import (Java, GetId, "getId");
   pragma Import (Java, GetInputStream, "getInputStream");

end Org.Omg.CORBA.Portable.ApplicationException;
pragma Import (Java, Org.Omg.CORBA.Portable.ApplicationException, "org.omg.CORBA.portable.ApplicationException");
pragma Extensions_Allowed (Off);
