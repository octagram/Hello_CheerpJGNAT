pragma Extensions_Allowed (On);
limited with Java.Io.Serializable;
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Portable.InputStream;
limited with Org.Omg.CORBA.Portable.OutputStream;
with Java.Lang.Object;

package Org.Omg.CORBA.Portable.BoxedValueHelper is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read_value (This : access Typ;
                        P1_InputStream : access Standard.Org.Omg.CORBA.Portable.InputStream.Typ'Class)
                        return access Java.Io.Serializable.Typ'Class is abstract;

   procedure Write_value (This : access Typ;
                          P1_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class;
                          P2_Serializable : access Standard.Java.Io.Serializable.Typ'Class) is abstract;

   function Get_id (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Read_value, "read_value");
   pragma Export (Java, Write_value, "write_value");
   pragma Export (Java, Get_id, "get_id");

end Org.Omg.CORBA.Portable.BoxedValueHelper;
pragma Import (Java, Org.Omg.CORBA.Portable.BoxedValueHelper, "org.omg.CORBA.portable.BoxedValueHelper");
pragma Extensions_Allowed (Off);
