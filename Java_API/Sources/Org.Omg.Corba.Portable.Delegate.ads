pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Context;
limited with Org.Omg.CORBA.ContextList;
limited with Org.Omg.CORBA.DomainManager;
limited with Org.Omg.CORBA.ExceptionList;
limited with Org.Omg.CORBA.NVList;
limited with Org.Omg.CORBA.NamedValue;
limited with Org.Omg.CORBA.ORB;
limited with Org.Omg.CORBA.Object;
limited with Org.Omg.CORBA.Policy;
limited with Org.Omg.CORBA.Portable.InputStream;
limited with Org.Omg.CORBA.Portable.OutputStream;
limited with Org.Omg.CORBA.Portable.ServantObject;
limited with Org.Omg.CORBA.Request;
limited with Org.Omg.CORBA.SetOverrideType;
with Java.Lang.Object;

package Org.Omg.CORBA.Portable.Delegate is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Delegate (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get_interface_def (This : access Typ;
                               P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class)
                               return access Org.Omg.CORBA.Object.Typ'Class is abstract;

   function Duplicate (This : access Typ;
                       P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class)
                       return access Org.Omg.CORBA.Object.Typ'Class is abstract;

   procedure Release (This : access Typ;
                      P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class) is abstract;

   function Is_a (This : access Typ;
                  P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class;
                  P2_String : access Standard.Java.Lang.String.Typ'Class)
                  return Java.Boolean is abstract;

   function Non_existent (This : access Typ;
                          P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class)
                          return Java.Boolean is abstract;

   function Is_equivalent (This : access Typ;
                           P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class;
                           P2_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class)
                           return Java.Boolean is abstract;

   function Hash (This : access Typ;
                  P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class;
                  P2_Int : Java.Int)
                  return Java.Int is abstract;

   function Request (This : access Typ;
                     P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Org.Omg.CORBA.Request.Typ'Class is abstract;

   function Create_request (This : access Typ;
                            P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class;
                            P2_Context : access Standard.Org.Omg.CORBA.Context.Typ'Class;
                            P3_String : access Standard.Java.Lang.String.Typ'Class;
                            P4_NVList : access Standard.Org.Omg.CORBA.NVList.Typ'Class;
                            P5_NamedValue : access Standard.Org.Omg.CORBA.NamedValue.Typ'Class)
                            return access Org.Omg.CORBA.Request.Typ'Class is abstract;

   function Create_request (This : access Typ;
                            P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class;
                            P2_Context : access Standard.Org.Omg.CORBA.Context.Typ'Class;
                            P3_String : access Standard.Java.Lang.String.Typ'Class;
                            P4_NVList : access Standard.Org.Omg.CORBA.NVList.Typ'Class;
                            P5_NamedValue : access Standard.Org.Omg.CORBA.NamedValue.Typ'Class;
                            P6_ExceptionList : access Standard.Org.Omg.CORBA.ExceptionList.Typ'Class;
                            P7_ContextList : access Standard.Org.Omg.CORBA.ContextList.Typ'Class)
                            return access Org.Omg.CORBA.Request.Typ'Class is abstract;

   function Orb (This : access Typ;
                 P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class)
                 return access Org.Omg.CORBA.ORB.Typ'Class;

   function Get_policy (This : access Typ;
                        P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class;
                        P2_Int : Java.Int)
                        return access Org.Omg.CORBA.Policy.Typ'Class;

   function Get_domain_managers (This : access Typ;
                                 P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class)
                                 return Standard.Java.Lang.Object.Ref;

   function Set_policy_override (This : access Typ;
                                 P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class;
                                 P2_Policy_Arr : access Org.Omg.CORBA.Policy.Arr_Obj;
                                 P3_SetOverrideType : access Standard.Org.Omg.CORBA.SetOverrideType.Typ'Class)
                                 return access Org.Omg.CORBA.Object.Typ'Class;

   function Is_local (This : access Typ;
                      P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class)
                      return Java.Boolean;

   function Servant_preinvoke (This : access Typ;
                               P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                               P3_Class : access Standard.Java.Lang.Class.Typ'Class)
                               return access Org.Omg.CORBA.Portable.ServantObject.Typ'Class;

   procedure Servant_postinvoke (This : access Typ;
                                 P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class;
                                 P2_ServantObject : access Standard.Org.Omg.CORBA.Portable.ServantObject.Typ'Class);

   function Request (This : access Typ;
                     P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                     P3_Boolean : Java.Boolean)
                     return access Org.Omg.CORBA.Portable.OutputStream.Typ'Class;

   function Invoke (This : access Typ;
                    P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class;
                    P2_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class)
                    return access Org.Omg.CORBA.Portable.InputStream.Typ'Class;
   --  can raise Org.Omg.CORBA.Portable.ApplicationException.Except and
   --  Org.Omg.CORBA.Portable.RemarshalException.Except

   procedure ReleaseReply (This : access Typ;
                           P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class;
                           P2_InputStream : access Standard.Org.Omg.CORBA.Portable.InputStream.Typ'Class);

   function ToString (This : access Typ;
                      P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class)
                      return access Java.Lang.String.Typ'Class;

   function HashCode (This : access Typ;
                      P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class)
                      return Java.Int;

   function Equals (This : access Typ;
                    P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class;
                    P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                    return Java.Boolean;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Delegate);
   pragma Export (Java, Get_interface_def, "get_interface_def");
   pragma Export (Java, Duplicate, "duplicate");
   pragma Export (Java, Release, "release");
   pragma Export (Java, Is_a, "is_a");
   pragma Export (Java, Non_existent, "non_existent");
   pragma Export (Java, Is_equivalent, "is_equivalent");
   pragma Export (Java, Hash, "hash");
   pragma Export (Java, Request, "request");
   pragma Export (Java, Create_request, "create_request");
   pragma Import (Java, Orb, "orb");
   pragma Import (Java, Get_policy, "get_policy");
   pragma Import (Java, Get_domain_managers, "get_domain_managers");
   pragma Import (Java, Set_policy_override, "set_policy_override");
   pragma Import (Java, Is_local, "is_local");
   pragma Import (Java, Servant_preinvoke, "servant_preinvoke");
   pragma Import (Java, Servant_postinvoke, "servant_postinvoke");
   pragma Import (Java, Invoke, "invoke");
   pragma Import (Java, ReleaseReply, "releaseReply");
   pragma Import (Java, ToString, "toString");
   pragma Import (Java, HashCode, "hashCode");
   pragma Import (Java, Equals, "equals");

end Org.Omg.CORBA.Portable.Delegate;
pragma Import (Java, Org.Omg.CORBA.Portable.Delegate, "org.omg.CORBA.portable.Delegate");
pragma Extensions_Allowed (Off);
