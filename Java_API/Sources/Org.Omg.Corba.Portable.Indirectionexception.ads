pragma Extensions_Allowed (On);
with Java.Io.Serializable;
with Java.Lang.Object;
with Org.Omg.CORBA.SystemException;

package Org.Omg.CORBA.Portable.IndirectionException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Org.Omg.CORBA.SystemException.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Offset : Java.Int;
      pragma Import (Java, Offset, "offset");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_IndirectionException (P1_Int : Java.Int; 
                                      This : Ref := null)
                                      return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "org.omg.CORBA.portable.IndirectionException");
   pragma Java_Constructor (New_IndirectionException);

end Org.Omg.CORBA.Portable.IndirectionException;
pragma Import (Java, Org.Omg.CORBA.Portable.IndirectionException, "org.omg.CORBA.portable.IndirectionException");
pragma Extensions_Allowed (Off);
