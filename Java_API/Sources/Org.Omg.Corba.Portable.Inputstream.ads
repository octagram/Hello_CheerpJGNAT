pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Java.Math.BigDecimal;
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.Context;
limited with Org.Omg.CORBA.ORB;
limited with Org.Omg.CORBA.Object;
limited with Org.Omg.CORBA.TypeCode;
with Java.Io.Closeable;
with Java.Io.InputStream;
with Java.Lang.Object;

package Org.Omg.CORBA.Portable.InputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref)
    is abstract new Java.Io.InputStream.Typ(Closeable_I)
      with null record;

   function New_InputStream (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read_boolean (This : access Typ)
                          return Java.Boolean is abstract;

   function Read_char (This : access Typ)
                       return Java.Char is abstract;

   function Read_wchar (This : access Typ)
                        return Java.Char is abstract;

   function Read_octet (This : access Typ)
                        return Java.Byte is abstract;

   function Read_short (This : access Typ)
                        return Java.Short is abstract;

   function Read_ushort (This : access Typ)
                         return Java.Short is abstract;

   function Read_long (This : access Typ)
                       return Java.Int is abstract;

   function Read_ulong (This : access Typ)
                        return Java.Int is abstract;

   function Read_longlong (This : access Typ)
                           return Java.Long is abstract;

   function Read_ulonglong (This : access Typ)
                            return Java.Long is abstract;

   function Read_float (This : access Typ)
                        return Java.Float is abstract;

   function Read_double (This : access Typ)
                         return Java.Double is abstract;

   function Read_string (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function Read_wstring (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure Read_boolean_array (This : access Typ;
                                 P1_Boolean_Arr : Java.Boolean_Arr;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int) is abstract;

   procedure Read_char_array (This : access Typ;
                              P1_Char_Arr : Java.Char_Arr;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int) is abstract;

   procedure Read_wchar_array (This : access Typ;
                               P1_Char_Arr : Java.Char_Arr;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int) is abstract;

   procedure Read_octet_array (This : access Typ;
                               P1_Byte_Arr : Java.Byte_Arr;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int) is abstract;

   procedure Read_short_array (This : access Typ;
                               P1_Short_Arr : Java.Short_Arr;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int) is abstract;

   procedure Read_ushort_array (This : access Typ;
                                P1_Short_Arr : Java.Short_Arr;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int) is abstract;

   procedure Read_long_array (This : access Typ;
                              P1_Int_Arr : Java.Int_Arr;
                              P2_Int : Java.Int;
                              P3_Int : Java.Int) is abstract;

   procedure Read_ulong_array (This : access Typ;
                               P1_Int_Arr : Java.Int_Arr;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int) is abstract;

   procedure Read_longlong_array (This : access Typ;
                                  P1_Long_Arr : Java.Long_Arr;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int) is abstract;

   procedure Read_ulonglong_array (This : access Typ;
                                   P1_Long_Arr : Java.Long_Arr;
                                   P2_Int : Java.Int;
                                   P3_Int : Java.Int) is abstract;

   procedure Read_float_array (This : access Typ;
                               P1_Float_Arr : Java.Float_Arr;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int) is abstract;

   procedure Read_double_array (This : access Typ;
                                P1_Double_Arr : Java.Double_Arr;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int) is abstract;

   function Read_Object (This : access Typ)
                         return access Org.Omg.CORBA.Object.Typ'Class is abstract;

   function Read_TypeCode (This : access Typ)
                           return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;

   function Read_any (This : access Typ)
                      return access Org.Omg.CORBA.Any.Typ'Class is abstract;

   function Read (This : access Typ)
                  return Java.Int;
   --  can raise Java.Io.IOException.Except

   function Read_fixed (This : access Typ)
                        return access Java.Math.BigDecimal.Typ'Class;

   function Read_Context (This : access Typ)
                          return access Org.Omg.CORBA.Context.Typ'Class;

   function Read_Object (This : access Typ;
                         P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                         return access Org.Omg.CORBA.Object.Typ'Class;

   function Orb (This : access Typ)
                 return access Org.Omg.CORBA.ORB.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InputStream);
   pragma Export (Java, Read_boolean, "read_boolean");
   pragma Export (Java, Read_char, "read_char");
   pragma Export (Java, Read_wchar, "read_wchar");
   pragma Export (Java, Read_octet, "read_octet");
   pragma Export (Java, Read_short, "read_short");
   pragma Export (Java, Read_ushort, "read_ushort");
   pragma Export (Java, Read_long, "read_long");
   pragma Export (Java, Read_ulong, "read_ulong");
   pragma Export (Java, Read_longlong, "read_longlong");
   pragma Export (Java, Read_ulonglong, "read_ulonglong");
   pragma Export (Java, Read_float, "read_float");
   pragma Export (Java, Read_double, "read_double");
   pragma Export (Java, Read_string, "read_string");
   pragma Export (Java, Read_wstring, "read_wstring");
   pragma Export (Java, Read_boolean_array, "read_boolean_array");
   pragma Export (Java, Read_char_array, "read_char_array");
   pragma Export (Java, Read_wchar_array, "read_wchar_array");
   pragma Export (Java, Read_octet_array, "read_octet_array");
   pragma Export (Java, Read_short_array, "read_short_array");
   pragma Export (Java, Read_ushort_array, "read_ushort_array");
   pragma Export (Java, Read_long_array, "read_long_array");
   pragma Export (Java, Read_ulong_array, "read_ulong_array");
   pragma Export (Java, Read_longlong_array, "read_longlong_array");
   pragma Export (Java, Read_ulonglong_array, "read_ulonglong_array");
   pragma Export (Java, Read_float_array, "read_float_array");
   pragma Export (Java, Read_double_array, "read_double_array");
   pragma Export (Java, Read_Object, "read_Object");
   pragma Export (Java, Read_TypeCode, "read_TypeCode");
   pragma Export (Java, Read_any, "read_any");
   pragma Import (Java, Read, "read");
   pragma Import (Java, Read_fixed, "read_fixed");
   pragma Import (Java, Read_Context, "read_Context");
   pragma Import (Java, Orb, "orb");

end Org.Omg.CORBA.Portable.InputStream;
pragma Import (Java, Org.Omg.CORBA.Portable.InputStream, "org.omg.CORBA.portable.InputStream");
pragma Extensions_Allowed (Off);
