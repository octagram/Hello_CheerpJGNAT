pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Portable.InputStream;
limited with Org.Omg.CORBA.Portable.OutputStream;
limited with Org.Omg.CORBA.Portable.ResponseHandler;
with Java.Lang.Object;

package Org.Omg.CORBA.Portable.InvokeHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function U_Invoke (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_InputStream : access Standard.Org.Omg.CORBA.Portable.InputStream.Typ'Class;
                      P3_ResponseHandler : access Standard.Org.Omg.CORBA.Portable.ResponseHandler.Typ'Class)
                      return access Org.Omg.CORBA.Portable.OutputStream.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.SystemException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, U_Invoke, "_invoke");

end Org.Omg.CORBA.Portable.InvokeHandler;
pragma Import (Java, Org.Omg.CORBA.Portable.InvokeHandler, "org.omg.CORBA.portable.InvokeHandler");
pragma Extensions_Allowed (Off);
