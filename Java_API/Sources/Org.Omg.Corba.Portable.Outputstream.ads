pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Math.BigDecimal;
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.Context;
limited with Org.Omg.CORBA.ContextList;
limited with Org.Omg.CORBA.ORB;
limited with Org.Omg.CORBA.Object;
limited with Org.Omg.CORBA.Portable.InputStream;
limited with Org.Omg.CORBA.TypeCode;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Io.OutputStream;
with Java.Lang.Object;

package Org.Omg.CORBA.Portable.OutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref)
    is abstract new Java.Io.OutputStream.Typ(Closeable_I,
                                             Flushable_I)
      with null record;

   function New_OutputStream (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Create_input_stream (This : access Typ)
                                 return access Org.Omg.CORBA.Portable.InputStream.Typ'Class is abstract;

   procedure Write_boolean (This : access Typ;
                            P1_Boolean : Java.Boolean) is abstract;

   procedure Write_char (This : access Typ;
                         P1_Char : Java.Char) is abstract;

   procedure Write_wchar (This : access Typ;
                          P1_Char : Java.Char) is abstract;

   procedure Write_octet (This : access Typ;
                          P1_Byte : Java.Byte) is abstract;

   procedure Write_short (This : access Typ;
                          P1_Short : Java.Short) is abstract;

   procedure Write_ushort (This : access Typ;
                           P1_Short : Java.Short) is abstract;

   procedure Write_long (This : access Typ;
                         P1_Int : Java.Int) is abstract;

   procedure Write_ulong (This : access Typ;
                          P1_Int : Java.Int) is abstract;

   procedure Write_longlong (This : access Typ;
                             P1_Long : Java.Long) is abstract;

   procedure Write_ulonglong (This : access Typ;
                              P1_Long : Java.Long) is abstract;

   procedure Write_float (This : access Typ;
                          P1_Float : Java.Float) is abstract;

   procedure Write_double (This : access Typ;
                           P1_Double : Java.Double) is abstract;

   procedure Write_string (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure Write_wstring (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure Write_boolean_array (This : access Typ;
                                  P1_Boolean_Arr : Java.Boolean_Arr;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int) is abstract;

   procedure Write_char_array (This : access Typ;
                               P1_Char_Arr : Java.Char_Arr;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int) is abstract;

   procedure Write_wchar_array (This : access Typ;
                                P1_Char_Arr : Java.Char_Arr;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int) is abstract;

   procedure Write_octet_array (This : access Typ;
                                P1_Byte_Arr : Java.Byte_Arr;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int) is abstract;

   procedure Write_short_array (This : access Typ;
                                P1_Short_Arr : Java.Short_Arr;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int) is abstract;

   procedure Write_ushort_array (This : access Typ;
                                 P1_Short_Arr : Java.Short_Arr;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int) is abstract;

   procedure Write_long_array (This : access Typ;
                               P1_Int_Arr : Java.Int_Arr;
                               P2_Int : Java.Int;
                               P3_Int : Java.Int) is abstract;

   procedure Write_ulong_array (This : access Typ;
                                P1_Int_Arr : Java.Int_Arr;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int) is abstract;

   procedure Write_longlong_array (This : access Typ;
                                   P1_Long_Arr : Java.Long_Arr;
                                   P2_Int : Java.Int;
                                   P3_Int : Java.Int) is abstract;

   procedure Write_ulonglong_array (This : access Typ;
                                    P1_Long_Arr : Java.Long_Arr;
                                    P2_Int : Java.Int;
                                    P3_Int : Java.Int) is abstract;

   procedure Write_float_array (This : access Typ;
                                P1_Float_Arr : Java.Float_Arr;
                                P2_Int : Java.Int;
                                P3_Int : Java.Int) is abstract;

   procedure Write_double_array (This : access Typ;
                                 P1_Double_Arr : Java.Double_Arr;
                                 P2_Int : Java.Int;
                                 P3_Int : Java.Int) is abstract;

   procedure Write_Object (This : access Typ;
                           P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class) is abstract;

   procedure Write_TypeCode (This : access Typ;
                             P1_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class) is abstract;

   procedure Write_any (This : access Typ;
                        P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class) is abstract;

   procedure Write (This : access Typ;
                    P1_Int : Java.Int);
   --  can raise Java.Io.IOException.Except

   procedure Write_fixed (This : access Typ;
                          P1_BigDecimal : access Standard.Java.Math.BigDecimal.Typ'Class);

   procedure Write_Context (This : access Typ;
                            P1_Context : access Standard.Org.Omg.CORBA.Context.Typ'Class;
                            P2_ContextList : access Standard.Org.Omg.CORBA.ContextList.Typ'Class);

   function Orb (This : access Typ)
                 return access Org.Omg.CORBA.ORB.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_OutputStream);
   pragma Export (Java, Create_input_stream, "create_input_stream");
   pragma Export (Java, Write_boolean, "write_boolean");
   pragma Export (Java, Write_char, "write_char");
   pragma Export (Java, Write_wchar, "write_wchar");
   pragma Export (Java, Write_octet, "write_octet");
   pragma Export (Java, Write_short, "write_short");
   pragma Export (Java, Write_ushort, "write_ushort");
   pragma Export (Java, Write_long, "write_long");
   pragma Export (Java, Write_ulong, "write_ulong");
   pragma Export (Java, Write_longlong, "write_longlong");
   pragma Export (Java, Write_ulonglong, "write_ulonglong");
   pragma Export (Java, Write_float, "write_float");
   pragma Export (Java, Write_double, "write_double");
   pragma Export (Java, Write_string, "write_string");
   pragma Export (Java, Write_wstring, "write_wstring");
   pragma Export (Java, Write_boolean_array, "write_boolean_array");
   pragma Export (Java, Write_char_array, "write_char_array");
   pragma Export (Java, Write_wchar_array, "write_wchar_array");
   pragma Export (Java, Write_octet_array, "write_octet_array");
   pragma Export (Java, Write_short_array, "write_short_array");
   pragma Export (Java, Write_ushort_array, "write_ushort_array");
   pragma Export (Java, Write_long_array, "write_long_array");
   pragma Export (Java, Write_ulong_array, "write_ulong_array");
   pragma Export (Java, Write_longlong_array, "write_longlong_array");
   pragma Export (Java, Write_ulonglong_array, "write_ulonglong_array");
   pragma Export (Java, Write_float_array, "write_float_array");
   pragma Export (Java, Write_double_array, "write_double_array");
   pragma Export (Java, Write_Object, "write_Object");
   pragma Export (Java, Write_TypeCode, "write_TypeCode");
   pragma Export (Java, Write_any, "write_any");
   pragma Export (Java, Write, "write");
   pragma Export (Java, Write_fixed, "write_fixed");
   pragma Export (Java, Write_Context, "write_Context");
   pragma Export (Java, Orb, "orb");

end Org.Omg.CORBA.Portable.OutputStream;
pragma Import (Java, Org.Omg.CORBA.Portable.OutputStream, "org.omg.CORBA.portable.OutputStream");
pragma Extensions_Allowed (Off);
