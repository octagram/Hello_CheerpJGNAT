pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.Portable.OutputStream;
with Java.Lang.Object;

package Org.Omg.CORBA.Portable.ResponseHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateReply (This : access Typ)
                         return access Org.Omg.CORBA.Portable.OutputStream.Typ'Class is abstract;

   function CreateExceptionReply (This : access Typ)
                                  return access Org.Omg.CORBA.Portable.OutputStream.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreateReply, "createReply");
   pragma Export (Java, CreateExceptionReply, "createExceptionReply");

end Org.Omg.CORBA.Portable.ResponseHandler;
pragma Import (Java, Org.Omg.CORBA.Portable.ResponseHandler, "org.omg.CORBA.portable.ResponseHandler");
pragma Extensions_Allowed (Off);
