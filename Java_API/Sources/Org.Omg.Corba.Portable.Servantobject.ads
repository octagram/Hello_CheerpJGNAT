pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Org.Omg.CORBA.Portable.ServantObject is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Servant : access Java.Lang.Object.Typ'Class;
      pragma Import (Java, Servant, "servant");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ServantObject (This : Ref := null)
                               return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ServantObject);

end Org.Omg.CORBA.Portable.ServantObject;
pragma Import (Java, Org.Omg.CORBA.Portable.ServantObject, "org.omg.CORBA.portable.ServantObject");
pragma Extensions_Allowed (Off);
