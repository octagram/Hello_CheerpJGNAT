pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.Portable.InputStream;
limited with Org.Omg.CORBA.Portable.OutputStream;
limited with Org.Omg.CORBA.TypeCode;
with Java.Lang.Object;

package Org.Omg.CORBA.Portable.Streamable is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure U_Read (This : access Typ;
                     P1_InputStream : access Standard.Org.Omg.CORBA.Portable.InputStream.Typ'Class) is abstract;

   procedure U_Write (This : access Typ;
                      P1_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class) is abstract;

   function U_Type (This : access Typ)
                    return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, U_Read, "_read");
   pragma Export (Java, U_Write, "_write");
   pragma Export (Java, U_Type, "_type");

end Org.Omg.CORBA.Portable.Streamable;
pragma Import (Java, Org.Omg.CORBA.Portable.Streamable, "org.omg.CORBA.portable.Streamable");
pragma Extensions_Allowed (Off);
