pragma Extensions_Allowed (On);
limited with Java.Io.Serializable;
limited with Org.Omg.CORBA_2_3.Portable.InputStream;
with Java.Lang.Object;

package Org.Omg.CORBA.Portable.ValueFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read_value (This : access Typ;
                        P1_InputStream : access Standard.Org.Omg.CORBA_2_3.Portable.InputStream.Typ'Class)
                        return access Java.Io.Serializable.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Read_value, "read_value");

end Org.Omg.CORBA.Portable.ValueFactory;
pragma Import (Java, Org.Omg.CORBA.Portable.ValueFactory, "org.omg.CORBA.portable.ValueFactory");
pragma Extensions_Allowed (Off);
