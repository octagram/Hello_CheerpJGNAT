pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Org.Omg.CORBA.Portable.ValueOutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Start_value (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure End_value (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Start_value, "start_value");
   pragma Export (Java, End_value, "end_value");

end Org.Omg.CORBA.Portable.ValueOutputStream;
pragma Import (Java, Org.Omg.CORBA.Portable.ValueOutputStream, "org.omg.CORBA.portable.ValueOutputStream");
pragma Extensions_Allowed (Off);
