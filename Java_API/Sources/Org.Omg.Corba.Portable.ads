pragma Extensions_Allowed (On);
package Org.Omg.CORBA.Portable is
   pragma Preelaborate;
end Org.Omg.CORBA.Portable;
pragma Import (Java, Org.Omg.CORBA.Portable, "org.omg.CORBA.portable");
pragma Extensions_Allowed (Off);
