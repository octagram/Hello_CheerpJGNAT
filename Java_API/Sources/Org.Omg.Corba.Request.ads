pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.Context;
limited with Org.Omg.CORBA.ContextList;
limited with Org.Omg.CORBA.Environment;
limited with Org.Omg.CORBA.ExceptionList;
limited with Org.Omg.CORBA.NVList;
limited with Org.Omg.CORBA.NamedValue;
limited with Org.Omg.CORBA.Object;
limited with Org.Omg.CORBA.TypeCode;
with Java.Lang.Object;

package Org.Omg.CORBA.Request is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Request (This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Target (This : access Typ)
                    return access Org.Omg.CORBA.Object.Typ'Class is abstract;

   function Operation (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   function Arguments (This : access Typ)
                       return access Org.Omg.CORBA.NVList.Typ'Class is abstract;

   function Result (This : access Typ)
                    return access Org.Omg.CORBA.NamedValue.Typ'Class is abstract;

   function Env (This : access Typ)
                 return access Org.Omg.CORBA.Environment.Typ'Class is abstract;

   function Exceptions (This : access Typ)
                        return access Org.Omg.CORBA.ExceptionList.Typ'Class is abstract;

   function Contexts (This : access Typ)
                      return access Org.Omg.CORBA.ContextList.Typ'Class is abstract;

   function Ctx (This : access Typ)
                 return access Org.Omg.CORBA.Context.Typ'Class is abstract;

   procedure Ctx (This : access Typ;
                  P1_Context : access Standard.Org.Omg.CORBA.Context.Typ'Class) is abstract;

   function Add_in_arg (This : access Typ)
                        return access Org.Omg.CORBA.Any.Typ'Class is abstract;

   function Add_named_in_arg (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Org.Omg.CORBA.Any.Typ'Class is abstract;

   function Add_inout_arg (This : access Typ)
                           return access Org.Omg.CORBA.Any.Typ'Class is abstract;

   function Add_named_inout_arg (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Org.Omg.CORBA.Any.Typ'Class is abstract;

   function Add_out_arg (This : access Typ)
                         return access Org.Omg.CORBA.Any.Typ'Class is abstract;

   function Add_named_out_arg (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Org.Omg.CORBA.Any.Typ'Class is abstract;

   procedure Set_return_type (This : access Typ;
                              P1_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class) is abstract;

   function Return_value (This : access Typ)
                          return access Org.Omg.CORBA.Any.Typ'Class is abstract;

   procedure Invoke (This : access Typ) is abstract;

   procedure Send_oneway (This : access Typ) is abstract;

   procedure Send_deferred (This : access Typ) is abstract;

   function Poll_response (This : access Typ)
                           return Java.Boolean is abstract;

   procedure Get_response (This : access Typ) is abstract;
   --  can raise Org.Omg.CORBA.WrongTransaction.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Request);
   pragma Export (Java, Target, "target");
   pragma Export (Java, Operation, "operation");
   pragma Export (Java, Arguments, "arguments");
   pragma Export (Java, Result, "result");
   pragma Export (Java, Env, "env");
   pragma Export (Java, Exceptions, "exceptions");
   pragma Export (Java, Contexts, "contexts");
   pragma Export (Java, Ctx, "ctx");
   pragma Export (Java, Add_in_arg, "add_in_arg");
   pragma Export (Java, Add_named_in_arg, "add_named_in_arg");
   pragma Export (Java, Add_inout_arg, "add_inout_arg");
   pragma Export (Java, Add_named_inout_arg, "add_named_inout_arg");
   pragma Export (Java, Add_out_arg, "add_out_arg");
   pragma Export (Java, Add_named_out_arg, "add_named_out_arg");
   pragma Export (Java, Set_return_type, "set_return_type");
   pragma Export (Java, Return_value, "return_value");
   pragma Export (Java, Invoke, "invoke");
   pragma Export (Java, Send_oneway, "send_oneway");
   pragma Export (Java, Send_deferred, "send_deferred");
   pragma Export (Java, Poll_response, "poll_response");
   pragma Export (Java, Get_response, "get_response");

end Org.Omg.CORBA.Request;
pragma Import (Java, Org.Omg.CORBA.Request, "org.omg.CORBA.Request");
pragma Extensions_Allowed (Off);
