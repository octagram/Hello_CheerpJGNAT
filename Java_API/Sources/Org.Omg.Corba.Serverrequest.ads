pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.Context;
limited with Org.Omg.CORBA.NVList;
with Java.Lang.Object;

package Org.Omg.CORBA.ServerRequest is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_ServerRequest (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Operation (This : access Typ)
                       return access Java.Lang.String.Typ'Class;

   procedure Arguments (This : access Typ;
                        P1_NVList : access Standard.Org.Omg.CORBA.NVList.Typ'Class);

   procedure Set_result (This : access Typ;
                         P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class);

   procedure Set_exception (This : access Typ;
                            P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class);

   function Ctx (This : access Typ)
                 return access Org.Omg.CORBA.Context.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ServerRequest);
   pragma Export (Java, Operation, "operation");
   pragma Export (Java, Arguments, "arguments");
   pragma Export (Java, Set_result, "set_result");
   pragma Export (Java, Set_exception, "set_exception");
   pragma Export (Java, Ctx, "ctx");

end Org.Omg.CORBA.ServerRequest;
pragma Import (Java, Org.Omg.CORBA.ServerRequest, "org.omg.CORBA.ServerRequest");
pragma Extensions_Allowed (Off);
