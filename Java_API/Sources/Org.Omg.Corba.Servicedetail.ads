pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.CORBA.ServiceDetail is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Service_detail_type : Java.Int;
      pragma Import (Java, Service_detail_type, "service_detail_type");

      Service_detail : Java.Byte_Arr;
      pragma Import (Java, Service_detail, "service_detail");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ServiceDetail (This : Ref := null)
                               return Ref;

   function New_ServiceDetail (P1_Int : Java.Int;
                               P2_Byte_Arr : Java.Byte_Arr; 
                               This : Ref := null)
                               return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ServiceDetail);

end Org.Omg.CORBA.ServiceDetail;
pragma Import (Java, Org.Omg.CORBA.ServiceDetail, "org.omg.CORBA.ServiceDetail");
pragma Extensions_Allowed (Off);
