pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.Portable.InputStream;
limited with Org.Omg.CORBA.Portable.OutputStream;
limited with Org.Omg.CORBA.ServiceDetail;
limited with Org.Omg.CORBA.TypeCode;
with Java.Lang.Object;

package Org.Omg.CORBA.ServiceDetailHelper is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_ServiceDetailHelper (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Write (P1_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class;
                    P2_ServiceDetail : access Standard.Org.Omg.CORBA.ServiceDetail.Typ'Class);

   function Read (P1_InputStream : access Standard.Org.Omg.CORBA.Portable.InputStream.Typ'Class)
                  return access Org.Omg.CORBA.ServiceDetail.Typ'Class;

   function Extract (P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class)
                     return access Org.Omg.CORBA.ServiceDetail.Typ'Class;

   procedure Insert (P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class;
                     P2_ServiceDetail : access Standard.Org.Omg.CORBA.ServiceDetail.Typ'Class);

   --  synchronized
   function type_K return access Org.Omg.CORBA.TypeCode.Typ'Class;

   function Id return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ServiceDetailHelper);
   pragma Import (Java, Write, "write");
   pragma Import (Java, Read, "read");
   pragma Import (Java, Extract, "extract");
   pragma Import (Java, Insert, "insert");
   pragma Import (Java, type_K, "type");
   pragma Import (Java, Id, "id");

end Org.Omg.CORBA.ServiceDetailHelper;
pragma Import (Java, Org.Omg.CORBA.ServiceDetailHelper, "org.omg.CORBA.ServiceDetailHelper");
pragma Extensions_Allowed (Off);
