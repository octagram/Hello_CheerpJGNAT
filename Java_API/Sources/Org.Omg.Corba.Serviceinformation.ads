pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.ServiceDetail;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.CORBA.ServiceInformation is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Service_options : Java.Int_Arr;
      pragma Import (Java, Service_options, "service_options");

      Service_details : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, Service_details, "service_details");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ServiceInformation (This : Ref := null)
                                    return Ref;

   function New_ServiceInformation (P1_Int_Arr : Java.Int_Arr;
                                    P2_ServiceDetail_Arr : access Org.Omg.CORBA.ServiceDetail.Arr_Obj; 
                                    This : Ref := null)
                                    return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ServiceInformation);

end Org.Omg.CORBA.ServiceInformation;
pragma Import (Java, Org.Omg.CORBA.ServiceInformation, "org.omg.CORBA.ServiceInformation");
pragma Extensions_Allowed (Off);
