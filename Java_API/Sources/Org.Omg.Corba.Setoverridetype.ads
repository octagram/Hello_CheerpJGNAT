pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.CORBA.SetOverrideType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Value (This : access Typ)
                   return Java.Int;

   function From_int (P1_Int : Java.Int)
                      return access Org.Omg.CORBA.SetOverrideType.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_SetOverrideType (P1_Int : Java.Int; 
                                 This : Ref := null)
                                 return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   U_SET_OVERRIDE : constant Java.Int;

   --  final
   U_ADD_OVERRIDE : constant Java.Int;

   --  final
   SET_OVERRIDE : access Org.Omg.CORBA.SetOverrideType.Typ'Class;

   --  final
   ADD_OVERRIDE : access Org.Omg.CORBA.SetOverrideType.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Value, "value");
   pragma Import (Java, From_int, "from_int");
   pragma Java_Constructor (New_SetOverrideType);
   pragma Import (Java, U_SET_OVERRIDE, "_SET_OVERRIDE");
   pragma Import (Java, U_ADD_OVERRIDE, "_ADD_OVERRIDE");
   pragma Import (Java, SET_OVERRIDE, "SET_OVERRIDE");
   pragma Import (Java, ADD_OVERRIDE, "ADD_OVERRIDE");

end Org.Omg.CORBA.SetOverrideType;
pragma Import (Java, Org.Omg.CORBA.SetOverrideType, "org.omg.CORBA.SetOverrideType");
pragma Extensions_Allowed (Off);
