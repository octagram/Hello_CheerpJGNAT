pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Portable.InputStream;
limited with Org.Omg.CORBA.Portable.OutputStream;
limited with Org.Omg.CORBA.TypeCode;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.Streamable;

package Org.Omg.CORBA.StringSeqHolder is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Streamable_I : Org.Omg.CORBA.Portable.Streamable.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Value : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, Value, "value");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StringSeqHolder (This : Ref := null)
                                 return Ref;

   function New_StringSeqHolder (P1_String_Arr : access Java.Lang.String.Arr_Obj; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure U_Read (This : access Typ;
                     P1_InputStream : access Standard.Org.Omg.CORBA.Portable.InputStream.Typ'Class);

   procedure U_Write (This : access Typ;
                      P1_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class);

   function U_Type (This : access Typ)
                    return access Org.Omg.CORBA.TypeCode.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StringSeqHolder);
   pragma Import (Java, U_Read, "_read");
   pragma Import (Java, U_Write, "_write");
   pragma Import (Java, U_Type, "_type");

end Org.Omg.CORBA.StringSeqHolder;
pragma Import (Java, Org.Omg.CORBA.StringSeqHolder, "org.omg.CORBA.StringSeqHolder");
pragma Extensions_Allowed (Off);
