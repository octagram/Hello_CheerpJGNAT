pragma Extensions_Allowed (On);
limited with Java.Io.Serializable;
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.Portable.InputStream;
limited with Org.Omg.CORBA.Portable.OutputStream;
limited with Org.Omg.CORBA.TypeCode;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.BoxedValueHelper;

package Org.Omg.CORBA.StringValueHelper is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(BoxedValueHelper_I : Org.Omg.CORBA.Portable.BoxedValueHelper.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StringValueHelper (This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Insert (P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class);

   function Extract (P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class)
                     return access Java.Lang.String.Typ'Class;

   --  synchronized
   function type_K return access Org.Omg.CORBA.TypeCode.Typ'Class;

   function Id return access Java.Lang.String.Typ'Class;

   function Read (P1_InputStream : access Standard.Org.Omg.CORBA.Portable.InputStream.Typ'Class)
                  return access Java.Lang.String.Typ'Class;

   function Read_value (This : access Typ;
                        P1_InputStream : access Standard.Org.Omg.CORBA.Portable.InputStream.Typ'Class)
                        return access Java.Io.Serializable.Typ'Class;

   procedure Write (P1_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Write_value (This : access Typ;
                          P1_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class;
                          P2_Serializable : access Standard.Java.Io.Serializable.Typ'Class);

   function Get_id (This : access Typ)
                    return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StringValueHelper);
   pragma Import (Java, Insert, "insert");
   pragma Import (Java, Extract, "extract");
   pragma Import (Java, type_K, "type");
   pragma Import (Java, Id, "id");
   pragma Import (Java, Read, "read");
   pragma Import (Java, Read_value, "read_value");
   pragma Import (Java, Write, "write");
   pragma Import (Java, Write_value, "write_value");
   pragma Import (Java, Get_id, "get_id");

end Org.Omg.CORBA.StringValueHelper;
pragma Import (Java, Org.Omg.CORBA.StringValueHelper, "org.omg.CORBA.StringValueHelper");
pragma Extensions_Allowed (Off);
