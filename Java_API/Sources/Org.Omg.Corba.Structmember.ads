pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.IDLType;
limited with Org.Omg.CORBA.TypeCode;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.CORBA.StructMember is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Name : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Name, "name");

      type_K : access Org.Omg.CORBA.TypeCode.Typ'Class;
      pragma Import (Java, type_K, "type");

      Type_def : access Org.Omg.CORBA.IDLType.Typ'Class;
      pragma Import (Java, Type_def, "type_def");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_StructMember (This : Ref := null)
                              return Ref;

   function New_StructMember (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class;
                              P3_IDLType : access Standard.Org.Omg.CORBA.IDLType.Typ'Class; 
                              This : Ref := null)
                              return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_StructMember);

end Org.Omg.CORBA.StructMember;
pragma Import (Java, Org.Omg.CORBA.StructMember, "org.omg.CORBA.StructMember");
pragma Extensions_Allowed (Off);
