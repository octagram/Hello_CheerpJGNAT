pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.CompletionStatus;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Lang.RuntimeException;

package Org.Omg.CORBA.SystemException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is abstract new Java.Lang.RuntimeException.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Minor : Java.Int;
      pragma Import (Java, Minor, "minor");

      Completed : access Org.Omg.CORBA.CompletionStatus.Typ'Class;
      pragma Import (Java, Completed, "completed");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   --  protected
   function New_SystemException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_Int : Java.Int;
                                 P3_CompletionStatus : access Standard.Org.Omg.CORBA.CompletionStatus.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "org.omg.CORBA.SystemException");
   pragma Java_Constructor (New_SystemException);
   pragma Import (Java, ToString, "toString");

end Org.Omg.CORBA.SystemException;
pragma Import (Java, Org.Omg.CORBA.SystemException, "org.omg.CORBA.SystemException");
pragma Extensions_Allowed (Off);
