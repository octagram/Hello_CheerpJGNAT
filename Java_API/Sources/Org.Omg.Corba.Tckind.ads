pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Org.Omg.CORBA.TCKind is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Value (This : access Typ)
                   return Java.Int;

   function From_int (P1_Int : Java.Int)
                      return access Org.Omg.CORBA.TCKind.Typ'Class;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   U_Tk_null : constant Java.Int;

   --  final
   U_Tk_void : constant Java.Int;

   --  final
   U_Tk_short : constant Java.Int;

   --  final
   U_Tk_long : constant Java.Int;

   --  final
   U_Tk_ushort : constant Java.Int;

   --  final
   U_Tk_ulong : constant Java.Int;

   --  final
   U_Tk_float : constant Java.Int;

   --  final
   U_Tk_double : constant Java.Int;

   --  final
   U_Tk_boolean : constant Java.Int;

   --  final
   U_Tk_char : constant Java.Int;

   --  final
   U_Tk_octet : constant Java.Int;

   --  final
   U_Tk_any : constant Java.Int;

   --  final
   U_Tk_TypeCode : constant Java.Int;

   --  final
   U_Tk_Principal : constant Java.Int;

   --  final
   U_Tk_objref : constant Java.Int;

   --  final
   U_Tk_struct : constant Java.Int;

   --  final
   U_Tk_union : constant Java.Int;

   --  final
   U_Tk_enum : constant Java.Int;

   --  final
   U_Tk_string : constant Java.Int;

   --  final
   U_Tk_sequence : constant Java.Int;

   --  final
   U_Tk_array : constant Java.Int;

   --  final
   U_Tk_alias : constant Java.Int;

   --  final
   U_Tk_except : constant Java.Int;

   --  final
   U_Tk_longlong : constant Java.Int;

   --  final
   U_Tk_ulonglong : constant Java.Int;

   --  final
   U_Tk_longdouble : constant Java.Int;

   --  final
   U_Tk_wchar : constant Java.Int;

   --  final
   U_Tk_wstring : constant Java.Int;

   --  final
   U_Tk_fixed : constant Java.Int;

   --  final
   U_Tk_value : constant Java.Int;

   --  final
   U_Tk_value_box : constant Java.Int;

   --  final
   U_Tk_native : constant Java.Int;

   --  final
   U_Tk_abstract_interface : constant Java.Int;

   --  final
   Tk_null : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_void : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_short : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_long : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_ushort : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_ulong : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_float : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_double : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_boolean : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_char : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_octet : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_any : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_TypeCode : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_Principal : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_objref : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_struct : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_union : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_enum : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_string : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_sequence : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_array : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_alias : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_except : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_longlong : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_ulonglong : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_longdouble : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_wchar : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_wstring : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_fixed : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_value : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_value_box : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_native : access Org.Omg.CORBA.TCKind.Typ'Class;

   --  final
   Tk_abstract_interface : access Org.Omg.CORBA.TCKind.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Value, "value");
   pragma Import (Java, From_int, "from_int");
   pragma Import (Java, U_Tk_null, "_tk_null");
   pragma Import (Java, U_Tk_void, "_tk_void");
   pragma Import (Java, U_Tk_short, "_tk_short");
   pragma Import (Java, U_Tk_long, "_tk_long");
   pragma Import (Java, U_Tk_ushort, "_tk_ushort");
   pragma Import (Java, U_Tk_ulong, "_tk_ulong");
   pragma Import (Java, U_Tk_float, "_tk_float");
   pragma Import (Java, U_Tk_double, "_tk_double");
   pragma Import (Java, U_Tk_boolean, "_tk_boolean");
   pragma Import (Java, U_Tk_char, "_tk_char");
   pragma Import (Java, U_Tk_octet, "_tk_octet");
   pragma Import (Java, U_Tk_any, "_tk_any");
   pragma Import (Java, U_Tk_TypeCode, "_tk_TypeCode");
   pragma Import (Java, U_Tk_Principal, "_tk_Principal");
   pragma Import (Java, U_Tk_objref, "_tk_objref");
   pragma Import (Java, U_Tk_struct, "_tk_struct");
   pragma Import (Java, U_Tk_union, "_tk_union");
   pragma Import (Java, U_Tk_enum, "_tk_enum");
   pragma Import (Java, U_Tk_string, "_tk_string");
   pragma Import (Java, U_Tk_sequence, "_tk_sequence");
   pragma Import (Java, U_Tk_array, "_tk_array");
   pragma Import (Java, U_Tk_alias, "_tk_alias");
   pragma Import (Java, U_Tk_except, "_tk_except");
   pragma Import (Java, U_Tk_longlong, "_tk_longlong");
   pragma Import (Java, U_Tk_ulonglong, "_tk_ulonglong");
   pragma Import (Java, U_Tk_longdouble, "_tk_longdouble");
   pragma Import (Java, U_Tk_wchar, "_tk_wchar");
   pragma Import (Java, U_Tk_wstring, "_tk_wstring");
   pragma Import (Java, U_Tk_fixed, "_tk_fixed");
   pragma Import (Java, U_Tk_value, "_tk_value");
   pragma Import (Java, U_Tk_value_box, "_tk_value_box");
   pragma Import (Java, U_Tk_native, "_tk_native");
   pragma Import (Java, U_Tk_abstract_interface, "_tk_abstract_interface");
   pragma Import (Java, Tk_null, "tk_null");
   pragma Import (Java, Tk_void, "tk_void");
   pragma Import (Java, Tk_short, "tk_short");
   pragma Import (Java, Tk_long, "tk_long");
   pragma Import (Java, Tk_ushort, "tk_ushort");
   pragma Import (Java, Tk_ulong, "tk_ulong");
   pragma Import (Java, Tk_float, "tk_float");
   pragma Import (Java, Tk_double, "tk_double");
   pragma Import (Java, Tk_boolean, "tk_boolean");
   pragma Import (Java, Tk_char, "tk_char");
   pragma Import (Java, Tk_octet, "tk_octet");
   pragma Import (Java, Tk_any, "tk_any");
   pragma Import (Java, Tk_TypeCode, "tk_TypeCode");
   pragma Import (Java, Tk_Principal, "tk_Principal");
   pragma Import (Java, Tk_objref, "tk_objref");
   pragma Import (Java, Tk_struct, "tk_struct");
   pragma Import (Java, Tk_union, "tk_union");
   pragma Import (Java, Tk_enum, "tk_enum");
   pragma Import (Java, Tk_string, "tk_string");
   pragma Import (Java, Tk_sequence, "tk_sequence");
   pragma Import (Java, Tk_array, "tk_array");
   pragma Import (Java, Tk_alias, "tk_alias");
   pragma Import (Java, Tk_except, "tk_except");
   pragma Import (Java, Tk_longlong, "tk_longlong");
   pragma Import (Java, Tk_ulonglong, "tk_ulonglong");
   pragma Import (Java, Tk_longdouble, "tk_longdouble");
   pragma Import (Java, Tk_wchar, "tk_wchar");
   pragma Import (Java, Tk_wstring, "tk_wstring");
   pragma Import (Java, Tk_fixed, "tk_fixed");
   pragma Import (Java, Tk_value, "tk_value");
   pragma Import (Java, Tk_value_box, "tk_value_box");
   pragma Import (Java, Tk_native, "tk_native");
   pragma Import (Java, Tk_abstract_interface, "tk_abstract_interface");

end Org.Omg.CORBA.TCKind;
pragma Import (Java, Org.Omg.CORBA.TCKind, "org.omg.CORBA.TCKind");
pragma Extensions_Allowed (Off);
