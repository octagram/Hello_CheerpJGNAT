pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.TCKind;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.CORBA.TypeCode is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;

   function New_TypeCode (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Equal (This : access Typ;
                   P1_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class)
                   return Java.Boolean is abstract;

   function Equivalent (This : access Typ;
                        P1_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class)
                        return Java.Boolean is abstract;

   function Get_compact_typecode (This : access Typ)
                                  return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;

   function Kind (This : access Typ)
                  return access Org.Omg.CORBA.TCKind.Typ'Class is abstract;

   function Id (This : access Typ)
                return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.TypeCodePackage.BadKind.Except

   function Name (This : access Typ)
                  return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.TypeCodePackage.BadKind.Except

   function Member_count (This : access Typ)
                          return Java.Int is abstract;
   --  can raise Org.Omg.CORBA.TypeCodePackage.BadKind.Except

   function Member_name (This : access Typ;
                         P1_Int : Java.Int)
                         return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.TypeCodePackage.BadKind.Except and
   --  Org.Omg.CORBA.TypeCodePackage.Bounds.Except

   function Member_type (This : access Typ;
                         P1_Int : Java.Int)
                         return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.TypeCodePackage.BadKind.Except and
   --  Org.Omg.CORBA.TypeCodePackage.Bounds.Except

   function Member_label (This : access Typ;
                          P1_Int : Java.Int)
                          return access Org.Omg.CORBA.Any.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.TypeCodePackage.BadKind.Except and
   --  Org.Omg.CORBA.TypeCodePackage.Bounds.Except

   function Discriminator_type (This : access Typ)
                                return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.TypeCodePackage.BadKind.Except

   function Default_index (This : access Typ)
                           return Java.Int is abstract;
   --  can raise Org.Omg.CORBA.TypeCodePackage.BadKind.Except

   function Length (This : access Typ)
                    return Java.Int is abstract;
   --  can raise Org.Omg.CORBA.TypeCodePackage.BadKind.Except

   function Content_type (This : access Typ)
                          return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.TypeCodePackage.BadKind.Except

   function Fixed_digits (This : access Typ)
                          return Java.Short is abstract;
   --  can raise Org.Omg.CORBA.TypeCodePackage.BadKind.Except

   function Fixed_scale (This : access Typ)
                         return Java.Short is abstract;
   --  can raise Org.Omg.CORBA.TypeCodePackage.BadKind.Except

   function Member_visibility (This : access Typ;
                               P1_Int : Java.Int)
                               return Java.Short is abstract;
   --  can raise Org.Omg.CORBA.TypeCodePackage.BadKind.Except and
   --  Org.Omg.CORBA.TypeCodePackage.Bounds.Except

   function Type_modifier (This : access Typ)
                           return Java.Short is abstract;
   --  can raise Org.Omg.CORBA.TypeCodePackage.BadKind.Except

   function Concrete_base_type (This : access Typ)
                                return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.TypeCodePackage.BadKind.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TypeCode);
   pragma Export (Java, Equal, "equal");
   pragma Export (Java, Equivalent, "equivalent");
   pragma Export (Java, Get_compact_typecode, "get_compact_typecode");
   pragma Export (Java, Kind, "kind");
   pragma Export (Java, Id, "id");
   pragma Export (Java, Name, "name");
   pragma Export (Java, Member_count, "member_count");
   pragma Export (Java, Member_name, "member_name");
   pragma Export (Java, Member_type, "member_type");
   pragma Export (Java, Member_label, "member_label");
   pragma Export (Java, Discriminator_type, "discriminator_type");
   pragma Export (Java, Default_index, "default_index");
   pragma Export (Java, Length, "length");
   pragma Export (Java, Content_type, "content_type");
   pragma Export (Java, Fixed_digits, "fixed_digits");
   pragma Export (Java, Fixed_scale, "fixed_scale");
   pragma Export (Java, Member_visibility, "member_visibility");
   pragma Export (Java, Type_modifier, "type_modifier");
   pragma Export (Java, Concrete_base_type, "concrete_base_type");

end Org.Omg.CORBA.TypeCode;
pragma Import (Java, Org.Omg.CORBA.TypeCode, "org.omg.CORBA.TypeCode");
pragma Extensions_Allowed (Off);
