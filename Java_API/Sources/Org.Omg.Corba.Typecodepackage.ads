pragma Extensions_Allowed (On);
package Org.Omg.CORBA.TypeCodePackage is
   pragma Preelaborate;
end Org.Omg.CORBA.TypeCodePackage;
pragma Import (Java, Org.Omg.CORBA.TypeCodePackage, "org.omg.CORBA.TypeCodePackage");
pragma Extensions_Allowed (Off);
