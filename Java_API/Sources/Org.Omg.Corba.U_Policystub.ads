pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Portable.Delegate;
with Java.Lang.Object;
with Org.Omg.CORBA.Object;
with Org.Omg.CORBA.Policy;
with Org.Omg.CORBA.Portable.ObjectImpl;

package Org.Omg.CORBA.U_PolicyStub is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Object_I : Org.Omg.CORBA.Object.Ref;
            Policy_I : Org.Omg.CORBA.Policy.Ref)
    is new Org.Omg.CORBA.Portable.ObjectImpl.Typ(Object_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_U_PolicyStub (This : Ref := null)
                              return Ref;

   function New_U_PolicyStub (P1_Delegate : access Standard.Org.Omg.CORBA.Portable.Delegate.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Policy_type (This : access Typ)
                         return Java.Int;

   function Copy (This : access Typ)
                  return access Org.Omg.CORBA.Policy.Typ'Class;

   procedure Destroy (This : access Typ);

   function U_Ids (This : access Typ)
                   return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_U_PolicyStub);
   pragma Import (Java, Policy_type, "policy_type");
   pragma Import (Java, Copy, "copy");
   pragma Import (Java, Destroy, "destroy");
   pragma Import (Java, U_Ids, "_ids");

end Org.Omg.CORBA.U_PolicyStub;
pragma Import (Java, Org.Omg.CORBA.U_PolicyStub, "org.omg.CORBA._PolicyStub");
pragma Extensions_Allowed (Off);
