pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.IDLType;
limited with Org.Omg.CORBA.TypeCode;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.CORBA.UnionMember is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Name : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Name, "name");

      Label : access Org.Omg.CORBA.Any.Typ'Class;
      pragma Import (Java, Label, "label");

      type_K : access Org.Omg.CORBA.TypeCode.Typ'Class;
      pragma Import (Java, type_K, "type");

      Type_def : access Org.Omg.CORBA.IDLType.Typ'Class;
      pragma Import (Java, Type_def, "type_def");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_UnionMember (This : Ref := null)
                             return Ref;

   function New_UnionMember (P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class;
                             P3_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class;
                             P4_IDLType : access Standard.Org.Omg.CORBA.IDLType.Typ'Class; 
                             This : Ref := null)
                             return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_UnionMember);

end Org.Omg.CORBA.UnionMember;
pragma Import (Java, Org.Omg.CORBA.UnionMember, "org.omg.CORBA.UnionMember");
pragma Extensions_Allowed (Off);
