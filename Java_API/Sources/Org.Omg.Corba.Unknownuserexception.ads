pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.Any;
with Java.Io.Serializable;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;
with Org.Omg.CORBA.UserException;

package Org.Omg.CORBA.UnknownUserException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Org.Omg.CORBA.UserException.Typ(Serializable_I,
                                           IDLEntity_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Except : access Org.Omg.CORBA.Any.Typ'Class;
      pragma Import (Java, Except, "except");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_UnknownUserException (This : Ref := null)
                                      return Ref;

   function New_UnknownUserException (P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class; 
                                      This : Ref := null)
                                      return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "org.omg.CORBA.UnknownUserException");
   pragma Java_Constructor (New_UnknownUserException);

end Org.Omg.CORBA.UnknownUserException;
pragma Import (Java, Org.Omg.CORBA.UnknownUserException, "org.omg.CORBA.UnknownUserException");
pragma Extensions_Allowed (Off);
