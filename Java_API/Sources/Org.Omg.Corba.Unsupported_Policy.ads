pragma Extensions_Allowed (On);
with Java.Lang.Object;

package Org.Omg.CORBA.UNSUPPORTED_POLICY is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   Value : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Value, "value");

end Org.Omg.CORBA.UNSUPPORTED_POLICY;
pragma Import (Java, Org.Omg.CORBA.UNSUPPORTED_POLICY, "org.omg.CORBA.UNSUPPORTED_POLICY");
pragma Extensions_Allowed (Off);
