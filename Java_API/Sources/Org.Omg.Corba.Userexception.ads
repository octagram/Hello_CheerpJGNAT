pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.CORBA.UserException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is abstract new Java.Lang.Exception_K.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   --  protected
   function New_UserException (This : Ref := null)
                               return Ref;

   --  protected
   function New_UserException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "org.omg.CORBA.UserException");
   pragma Java_Constructor (New_UserException);

end Org.Omg.CORBA.UserException;
pragma Import (Java, Org.Omg.CORBA.UserException, "org.omg.CORBA.UserException");
pragma Extensions_Allowed (Off);
