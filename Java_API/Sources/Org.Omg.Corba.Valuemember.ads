pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.IDLType;
limited with Org.Omg.CORBA.TypeCode;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.CORBA.ValueMember is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Name : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Name, "name");

      Id : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Id, "id");

      Defined_in : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Defined_in, "defined_in");

      Version : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Version, "version");

      type_K : access Org.Omg.CORBA.TypeCode.Typ'Class;
      pragma Import (Java, type_K, "type");

      Type_def : access Org.Omg.CORBA.IDLType.Typ'Class;
      pragma Import (Java, Type_def, "type_def");

      access_K : Java.Short;
      pragma Import (Java, access_K, "access");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ValueMember (This : Ref := null)
                             return Ref;

   function New_ValueMember (P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_String : access Standard.Java.Lang.String.Typ'Class;
                             P4_String : access Standard.Java.Lang.String.Typ'Class;
                             P5_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class;
                             P6_IDLType : access Standard.Org.Omg.CORBA.IDLType.Typ'Class;
                             P7_Short : Java.Short; 
                             This : Ref := null)
                             return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ValueMember);

end Org.Omg.CORBA.ValueMember;
pragma Import (Java, Org.Omg.CORBA.ValueMember, "org.omg.CORBA.ValueMember");
pragma Extensions_Allowed (Off);
