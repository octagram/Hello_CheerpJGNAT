pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.Portable.InputStream;
limited with Org.Omg.CORBA.Portable.OutputStream;
limited with Org.Omg.CORBA.TypeCode;
with Java.Lang.Object;

package Org.Omg.CORBA.WStringSeqHelper is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_WStringSeqHelper (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Insert (P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class;
                     P2_String_Arr : access Java.Lang.String.Arr_Obj);

   function Extract (P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class)
                     return Standard.Java.Lang.Object.Ref;

   --  synchronized
   function type_K return access Org.Omg.CORBA.TypeCode.Typ'Class;

   function Id return access Java.Lang.String.Typ'Class;

   function Read (P1_InputStream : access Standard.Org.Omg.CORBA.Portable.InputStream.Typ'Class)
                  return Standard.Java.Lang.Object.Ref;

   procedure Write (P1_OutputStream : access Standard.Org.Omg.CORBA.Portable.OutputStream.Typ'Class;
                    P2_String_Arr : access Java.Lang.String.Arr_Obj);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_WStringSeqHelper);
   pragma Import (Java, Insert, "insert");
   pragma Import (Java, Extract, "extract");
   pragma Import (Java, type_K, "type");
   pragma Import (Java, Id, "id");
   pragma Import (Java, Read, "read");
   pragma Import (Java, Write, "write");

end Org.Omg.CORBA.WStringSeqHelper;
pragma Import (Java, Org.Omg.CORBA.WStringSeqHelper, "org.omg.CORBA.WStringSeqHelper");
pragma Extensions_Allowed (Off);
