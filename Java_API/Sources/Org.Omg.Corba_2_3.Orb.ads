pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Object;
limited with Org.Omg.CORBA.Portable.ValueFactory;
with Java.Lang.Object;
with Org.Omg.CORBA.ORB;

package Org.Omg.CORBA_2_3.ORB is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Org.Omg.CORBA.ORB.Typ
      with null record;

   function New_ORB (This : Ref := null)
                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Register_value_factory (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_ValueFactory : access Standard.Org.Omg.CORBA.Portable.ValueFactory.Typ'Class)
                                    return access Org.Omg.CORBA.Portable.ValueFactory.Typ'Class;

   procedure Unregister_value_factory (This : access Typ;
                                       P1_String : access Standard.Java.Lang.String.Typ'Class);

   function Lookup_value_factory (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Org.Omg.CORBA.Portable.ValueFactory.Typ'Class;

   function Get_value_def (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Org.Omg.CORBA.Object.Typ'Class;
   --  can raise Org.Omg.CORBA.BAD_PARAM.Except

   procedure Set_delegate (This : access Typ;
                           P1_Object : access Standard.Java.Lang.Object.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ORB);
   pragma Import (Java, Register_value_factory, "register_value_factory");
   pragma Import (Java, Unregister_value_factory, "unregister_value_factory");
   pragma Import (Java, Lookup_value_factory, "lookup_value_factory");
   pragma Import (Java, Get_value_def, "get_value_def");
   pragma Import (Java, Set_delegate, "set_delegate");

end Org.Omg.CORBA_2_3.ORB;
pragma Import (Java, Org.Omg.CORBA_2_3.ORB, "org.omg.CORBA_2_3.ORB");
pragma Extensions_Allowed (Off);
