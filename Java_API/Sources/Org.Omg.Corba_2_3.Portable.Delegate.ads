pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Object;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.Delegate;

package Org.Omg.CORBA_2_3.Portable.Delegate is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Org.Omg.CORBA.Portable.Delegate.Typ
      with null record;

   function New_Delegate (This : Ref := null)
                          return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get_codebase (This : access Typ;
                          P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class)
                          return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Delegate);
   pragma Import (Java, Get_codebase, "get_codebase");

end Org.Omg.CORBA_2_3.Portable.Delegate;
pragma Import (Java, Org.Omg.CORBA_2_3.Portable.Delegate, "org.omg.CORBA_2_3.portable.Delegate");
pragma Extensions_Allowed (Off);
