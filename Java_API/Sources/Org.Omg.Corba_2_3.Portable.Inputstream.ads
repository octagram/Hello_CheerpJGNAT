pragma Extensions_Allowed (On);
limited with Java.Io.Serializable;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Portable.BoxedValueHelper;
with Java.Io.Closeable;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.InputStream;

package Org.Omg.CORBA_2_3.Portable.InputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref)
    is abstract new Org.Omg.CORBA.Portable.InputStream.Typ(Closeable_I)
      with null record;

   function New_InputStream (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Read_value (This : access Typ)
                        return access Java.Io.Serializable.Typ'Class;

   function Read_value (This : access Typ;
                        P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                        return access Java.Io.Serializable.Typ'Class;

   function Read_value (This : access Typ;
                        P1_BoxedValueHelper : access Standard.Org.Omg.CORBA.Portable.BoxedValueHelper.Typ'Class)
                        return access Java.Io.Serializable.Typ'Class;

   function Read_value (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Io.Serializable.Typ'Class;

   function Read_value (This : access Typ;
                        P1_Serializable : access Standard.Java.Io.Serializable.Typ'Class)
                        return access Java.Io.Serializable.Typ'Class;

   function Read_abstract_interface (This : access Typ)
                                     return access Java.Lang.Object.Typ'Class;

   function Read_abstract_interface (This : access Typ;
                                     P1_Class : access Standard.Java.Lang.Class.Typ'Class)
                                     return access Java.Lang.Object.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InputStream);
   pragma Import (Java, Read_value, "read_value");
   pragma Import (Java, Read_abstract_interface, "read_abstract_interface");

end Org.Omg.CORBA_2_3.Portable.InputStream;
pragma Import (Java, Org.Omg.CORBA_2_3.Portable.InputStream, "org.omg.CORBA_2_3.portable.InputStream");
pragma Extensions_Allowed (Off);
