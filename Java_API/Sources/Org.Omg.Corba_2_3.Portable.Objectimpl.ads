pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.Omg.CORBA.Object;
with Org.Omg.CORBA.Portable.ObjectImpl;

package Org.Omg.CORBA_2_3.Portable.ObjectImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Object_I : Org.Omg.CORBA.Object.Ref)
    is abstract new Org.Omg.CORBA.Portable.ObjectImpl.Typ(Object_I)
      with null record;

   function New_ObjectImpl (This : Ref := null)
                            return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function U_Get_codebase (This : access Typ)
                            return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ObjectImpl);
   pragma Import (Java, U_Get_codebase, "_get_codebase");

end Org.Omg.CORBA_2_3.Portable.ObjectImpl;
pragma Import (Java, Org.Omg.CORBA_2_3.Portable.ObjectImpl, "org.omg.CORBA_2_3.portable.ObjectImpl");
pragma Extensions_Allowed (Off);
