pragma Extensions_Allowed (On);
limited with Java.Io.Serializable;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Portable.BoxedValueHelper;
with Java.Io.Closeable;
with Java.Io.Flushable;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.OutputStream;

package Org.Omg.CORBA_2_3.Portable.OutputStream is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Closeable_I : Java.Io.Closeable.Ref;
            Flushable_I : Java.Io.Flushable.Ref)
    is abstract new Org.Omg.CORBA.Portable.OutputStream.Typ(Closeable_I,
                                                            Flushable_I)
      with null record;

   function New_OutputStream (This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Write_value (This : access Typ;
                          P1_Serializable : access Standard.Java.Io.Serializable.Typ'Class);

   procedure Write_value (This : access Typ;
                          P1_Serializable : access Standard.Java.Io.Serializable.Typ'Class;
                          P2_Class : access Standard.Java.Lang.Class.Typ'Class);

   procedure Write_value (This : access Typ;
                          P1_Serializable : access Standard.Java.Io.Serializable.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Write_value (This : access Typ;
                          P1_Serializable : access Standard.Java.Io.Serializable.Typ'Class;
                          P2_BoxedValueHelper : access Standard.Org.Omg.CORBA.Portable.BoxedValueHelper.Typ'Class);

   procedure Write_abstract_interface (This : access Typ;
                                       P1_Object : access Standard.Java.Lang.Object.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_OutputStream);
   pragma Import (Java, Write_value, "write_value");
   pragma Import (Java, Write_abstract_interface, "write_abstract_interface");

end Org.Omg.CORBA_2_3.Portable.OutputStream;
pragma Import (Java, Org.Omg.CORBA_2_3.Portable.OutputStream, "org.omg.CORBA_2_3.portable.OutputStream");
pragma Extensions_Allowed (Off);
