pragma Extensions_Allowed (On);
package Org.Omg.CORBA_2_3.Portable is
   pragma Preelaborate;
end Org.Omg.CORBA_2_3.Portable;
pragma Import (Java, Org.Omg.CORBA_2_3.Portable, "org.omg.CORBA_2_3.portable");
pragma Extensions_Allowed (Off);
