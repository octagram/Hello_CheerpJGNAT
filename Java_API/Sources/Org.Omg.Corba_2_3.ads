pragma Extensions_Allowed (On);
package Org.Omg.CORBA_2_3 is
   pragma Preelaborate;
end Org.Omg.CORBA_2_3;
pragma Import (Java, Org.Omg.CORBA_2_3, "org.omg.CORBA_2_3");
pragma Extensions_Allowed (Off);
