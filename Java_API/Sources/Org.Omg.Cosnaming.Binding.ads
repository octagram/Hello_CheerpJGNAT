pragma Extensions_Allowed (On);
limited with Org.Omg.CosNaming.BindingType;
limited with Org.Omg.CosNaming.NameComponent;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.CosNaming.Binding is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Binding_name : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, Binding_name, "binding_name");

      Binding_type : access Org.Omg.CosNaming.BindingType.Typ'Class;
      pragma Import (Java, Binding_type, "binding_type");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Binding (This : Ref := null)
                         return Ref;

   function New_Binding (P1_NameComponent_Arr : access Org.Omg.CosNaming.NameComponent.Arr_Obj;
                         P2_BindingType : access Standard.Org.Omg.CosNaming.BindingType.Typ'Class; 
                         This : Ref := null)
                         return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Binding);

end Org.Omg.CosNaming.Binding;
pragma Import (Java, Org.Omg.CosNaming.Binding, "org.omg.CosNaming.Binding");
pragma Extensions_Allowed (Off);
