pragma Extensions_Allowed (On);
limited with Org.Omg.CosNaming.BindingHolder;
limited with Org.Omg.CosNaming.BindingListHolder;
with Java.Lang.Object;

package Org.Omg.CosNaming.BindingIteratorOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Next_one (This : access Typ;
                      P1_BindingHolder : access Standard.Org.Omg.CosNaming.BindingHolder.Typ'Class)
                      return Java.Boolean is abstract;

   function Next_n (This : access Typ;
                    P1_Int : Java.Int;
                    P2_BindingListHolder : access Standard.Org.Omg.CosNaming.BindingListHolder.Typ'Class)
                    return Java.Boolean is abstract;

   procedure Destroy (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Next_one, "next_one");
   pragma Export (Java, Next_n, "next_n");
   pragma Export (Java, Destroy, "destroy");

end Org.Omg.CosNaming.BindingIteratorOperations;
pragma Import (Java, Org.Omg.CosNaming.BindingIteratorOperations, "org.omg.CosNaming.BindingIteratorOperations");
pragma Extensions_Allowed (Off);
