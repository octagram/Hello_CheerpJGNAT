pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.CosNaming.BindingType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Value (This : access Typ)
                   return Java.Int;

   function From_int (P1_Int : Java.Int)
                      return access Org.Omg.CosNaming.BindingType.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_BindingType (P1_Int : Java.Int; 
                             This : Ref := null)
                             return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   U_Nobject : constant Java.Int;

   --  final
   Nobject : access Org.Omg.CosNaming.BindingType.Typ'Class;

   --  final
   U_Ncontext : constant Java.Int;

   --  final
   Ncontext : access Org.Omg.CosNaming.BindingType.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Value, "value");
   pragma Import (Java, From_int, "from_int");
   pragma Java_Constructor (New_BindingType);
   pragma Import (Java, U_Nobject, "_nobject");
   pragma Import (Java, Nobject, "nobject");
   pragma Import (Java, U_Ncontext, "_ncontext");
   pragma Import (Java, Ncontext, "ncontext");

end Org.Omg.CosNaming.BindingType;
pragma Import (Java, Org.Omg.CosNaming.BindingType, "org.omg.CosNaming.BindingType");
pragma Extensions_Allowed (Off);
