pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.CosNaming.NameComponent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Id : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Id, "id");

      Kind : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Kind, "kind");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NameComponent (This : Ref := null)
                               return Ref;

   function New_NameComponent (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class; 
                               This : Ref := null)
                               return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NameComponent);

end Org.Omg.CosNaming.NameComponent;
pragma Import (Java, Org.Omg.CosNaming.NameComponent, "org.omg.CosNaming.NameComponent");
pragma Extensions_Allowed (Off);
