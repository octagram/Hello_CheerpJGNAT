pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;
with Org.Omg.CosNaming.NamingContext;
with Org.Omg.CosNaming.NamingContextExtOperations;

package Org.Omg.CosNaming.NamingContextExt is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref;
            NamingContext_I : Org.Omg.CosNaming.NamingContext.Ref;
            NamingContextExtOperations_I : Org.Omg.CosNaming.NamingContextExtOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   
private
   pragma Convention (Java, Typ);

end Org.Omg.CosNaming.NamingContextExt;
pragma Import (Java, Org.Omg.CosNaming.NamingContextExt, "org.omg.CosNaming.NamingContextExt");
pragma Extensions_Allowed (Off);
