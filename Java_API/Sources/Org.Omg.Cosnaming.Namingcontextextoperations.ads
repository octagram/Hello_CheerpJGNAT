pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Object;
limited with Org.Omg.CosNaming.NameComponent;
with Java.Lang.Object;
with Org.Omg.CosNaming.NamingContextOperations;

package Org.Omg.CosNaming.NamingContextExtOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            NamingContextOperations_I : Org.Omg.CosNaming.NamingContextOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function To_string (This : access Typ;
                       P1_NameComponent_Arr : access Org.Omg.CosNaming.NameComponent.Arr_Obj)
                       return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.Omg.CosNaming.NamingContextPackage.InvalidName.Except

   function To_name (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Org.Omg.CosNaming.NamingContextPackage.InvalidName.Except

   function To_url (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                    P2_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.Omg.CosNaming.NamingContextExtPackage.InvalidAddress.Except and
   --  Org.Omg.CosNaming.NamingContextPackage.InvalidName.Except

   function Resolve_str (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Org.Omg.CORBA.Object.Typ'Class is abstract;
   --  can raise Org.Omg.CosNaming.NamingContextPackage.NotFound.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.CannotProceed.Except and
   --  Org.Omg.CosNaming.NamingContextPackage.InvalidName.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, To_string, "to_string");
   pragma Export (Java, To_name, "to_name");
   pragma Export (Java, To_url, "to_url");
   pragma Export (Java, Resolve_str, "resolve_str");

end Org.Omg.CosNaming.NamingContextExtOperations;
pragma Import (Java, Org.Omg.CosNaming.NamingContextExtOperations, "org.omg.CosNaming.NamingContextExtOperations");
pragma Extensions_Allowed (Off);
