pragma Extensions_Allowed (On);
package Org.Omg.CosNaming.NamingContextExtPackage is
   pragma Preelaborate;
end Org.Omg.CosNaming.NamingContextExtPackage;
pragma Import (Java, Org.Omg.CosNaming.NamingContextExtPackage, "org.omg.CosNaming.NamingContextExtPackage");
pragma Extensions_Allowed (Off);
