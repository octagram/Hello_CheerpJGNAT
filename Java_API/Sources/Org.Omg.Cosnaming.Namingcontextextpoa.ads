pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.ORB;
limited with Org.Omg.CORBA.Portable.InputStream;
limited with Org.Omg.CORBA.Portable.OutputStream;
limited with Org.Omg.CORBA.Portable.ResponseHandler;
limited with Org.Omg.CosNaming.NamingContextExt;
limited with Org.Omg.PortableServer.POA;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.InvokeHandler;
with Org.Omg.CosNaming.NamingContextExtOperations;
with Org.Omg.PortableServer.Servant;

package Org.Omg.CosNaming.NamingContextExtPOA is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(InvokeHandler_I : Org.Omg.CORBA.Portable.InvokeHandler.Ref;
            NamingContextExtOperations_I : Org.Omg.CosNaming.NamingContextExtOperations.Ref)
    is abstract new Org.Omg.PortableServer.Servant.Typ
      with null record;

   function New_NamingContextExtPOA (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function U_Invoke (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_InputStream : access Standard.Org.Omg.CORBA.Portable.InputStream.Typ'Class;
                      P3_ResponseHandler : access Standard.Org.Omg.CORBA.Portable.ResponseHandler.Typ'Class)
                      return access Org.Omg.CORBA.Portable.OutputStream.Typ'Class;

   function U_All_interfaces (This : access Typ;
                              P1_POA : access Standard.Org.Omg.PortableServer.POA.Typ'Class;
                              P2_Byte_Arr : Java.Byte_Arr)
                              return Standard.Java.Lang.Object.Ref;

   function U_This (This : access Typ)
                    return access Org.Omg.CosNaming.NamingContextExt.Typ'Class;

   function U_This (This : access Typ;
                    P1_ORB : access Standard.Org.Omg.CORBA.ORB.Typ'Class)
                    return access Org.Omg.CosNaming.NamingContextExt.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NamingContextExtPOA);
   pragma Import (Java, U_Invoke, "_invoke");
   pragma Import (Java, U_All_interfaces, "_all_interfaces");
   pragma Import (Java, U_This, "_this");

end Org.Omg.CosNaming.NamingContextExtPOA;
pragma Import (Java, Org.Omg.CosNaming.NamingContextExtPOA, "org.omg.CosNaming.NamingContextExtPOA");
pragma Extensions_Allowed (Off);
