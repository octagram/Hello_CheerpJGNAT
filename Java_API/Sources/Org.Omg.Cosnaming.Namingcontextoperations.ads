pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.Object;
limited with Org.Omg.CosNaming.BindingIteratorHolder;
limited with Org.Omg.CosNaming.BindingListHolder;
limited with Org.Omg.CosNaming.NameComponent;
limited with Org.Omg.CosNaming.NamingContext;
with Java.Lang.Object;

package Org.Omg.CosNaming.NamingContextOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Bind (This : access Typ;
                   P1_NameComponent_Arr : access Org.Omg.CosNaming.NameComponent.Arr_Obj;
                   P2_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class) is abstract;
   --  can raise Org.Omg.CosNaming.NamingContextPackage.NotFound.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.CannotProceed.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.InvalidName.Except and
   --  Org.Omg.CosNaming.NamingContextPackage.AlreadyBound.Except

   procedure Bind_context (This : access Typ;
                           P1_NameComponent_Arr : access Org.Omg.CosNaming.NameComponent.Arr_Obj;
                           P2_NamingContext : access Standard.Org.Omg.CosNaming.NamingContext.Typ'Class) is abstract;
   --  can raise Org.Omg.CosNaming.NamingContextPackage.NotFound.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.CannotProceed.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.InvalidName.Except and
   --  Org.Omg.CosNaming.NamingContextPackage.AlreadyBound.Except

   procedure Rebind (This : access Typ;
                     P1_NameComponent_Arr : access Org.Omg.CosNaming.NameComponent.Arr_Obj;
                     P2_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class) is abstract;
   --  can raise Org.Omg.CosNaming.NamingContextPackage.NotFound.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.CannotProceed.Except and
   --  Org.Omg.CosNaming.NamingContextPackage.InvalidName.Except

   procedure Rebind_context (This : access Typ;
                             P1_NameComponent_Arr : access Org.Omg.CosNaming.NameComponent.Arr_Obj;
                             P2_NamingContext : access Standard.Org.Omg.CosNaming.NamingContext.Typ'Class) is abstract;
   --  can raise Org.Omg.CosNaming.NamingContextPackage.NotFound.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.CannotProceed.Except and
   --  Org.Omg.CosNaming.NamingContextPackage.InvalidName.Except

   function Resolve (This : access Typ;
                     P1_NameComponent_Arr : access Org.Omg.CosNaming.NameComponent.Arr_Obj)
                     return access Org.Omg.CORBA.Object.Typ'Class is abstract;
   --  can raise Org.Omg.CosNaming.NamingContextPackage.NotFound.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.CannotProceed.Except and
   --  Org.Omg.CosNaming.NamingContextPackage.InvalidName.Except

   procedure Unbind (This : access Typ;
                     P1_NameComponent_Arr : access Org.Omg.CosNaming.NameComponent.Arr_Obj) is abstract;
   --  can raise Org.Omg.CosNaming.NamingContextPackage.NotFound.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.CannotProceed.Except and
   --  Org.Omg.CosNaming.NamingContextPackage.InvalidName.Except

   procedure List (This : access Typ;
                   P1_Int : Java.Int;
                   P2_BindingListHolder : access Standard.Org.Omg.CosNaming.BindingListHolder.Typ'Class;
                   P3_BindingIteratorHolder : access Standard.Org.Omg.CosNaming.BindingIteratorHolder.Typ'Class) is abstract;

   function New_context (This : access Typ)
                         return access Org.Omg.CosNaming.NamingContext.Typ'Class is abstract;

   function Bind_new_context (This : access Typ;
                              P1_NameComponent_Arr : access Org.Omg.CosNaming.NameComponent.Arr_Obj)
                              return access Org.Omg.CosNaming.NamingContext.Typ'Class is abstract;
   --  can raise Org.Omg.CosNaming.NamingContextPackage.NotFound.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.AlreadyBound.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.CannotProceed.Except and
   --  Org.Omg.CosNaming.NamingContextPackage.InvalidName.Except

   procedure Destroy (This : access Typ) is abstract;
   --  can raise Org.Omg.CosNaming.NamingContextPackage.NotEmpty.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Bind, "bind");
   pragma Export (Java, Bind_context, "bind_context");
   pragma Export (Java, Rebind, "rebind");
   pragma Export (Java, Rebind_context, "rebind_context");
   pragma Export (Java, Resolve, "resolve");
   pragma Export (Java, Unbind, "unbind");
   pragma Export (Java, List, "list");
   pragma Export (Java, New_context, "new_context");
   pragma Export (Java, Bind_new_context, "bind_new_context");
   pragma Export (Java, Destroy, "destroy");

end Org.Omg.CosNaming.NamingContextOperations;
pragma Import (Java, Org.Omg.CosNaming.NamingContextOperations, "org.omg.CosNaming.NamingContextOperations");
pragma Extensions_Allowed (Off);
