pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CosNaming.NameComponent;
limited with Org.Omg.CosNaming.NamingContext;
with Java.Io.Serializable;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;
with Org.Omg.CORBA.UserException;

package Org.Omg.CosNaming.NamingContextPackage.CannotProceed is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Org.Omg.CORBA.UserException.Typ(Serializable_I,
                                           IDLEntity_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Cxt : access Org.Omg.CosNaming.NamingContext.Typ'Class;
      pragma Import (Java, Cxt, "cxt");

      Rest_of_name : Standard.Java.Lang.Object.Ref;
      pragma Import (Java, Rest_of_name, "rest_of_name");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_CannotProceed (This : Ref := null)
                               return Ref;

   function New_CannotProceed (P1_NamingContext : access Standard.Org.Omg.CosNaming.NamingContext.Typ'Class;
                               P2_NameComponent_Arr : access Org.Omg.CosNaming.NameComponent.Arr_Obj; 
                               This : Ref := null)
                               return Ref;

   function New_CannotProceed (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_NamingContext : access Standard.Org.Omg.CosNaming.NamingContext.Typ'Class;
                               P3_NameComponent_Arr : access Org.Omg.CosNaming.NameComponent.Arr_Obj; 
                               This : Ref := null)
                               return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "org.omg.CosNaming.NamingContextPackage.CannotProceed");
   pragma Java_Constructor (New_CannotProceed);

end Org.Omg.CosNaming.NamingContextPackage.CannotProceed;
pragma Import (Java, Org.Omg.CosNaming.NamingContextPackage.CannotProceed, "org.omg.CosNaming.NamingContextPackage.CannotProceed");
pragma Extensions_Allowed (Off);
