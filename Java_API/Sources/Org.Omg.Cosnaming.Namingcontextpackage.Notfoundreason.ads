pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.CosNaming.NamingContextPackage.NotFoundReason is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Value (This : access Typ)
                   return Java.Int;

   function From_int (P1_Int : Java.Int)
                      return access Org.Omg.CosNaming.NamingContextPackage.NotFoundReason.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_NotFoundReason (P1_Int : Java.Int; 
                                This : Ref := null)
                                return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   U_Missing_node : constant Java.Int;

   --  final
   Missing_node : access Org.Omg.CosNaming.NamingContextPackage.NotFoundReason.Typ'Class;

   --  final
   U_Not_context : constant Java.Int;

   --  final
   Not_context : access Org.Omg.CosNaming.NamingContextPackage.NotFoundReason.Typ'Class;

   --  final
   U_Not_object : constant Java.Int;

   --  final
   Not_object : access Org.Omg.CosNaming.NamingContextPackage.NotFoundReason.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Value, "value");
   pragma Import (Java, From_int, "from_int");
   pragma Java_Constructor (New_NotFoundReason);
   pragma Import (Java, U_Missing_node, "_missing_node");
   pragma Import (Java, Missing_node, "missing_node");
   pragma Import (Java, U_Not_context, "_not_context");
   pragma Import (Java, Not_context, "not_context");
   pragma Import (Java, U_Not_object, "_not_object");
   pragma Import (Java, Not_object, "not_object");

end Org.Omg.CosNaming.NamingContextPackage.NotFoundReason;
pragma Import (Java, Org.Omg.CosNaming.NamingContextPackage.NotFoundReason, "org.omg.CosNaming.NamingContextPackage.NotFoundReason");
pragma Extensions_Allowed (Off);
