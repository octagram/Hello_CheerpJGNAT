pragma Extensions_Allowed (On);
package Org.Omg.CosNaming.NamingContextPackage is
   pragma Preelaborate;
end Org.Omg.CosNaming.NamingContextPackage;
pragma Import (Java, Org.Omg.CosNaming.NamingContextPackage, "org.omg.CosNaming.NamingContextPackage");
pragma Extensions_Allowed (Off);
