pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CosNaming.BindingHolder;
limited with Org.Omg.CosNaming.BindingListHolder;
with Java.Lang.Object;
with Org.Omg.CORBA.Object;
with Org.Omg.CORBA.Portable.ObjectImpl;
with Org.Omg.CosNaming.BindingIterator;

package Org.Omg.CosNaming.U_BindingIteratorStub is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Object_I : Org.Omg.CORBA.Object.Ref;
            BindingIterator_I : Org.Omg.CosNaming.BindingIterator.Ref)
    is new Org.Omg.CORBA.Portable.ObjectImpl.Typ(Object_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_U_BindingIteratorStub (This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Next_one (This : access Typ;
                      P1_BindingHolder : access Standard.Org.Omg.CosNaming.BindingHolder.Typ'Class)
                      return Java.Boolean;

   function Next_n (This : access Typ;
                    P1_Int : Java.Int;
                    P2_BindingListHolder : access Standard.Org.Omg.CosNaming.BindingListHolder.Typ'Class)
                    return Java.Boolean;

   procedure Destroy (This : access Typ);

   function U_Ids (This : access Typ)
                   return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_U_BindingIteratorStub);
   pragma Import (Java, Next_one, "next_one");
   pragma Import (Java, Next_n, "next_n");
   pragma Import (Java, Destroy, "destroy");
   pragma Import (Java, U_Ids, "_ids");

end Org.Omg.CosNaming.U_BindingIteratorStub;
pragma Import (Java, Org.Omg.CosNaming.U_BindingIteratorStub, "org.omg.CosNaming._BindingIteratorStub");
pragma Extensions_Allowed (Off);
