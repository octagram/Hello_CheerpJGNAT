pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.ServerRequest;
with Java.Lang.Object;
with Org.Omg.CORBA.DynamicImplementation;
with Org.Omg.CORBA.Object;
with Org.Omg.CosNaming.NamingContext;

package Org.Omg.CosNaming.U_NamingContextImplBase is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Object_I : Org.Omg.CORBA.Object.Ref;
            NamingContext_I : Org.Omg.CosNaming.NamingContext.Ref)
    is abstract new Org.Omg.CORBA.DynamicImplementation.Typ(Object_I)
      with null record;

   function New_U_NamingContextImplBase (This : Ref := null)
                                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function U_Ids (This : access Typ)
                   return Standard.Java.Lang.Object.Ref;

   procedure Invoke (This : access Typ;
                     P1_ServerRequest : access Standard.Org.Omg.CORBA.ServerRequest.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_U_NamingContextImplBase);
   pragma Import (Java, U_Ids, "_ids");
   pragma Import (Java, Invoke, "invoke");

end Org.Omg.CosNaming.U_NamingContextImplBase;
pragma Import (Java, Org.Omg.CosNaming.U_NamingContextImplBase, "org.omg.CosNaming._NamingContextImplBase");
pragma Extensions_Allowed (Off);
