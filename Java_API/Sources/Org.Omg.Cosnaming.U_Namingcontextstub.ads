pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CosNaming.BindingIteratorHolder;
limited with Org.Omg.CosNaming.BindingListHolder;
limited with Org.Omg.CosNaming.NameComponent;
with Java.Lang.Object;
with Org.Omg.CORBA.Object;
with Org.Omg.CORBA.Portable.ObjectImpl;
with Org.Omg.CosNaming.NamingContext;

package Org.Omg.CosNaming.U_NamingContextStub is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Object_I : Org.Omg.CORBA.Object.Ref;
            NamingContext_I : Org.Omg.CosNaming.NamingContext.Ref)
    is new Org.Omg.CORBA.Portable.ObjectImpl.Typ(Object_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_U_NamingContextStub (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Bind (This : access Typ;
                   P1_NameComponent_Arr : access Org.Omg.CosNaming.NameComponent.Arr_Obj;
                   P2_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class);
   --  can raise Org.Omg.CosNaming.NamingContextPackage.NotFound.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.CannotProceed.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.InvalidName.Except and
   --  Org.Omg.CosNaming.NamingContextPackage.AlreadyBound.Except

   procedure Bind_context (This : access Typ;
                           P1_NameComponent_Arr : access Org.Omg.CosNaming.NameComponent.Arr_Obj;
                           P2_NamingContext : access Standard.Org.Omg.CosNaming.NamingContext.Typ'Class);
   --  can raise Org.Omg.CosNaming.NamingContextPackage.NotFound.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.CannotProceed.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.InvalidName.Except and
   --  Org.Omg.CosNaming.NamingContextPackage.AlreadyBound.Except

   procedure Rebind (This : access Typ;
                     P1_NameComponent_Arr : access Org.Omg.CosNaming.NameComponent.Arr_Obj;
                     P2_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class);
   --  can raise Org.Omg.CosNaming.NamingContextPackage.NotFound.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.CannotProceed.Except and
   --  Org.Omg.CosNaming.NamingContextPackage.InvalidName.Except

   procedure Rebind_context (This : access Typ;
                             P1_NameComponent_Arr : access Org.Omg.CosNaming.NameComponent.Arr_Obj;
                             P2_NamingContext : access Standard.Org.Omg.CosNaming.NamingContext.Typ'Class);
   --  can raise Org.Omg.CosNaming.NamingContextPackage.NotFound.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.CannotProceed.Except and
   --  Org.Omg.CosNaming.NamingContextPackage.InvalidName.Except

   function Resolve (This : access Typ;
                     P1_NameComponent_Arr : access Org.Omg.CosNaming.NameComponent.Arr_Obj)
                     return access Org.Omg.CORBA.Object.Typ'Class;
   --  can raise Org.Omg.CosNaming.NamingContextPackage.NotFound.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.CannotProceed.Except and
   --  Org.Omg.CosNaming.NamingContextPackage.InvalidName.Except

   procedure Unbind (This : access Typ;
                     P1_NameComponent_Arr : access Org.Omg.CosNaming.NameComponent.Arr_Obj);
   --  can raise Org.Omg.CosNaming.NamingContextPackage.NotFound.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.CannotProceed.Except and
   --  Org.Omg.CosNaming.NamingContextPackage.InvalidName.Except

   procedure List (This : access Typ;
                   P1_Int : Java.Int;
                   P2_BindingListHolder : access Standard.Org.Omg.CosNaming.BindingListHolder.Typ'Class;
                   P3_BindingIteratorHolder : access Standard.Org.Omg.CosNaming.BindingIteratorHolder.Typ'Class);

   function New_context (This : access Typ)
                         return access Org.Omg.CosNaming.NamingContext.Typ'Class;

   function Bind_new_context (This : access Typ;
                              P1_NameComponent_Arr : access Org.Omg.CosNaming.NameComponent.Arr_Obj)
                              return access Org.Omg.CosNaming.NamingContext.Typ'Class;
   --  can raise Org.Omg.CosNaming.NamingContextPackage.NotFound.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.AlreadyBound.Except,
   --  Org.Omg.CosNaming.NamingContextPackage.CannotProceed.Except and
   --  Org.Omg.CosNaming.NamingContextPackage.InvalidName.Except

   procedure Destroy (This : access Typ);
   --  can raise Org.Omg.CosNaming.NamingContextPackage.NotEmpty.Except

   function U_Ids (This : access Typ)
                   return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_U_NamingContextStub);
   pragma Import (Java, Bind, "bind");
   pragma Import (Java, Bind_context, "bind_context");
   pragma Import (Java, Rebind, "rebind");
   pragma Import (Java, Rebind_context, "rebind_context");
   pragma Import (Java, Resolve, "resolve");
   pragma Import (Java, Unbind, "unbind");
   pragma Import (Java, List, "list");
   pragma Import (Java, New_context, "new_context");
   pragma Import (Java, Bind_new_context, "bind_new_context");
   pragma Import (Java, Destroy, "destroy");
   pragma Import (Java, U_Ids, "_ids");

end Org.Omg.CosNaming.U_NamingContextStub;
pragma Import (Java, Org.Omg.CosNaming.U_NamingContextStub, "org.omg.CosNaming._NamingContextStub");
pragma Extensions_Allowed (Off);
