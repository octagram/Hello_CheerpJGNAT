pragma Extensions_Allowed (On);
package Org.Omg.CosNaming is
   pragma Preelaborate;
end Org.Omg.CosNaming;
pragma Import (Java, Org.Omg.CosNaming, "org.omg.CosNaming");
pragma Extensions_Allowed (Off);
