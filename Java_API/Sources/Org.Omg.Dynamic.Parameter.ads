pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.ParameterMode;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.Dynamic.Parameter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Argument : access Org.Omg.CORBA.Any.Typ'Class;
      pragma Import (Java, Argument, "argument");

      Mode : access Org.Omg.CORBA.ParameterMode.Typ'Class;
      pragma Import (Java, Mode, "mode");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Parameter (This : Ref := null)
                           return Ref;

   function New_Parameter (P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class;
                           P2_ParameterMode : access Standard.Org.Omg.CORBA.ParameterMode.Typ'Class; 
                           This : Ref := null)
                           return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Parameter);

end Org.Omg.Dynamic.Parameter;
pragma Import (Java, Org.Omg.Dynamic.Parameter, "org.omg.Dynamic.Parameter");
pragma Extensions_Allowed (Off);
