pragma Extensions_Allowed (On);
package Org.Omg.Dynamic is
   pragma Preelaborate;
end Org.Omg.Dynamic;
pragma Import (Java, Org.Omg.Dynamic, "org.omg.Dynamic");
pragma Extensions_Allowed (Off);
