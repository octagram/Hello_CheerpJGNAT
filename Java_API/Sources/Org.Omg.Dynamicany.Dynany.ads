pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Object;
with Org.Omg.CORBA.Portable.IDLEntity;
with Org.Omg.DynamicAny.DynAnyOperations;

package Org.Omg.DynamicAny.DynAny is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Object_I : Org.Omg.CORBA.Object.Ref;
            IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref;
            DynAnyOperations_I : Org.Omg.DynamicAny.DynAnyOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   
private
   pragma Convention (Java, Typ);

end Org.Omg.DynamicAny.DynAny;
pragma Import (Java, Org.Omg.DynamicAny.DynAny, "org.omg.DynamicAny.DynAny");
pragma Extensions_Allowed (Off);
