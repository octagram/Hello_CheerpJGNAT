pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.TypeCode;
limited with Org.Omg.DynamicAny.DynAny;
with Java.Lang.Object;

package Org.Omg.DynamicAny.DynAnyFactoryOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Create_dyn_any (This : access Typ;
                            P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class)
                            return access Org.Omg.DynamicAny.DynAny.Typ'Class is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyFactoryPackage.InconsistentTypeCode.Except

   function Create_dyn_any_from_type_code (This : access Typ;
                                           P1_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class)
                                           return access Org.Omg.DynamicAny.DynAny.Typ'Class is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyFactoryPackage.InconsistentTypeCode.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Create_dyn_any, "create_dyn_any");
   pragma Export (Java, Create_dyn_any_from_type_code, "create_dyn_any_from_type_code");

end Org.Omg.DynamicAny.DynAnyFactoryOperations;
pragma Import (Java, Org.Omg.DynamicAny.DynAnyFactoryOperations, "org.omg.DynamicAny.DynAnyFactoryOperations");
pragma Extensions_Allowed (Off);
