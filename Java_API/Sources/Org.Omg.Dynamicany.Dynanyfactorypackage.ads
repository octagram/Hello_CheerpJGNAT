pragma Extensions_Allowed (On);
package Org.Omg.DynamicAny.DynAnyFactoryPackage is
   pragma Preelaborate;
end Org.Omg.DynamicAny.DynAnyFactoryPackage;
pragma Import (Java, Org.Omg.DynamicAny.DynAnyFactoryPackage, "org.omg.DynamicAny.DynAnyFactoryPackage");
pragma Extensions_Allowed (Off);
