pragma Extensions_Allowed (On);
limited with Java.Io.Serializable;
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.Object;
limited with Org.Omg.CORBA.TypeCode;
limited with Org.Omg.DynamicAny.DynAny;
with Java.Lang.Object;

package Org.Omg.DynamicAny.DynAnyOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function type_K (This : access Typ)
                    return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;

   procedure Assign (This : access Typ;
                     P1_DynAny : access Standard.Org.Omg.DynamicAny.DynAny.Typ'Class) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except

   procedure From_any (This : access Typ;
                       P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function To_any (This : access Typ)
                    return access Org.Omg.CORBA.Any.Typ'Class is abstract;

   function Equal (This : access Typ;
                   P1_DynAny : access Standard.Org.Omg.DynamicAny.DynAny.Typ'Class)
                   return Java.Boolean is abstract;

   procedure Destroy (This : access Typ) is abstract;

   function Copy (This : access Typ)
                  return access Org.Omg.DynamicAny.DynAny.Typ'Class is abstract;

   procedure Insert_boolean (This : access Typ;
                             P1_Boolean : Java.Boolean) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_octet (This : access Typ;
                           P1_Byte : Java.Byte) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_char (This : access Typ;
                          P1_Char : Java.Char) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_short (This : access Typ;
                           P1_Short : Java.Short) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_ushort (This : access Typ;
                            P1_Short : Java.Short) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_long (This : access Typ;
                          P1_Int : Java.Int) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_ulong (This : access Typ;
                           P1_Int : Java.Int) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_float (This : access Typ;
                           P1_Float : Java.Float) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_double (This : access Typ;
                            P1_Double : Java.Double) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_string (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_reference (This : access Typ;
                               P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_typecode (This : access Typ;
                              P1_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_longlong (This : access Typ;
                              P1_Long : Java.Long) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_ulonglong (This : access Typ;
                               P1_Long : Java.Long) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_wchar (This : access Typ;
                           P1_Char : Java.Char) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_wstring (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_any (This : access Typ;
                         P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_dyn_any (This : access Typ;
                             P1_DynAny : access Standard.Org.Omg.DynamicAny.DynAny.Typ'Class) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_val (This : access Typ;
                         P1_Serializable : access Standard.Java.Io.Serializable.Typ'Class) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_boolean (This : access Typ)
                         return Java.Boolean is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_octet (This : access Typ)
                       return Java.Byte is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_char (This : access Typ)
                      return Java.Char is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_short (This : access Typ)
                       return Java.Short is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_ushort (This : access Typ)
                        return Java.Short is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_long (This : access Typ)
                      return Java.Int is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_ulong (This : access Typ)
                       return Java.Int is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_float (This : access Typ)
                       return Java.Float is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_double (This : access Typ)
                        return Java.Double is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_string (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_reference (This : access Typ)
                           return access Org.Omg.CORBA.Object.Typ'Class is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_typecode (This : access Typ)
                          return access Org.Omg.CORBA.TypeCode.Typ'Class is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_longlong (This : access Typ)
                          return Java.Long is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_ulonglong (This : access Typ)
                           return Java.Long is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_wchar (This : access Typ)
                       return Java.Char is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_wstring (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_any (This : access Typ)
                     return access Org.Omg.CORBA.Any.Typ'Class is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_dyn_any (This : access Typ)
                         return access Org.Omg.DynamicAny.DynAny.Typ'Class is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_val (This : access Typ)
                     return access Java.Io.Serializable.Typ'Class is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Seek (This : access Typ;
                  P1_Int : Java.Int)
                  return Java.Boolean is abstract;

   procedure Rewind (This : access Typ) is abstract;

   function Next (This : access Typ)
                  return Java.Boolean is abstract;

   function Component_count (This : access Typ)
                             return Java.Int is abstract;

   function Current_component (This : access Typ)
                               return access Org.Omg.DynamicAny.DynAny.Typ'Class is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, type_K, "type");
   pragma Export (Java, Assign, "assign");
   pragma Export (Java, From_any, "from_any");
   pragma Export (Java, To_any, "to_any");
   pragma Export (Java, Equal, "equal");
   pragma Export (Java, Destroy, "destroy");
   pragma Export (Java, Copy, "copy");
   pragma Export (Java, Insert_boolean, "insert_boolean");
   pragma Export (Java, Insert_octet, "insert_octet");
   pragma Export (Java, Insert_char, "insert_char");
   pragma Export (Java, Insert_short, "insert_short");
   pragma Export (Java, Insert_ushort, "insert_ushort");
   pragma Export (Java, Insert_long, "insert_long");
   pragma Export (Java, Insert_ulong, "insert_ulong");
   pragma Export (Java, Insert_float, "insert_float");
   pragma Export (Java, Insert_double, "insert_double");
   pragma Export (Java, Insert_string, "insert_string");
   pragma Export (Java, Insert_reference, "insert_reference");
   pragma Export (Java, Insert_typecode, "insert_typecode");
   pragma Export (Java, Insert_longlong, "insert_longlong");
   pragma Export (Java, Insert_ulonglong, "insert_ulonglong");
   pragma Export (Java, Insert_wchar, "insert_wchar");
   pragma Export (Java, Insert_wstring, "insert_wstring");
   pragma Export (Java, Insert_any, "insert_any");
   pragma Export (Java, Insert_dyn_any, "insert_dyn_any");
   pragma Export (Java, Insert_val, "insert_val");
   pragma Export (Java, Get_boolean, "get_boolean");
   pragma Export (Java, Get_octet, "get_octet");
   pragma Export (Java, Get_char, "get_char");
   pragma Export (Java, Get_short, "get_short");
   pragma Export (Java, Get_ushort, "get_ushort");
   pragma Export (Java, Get_long, "get_long");
   pragma Export (Java, Get_ulong, "get_ulong");
   pragma Export (Java, Get_float, "get_float");
   pragma Export (Java, Get_double, "get_double");
   pragma Export (Java, Get_string, "get_string");
   pragma Export (Java, Get_reference, "get_reference");
   pragma Export (Java, Get_typecode, "get_typecode");
   pragma Export (Java, Get_longlong, "get_longlong");
   pragma Export (Java, Get_ulonglong, "get_ulonglong");
   pragma Export (Java, Get_wchar, "get_wchar");
   pragma Export (Java, Get_wstring, "get_wstring");
   pragma Export (Java, Get_any, "get_any");
   pragma Export (Java, Get_dyn_any, "get_dyn_any");
   pragma Export (Java, Get_val, "get_val");
   pragma Export (Java, Seek, "seek");
   pragma Export (Java, Rewind, "rewind");
   pragma Export (Java, Next, "next");
   pragma Export (Java, Component_count, "component_count");
   pragma Export (Java, Current_component, "current_component");

end Org.Omg.DynamicAny.DynAnyOperations;
pragma Import (Java, Org.Omg.DynamicAny.DynAnyOperations, "org.omg.DynamicAny.DynAnyOperations");
pragma Extensions_Allowed (Off);
