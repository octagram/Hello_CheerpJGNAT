pragma Extensions_Allowed (On);
package Org.Omg.DynamicAny.DynAnyPackage is
   pragma Preelaborate;
end Org.Omg.DynamicAny.DynAnyPackage;
pragma Import (Java, Org.Omg.DynamicAny.DynAnyPackage, "org.omg.DynamicAny.DynAnyPackage");
pragma Extensions_Allowed (Off);
