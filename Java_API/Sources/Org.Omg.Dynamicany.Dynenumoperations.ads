pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.Omg.DynamicAny.DynAnyOperations;

package Org.Omg.DynamicAny.DynEnumOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DynAnyOperations_I : Org.Omg.DynamicAny.DynAnyOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get_as_string (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure Set_as_string (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_as_ulong (This : access Typ)
                          return Java.Int is abstract;

   procedure Set_as_ulong (This : access Typ;
                           P1_Int : Java.Int) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Get_as_string, "get_as_string");
   pragma Export (Java, Set_as_string, "set_as_string");
   pragma Export (Java, Get_as_ulong, "get_as_ulong");
   pragma Export (Java, Set_as_ulong, "set_as_ulong");

end Org.Omg.DynamicAny.DynEnumOperations;
pragma Import (Java, Org.Omg.DynamicAny.DynEnumOperations, "org.omg.DynamicAny.DynEnumOperations");
pragma Extensions_Allowed (Off);
