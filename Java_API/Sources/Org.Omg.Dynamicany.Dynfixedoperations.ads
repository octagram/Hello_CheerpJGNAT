pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.Omg.DynamicAny.DynAnyOperations;

package Org.Omg.DynamicAny.DynFixedOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DynAnyOperations_I : Org.Omg.DynamicAny.DynAnyOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get_value (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   function Set_value (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return Java.Boolean is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Get_value, "get_value");
   pragma Export (Java, Set_value, "set_value");

end Org.Omg.DynamicAny.DynFixedOperations;
pragma Import (Java, Org.Omg.DynamicAny.DynFixedOperations, "org.omg.DynamicAny.DynFixedOperations");
pragma Extensions_Allowed (Off);
