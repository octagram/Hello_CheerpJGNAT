pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.DynamicAny.DynAny;
with Java.Lang.Object;
with Org.Omg.DynamicAny.DynAnyOperations;

package Org.Omg.DynamicAny.DynSequenceOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DynAnyOperations_I : Org.Omg.DynamicAny.DynAnyOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get_length (This : access Typ)
                        return Java.Int is abstract;

   procedure Set_length (This : access Typ;
                         P1_Int : Java.Int) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_elements (This : access Typ)
                          return Standard.Java.Lang.Object.Ref is abstract;

   procedure Set_elements (This : access Typ;
                           P1_Any_Arr : access Org.Omg.CORBA.Any.Arr_Obj) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_elements_as_dyn_any (This : access Typ)
                                     return Standard.Java.Lang.Object.Ref is abstract;

   procedure Set_elements_as_dyn_any (This : access Typ;
                                      P1_DynAny_Arr : access Org.Omg.DynamicAny.DynAny.Arr_Obj) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Get_length, "get_length");
   pragma Export (Java, Set_length, "set_length");
   pragma Export (Java, Get_elements, "get_elements");
   pragma Export (Java, Set_elements, "set_elements");
   pragma Export (Java, Get_elements_as_dyn_any, "get_elements_as_dyn_any");
   pragma Export (Java, Set_elements_as_dyn_any, "set_elements_as_dyn_any");

end Org.Omg.DynamicAny.DynSequenceOperations;
pragma Import (Java, Org.Omg.DynamicAny.DynSequenceOperations, "org.omg.DynamicAny.DynSequenceOperations");
pragma Extensions_Allowed (Off);
