pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.TCKind;
limited with Org.Omg.DynamicAny.DynAny;
with Java.Lang.Object;
with Org.Omg.DynamicAny.DynAnyOperations;

package Org.Omg.DynamicAny.DynUnionOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DynAnyOperations_I : Org.Omg.DynamicAny.DynAnyOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get_discriminator (This : access Typ)
                               return access Org.Omg.DynamicAny.DynAny.Typ'Class is abstract;

   procedure Set_discriminator (This : access Typ;
                                P1_DynAny : access Standard.Org.Omg.DynamicAny.DynAny.Typ'Class) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except

   procedure Set_to_default_member (This : access Typ) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except

   procedure Set_to_no_active_member (This : access Typ) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except

   function Has_no_active_member (This : access Typ)
                                  return Java.Boolean is abstract;

   function Discriminator_kind (This : access Typ)
                                return access Org.Omg.CORBA.TCKind.Typ'Class is abstract;

   function Member_kind (This : access Typ)
                         return access Org.Omg.CORBA.TCKind.Typ'Class is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Member (This : access Typ)
                    return access Org.Omg.DynamicAny.DynAny.Typ'Class is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Member_name (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Get_discriminator, "get_discriminator");
   pragma Export (Java, Set_discriminator, "set_discriminator");
   pragma Export (Java, Set_to_default_member, "set_to_default_member");
   pragma Export (Java, Set_to_no_active_member, "set_to_no_active_member");
   pragma Export (Java, Has_no_active_member, "has_no_active_member");
   pragma Export (Java, Discriminator_kind, "discriminator_kind");
   pragma Export (Java, Member_kind, "member_kind");
   pragma Export (Java, Member, "member");
   pragma Export (Java, Member_name, "member_name");

end Org.Omg.DynamicAny.DynUnionOperations;
pragma Import (Java, Org.Omg.DynamicAny.DynUnionOperations, "org.omg.DynamicAny.DynUnionOperations");
pragma Extensions_Allowed (Off);
