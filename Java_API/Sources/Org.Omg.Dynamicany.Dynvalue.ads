pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;
with Org.Omg.DynamicAny.DynValueCommon;
with Org.Omg.DynamicAny.DynValueOperations;

package Org.Omg.DynamicAny.DynValue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref;
            DynValueCommon_I : Org.Omg.DynamicAny.DynValueCommon.Ref;
            DynValueOperations_I : Org.Omg.DynamicAny.DynValueOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   
private
   pragma Convention (Java, Typ);

end Org.Omg.DynamicAny.DynValue;
pragma Import (Java, Org.Omg.DynamicAny.DynValue, "org.omg.DynamicAny.DynValue");
pragma Extensions_Allowed (Off);
