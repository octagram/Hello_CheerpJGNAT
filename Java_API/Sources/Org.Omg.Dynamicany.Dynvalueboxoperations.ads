pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.DynamicAny.DynAny;
with Java.Lang.Object;
with Org.Omg.DynamicAny.DynValueCommonOperations;

package Org.Omg.DynamicAny.DynValueBoxOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DynValueCommonOperations_I : Org.Omg.DynamicAny.DynValueCommonOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get_boxed_value (This : access Typ)
                             return access Org.Omg.CORBA.Any.Typ'Class is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Set_boxed_value (This : access Typ;
                              P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except

   function Get_boxed_value_as_dyn_any (This : access Typ)
                                        return access Org.Omg.DynamicAny.DynAny.Typ'Class is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Set_boxed_value_as_dyn_any (This : access Typ;
                                         P1_DynAny : access Standard.Org.Omg.DynamicAny.DynAny.Typ'Class) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Get_boxed_value, "get_boxed_value");
   pragma Export (Java, Set_boxed_value, "set_boxed_value");
   pragma Export (Java, Get_boxed_value_as_dyn_any, "get_boxed_value_as_dyn_any");
   pragma Export (Java, Set_boxed_value_as_dyn_any, "set_boxed_value_as_dyn_any");

end Org.Omg.DynamicAny.DynValueBoxOperations;
pragma Import (Java, Org.Omg.DynamicAny.DynValueBoxOperations, "org.omg.DynamicAny.DynValueBoxOperations");
pragma Extensions_Allowed (Off);
