pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.TCKind;
limited with Org.Omg.DynamicAny.NameDynAnyPair;
limited with Org.Omg.DynamicAny.NameValuePair;
with Java.Lang.Object;
with Org.Omg.DynamicAny.DynValueCommonOperations;

package Org.Omg.DynamicAny.DynValueOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DynValueCommonOperations_I : Org.Omg.DynamicAny.DynValueCommonOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Current_member_name (This : access Typ)
                                 return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Current_member_kind (This : access Typ)
                                 return access Org.Omg.CORBA.TCKind.Typ'Class is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_members (This : access Typ)
                         return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Set_members (This : access Typ;
                          P1_NameValuePair_Arr : access Org.Omg.DynamicAny.NameValuePair.Arr_Obj) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_members_as_dyn_any (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Set_members_as_dyn_any (This : access Typ;
                                     P1_NameDynAnyPair_Arr : access Org.Omg.DynamicAny.NameDynAnyPair.Arr_Obj) is abstract;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Current_member_name, "current_member_name");
   pragma Export (Java, Current_member_kind, "current_member_kind");
   pragma Export (Java, Get_members, "get_members");
   pragma Export (Java, Set_members, "set_members");
   pragma Export (Java, Get_members_as_dyn_any, "get_members_as_dyn_any");
   pragma Export (Java, Set_members_as_dyn_any, "set_members_as_dyn_any");

end Org.Omg.DynamicAny.DynValueOperations;
pragma Import (Java, Org.Omg.DynamicAny.DynValueOperations, "org.omg.DynamicAny.DynValueOperations");
pragma Extensions_Allowed (Off);
