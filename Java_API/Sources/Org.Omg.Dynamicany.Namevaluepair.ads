pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Any;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.DynamicAny.NameValuePair is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Id : access Java.Lang.String.Typ'Class;
      pragma Import (Java, Id, "id");

      Value : access Org.Omg.CORBA.Any.Typ'Class;
      pragma Import (Java, Value, "value");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NameValuePair (This : Ref := null)
                               return Ref;

   function New_NameValuePair (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class; 
                               This : Ref := null)
                               return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NameValuePair);

end Org.Omg.DynamicAny.NameValuePair;
pragma Import (Java, Org.Omg.DynamicAny.NameValuePair, "org.omg.DynamicAny.NameValuePair");
pragma Extensions_Allowed (Off);
