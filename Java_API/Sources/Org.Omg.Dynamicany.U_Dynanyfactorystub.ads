pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.TypeCode;
limited with Org.Omg.DynamicAny.DynAny;
with Java.Lang.Object;
with Org.Omg.CORBA.Object;
with Org.Omg.CORBA.Portable.ObjectImpl;
with Org.Omg.DynamicAny.DynAnyFactory;

package Org.Omg.DynamicAny.U_DynAnyFactoryStub is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Object_I : Org.Omg.CORBA.Object.Ref;
            DynAnyFactory_I : Org.Omg.DynamicAny.DynAnyFactory.Ref)
    is new Org.Omg.CORBA.Portable.ObjectImpl.Typ(Object_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_U_DynAnyFactoryStub (This : Ref := null)
                                     return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Create_dyn_any (This : access Typ;
                            P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class)
                            return access Org.Omg.DynamicAny.DynAny.Typ'Class;
   --  can raise Org.Omg.DynamicAny.DynAnyFactoryPackage.InconsistentTypeCode.Except

   function Create_dyn_any_from_type_code (This : access Typ;
                                           P1_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class)
                                           return access Org.Omg.DynamicAny.DynAny.Typ'Class;
   --  can raise Org.Omg.DynamicAny.DynAnyFactoryPackage.InconsistentTypeCode.Except

   function U_Ids (This : access Typ)
                   return Standard.Java.Lang.Object.Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   U_OpsClass : access Java.Lang.Class.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_U_DynAnyFactoryStub);
   pragma Import (Java, Create_dyn_any, "create_dyn_any");
   pragma Import (Java, Create_dyn_any_from_type_code, "create_dyn_any_from_type_code");
   pragma Import (Java, U_Ids, "_ids");
   pragma Import (Java, U_OpsClass, "_opsClass");

end Org.Omg.DynamicAny.U_DynAnyFactoryStub;
pragma Import (Java, Org.Omg.DynamicAny.U_DynAnyFactoryStub, "org.omg.DynamicAny._DynAnyFactoryStub");
pragma Extensions_Allowed (Off);
