pragma Extensions_Allowed (On);
limited with Java.Io.Serializable;
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.TCKind;
limited with Org.Omg.CORBA.TypeCode;
limited with Org.Omg.DynamicAny.DynAny;
limited with Org.Omg.DynamicAny.NameDynAnyPair;
limited with Org.Omg.DynamicAny.NameValuePair;
with Java.Lang.Object;
with Org.Omg.CORBA.Object;
with Org.Omg.CORBA.Portable.ObjectImpl;
with Org.Omg.DynamicAny.DynValue;

package Org.Omg.DynamicAny.U_DynValueStub is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Object_I : Org.Omg.CORBA.Object.Ref;
            DynValue_I : Org.Omg.DynamicAny.DynValue.Ref)
    is new Org.Omg.CORBA.Portable.ObjectImpl.Typ(Object_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_U_DynValueStub (This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Current_member_name (This : access Typ)
                                 return access Java.Lang.String.Typ'Class;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Current_member_kind (This : access Typ)
                                 return access Org.Omg.CORBA.TCKind.Typ'Class;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_members (This : access Typ)
                         return Standard.Java.Lang.Object.Ref;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Set_members (This : access Typ;
                          P1_NameValuePair_Arr : access Org.Omg.DynamicAny.NameValuePair.Arr_Obj);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_members_as_dyn_any (This : access Typ)
                                    return Standard.Java.Lang.Object.Ref;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Set_members_as_dyn_any (This : access Typ;
                                     P1_NameDynAnyPair_Arr : access Org.Omg.DynamicAny.NameDynAnyPair.Arr_Obj);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Is_null (This : access Typ)
                     return Java.Boolean;

   procedure Set_to_null (This : access Typ);

   procedure Set_to_value (This : access Typ);

   function type_K (This : access Typ)
                    return access Org.Omg.CORBA.TypeCode.Typ'Class;

   procedure Assign (This : access Typ;
                     P1_DynAny : access Standard.Org.Omg.DynamicAny.DynAny.Typ'Class);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except

   procedure From_any (This : access Typ;
                       P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function To_any (This : access Typ)
                    return access Org.Omg.CORBA.Any.Typ'Class;

   function Equal (This : access Typ;
                   P1_DynAny : access Standard.Org.Omg.DynamicAny.DynAny.Typ'Class)
                   return Java.Boolean;

   procedure Destroy (This : access Typ);

   function Copy (This : access Typ)
                  return access Org.Omg.DynamicAny.DynAny.Typ'Class;

   procedure Insert_boolean (This : access Typ;
                             P1_Boolean : Java.Boolean);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_octet (This : access Typ;
                           P1_Byte : Java.Byte);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_char (This : access Typ;
                          P1_Char : Java.Char);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_short (This : access Typ;
                           P1_Short : Java.Short);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_ushort (This : access Typ;
                            P1_Short : Java.Short);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_long (This : access Typ;
                          P1_Int : Java.Int);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_ulong (This : access Typ;
                           P1_Int : Java.Int);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_float (This : access Typ;
                           P1_Float : Java.Float);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_double (This : access Typ;
                            P1_Double : Java.Double);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_string (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_reference (This : access Typ;
                               P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_typecode (This : access Typ;
                              P1_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_longlong (This : access Typ;
                              P1_Long : Java.Long);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_ulonglong (This : access Typ;
                               P1_Long : Java.Long);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_wchar (This : access Typ;
                           P1_Char : Java.Char);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_wstring (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_any (This : access Typ;
                         P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_dyn_any (This : access Typ;
                             P1_DynAny : access Standard.Org.Omg.DynamicAny.DynAny.Typ'Class);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   procedure Insert_val (This : access Typ;
                         P1_Serializable : access Standard.Java.Io.Serializable.Typ'Class);
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_boolean (This : access Typ)
                         return Java.Boolean;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_octet (This : access Typ)
                       return Java.Byte;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_char (This : access Typ)
                      return Java.Char;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_short (This : access Typ)
                       return Java.Short;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_ushort (This : access Typ)
                        return Java.Short;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_long (This : access Typ)
                      return Java.Int;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_ulong (This : access Typ)
                       return Java.Int;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_float (This : access Typ)
                       return Java.Float;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_double (This : access Typ)
                        return Java.Double;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_string (This : access Typ)
                        return access Java.Lang.String.Typ'Class;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_reference (This : access Typ)
                           return access Org.Omg.CORBA.Object.Typ'Class;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_typecode (This : access Typ)
                          return access Org.Omg.CORBA.TypeCode.Typ'Class;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_longlong (This : access Typ)
                          return Java.Long;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_ulonglong (This : access Typ)
                           return Java.Long;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_wchar (This : access Typ)
                       return Java.Char;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_wstring (This : access Typ)
                         return access Java.Lang.String.Typ'Class;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_any (This : access Typ)
                     return access Org.Omg.CORBA.Any.Typ'Class;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_dyn_any (This : access Typ)
                         return access Org.Omg.DynamicAny.DynAny.Typ'Class;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Get_val (This : access Typ)
                     return access Java.Io.Serializable.Typ'Class;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except and
   --  Org.Omg.DynamicAny.DynAnyPackage.InvalidValue.Except

   function Seek (This : access Typ;
                  P1_Int : Java.Int)
                  return Java.Boolean;

   procedure Rewind (This : access Typ);

   function Next (This : access Typ)
                  return Java.Boolean;

   function Component_count (This : access Typ)
                             return Java.Int;

   function Current_component (This : access Typ)
                               return access Org.Omg.DynamicAny.DynAny.Typ'Class;
   --  can raise Org.Omg.DynamicAny.DynAnyPackage.TypeMismatch.Except

   function U_Ids (This : access Typ)
                   return Standard.Java.Lang.Object.Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   U_OpsClass : access Java.Lang.Class.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_U_DynValueStub);
   pragma Import (Java, Current_member_name, "current_member_name");
   pragma Import (Java, Current_member_kind, "current_member_kind");
   pragma Import (Java, Get_members, "get_members");
   pragma Import (Java, Set_members, "set_members");
   pragma Import (Java, Get_members_as_dyn_any, "get_members_as_dyn_any");
   pragma Import (Java, Set_members_as_dyn_any, "set_members_as_dyn_any");
   pragma Import (Java, Is_null, "is_null");
   pragma Import (Java, Set_to_null, "set_to_null");
   pragma Import (Java, Set_to_value, "set_to_value");
   pragma Import (Java, type_K, "type");
   pragma Import (Java, Assign, "assign");
   pragma Import (Java, From_any, "from_any");
   pragma Import (Java, To_any, "to_any");
   pragma Import (Java, Equal, "equal");
   pragma Import (Java, Destroy, "destroy");
   pragma Import (Java, Copy, "copy");
   pragma Import (Java, Insert_boolean, "insert_boolean");
   pragma Import (Java, Insert_octet, "insert_octet");
   pragma Import (Java, Insert_char, "insert_char");
   pragma Import (Java, Insert_short, "insert_short");
   pragma Import (Java, Insert_ushort, "insert_ushort");
   pragma Import (Java, Insert_long, "insert_long");
   pragma Import (Java, Insert_ulong, "insert_ulong");
   pragma Import (Java, Insert_float, "insert_float");
   pragma Import (Java, Insert_double, "insert_double");
   pragma Import (Java, Insert_string, "insert_string");
   pragma Import (Java, Insert_reference, "insert_reference");
   pragma Import (Java, Insert_typecode, "insert_typecode");
   pragma Import (Java, Insert_longlong, "insert_longlong");
   pragma Import (Java, Insert_ulonglong, "insert_ulonglong");
   pragma Import (Java, Insert_wchar, "insert_wchar");
   pragma Import (Java, Insert_wstring, "insert_wstring");
   pragma Import (Java, Insert_any, "insert_any");
   pragma Import (Java, Insert_dyn_any, "insert_dyn_any");
   pragma Import (Java, Insert_val, "insert_val");
   pragma Import (Java, Get_boolean, "get_boolean");
   pragma Import (Java, Get_octet, "get_octet");
   pragma Import (Java, Get_char, "get_char");
   pragma Import (Java, Get_short, "get_short");
   pragma Import (Java, Get_ushort, "get_ushort");
   pragma Import (Java, Get_long, "get_long");
   pragma Import (Java, Get_ulong, "get_ulong");
   pragma Import (Java, Get_float, "get_float");
   pragma Import (Java, Get_double, "get_double");
   pragma Import (Java, Get_string, "get_string");
   pragma Import (Java, Get_reference, "get_reference");
   pragma Import (Java, Get_typecode, "get_typecode");
   pragma Import (Java, Get_longlong, "get_longlong");
   pragma Import (Java, Get_ulonglong, "get_ulonglong");
   pragma Import (Java, Get_wchar, "get_wchar");
   pragma Import (Java, Get_wstring, "get_wstring");
   pragma Import (Java, Get_any, "get_any");
   pragma Import (Java, Get_dyn_any, "get_dyn_any");
   pragma Import (Java, Get_val, "get_val");
   pragma Import (Java, Seek, "seek");
   pragma Import (Java, Rewind, "rewind");
   pragma Import (Java, Next, "next");
   pragma Import (Java, Component_count, "component_count");
   pragma Import (Java, Current_component, "current_component");
   pragma Import (Java, U_Ids, "_ids");
   pragma Import (Java, U_OpsClass, "_opsClass");

end Org.Omg.DynamicAny.U_DynValueStub;
pragma Import (Java, Org.Omg.DynamicAny.U_DynValueStub, "org.omg.DynamicAny._DynValueStub");
pragma Extensions_Allowed (Off);
