pragma Extensions_Allowed (On);
package Org.Omg.DynamicAny is
   pragma Preelaborate;
end Org.Omg.DynamicAny;
pragma Import (Java, Org.Omg.DynamicAny, "org.omg.DynamicAny");
pragma Extensions_Allowed (Off);
