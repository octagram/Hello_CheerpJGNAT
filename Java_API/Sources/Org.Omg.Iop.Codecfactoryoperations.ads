pragma Extensions_Allowed (On);
limited with Org.Omg.IOP.Codec;
limited with Org.Omg.IOP.Encoding;
with Java.Lang.Object;

package Org.Omg.IOP.CodecFactoryOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Create_codec (This : access Typ;
                          P1_Encoding : access Standard.Org.Omg.IOP.Encoding.Typ'Class)
                          return access Org.Omg.IOP.Codec.Typ'Class is abstract;
   --  can raise Org.Omg.IOP.CodecFactoryPackage.UnknownEncoding.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Create_codec, "create_codec");

end Org.Omg.IOP.CodecFactoryOperations;
pragma Import (Java, Org.Omg.IOP.CodecFactoryOperations, "org.omg.IOP.CodecFactoryOperations");
pragma Extensions_Allowed (Off);
