pragma Extensions_Allowed (On);
package Org.Omg.IOP.CodecFactoryPackage is
   pragma Preelaborate;
end Org.Omg.IOP.CodecFactoryPackage;
pragma Import (Java, Org.Omg.IOP.CodecFactoryPackage, "org.omg.IOP.CodecFactoryPackage");
pragma Extensions_Allowed (Off);
