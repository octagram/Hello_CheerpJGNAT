pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.TypeCode;
with Java.Lang.Object;

package Org.Omg.IOP.CodecOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Encode (This : access Typ;
                    P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class)
                    return Java.Byte_Arr is abstract;
   --  can raise Org.Omg.IOP.CodecPackage.InvalidTypeForEncoding.Except

   function Decode (This : access Typ;
                    P1_Byte_Arr : Java.Byte_Arr)
                    return access Org.Omg.CORBA.Any.Typ'Class is abstract;
   --  can raise Org.Omg.IOP.CodecPackage.FormatMismatch.Except

   function Encode_value (This : access Typ;
                          P1_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class)
                          return Java.Byte_Arr is abstract;
   --  can raise Org.Omg.IOP.CodecPackage.InvalidTypeForEncoding.Except

   function Decode_value (This : access Typ;
                          P1_Byte_Arr : Java.Byte_Arr;
                          P2_TypeCode : access Standard.Org.Omg.CORBA.TypeCode.Typ'Class)
                          return access Org.Omg.CORBA.Any.Typ'Class is abstract;
   --  can raise Org.Omg.IOP.CodecPackage.FormatMismatch.Except and
   --  Org.Omg.IOP.CodecPackage.TypeMismatch.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Encode, "encode");
   pragma Export (Java, Decode, "decode");
   pragma Export (Java, Encode_value, "encode_value");
   pragma Export (Java, Decode_value, "decode_value");

end Org.Omg.IOP.CodecOperations;
pragma Import (Java, Org.Omg.IOP.CodecOperations, "org.omg.IOP.CodecOperations");
pragma Extensions_Allowed (Off);
