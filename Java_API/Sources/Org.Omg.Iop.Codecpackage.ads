pragma Extensions_Allowed (On);
package Org.Omg.IOP.CodecPackage is
   pragma Preelaborate;
end Org.Omg.IOP.CodecPackage;
pragma Import (Java, Org.Omg.IOP.CodecPackage, "org.omg.IOP.CodecPackage");
pragma Extensions_Allowed (Off);
