pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.IOP.Encoding is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Format : Java.Short;
      pragma Import (Java, Format, "format");

      Major_version : Java.Byte;
      pragma Import (Java, Major_version, "major_version");

      Minor_version : Java.Byte;
      pragma Import (Java, Minor_version, "minor_version");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Encoding (This : Ref := null)
                          return Ref;

   function New_Encoding (P1_Short : Java.Short;
                          P2_Byte : Java.Byte;
                          P3_Byte : Java.Byte; 
                          This : Ref := null)
                          return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Encoding);

end Org.Omg.IOP.Encoding;
pragma Import (Java, Org.Omg.IOP.Encoding, "org.omg.IOP.Encoding");
pragma Extensions_Allowed (Off);
