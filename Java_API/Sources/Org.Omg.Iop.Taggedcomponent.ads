pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.IOP.TaggedComponent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Tag : Java.Int;
      pragma Import (Java, Tag, "tag");

      Component_data : Java.Byte_Arr;
      pragma Import (Java, Component_data, "component_data");

   end record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_TaggedComponent (This : Ref := null)
                                 return Ref;

   function New_TaggedComponent (P1_Int : Java.Int;
                                 P2_Byte_Arr : Java.Byte_Arr; 
                                 This : Ref := null)
                                 return Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_TaggedComponent);

end Org.Omg.IOP.TaggedComponent;
pragma Import (Java, Org.Omg.IOP.TaggedComponent, "org.omg.IOP.TaggedComponent");
pragma Extensions_Allowed (Off);
