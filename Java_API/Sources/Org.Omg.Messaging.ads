pragma Extensions_Allowed (On);
package Org.Omg.Messaging is
   pragma Preelaborate;
end Org.Omg.Messaging;
pragma Import (Java, Org.Omg.Messaging, "org.omg.Messaging");
pragma Extensions_Allowed (Off);
