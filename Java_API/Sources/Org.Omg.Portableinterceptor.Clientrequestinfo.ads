pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;
with Org.Omg.PortableInterceptor.ClientRequestInfoOperations;
with Org.Omg.PortableInterceptor.RequestInfo;

package Org.Omg.PortableInterceptor.ClientRequestInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref;
            ClientRequestInfoOperations_I : Org.Omg.PortableInterceptor.ClientRequestInfoOperations.Ref;
            RequestInfo_I : Org.Omg.PortableInterceptor.RequestInfo.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   
private
   pragma Convention (Java, Typ);

end Org.Omg.PortableInterceptor.ClientRequestInfo;
pragma Import (Java, Org.Omg.PortableInterceptor.ClientRequestInfo, "org.omg.PortableInterceptor.ClientRequestInfo");
pragma Extensions_Allowed (Off);
