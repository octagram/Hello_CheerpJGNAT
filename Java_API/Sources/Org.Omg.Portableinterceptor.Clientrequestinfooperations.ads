pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.Object;
limited with Org.Omg.CORBA.Policy;
limited with Org.Omg.IOP.ServiceContext;
limited with Org.Omg.IOP.TaggedComponent;
limited with Org.Omg.IOP.TaggedProfile;
with Java.Lang.Object;
with Org.Omg.PortableInterceptor.RequestInfoOperations;

package Org.Omg.PortableInterceptor.ClientRequestInfoOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            RequestInfoOperations_I : Org.Omg.PortableInterceptor.RequestInfoOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Target (This : access Typ)
                    return access Org.Omg.CORBA.Object.Typ'Class is abstract;

   function Effective_target (This : access Typ)
                              return access Org.Omg.CORBA.Object.Typ'Class is abstract;

   function Effective_profile (This : access Typ)
                               return access Org.Omg.IOP.TaggedProfile.Typ'Class is abstract;

   function Received_exception (This : access Typ)
                                return access Org.Omg.CORBA.Any.Typ'Class is abstract;

   function Received_exception_id (This : access Typ)
                                   return access Java.Lang.String.Typ'Class is abstract;

   function Get_effective_component (This : access Typ;
                                     P1_Int : Java.Int)
                                     return access Org.Omg.IOP.TaggedComponent.Typ'Class is abstract;

   function Get_effective_components (This : access Typ;
                                      P1_Int : Java.Int)
                                      return Standard.Java.Lang.Object.Ref is abstract;

   function Get_request_policy (This : access Typ;
                                P1_Int : Java.Int)
                                return access Org.Omg.CORBA.Policy.Typ'Class is abstract;

   procedure Add_request_service_context (This : access Typ;
                                          P1_ServiceContext : access Standard.Org.Omg.IOP.ServiceContext.Typ'Class;
                                          P2_Boolean : Java.Boolean) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Target, "target");
   pragma Export (Java, Effective_target, "effective_target");
   pragma Export (Java, Effective_profile, "effective_profile");
   pragma Export (Java, Received_exception, "received_exception");
   pragma Export (Java, Received_exception_id, "received_exception_id");
   pragma Export (Java, Get_effective_component, "get_effective_component");
   pragma Export (Java, Get_effective_components, "get_effective_components");
   pragma Export (Java, Get_request_policy, "get_request_policy");
   pragma Export (Java, Add_request_service_context, "add_request_service_context");

end Org.Omg.PortableInterceptor.ClientRequestInfoOperations;
pragma Import (Java, Org.Omg.PortableInterceptor.ClientRequestInfoOperations, "org.omg.PortableInterceptor.ClientRequestInfoOperations");
pragma Extensions_Allowed (Off);
