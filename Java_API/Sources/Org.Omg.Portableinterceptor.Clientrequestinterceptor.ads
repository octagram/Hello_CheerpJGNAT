pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;
with Org.Omg.PortableInterceptor.ClientRequestInterceptorOperations;
with Org.Omg.PortableInterceptor.Interceptor;

package Org.Omg.PortableInterceptor.ClientRequestInterceptor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref;
            ClientRequestInterceptorOperations_I : Org.Omg.PortableInterceptor.ClientRequestInterceptorOperations.Ref;
            Interceptor_I : Org.Omg.PortableInterceptor.Interceptor.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   
private
   pragma Convention (Java, Typ);

end Org.Omg.PortableInterceptor.ClientRequestInterceptor;
pragma Import (Java, Org.Omg.PortableInterceptor.ClientRequestInterceptor, "org.omg.PortableInterceptor.ClientRequestInterceptor");
pragma Extensions_Allowed (Off);
