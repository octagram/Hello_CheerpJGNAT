pragma Extensions_Allowed (On);
limited with Org.Omg.PortableInterceptor.ClientRequestInfo;
with Java.Lang.Object;
with Org.Omg.PortableInterceptor.InterceptorOperations;

package Org.Omg.PortableInterceptor.ClientRequestInterceptorOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            InterceptorOperations_I : Org.Omg.PortableInterceptor.InterceptorOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Send_request (This : access Typ;
                           P1_ClientRequestInfo : access Standard.Org.Omg.PortableInterceptor.ClientRequestInfo.Typ'Class) is abstract;
   --  can raise Org.Omg.PortableInterceptor.ForwardRequest.Except

   procedure Send_poll (This : access Typ;
                        P1_ClientRequestInfo : access Standard.Org.Omg.PortableInterceptor.ClientRequestInfo.Typ'Class) is abstract;

   procedure Receive_reply (This : access Typ;
                            P1_ClientRequestInfo : access Standard.Org.Omg.PortableInterceptor.ClientRequestInfo.Typ'Class) is abstract;

   procedure Receive_exception (This : access Typ;
                                P1_ClientRequestInfo : access Standard.Org.Omg.PortableInterceptor.ClientRequestInfo.Typ'Class) is abstract;
   --  can raise Org.Omg.PortableInterceptor.ForwardRequest.Except

   procedure Receive_other (This : access Typ;
                            P1_ClientRequestInfo : access Standard.Org.Omg.PortableInterceptor.ClientRequestInfo.Typ'Class) is abstract;
   --  can raise Org.Omg.PortableInterceptor.ForwardRequest.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Send_request, "send_request");
   pragma Export (Java, Send_poll, "send_poll");
   pragma Export (Java, Receive_reply, "receive_reply");
   pragma Export (Java, Receive_exception, "receive_exception");
   pragma Export (Java, Receive_other, "receive_other");

end Org.Omg.PortableInterceptor.ClientRequestInterceptorOperations;
pragma Import (Java, Org.Omg.PortableInterceptor.ClientRequestInterceptorOperations, "org.omg.PortableInterceptor.ClientRequestInterceptorOperations");
pragma Extensions_Allowed (Off);
