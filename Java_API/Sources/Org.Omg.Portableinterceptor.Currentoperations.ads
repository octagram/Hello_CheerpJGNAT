pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.Any;
with Java.Lang.Object;
with Org.Omg.CORBA.CurrentOperations;

package Org.Omg.PortableInterceptor.CurrentOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            CurrentOperations_I : Org.Omg.CORBA.CurrentOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get_slot (This : access Typ;
                      P1_Int : Java.Int)
                      return access Org.Omg.CORBA.Any.Typ'Class is abstract;
   --  can raise Org.Omg.PortableInterceptor.InvalidSlot.Except

   procedure Set_slot (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class) is abstract;
   --  can raise Org.Omg.PortableInterceptor.InvalidSlot.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Get_slot, "get_slot");
   pragma Export (Java, Set_slot, "set_slot");

end Org.Omg.PortableInterceptor.CurrentOperations;
pragma Import (Java, Org.Omg.PortableInterceptor.CurrentOperations, "org.omg.PortableInterceptor.CurrentOperations");
pragma Extensions_Allowed (Off);
