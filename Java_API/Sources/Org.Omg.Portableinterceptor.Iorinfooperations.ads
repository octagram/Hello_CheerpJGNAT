pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.Policy;
limited with Org.Omg.IOP.TaggedComponent;
limited with Org.Omg.PortableInterceptor.ObjectReferenceFactory;
limited with Org.Omg.PortableInterceptor.ObjectReferenceTemplate;
with Java.Lang.Object;

package Org.Omg.PortableInterceptor.IORInfoOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get_effective_policy (This : access Typ;
                                  P1_Int : Java.Int)
                                  return access Org.Omg.CORBA.Policy.Typ'Class is abstract;

   procedure Add_ior_component (This : access Typ;
                                P1_TaggedComponent : access Standard.Org.Omg.IOP.TaggedComponent.Typ'Class) is abstract;

   procedure Add_ior_component_to_profile (This : access Typ;
                                           P1_TaggedComponent : access Standard.Org.Omg.IOP.TaggedComponent.Typ'Class;
                                           P2_Int : Java.Int) is abstract;

   function Manager_id (This : access Typ)
                        return Java.Int is abstract;

   function State (This : access Typ)
                   return Java.Short is abstract;

   function Adapter_template (This : access Typ)
                              return access Org.Omg.PortableInterceptor.ObjectReferenceTemplate.Typ'Class is abstract;

   function Current_factory (This : access Typ)
                             return access Org.Omg.PortableInterceptor.ObjectReferenceFactory.Typ'Class is abstract;

   procedure Current_factory (This : access Typ;
                              P1_ObjectReferenceFactory : access Standard.Org.Omg.PortableInterceptor.ObjectReferenceFactory.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Get_effective_policy, "get_effective_policy");
   pragma Export (Java, Add_ior_component, "add_ior_component");
   pragma Export (Java, Add_ior_component_to_profile, "add_ior_component_to_profile");
   pragma Export (Java, Manager_id, "manager_id");
   pragma Export (Java, State, "state");
   pragma Export (Java, Adapter_template, "adapter_template");
   pragma Export (Java, Current_factory, "current_factory");

end Org.Omg.PortableInterceptor.IORInfoOperations;
pragma Import (Java, Org.Omg.PortableInterceptor.IORInfoOperations, "org.omg.PortableInterceptor.IORInfoOperations");
pragma Extensions_Allowed (Off);
