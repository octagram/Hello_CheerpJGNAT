pragma Extensions_Allowed (On);
limited with Org.Omg.PortableInterceptor.IORInfo;
limited with Org.Omg.PortableInterceptor.ObjectReferenceTemplate;
with Java.Lang.Object;
with Org.Omg.PortableInterceptor.IORInterceptorOperations;

package Org.Omg.PortableInterceptor.IORInterceptor_3_0Operations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            IORInterceptorOperations_I : Org.Omg.PortableInterceptor.IORInterceptorOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Components_established (This : access Typ;
                                     P1_IORInfo : access Standard.Org.Omg.PortableInterceptor.IORInfo.Typ'Class) is abstract;

   procedure Adapter_manager_state_changed (This : access Typ;
                                            P1_Int : Java.Int;
                                            P2_Short : Java.Short) is abstract;

   procedure Adapter_state_changed (This : access Typ;
                                    P1_ObjectReferenceTemplate_Arr : access Org.Omg.PortableInterceptor.ObjectReferenceTemplate.Arr_Obj;
                                    P2_Short : Java.Short) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Components_established, "components_established");
   pragma Export (Java, Adapter_manager_state_changed, "adapter_manager_state_changed");
   pragma Export (Java, Adapter_state_changed, "adapter_state_changed");

end Org.Omg.PortableInterceptor.IORInterceptor_3_0Operations;
pragma Import (Java, Org.Omg.PortableInterceptor.IORInterceptor_3_0Operations, "org.omg.PortableInterceptor.IORInterceptor_3_0Operations");
pragma Extensions_Allowed (Off);
