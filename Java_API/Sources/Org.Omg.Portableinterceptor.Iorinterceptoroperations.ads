pragma Extensions_Allowed (On);
limited with Org.Omg.PortableInterceptor.IORInfo;
with Java.Lang.Object;
with Org.Omg.PortableInterceptor.InterceptorOperations;

package Org.Omg.PortableInterceptor.IORInterceptorOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            InterceptorOperations_I : Org.Omg.PortableInterceptor.InterceptorOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Establish_components (This : access Typ;
                                   P1_IORInfo : access Standard.Org.Omg.PortableInterceptor.IORInfo.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Establish_components, "establish_components");

end Org.Omg.PortableInterceptor.IORInterceptorOperations;
pragma Import (Java, Org.Omg.PortableInterceptor.IORInterceptorOperations, "org.omg.PortableInterceptor.IORInterceptorOperations");
pragma Extensions_Allowed (Off);
