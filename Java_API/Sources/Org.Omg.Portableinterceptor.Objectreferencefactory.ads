pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Object;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.ValueBase;

package Org.Omg.PortableInterceptor.ObjectReferenceFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ValueBase_I : Org.Omg.CORBA.Portable.ValueBase.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Make_object (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Byte_Arr : Java.Byte_Arr)
                         return access Org.Omg.CORBA.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Make_object, "make_object");

end Org.Omg.PortableInterceptor.ObjectReferenceFactory;
pragma Import (Java, Org.Omg.PortableInterceptor.ObjectReferenceFactory, "org.omg.PortableInterceptor.ObjectReferenceFactory");
pragma Extensions_Allowed (Off);
