pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.Omg.PortableInterceptor.ObjectReferenceFactory;

package Org.Omg.PortableInterceptor.ObjectReferenceTemplate is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ObjectReferenceFactory_I : Org.Omg.PortableInterceptor.ObjectReferenceFactory.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Server_id (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   function Orb_id (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   function Adapter_name (This : access Typ)
                          return Standard.Java.Lang.Object.Ref is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Server_id, "server_id");
   pragma Export (Java, Orb_id, "orb_id");
   pragma Export (Java, Adapter_name, "adapter_name");

end Org.Omg.PortableInterceptor.ObjectReferenceTemplate;
pragma Import (Java, Org.Omg.PortableInterceptor.ObjectReferenceTemplate, "org.omg.PortableInterceptor.ObjectReferenceTemplate");
pragma Extensions_Allowed (Off);
