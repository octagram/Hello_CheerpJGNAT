pragma Extensions_Allowed (On);
limited with Org.Omg.PortableInterceptor.ORBInitInfo;
with Java.Lang.Object;

package Org.Omg.PortableInterceptor.ORBInitializerOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Pre_init (This : access Typ;
                       P1_ORBInitInfo : access Standard.Org.Omg.PortableInterceptor.ORBInitInfo.Typ'Class) is abstract;

   procedure Post_init (This : access Typ;
                        P1_ORBInitInfo : access Standard.Org.Omg.PortableInterceptor.ORBInitInfo.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Pre_init, "pre_init");
   pragma Export (Java, Post_init, "post_init");

end Org.Omg.PortableInterceptor.ORBInitializerOperations;
pragma Import (Java, Org.Omg.PortableInterceptor.ORBInitializerOperations, "org.omg.PortableInterceptor.ORBInitializerOperations");
pragma Extensions_Allowed (Off);
