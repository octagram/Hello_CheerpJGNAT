pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Object;
limited with Org.Omg.IOP.CodecFactory;
limited with Org.Omg.PortableInterceptor.ClientRequestInterceptor;
limited with Org.Omg.PortableInterceptor.IORInterceptor;
limited with Org.Omg.PortableInterceptor.PolicyFactory;
limited with Org.Omg.PortableInterceptor.ServerRequestInterceptor;
with Java.Lang.Object;

package Org.Omg.PortableInterceptor.ORBInitInfoOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Arguments (This : access Typ)
                       return Standard.Java.Lang.Object.Ref is abstract;

   function Orb_id (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   function Codec_factory (This : access Typ)
                           return access Org.Omg.IOP.CodecFactory.Typ'Class is abstract;

   procedure Register_initial_reference (This : access Typ;
                                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class) is abstract;
   --  can raise Org.Omg.PortableInterceptor.ORBInitInfoPackage.InvalidName.Except

   function Resolve_initial_references (This : access Typ;
                                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                                        return access Org.Omg.CORBA.Object.Typ'Class is abstract;
   --  can raise Org.Omg.PortableInterceptor.ORBInitInfoPackage.InvalidName.Except

   procedure Add_client_request_interceptor (This : access Typ;
                                             P1_ClientRequestInterceptor : access Standard.Org.Omg.PortableInterceptor.ClientRequestInterceptor.Typ'Class) is abstract;
   --  can raise Org.Omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName.Except

   procedure Add_server_request_interceptor (This : access Typ;
                                             P1_ServerRequestInterceptor : access Standard.Org.Omg.PortableInterceptor.ServerRequestInterceptor.Typ'Class) is abstract;
   --  can raise Org.Omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName.Except

   procedure Add_ior_interceptor (This : access Typ;
                                  P1_IORInterceptor : access Standard.Org.Omg.PortableInterceptor.IORInterceptor.Typ'Class) is abstract;
   --  can raise Org.Omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName.Except

   function Allocate_slot_id (This : access Typ)
                              return Java.Int is abstract;

   procedure Register_policy_factory (This : access Typ;
                                      P1_Int : Java.Int;
                                      P2_PolicyFactory : access Standard.Org.Omg.PortableInterceptor.PolicyFactory.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Arguments, "arguments");
   pragma Export (Java, Orb_id, "orb_id");
   pragma Export (Java, Codec_factory, "codec_factory");
   pragma Export (Java, Register_initial_reference, "register_initial_reference");
   pragma Export (Java, Resolve_initial_references, "resolve_initial_references");
   pragma Export (Java, Add_client_request_interceptor, "add_client_request_interceptor");
   pragma Export (Java, Add_server_request_interceptor, "add_server_request_interceptor");
   pragma Export (Java, Add_ior_interceptor, "add_ior_interceptor");
   pragma Export (Java, Allocate_slot_id, "allocate_slot_id");
   pragma Export (Java, Register_policy_factory, "register_policy_factory");

end Org.Omg.PortableInterceptor.ORBInitInfoOperations;
pragma Import (Java, Org.Omg.PortableInterceptor.ORBInitInfoOperations, "org.omg.PortableInterceptor.ORBInitInfoOperations");
pragma Extensions_Allowed (Off);
