pragma Extensions_Allowed (On);
package Org.Omg.PortableInterceptor.ORBInitInfoPackage is
   pragma Preelaborate;
end Org.Omg.PortableInterceptor.ORBInitInfoPackage;
pragma Import (Java, Org.Omg.PortableInterceptor.ORBInitInfoPackage, "org.omg.PortableInterceptor.ORBInitInfoPackage");
pragma Extensions_Allowed (Off);
