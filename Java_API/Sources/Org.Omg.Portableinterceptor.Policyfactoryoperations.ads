pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.Policy;
with Java.Lang.Object;

package Org.Omg.PortableInterceptor.PolicyFactoryOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Create_policy (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class)
                           return access Org.Omg.CORBA.Policy.Typ'Class is abstract;
   --  can raise Org.Omg.CORBA.PolicyError.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Create_policy, "create_policy");

end Org.Omg.PortableInterceptor.PolicyFactoryOperations;
pragma Import (Java, Org.Omg.PortableInterceptor.PolicyFactoryOperations, "org.omg.PortableInterceptor.PolicyFactoryOperations");
pragma Extensions_Allowed (Off);
