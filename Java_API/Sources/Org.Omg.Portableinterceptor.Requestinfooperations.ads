pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.Object;
limited with Org.Omg.CORBA.TypeCode;
limited with Org.Omg.Dynamic.Parameter;
limited with Org.Omg.IOP.ServiceContext;
with Java.Lang.Object;

package Org.Omg.PortableInterceptor.RequestInfoOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Request_id (This : access Typ)
                        return Java.Int is abstract;

   function Operation (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   function Arguments (This : access Typ)
                       return Standard.Java.Lang.Object.Ref is abstract;

   function Exceptions (This : access Typ)
                        return Standard.Java.Lang.Object.Ref is abstract;

   function Contexts (This : access Typ)
                      return Standard.Java.Lang.Object.Ref is abstract;

   function Operation_context (This : access Typ)
                               return Standard.Java.Lang.Object.Ref is abstract;

   function Result (This : access Typ)
                    return access Org.Omg.CORBA.Any.Typ'Class is abstract;

   function Response_expected (This : access Typ)
                               return Java.Boolean is abstract;

   function Sync_scope (This : access Typ)
                        return Java.Short is abstract;

   function Reply_status (This : access Typ)
                          return Java.Short is abstract;

   function Forward_reference (This : access Typ)
                               return access Org.Omg.CORBA.Object.Typ'Class is abstract;

   function Get_slot (This : access Typ;
                      P1_Int : Java.Int)
                      return access Org.Omg.CORBA.Any.Typ'Class is abstract;
   --  can raise Org.Omg.PortableInterceptor.InvalidSlot.Except

   function Get_request_service_context (This : access Typ;
                                         P1_Int : Java.Int)
                                         return access Org.Omg.IOP.ServiceContext.Typ'Class is abstract;

   function Get_reply_service_context (This : access Typ;
                                       P1_Int : Java.Int)
                                       return access Org.Omg.IOP.ServiceContext.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Request_id, "request_id");
   pragma Export (Java, Operation, "operation");
   pragma Export (Java, Arguments, "arguments");
   pragma Export (Java, Exceptions, "exceptions");
   pragma Export (Java, Contexts, "contexts");
   pragma Export (Java, Operation_context, "operation_context");
   pragma Export (Java, Result, "result");
   pragma Export (Java, Response_expected, "response_expected");
   pragma Export (Java, Sync_scope, "sync_scope");
   pragma Export (Java, Reply_status, "reply_status");
   pragma Export (Java, Forward_reference, "forward_reference");
   pragma Export (Java, Get_slot, "get_slot");
   pragma Export (Java, Get_request_service_context, "get_request_service_context");
   pragma Export (Java, Get_reply_service_context, "get_reply_service_context");

end Org.Omg.PortableInterceptor.RequestInfoOperations;
pragma Import (Java, Org.Omg.PortableInterceptor.RequestInfoOperations, "org.omg.PortableInterceptor.RequestInfoOperations");
pragma Extensions_Allowed (Off);
