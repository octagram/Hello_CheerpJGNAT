pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Any;
limited with Org.Omg.CORBA.Policy;
limited with Org.Omg.IOP.ServiceContext;
with Java.Lang.Object;
with Org.Omg.PortableInterceptor.RequestInfoOperations;

package Org.Omg.PortableInterceptor.ServerRequestInfoOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            RequestInfoOperations_I : Org.Omg.PortableInterceptor.RequestInfoOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Sending_exception (This : access Typ)
                               return access Org.Omg.CORBA.Any.Typ'Class is abstract;

   function Object_id (This : access Typ)
                       return Java.Byte_Arr is abstract;

   function Adapter_id (This : access Typ)
                        return Java.Byte_Arr is abstract;

   function Server_id (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   function Orb_id (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   function Adapter_name (This : access Typ)
                          return Standard.Java.Lang.Object.Ref is abstract;

   function Target_most_derived_interface (This : access Typ)
                                           return access Java.Lang.String.Typ'Class is abstract;

   function Get_server_policy (This : access Typ;
                               P1_Int : Java.Int)
                               return access Org.Omg.CORBA.Policy.Typ'Class is abstract;

   procedure Set_slot (This : access Typ;
                       P1_Int : Java.Int;
                       P2_Any : access Standard.Org.Omg.CORBA.Any.Typ'Class) is abstract;
   --  can raise Org.Omg.PortableInterceptor.InvalidSlot.Except

   function Target_is_a (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Boolean is abstract;

   procedure Add_reply_service_context (This : access Typ;
                                        P1_ServiceContext : access Standard.Org.Omg.IOP.ServiceContext.Typ'Class;
                                        P2_Boolean : Java.Boolean) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Sending_exception, "sending_exception");
   pragma Export (Java, Object_id, "object_id");
   pragma Export (Java, Adapter_id, "adapter_id");
   pragma Export (Java, Server_id, "server_id");
   pragma Export (Java, Orb_id, "orb_id");
   pragma Export (Java, Adapter_name, "adapter_name");
   pragma Export (Java, Target_most_derived_interface, "target_most_derived_interface");
   pragma Export (Java, Get_server_policy, "get_server_policy");
   pragma Export (Java, Set_slot, "set_slot");
   pragma Export (Java, Target_is_a, "target_is_a");
   pragma Export (Java, Add_reply_service_context, "add_reply_service_context");

end Org.Omg.PortableInterceptor.ServerRequestInfoOperations;
pragma Import (Java, Org.Omg.PortableInterceptor.ServerRequestInfoOperations, "org.omg.PortableInterceptor.ServerRequestInfoOperations");
pragma Extensions_Allowed (Off);
