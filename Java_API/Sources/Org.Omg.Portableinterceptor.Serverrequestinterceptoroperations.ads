pragma Extensions_Allowed (On);
limited with Org.Omg.PortableInterceptor.ServerRequestInfo;
with Java.Lang.Object;
with Org.Omg.PortableInterceptor.InterceptorOperations;

package Org.Omg.PortableInterceptor.ServerRequestInterceptorOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            InterceptorOperations_I : Org.Omg.PortableInterceptor.InterceptorOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Receive_request_service_contexts (This : access Typ;
                                               P1_ServerRequestInfo : access Standard.Org.Omg.PortableInterceptor.ServerRequestInfo.Typ'Class) is abstract;
   --  can raise Org.Omg.PortableInterceptor.ForwardRequest.Except

   procedure Receive_request (This : access Typ;
                              P1_ServerRequestInfo : access Standard.Org.Omg.PortableInterceptor.ServerRequestInfo.Typ'Class) is abstract;
   --  can raise Org.Omg.PortableInterceptor.ForwardRequest.Except

   procedure Send_reply (This : access Typ;
                         P1_ServerRequestInfo : access Standard.Org.Omg.PortableInterceptor.ServerRequestInfo.Typ'Class) is abstract;

   procedure Send_exception (This : access Typ;
                             P1_ServerRequestInfo : access Standard.Org.Omg.PortableInterceptor.ServerRequestInfo.Typ'Class) is abstract;
   --  can raise Org.Omg.PortableInterceptor.ForwardRequest.Except

   procedure Send_other (This : access Typ;
                         P1_ServerRequestInfo : access Standard.Org.Omg.PortableInterceptor.ServerRequestInfo.Typ'Class) is abstract;
   --  can raise Org.Omg.PortableInterceptor.ForwardRequest.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Receive_request_service_contexts, "receive_request_service_contexts");
   pragma Export (Java, Receive_request, "receive_request");
   pragma Export (Java, Send_reply, "send_reply");
   pragma Export (Java, Send_exception, "send_exception");
   pragma Export (Java, Send_other, "send_other");

end Org.Omg.PortableInterceptor.ServerRequestInterceptorOperations;
pragma Import (Java, Org.Omg.PortableInterceptor.ServerRequestInterceptorOperations, "org.omg.PortableInterceptor.ServerRequestInterceptorOperations");
pragma Extensions_Allowed (Off);
