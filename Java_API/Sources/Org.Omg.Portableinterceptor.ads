pragma Extensions_Allowed (On);
package Org.Omg.PortableInterceptor is
   pragma Preelaborate;
end Org.Omg.PortableInterceptor;
pragma Import (Java, Org.Omg.PortableInterceptor, "org.omg.PortableInterceptor");
pragma Extensions_Allowed (Off);
