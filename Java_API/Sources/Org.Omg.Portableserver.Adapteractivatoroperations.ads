pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.PortableServer.POA;
with Java.Lang.Object;

package Org.Omg.PortableServer.AdapterActivatorOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Unknown_adapter (This : access Typ;
                             P1_POA : access Standard.Org.Omg.PortableServer.POA.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class)
                             return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Unknown_adapter, "unknown_adapter");

end Org.Omg.PortableServer.AdapterActivatorOperations;
pragma Import (Java, Org.Omg.PortableServer.AdapterActivatorOperations, "org.omg.PortableServer.AdapterActivatorOperations");
pragma Extensions_Allowed (Off);
