pragma Extensions_Allowed (On);
limited with Org.Omg.PortableServer.POA;
with Java.Lang.Object;
with Org.Omg.CORBA.CurrentOperations;

package Org.Omg.PortableServer.CurrentOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            CurrentOperations_I : Org.Omg.CORBA.CurrentOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Get_POA (This : access Typ)
                     return access Org.Omg.PortableServer.POA.Typ'Class is abstract;
   --  can raise Org.Omg.PortableServer.CurrentPackage.NoContext.Except

   function Get_object_id (This : access Typ)
                           return Java.Byte_Arr is abstract;
   --  can raise Org.Omg.PortableServer.CurrentPackage.NoContext.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Get_POA, "get_POA");
   pragma Export (Java, Get_object_id, "get_object_id");

end Org.Omg.PortableServer.CurrentOperations;
pragma Import (Java, Org.Omg.PortableServer.CurrentOperations, "org.omg.PortableServer.CurrentOperations");
pragma Extensions_Allowed (Off);
