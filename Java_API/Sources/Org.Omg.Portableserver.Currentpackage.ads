pragma Extensions_Allowed (On);
package Org.Omg.PortableServer.CurrentPackage is
   pragma Preelaborate;
end Org.Omg.PortableServer.CurrentPackage;
pragma Import (Java, Org.Omg.PortableServer.CurrentPackage, "org.omg.PortableServer.CurrentPackage");
pragma Extensions_Allowed (Off);
