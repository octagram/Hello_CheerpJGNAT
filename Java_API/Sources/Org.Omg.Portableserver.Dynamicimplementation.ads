pragma Extensions_Allowed (On);
limited with Org.Omg.CORBA.ServerRequest;
with Java.Lang.Object;
with Org.Omg.PortableServer.Servant;

package Org.Omg.PortableServer.DynamicImplementation is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Org.Omg.PortableServer.Servant.Typ
      with null record;

   function New_DynamicImplementation (This : Ref := null)
                                       return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Invoke (This : access Typ;
                     P1_ServerRequest : access Standard.Org.Omg.CORBA.ServerRequest.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DynamicImplementation);
   pragma Export (Java, Invoke, "invoke");

end Org.Omg.PortableServer.DynamicImplementation;
pragma Import (Java, Org.Omg.PortableServer.DynamicImplementation, "org.omg.PortableServer.DynamicImplementation");
pragma Extensions_Allowed (Off);
