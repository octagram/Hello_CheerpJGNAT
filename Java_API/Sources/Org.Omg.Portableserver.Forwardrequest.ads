pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Object;
with Java.Io.Serializable;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;
with Org.Omg.CORBA.UserException;

package Org.Omg.PortableServer.ForwardRequest is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Org.Omg.CORBA.UserException.Typ(Serializable_I,
                                           IDLEntity_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Forward_reference : access Org.Omg.CORBA.Object.Typ'Class;
      pragma Import (Java, Forward_reference, "forward_reference");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ForwardRequest (This : Ref := null)
                                return Ref;

   function New_ForwardRequest (P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   function New_ForwardRequest (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class; 
                                This : Ref := null)
                                return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "org.omg.PortableServer.ForwardRequest");
   pragma Java_Constructor (New_ForwardRequest);

end Org.Omg.PortableServer.ForwardRequest;
pragma Import (Java, Org.Omg.PortableServer.ForwardRequest, "org.omg.PortableServer.ForwardRequest");
pragma Extensions_Allowed (Off);
