pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.PortableServer.IdAssignmentPolicyValue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Value (This : access Typ)
                   return Java.Int;

   function From_int (P1_Int : Java.Int)
                      return access Org.Omg.PortableServer.IdAssignmentPolicyValue.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_IdAssignmentPolicyValue (P1_Int : Java.Int; 
                                         This : Ref := null)
                                         return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   U_USER_ID : constant Java.Int;

   --  final
   USER_ID : access Org.Omg.PortableServer.IdAssignmentPolicyValue.Typ'Class;

   --  final
   U_SYSTEM_ID : constant Java.Int;

   --  final
   SYSTEM_ID : access Org.Omg.PortableServer.IdAssignmentPolicyValue.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Value, "value");
   pragma Import (Java, From_int, "from_int");
   pragma Java_Constructor (New_IdAssignmentPolicyValue);
   pragma Import (Java, U_USER_ID, "_USER_ID");
   pragma Import (Java, USER_ID, "USER_ID");
   pragma Import (Java, U_SYSTEM_ID, "_SYSTEM_ID");
   pragma Import (Java, SYSTEM_ID, "SYSTEM_ID");

end Org.Omg.PortableServer.IdAssignmentPolicyValue;
pragma Import (Java, Org.Omg.PortableServer.IdAssignmentPolicyValue, "org.omg.PortableServer.IdAssignmentPolicyValue");
pragma Extensions_Allowed (Off);
