pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.PortableServer.IdUniquenessPolicyValue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Value (This : access Typ)
                   return Java.Int;

   function From_int (P1_Int : Java.Int)
                      return access Org.Omg.PortableServer.IdUniquenessPolicyValue.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_IdUniquenessPolicyValue (P1_Int : Java.Int; 
                                         This : Ref := null)
                                         return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   U_UNIQUE_ID : constant Java.Int;

   --  final
   UNIQUE_ID : access Org.Omg.PortableServer.IdUniquenessPolicyValue.Typ'Class;

   --  final
   U_MULTIPLE_ID : constant Java.Int;

   --  final
   MULTIPLE_ID : access Org.Omg.PortableServer.IdUniquenessPolicyValue.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Value, "value");
   pragma Import (Java, From_int, "from_int");
   pragma Java_Constructor (New_IdUniquenessPolicyValue);
   pragma Import (Java, U_UNIQUE_ID, "_UNIQUE_ID");
   pragma Import (Java, UNIQUE_ID, "UNIQUE_ID");
   pragma Import (Java, U_MULTIPLE_ID, "_MULTIPLE_ID");
   pragma Import (Java, MULTIPLE_ID, "MULTIPLE_ID");

end Org.Omg.PortableServer.IdUniquenessPolicyValue;
pragma Import (Java, Org.Omg.PortableServer.IdUniquenessPolicyValue, "org.omg.PortableServer.IdUniquenessPolicyValue");
pragma Extensions_Allowed (Off);
