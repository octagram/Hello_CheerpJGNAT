pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.PortableServer.ImplicitActivationPolicyValue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Value (This : access Typ)
                   return Java.Int;

   function From_int (P1_Int : Java.Int)
                      return access Org.Omg.PortableServer.ImplicitActivationPolicyValue.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_ImplicitActivationPolicyValue (P1_Int : Java.Int; 
                                               This : Ref := null)
                                               return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   U_IMPLICIT_ACTIVATION : constant Java.Int;

   --  final
   IMPLICIT_ACTIVATION : access Org.Omg.PortableServer.ImplicitActivationPolicyValue.Typ'Class;

   --  final
   U_NO_IMPLICIT_ACTIVATION : constant Java.Int;

   --  final
   NO_IMPLICIT_ACTIVATION : access Org.Omg.PortableServer.ImplicitActivationPolicyValue.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Value, "value");
   pragma Import (Java, From_int, "from_int");
   pragma Java_Constructor (New_ImplicitActivationPolicyValue);
   pragma Import (Java, U_IMPLICIT_ACTIVATION, "_IMPLICIT_ACTIVATION");
   pragma Import (Java, IMPLICIT_ACTIVATION, "IMPLICIT_ACTIVATION");
   pragma Import (Java, U_NO_IMPLICIT_ACTIVATION, "_NO_IMPLICIT_ACTIVATION");
   pragma Import (Java, NO_IMPLICIT_ACTIVATION, "NO_IMPLICIT_ACTIVATION");

end Org.Omg.PortableServer.ImplicitActivationPolicyValue;
pragma Import (Java, Org.Omg.PortableServer.ImplicitActivationPolicyValue, "org.omg.PortableServer.ImplicitActivationPolicyValue");
pragma Extensions_Allowed (Off);
