pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Policy;
with Org.Omg.CORBA.Portable.IDLEntity;
with Org.Omg.PortableServer.LifespanPolicyOperations;

package Org.Omg.PortableServer.LifespanPolicy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Policy_I : Org.Omg.CORBA.Policy.Ref;
            IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref;
            LifespanPolicyOperations_I : Org.Omg.PortableServer.LifespanPolicyOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   
private
   pragma Convention (Java, Typ);

end Org.Omg.PortableServer.LifespanPolicy;
pragma Import (Java, Org.Omg.PortableServer.LifespanPolicy, "org.omg.PortableServer.LifespanPolicy");
pragma Extensions_Allowed (Off);
