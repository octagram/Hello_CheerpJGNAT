pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.PortableServer.LifespanPolicyValue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Value (This : access Typ)
                   return Java.Int;

   function From_int (P1_Int : Java.Int)
                      return access Org.Omg.PortableServer.LifespanPolicyValue.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_LifespanPolicyValue (P1_Int : Java.Int; 
                                     This : Ref := null)
                                     return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   U_TRANSIENT : constant Java.Int;

   --  final
   TRANSIENT : access Org.Omg.PortableServer.LifespanPolicyValue.Typ'Class;

   --  final
   U_PERSISTENT : constant Java.Int;

   --  final
   PERSISTENT : access Org.Omg.PortableServer.LifespanPolicyValue.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Value, "value");
   pragma Import (Java, From_int, "from_int");
   pragma Java_Constructor (New_LifespanPolicyValue);
   pragma Import (Java, U_TRANSIENT, "_TRANSIENT");
   pragma Import (Java, TRANSIENT, "TRANSIENT");
   pragma Import (Java, U_PERSISTENT, "_PERSISTENT");
   pragma Import (Java, PERSISTENT, "PERSISTENT");

end Org.Omg.PortableServer.LifespanPolicyValue;
pragma Import (Java, Org.Omg.PortableServer.LifespanPolicyValue, "org.omg.PortableServer.LifespanPolicyValue");
pragma Extensions_Allowed (Off);
