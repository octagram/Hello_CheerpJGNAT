pragma Extensions_Allowed (On);
limited with Org.Omg.PortableServer.POAManagerPackage.State;
with Java.Lang.Object;

package Org.Omg.PortableServer.POAManagerOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Activate (This : access Typ) is abstract;
   --  can raise Org.Omg.PortableServer.POAManagerPackage.AdapterInactive.Except

   procedure Hold_requests (This : access Typ;
                            P1_Boolean : Java.Boolean) is abstract;
   --  can raise Org.Omg.PortableServer.POAManagerPackage.AdapterInactive.Except

   procedure Discard_requests (This : access Typ;
                               P1_Boolean : Java.Boolean) is abstract;
   --  can raise Org.Omg.PortableServer.POAManagerPackage.AdapterInactive.Except

   procedure Deactivate (This : access Typ;
                         P1_Boolean : Java.Boolean;
                         P2_Boolean : Java.Boolean) is abstract;
   --  can raise Org.Omg.PortableServer.POAManagerPackage.AdapterInactive.Except

   function Get_state (This : access Typ)
                       return access Org.Omg.PortableServer.POAManagerPackage.State.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Activate, "activate");
   pragma Export (Java, Hold_requests, "hold_requests");
   pragma Export (Java, Discard_requests, "discard_requests");
   pragma Export (Java, Deactivate, "deactivate");
   pragma Export (Java, Get_state, "get_state");

end Org.Omg.PortableServer.POAManagerOperations;
pragma Import (Java, Org.Omg.PortableServer.POAManagerOperations, "org.omg.PortableServer.POAManagerOperations");
pragma Extensions_Allowed (Off);
