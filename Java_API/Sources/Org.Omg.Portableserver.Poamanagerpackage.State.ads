pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.PortableServer.POAManagerPackage.State is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Value (This : access Typ)
                   return Java.Int;

   function From_int (P1_Int : Java.Int)
                      return access Org.Omg.PortableServer.POAManagerPackage.State.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_State (P1_Int : Java.Int; 
                       This : Ref := null)
                       return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   U_HOLDING : constant Java.Int;

   --  final
   HOLDING : access Org.Omg.PortableServer.POAManagerPackage.State.Typ'Class;

   --  final
   U_ACTIVE : constant Java.Int;

   --  final
   ACTIVE : access Org.Omg.PortableServer.POAManagerPackage.State.Typ'Class;

   --  final
   U_DISCARDING : constant Java.Int;

   --  final
   DISCARDING : access Org.Omg.PortableServer.POAManagerPackage.State.Typ'Class;

   --  final
   U_INACTIVE : constant Java.Int;

   --  final
   INACTIVE : access Org.Omg.PortableServer.POAManagerPackage.State.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Value, "value");
   pragma Import (Java, From_int, "from_int");
   pragma Java_Constructor (New_State);
   pragma Import (Java, U_HOLDING, "_HOLDING");
   pragma Import (Java, HOLDING, "HOLDING");
   pragma Import (Java, U_ACTIVE, "_ACTIVE");
   pragma Import (Java, ACTIVE, "ACTIVE");
   pragma Import (Java, U_DISCARDING, "_DISCARDING");
   pragma Import (Java, DISCARDING, "DISCARDING");
   pragma Import (Java, U_INACTIVE, "_INACTIVE");
   pragma Import (Java, INACTIVE, "INACTIVE");

end Org.Omg.PortableServer.POAManagerPackage.State;
pragma Import (Java, Org.Omg.PortableServer.POAManagerPackage.State, "org.omg.PortableServer.POAManagerPackage.State");
pragma Extensions_Allowed (Off);
