pragma Extensions_Allowed (On);
package Org.Omg.PortableServer.POAManagerPackage is
   pragma Preelaborate;
end Org.Omg.PortableServer.POAManagerPackage;
pragma Import (Java, Org.Omg.PortableServer.POAManagerPackage, "org.omg.PortableServer.POAManagerPackage");
pragma Extensions_Allowed (Off);
