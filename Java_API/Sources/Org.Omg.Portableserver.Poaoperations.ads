pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.Object;
limited with Org.Omg.CORBA.Policy;
limited with Org.Omg.PortableServer.AdapterActivator;
limited with Org.Omg.PortableServer.IdAssignmentPolicy;
limited with Org.Omg.PortableServer.IdAssignmentPolicyValue;
limited with Org.Omg.PortableServer.IdUniquenessPolicy;
limited with Org.Omg.PortableServer.IdUniquenessPolicyValue;
limited with Org.Omg.PortableServer.ImplicitActivationPolicy;
limited with Org.Omg.PortableServer.ImplicitActivationPolicyValue;
limited with Org.Omg.PortableServer.LifespanPolicy;
limited with Org.Omg.PortableServer.LifespanPolicyValue;
limited with Org.Omg.PortableServer.POA;
limited with Org.Omg.PortableServer.POAManager;
limited with Org.Omg.PortableServer.RequestProcessingPolicy;
limited with Org.Omg.PortableServer.RequestProcessingPolicyValue;
limited with Org.Omg.PortableServer.Servant;
limited with Org.Omg.PortableServer.ServantManager;
limited with Org.Omg.PortableServer.ServantRetentionPolicy;
limited with Org.Omg.PortableServer.ServantRetentionPolicyValue;
limited with Org.Omg.PortableServer.ThreadPolicy;
limited with Org.Omg.PortableServer.ThreadPolicyValue;
with Java.Lang.Object;

package Org.Omg.PortableServer.POAOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Create_POA (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_POAManager : access Standard.Org.Omg.PortableServer.POAManager.Typ'Class;
                        P3_Policy_Arr : access Org.Omg.CORBA.Policy.Arr_Obj)
                        return access Org.Omg.PortableServer.POA.Typ'Class is abstract;
   --  can raise Org.Omg.PortableServer.POAPackage.AdapterAlreadyExists.Except and
   --  Org.Omg.PortableServer.POAPackage.InvalidPolicy.Except

   function Find_POA (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Boolean : Java.Boolean)
                      return access Org.Omg.PortableServer.POA.Typ'Class is abstract;
   --  can raise Org.Omg.PortableServer.POAPackage.AdapterNonExistent.Except

   procedure Destroy (This : access Typ;
                      P1_Boolean : Java.Boolean;
                      P2_Boolean : Java.Boolean) is abstract;

   function Create_thread_policy (This : access Typ;
                                  P1_ThreadPolicyValue : access Standard.Org.Omg.PortableServer.ThreadPolicyValue.Typ'Class)
                                  return access Org.Omg.PortableServer.ThreadPolicy.Typ'Class is abstract;

   function Create_lifespan_policy (This : access Typ;
                                    P1_LifespanPolicyValue : access Standard.Org.Omg.PortableServer.LifespanPolicyValue.Typ'Class)
                                    return access Org.Omg.PortableServer.LifespanPolicy.Typ'Class is abstract;

   function Create_id_uniqueness_policy (This : access Typ;
                                         P1_IdUniquenessPolicyValue : access Standard.Org.Omg.PortableServer.IdUniquenessPolicyValue.Typ'Class)
                                         return access Org.Omg.PortableServer.IdUniquenessPolicy.Typ'Class is abstract;

   function Create_id_assignment_policy (This : access Typ;
                                         P1_IdAssignmentPolicyValue : access Standard.Org.Omg.PortableServer.IdAssignmentPolicyValue.Typ'Class)
                                         return access Org.Omg.PortableServer.IdAssignmentPolicy.Typ'Class is abstract;

   function Create_implicit_activation_policy (This : access Typ;
                                               P1_ImplicitActivationPolicyValue : access Standard.Org.Omg.PortableServer.ImplicitActivationPolicyValue.Typ'Class)
                                               return access Org.Omg.PortableServer.ImplicitActivationPolicy.Typ'Class is abstract;

   function Create_servant_retention_policy (This : access Typ;
                                             P1_ServantRetentionPolicyValue : access Standard.Org.Omg.PortableServer.ServantRetentionPolicyValue.Typ'Class)
                                             return access Org.Omg.PortableServer.ServantRetentionPolicy.Typ'Class is abstract;

   function Create_request_processing_policy (This : access Typ;
                                              P1_RequestProcessingPolicyValue : access Standard.Org.Omg.PortableServer.RequestProcessingPolicyValue.Typ'Class)
                                              return access Org.Omg.PortableServer.RequestProcessingPolicy.Typ'Class is abstract;

   function The_name (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   function The_parent (This : access Typ)
                        return access Org.Omg.PortableServer.POA.Typ'Class is abstract;

   function The_children (This : access Typ)
                          return Standard.Java.Lang.Object.Ref is abstract;

   function The_POAManager (This : access Typ)
                            return access Org.Omg.PortableServer.POAManager.Typ'Class is abstract;

   function The_activator (This : access Typ)
                           return access Org.Omg.PortableServer.AdapterActivator.Typ'Class is abstract;

   procedure The_activator (This : access Typ;
                            P1_AdapterActivator : access Standard.Org.Omg.PortableServer.AdapterActivator.Typ'Class) is abstract;

   function Get_servant_manager (This : access Typ)
                                 return access Org.Omg.PortableServer.ServantManager.Typ'Class is abstract;
   --  can raise Org.Omg.PortableServer.POAPackage.WrongPolicy.Except

   procedure Set_servant_manager (This : access Typ;
                                  P1_ServantManager : access Standard.Org.Omg.PortableServer.ServantManager.Typ'Class) is abstract;
   --  can raise Org.Omg.PortableServer.POAPackage.WrongPolicy.Except

   function Get_servant (This : access Typ)
                         return access Org.Omg.PortableServer.Servant.Typ'Class is abstract;
   --  can raise Org.Omg.PortableServer.POAPackage.NoServant.Except and
   --  Org.Omg.PortableServer.POAPackage.WrongPolicy.Except

   procedure Set_servant (This : access Typ;
                          P1_Servant : access Standard.Org.Omg.PortableServer.Servant.Typ'Class) is abstract;
   --  can raise Org.Omg.PortableServer.POAPackage.WrongPolicy.Except

   function Activate_object (This : access Typ;
                             P1_Servant : access Standard.Org.Omg.PortableServer.Servant.Typ'Class)
                             return Java.Byte_Arr is abstract;
   --  can raise Org.Omg.PortableServer.POAPackage.ServantAlreadyActive.Except and
   --  Org.Omg.PortableServer.POAPackage.WrongPolicy.Except

   procedure Activate_object_with_id (This : access Typ;
                                      P1_Byte_Arr : Java.Byte_Arr;
                                      P2_Servant : access Standard.Org.Omg.PortableServer.Servant.Typ'Class) is abstract;
   --  can raise Org.Omg.PortableServer.POAPackage.ServantAlreadyActive.Except,
   --  Org.Omg.PortableServer.POAPackage.ObjectAlreadyActive.Except and
   --  Org.Omg.PortableServer.POAPackage.WrongPolicy.Except

   procedure Deactivate_object (This : access Typ;
                                P1_Byte_Arr : Java.Byte_Arr) is abstract;
   --  can raise Org.Omg.PortableServer.POAPackage.ObjectNotActive.Except and
   --  Org.Omg.PortableServer.POAPackage.WrongPolicy.Except

   function Create_reference (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Org.Omg.CORBA.Object.Typ'Class is abstract;
   --  can raise Org.Omg.PortableServer.POAPackage.WrongPolicy.Except

   function Create_reference_with_id (This : access Typ;
                                      P1_Byte_Arr : Java.Byte_Arr;
                                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                                      return access Org.Omg.CORBA.Object.Typ'Class is abstract;

   function Servant_to_id (This : access Typ;
                           P1_Servant : access Standard.Org.Omg.PortableServer.Servant.Typ'Class)
                           return Java.Byte_Arr is abstract;
   --  can raise Org.Omg.PortableServer.POAPackage.ServantNotActive.Except and
   --  Org.Omg.PortableServer.POAPackage.WrongPolicy.Except

   function Servant_to_reference (This : access Typ;
                                  P1_Servant : access Standard.Org.Omg.PortableServer.Servant.Typ'Class)
                                  return access Org.Omg.CORBA.Object.Typ'Class is abstract;
   --  can raise Org.Omg.PortableServer.POAPackage.ServantNotActive.Except and
   --  Org.Omg.PortableServer.POAPackage.WrongPolicy.Except

   function Reference_to_servant (This : access Typ;
                                  P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class)
                                  return access Org.Omg.PortableServer.Servant.Typ'Class is abstract;
   --  can raise Org.Omg.PortableServer.POAPackage.ObjectNotActive.Except,
   --  Org.Omg.PortableServer.POAPackage.WrongPolicy.Except and
   --  Org.Omg.PortableServer.POAPackage.WrongAdapter.Except

   function Reference_to_id (This : access Typ;
                             P1_Object : access Standard.Org.Omg.CORBA.Object.Typ'Class)
                             return Java.Byte_Arr is abstract;
   --  can raise Org.Omg.PortableServer.POAPackage.WrongAdapter.Except and
   --  Org.Omg.PortableServer.POAPackage.WrongPolicy.Except

   function Id_to_servant (This : access Typ;
                           P1_Byte_Arr : Java.Byte_Arr)
                           return access Org.Omg.PortableServer.Servant.Typ'Class is abstract;
   --  can raise Org.Omg.PortableServer.POAPackage.ObjectNotActive.Except and
   --  Org.Omg.PortableServer.POAPackage.WrongPolicy.Except

   function Id_to_reference (This : access Typ;
                             P1_Byte_Arr : Java.Byte_Arr)
                             return access Org.Omg.CORBA.Object.Typ'Class is abstract;
   --  can raise Org.Omg.PortableServer.POAPackage.ObjectNotActive.Except and
   --  Org.Omg.PortableServer.POAPackage.WrongPolicy.Except

   function Id (This : access Typ)
                return Java.Byte_Arr is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Create_POA, "create_POA");
   pragma Export (Java, Find_POA, "find_POA");
   pragma Export (Java, Destroy, "destroy");
   pragma Export (Java, Create_thread_policy, "create_thread_policy");
   pragma Export (Java, Create_lifespan_policy, "create_lifespan_policy");
   pragma Export (Java, Create_id_uniqueness_policy, "create_id_uniqueness_policy");
   pragma Export (Java, Create_id_assignment_policy, "create_id_assignment_policy");
   pragma Export (Java, Create_implicit_activation_policy, "create_implicit_activation_policy");
   pragma Export (Java, Create_servant_retention_policy, "create_servant_retention_policy");
   pragma Export (Java, Create_request_processing_policy, "create_request_processing_policy");
   pragma Export (Java, The_name, "the_name");
   pragma Export (Java, The_parent, "the_parent");
   pragma Export (Java, The_children, "the_children");
   pragma Export (Java, The_POAManager, "the_POAManager");
   pragma Export (Java, The_activator, "the_activator");
   pragma Export (Java, Get_servant_manager, "get_servant_manager");
   pragma Export (Java, Set_servant_manager, "set_servant_manager");
   pragma Export (Java, Get_servant, "get_servant");
   pragma Export (Java, Set_servant, "set_servant");
   pragma Export (Java, Activate_object, "activate_object");
   pragma Export (Java, Activate_object_with_id, "activate_object_with_id");
   pragma Export (Java, Deactivate_object, "deactivate_object");
   pragma Export (Java, Create_reference, "create_reference");
   pragma Export (Java, Create_reference_with_id, "create_reference_with_id");
   pragma Export (Java, Servant_to_id, "servant_to_id");
   pragma Export (Java, Servant_to_reference, "servant_to_reference");
   pragma Export (Java, Reference_to_servant, "reference_to_servant");
   pragma Export (Java, Reference_to_id, "reference_to_id");
   pragma Export (Java, Id_to_servant, "id_to_servant");
   pragma Export (Java, Id_to_reference, "id_to_reference");
   pragma Export (Java, Id, "id");

end Org.Omg.PortableServer.POAOperations;
pragma Import (Java, Org.Omg.PortableServer.POAOperations, "org.omg.PortableServer.POAOperations");
pragma Extensions_Allowed (Off);
