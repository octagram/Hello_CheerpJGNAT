pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;
with Org.Omg.CORBA.UserException;

package Org.Omg.PortableServer.POAPackage.InvalidPolicy is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Org.Omg.CORBA.UserException.Typ(Serializable_I,
                                           IDLEntity_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Index : Java.Short;
      pragma Import (Java, Index, "index");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InvalidPolicy (This : Ref := null)
                               return Ref;

   function New_InvalidPolicy (P1_Short : Java.Short; 
                               This : Ref := null)
                               return Ref;

   function New_InvalidPolicy (P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_Short : Java.Short; 
                               This : Ref := null)
                               return Ref;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "org.omg.PortableServer.POAPackage.InvalidPolicy");
   pragma Java_Constructor (New_InvalidPolicy);

end Org.Omg.PortableServer.POAPackage.InvalidPolicy;
pragma Import (Java, Org.Omg.PortableServer.POAPackage.InvalidPolicy, "org.omg.PortableServer.POAPackage.InvalidPolicy");
pragma Extensions_Allowed (Off);
