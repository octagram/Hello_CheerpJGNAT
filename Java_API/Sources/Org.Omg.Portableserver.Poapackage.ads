pragma Extensions_Allowed (On);
package Org.Omg.PortableServer.POAPackage is
   pragma Preelaborate;
end Org.Omg.PortableServer.POAPackage;
pragma Import (Java, Org.Omg.PortableServer.POAPackage, "org.omg.PortableServer.POAPackage");
pragma Extensions_Allowed (Off);
