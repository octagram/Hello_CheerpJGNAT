pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.ORB;
limited with Org.Omg.CORBA.Object;
limited with Org.Omg.PortableServer.POA;
limited with Org.Omg.PortableServer.Servant;
with Java.Lang.Object;

package Org.Omg.PortableServer.Portable.Delegate is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Orb (This : access Typ;
                 P1_Servant : access Standard.Org.Omg.PortableServer.Servant.Typ'Class)
                 return access Org.Omg.CORBA.ORB.Typ'Class is abstract;

   function This_object (This : access Typ;
                         P1_Servant : access Standard.Org.Omg.PortableServer.Servant.Typ'Class)
                         return access Org.Omg.CORBA.Object.Typ'Class is abstract;

   function Poa (This : access Typ;
                 P1_Servant : access Standard.Org.Omg.PortableServer.Servant.Typ'Class)
                 return access Org.Omg.PortableServer.POA.Typ'Class is abstract;

   function Object_id (This : access Typ;
                       P1_Servant : access Standard.Org.Omg.PortableServer.Servant.Typ'Class)
                       return Java.Byte_Arr is abstract;

   function Default_POA (This : access Typ;
                         P1_Servant : access Standard.Org.Omg.PortableServer.Servant.Typ'Class)
                         return access Org.Omg.PortableServer.POA.Typ'Class is abstract;

   function Is_a (This : access Typ;
                  P1_Servant : access Standard.Org.Omg.PortableServer.Servant.Typ'Class;
                  P2_String : access Standard.Java.Lang.String.Typ'Class)
                  return Java.Boolean is abstract;

   function Non_existent (This : access Typ;
                          P1_Servant : access Standard.Org.Omg.PortableServer.Servant.Typ'Class)
                          return Java.Boolean is abstract;

   function Get_interface_def (This : access Typ;
                               P1_Servant : access Standard.Org.Omg.PortableServer.Servant.Typ'Class)
                               return access Org.Omg.CORBA.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Orb, "orb");
   pragma Export (Java, This_object, "this_object");
   pragma Export (Java, Poa, "poa");
   pragma Export (Java, Object_id, "object_id");
   pragma Export (Java, Default_POA, "default_POA");
   pragma Export (Java, Is_a, "is_a");
   pragma Export (Java, Non_existent, "non_existent");
   pragma Export (Java, Get_interface_def, "get_interface_def");

end Org.Omg.PortableServer.Portable.Delegate;
pragma Import (Java, Org.Omg.PortableServer.Portable.Delegate, "org.omg.PortableServer.portable.Delegate");
pragma Extensions_Allowed (Off);
