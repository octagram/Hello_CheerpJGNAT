pragma Extensions_Allowed (On);
package Org.Omg.PortableServer.Portable is
   pragma Preelaborate;
end Org.Omg.PortableServer.Portable;
pragma Import (Java, Org.Omg.PortableServer.Portable, "org.omg.PortableServer.portable");
pragma Extensions_Allowed (Off);
