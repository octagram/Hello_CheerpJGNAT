pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.PortableServer.RequestProcessingPolicyValue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Value (This : access Typ)
                   return Java.Int;

   function From_int (P1_Int : Java.Int)
                      return access Org.Omg.PortableServer.RequestProcessingPolicyValue.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_RequestProcessingPolicyValue (P1_Int : Java.Int; 
                                              This : Ref := null)
                                              return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   U_USE_ACTIVE_OBJECT_MAP_ONLY : constant Java.Int;

   --  final
   USE_ACTIVE_OBJECT_MAP_ONLY : access Org.Omg.PortableServer.RequestProcessingPolicyValue.Typ'Class;

   --  final
   U_USE_DEFAULT_SERVANT : constant Java.Int;

   --  final
   USE_DEFAULT_SERVANT : access Org.Omg.PortableServer.RequestProcessingPolicyValue.Typ'Class;

   --  final
   U_USE_SERVANT_MANAGER : constant Java.Int;

   --  final
   USE_SERVANT_MANAGER : access Org.Omg.PortableServer.RequestProcessingPolicyValue.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Value, "value");
   pragma Import (Java, From_int, "from_int");
   pragma Java_Constructor (New_RequestProcessingPolicyValue);
   pragma Import (Java, U_USE_ACTIVE_OBJECT_MAP_ONLY, "_USE_ACTIVE_OBJECT_MAP_ONLY");
   pragma Import (Java, USE_ACTIVE_OBJECT_MAP_ONLY, "USE_ACTIVE_OBJECT_MAP_ONLY");
   pragma Import (Java, U_USE_DEFAULT_SERVANT, "_USE_DEFAULT_SERVANT");
   pragma Import (Java, USE_DEFAULT_SERVANT, "USE_DEFAULT_SERVANT");
   pragma Import (Java, U_USE_SERVANT_MANAGER, "_USE_SERVANT_MANAGER");
   pragma Import (Java, USE_SERVANT_MANAGER, "USE_SERVANT_MANAGER");

end Org.Omg.PortableServer.RequestProcessingPolicyValue;
pragma Import (Java, Org.Omg.PortableServer.RequestProcessingPolicyValue, "org.omg.PortableServer.RequestProcessingPolicyValue");
pragma Extensions_Allowed (Off);
