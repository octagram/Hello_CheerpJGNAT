pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.CORBA.ORB;
limited with Org.Omg.CORBA.Object;
limited with Org.Omg.PortableServer.POA;
limited with Org.Omg.PortableServer.Portable.Delegate;
with Java.Lang.Object;

package Org.Omg.PortableServer.Servant is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is abstract new Java.Lang.Object.Typ
      with null record;

   function New_Servant (This : Ref := null)
                         return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   --  final
   function U_Get_delegate (This : access Typ)
                            return access Org.Omg.PortableServer.Portable.Delegate.Typ'Class;

   --  final
   procedure U_Set_delegate (This : access Typ;
                             P1_Delegate : access Standard.Org.Omg.PortableServer.Portable.Delegate.Typ'Class);

   --  final
   function U_This_object (This : access Typ)
                           return access Org.Omg.CORBA.Object.Typ'Class;

   --  final
   function U_This_object (This : access Typ;
                           P1_ORB : access Standard.Org.Omg.CORBA.ORB.Typ'Class)
                           return access Org.Omg.CORBA.Object.Typ'Class;

   --  final
   function U_Orb (This : access Typ)
                   return access Org.Omg.CORBA.ORB.Typ'Class;

   --  final
   function U_Poa (This : access Typ)
                   return access Org.Omg.PortableServer.POA.Typ'Class;

   --  final
   function U_Object_id (This : access Typ)
                         return Java.Byte_Arr;

   function U_Default_POA (This : access Typ)
                           return access Org.Omg.PortableServer.POA.Typ'Class;

   function U_Is_a (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return Java.Boolean;

   function U_Non_existent (This : access Typ)
                            return Java.Boolean;

   function U_Get_interface_def (This : access Typ)
                                 return access Org.Omg.CORBA.Object.Typ'Class;

   function U_All_interfaces (This : access Typ;
                              P1_POA : access Standard.Org.Omg.PortableServer.POA.Typ'Class;
                              P2_Byte_Arr : Java.Byte_Arr)
                              return Standard.Java.Lang.Object.Ref is abstract;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Servant);
   pragma Import (Java, U_Get_delegate, "_get_delegate");
   pragma Import (Java, U_Set_delegate, "_set_delegate");
   pragma Import (Java, U_This_object, "_this_object");
   pragma Import (Java, U_Orb, "_orb");
   pragma Import (Java, U_Poa, "_poa");
   pragma Import (Java, U_Object_id, "_object_id");
   pragma Import (Java, U_Default_POA, "_default_POA");
   pragma Import (Java, U_Is_a, "_is_a");
   pragma Import (Java, U_Non_existent, "_non_existent");
   pragma Import (Java, U_Get_interface_def, "_get_interface_def");
   pragma Export (Java, U_All_interfaces, "_all_interfaces");

end Org.Omg.PortableServer.Servant;
pragma Import (Java, Org.Omg.PortableServer.Servant, "org.omg.PortableServer.Servant");
pragma Extensions_Allowed (Off);
