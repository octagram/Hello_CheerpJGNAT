pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;
with Org.Omg.PortableServer.ServantActivatorOperations;
with Org.Omg.PortableServer.ServantManager;

package Org.Omg.PortableServer.ServantActivator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref;
            ServantActivatorOperations_I : Org.Omg.PortableServer.ServantActivatorOperations.Ref;
            ServantManager_I : Org.Omg.PortableServer.ServantManager.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   
private
   pragma Convention (Java, Typ);

end Org.Omg.PortableServer.ServantActivator;
pragma Import (Java, Org.Omg.PortableServer.ServantActivator, "org.omg.PortableServer.ServantActivator");
pragma Extensions_Allowed (Off);
