pragma Extensions_Allowed (On);
limited with Org.Omg.PortableServer.POA;
limited with Org.Omg.PortableServer.Servant;
with Java.Lang.Object;
with Org.Omg.PortableServer.ServantManagerOperations;

package Org.Omg.PortableServer.ServantActivatorOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ServantManagerOperations_I : Org.Omg.PortableServer.ServantManagerOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Incarnate (This : access Typ;
                       P1_Byte_Arr : Java.Byte_Arr;
                       P2_POA : access Standard.Org.Omg.PortableServer.POA.Typ'Class)
                       return access Org.Omg.PortableServer.Servant.Typ'Class is abstract;
   --  can raise Org.Omg.PortableServer.ForwardRequest.Except

   procedure Etherealize (This : access Typ;
                          P1_Byte_Arr : Java.Byte_Arr;
                          P2_POA : access Standard.Org.Omg.PortableServer.POA.Typ'Class;
                          P3_Servant : access Standard.Org.Omg.PortableServer.Servant.Typ'Class;
                          P4_Boolean : Java.Boolean;
                          P5_Boolean : Java.Boolean) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Incarnate, "incarnate");
   pragma Export (Java, Etherealize, "etherealize");

end Org.Omg.PortableServer.ServantActivatorOperations;
pragma Import (Java, Org.Omg.PortableServer.ServantActivatorOperations, "org.omg.PortableServer.ServantActivatorOperations");
pragma Extensions_Allowed (Off);
