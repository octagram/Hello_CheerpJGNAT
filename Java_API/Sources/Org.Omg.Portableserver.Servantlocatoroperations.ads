pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Omg.PortableServer.POA;
limited with Org.Omg.PortableServer.Servant;
limited with Org.Omg.PortableServer.ServantLocatorPackage.CookieHolder;
with Java.Lang.Object;
with Org.Omg.PortableServer.ServantManagerOperations;

package Org.Omg.PortableServer.ServantLocatorOperations is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            ServantManagerOperations_I : Org.Omg.PortableServer.ServantManagerOperations.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Preinvoke (This : access Typ;
                       P1_Byte_Arr : Java.Byte_Arr;
                       P2_POA : access Standard.Org.Omg.PortableServer.POA.Typ'Class;
                       P3_String : access Standard.Java.Lang.String.Typ'Class;
                       P4_CookieHolder : access Standard.Org.Omg.PortableServer.ServantLocatorPackage.CookieHolder.Typ'Class)
                       return access Org.Omg.PortableServer.Servant.Typ'Class is abstract;
   --  can raise Org.Omg.PortableServer.ForwardRequest.Except

   procedure Postinvoke (This : access Typ;
                         P1_Byte_Arr : Java.Byte_Arr;
                         P2_POA : access Standard.Org.Omg.PortableServer.POA.Typ'Class;
                         P3_String : access Standard.Java.Lang.String.Typ'Class;
                         P4_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P5_Servant : access Standard.Org.Omg.PortableServer.Servant.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Preinvoke, "preinvoke");
   pragma Export (Java, Postinvoke, "postinvoke");

end Org.Omg.PortableServer.ServantLocatorOperations;
pragma Import (Java, Org.Omg.PortableServer.ServantLocatorOperations, "org.omg.PortableServer.ServantLocatorOperations");
pragma Extensions_Allowed (Off);
