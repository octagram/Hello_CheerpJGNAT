pragma Extensions_Allowed (On);
package Org.Omg.PortableServer.ServantLocatorPackage is
   pragma Preelaborate;
end Org.Omg.PortableServer.ServantLocatorPackage;
pragma Import (Java, Org.Omg.PortableServer.ServantLocatorPackage, "org.omg.PortableServer.ServantLocatorPackage");
pragma Extensions_Allowed (Off);
