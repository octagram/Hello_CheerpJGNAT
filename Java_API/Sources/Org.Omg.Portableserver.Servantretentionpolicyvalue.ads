pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.PortableServer.ServantRetentionPolicyValue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Value (This : access Typ)
                   return Java.Int;

   function From_int (P1_Int : Java.Int)
                      return access Org.Omg.PortableServer.ServantRetentionPolicyValue.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_ServantRetentionPolicyValue (P1_Int : Java.Int; 
                                             This : Ref := null)
                                             return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   U_RETAIN : constant Java.Int;

   --  final
   RETAIN : access Org.Omg.PortableServer.ServantRetentionPolicyValue.Typ'Class;

   --  final
   U_NON_RETAIN : constant Java.Int;

   --  final
   NON_RETAIN : access Org.Omg.PortableServer.ServantRetentionPolicyValue.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Value, "value");
   pragma Import (Java, From_int, "from_int");
   pragma Java_Constructor (New_ServantRetentionPolicyValue);
   pragma Import (Java, U_RETAIN, "_RETAIN");
   pragma Import (Java, RETAIN, "RETAIN");
   pragma Import (Java, U_NON_RETAIN, "_NON_RETAIN");
   pragma Import (Java, NON_RETAIN, "NON_RETAIN");

end Org.Omg.PortableServer.ServantRetentionPolicyValue;
pragma Import (Java, Org.Omg.PortableServer.ServantRetentionPolicyValue, "org.omg.PortableServer.ServantRetentionPolicyValue");
pragma Extensions_Allowed (Off);
