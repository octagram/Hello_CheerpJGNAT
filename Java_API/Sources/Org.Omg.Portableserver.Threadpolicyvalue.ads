pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Omg.CORBA.Portable.IDLEntity;

package Org.Omg.PortableServer.ThreadPolicyValue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(IDLEntity_I : Org.Omg.CORBA.Portable.IDLEntity.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Value (This : access Typ)
                   return Java.Int;

   function From_int (P1_Int : Java.Int)
                      return access Org.Omg.PortableServer.ThreadPolicyValue.Typ'Class;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   --  protected
   function New_ThreadPolicyValue (P1_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   U_ORB_CTRL_MODEL : constant Java.Int;

   --  final
   ORB_CTRL_MODEL : access Org.Omg.PortableServer.ThreadPolicyValue.Typ'Class;

   --  final
   U_SINGLE_THREAD_MODEL : constant Java.Int;

   --  final
   SINGLE_THREAD_MODEL : access Org.Omg.PortableServer.ThreadPolicyValue.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Value, "value");
   pragma Import (Java, From_int, "from_int");
   pragma Java_Constructor (New_ThreadPolicyValue);
   pragma Import (Java, U_ORB_CTRL_MODEL, "_ORB_CTRL_MODEL");
   pragma Import (Java, ORB_CTRL_MODEL, "ORB_CTRL_MODEL");
   pragma Import (Java, U_SINGLE_THREAD_MODEL, "_SINGLE_THREAD_MODEL");
   pragma Import (Java, SINGLE_THREAD_MODEL, "SINGLE_THREAD_MODEL");

end Org.Omg.PortableServer.ThreadPolicyValue;
pragma Import (Java, Org.Omg.PortableServer.ThreadPolicyValue, "org.omg.PortableServer.ThreadPolicyValue");
pragma Extensions_Allowed (Off);
