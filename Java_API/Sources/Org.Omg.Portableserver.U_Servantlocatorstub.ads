pragma Extensions_Allowed (On);
limited with Java.Lang.Class;
limited with Java.Lang.String;
limited with Org.Omg.PortableServer.POA;
limited with Org.Omg.PortableServer.Servant;
limited with Org.Omg.PortableServer.ServantLocatorPackage.CookieHolder;
with Java.Lang.Object;
with Org.Omg.CORBA.Object;
with Org.Omg.CORBA.Portable.ObjectImpl;
with Org.Omg.PortableServer.ServantLocator;

package Org.Omg.PortableServer.U_ServantLocatorStub is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Object_I : Org.Omg.CORBA.Object.Ref;
            ServantLocator_I : Org.Omg.PortableServer.ServantLocator.Ref)
    is new Org.Omg.CORBA.Portable.ObjectImpl.Typ(Object_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_U_ServantLocatorStub (This : Ref := null)
                                      return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function Preinvoke (This : access Typ;
                       P1_Byte_Arr : Java.Byte_Arr;
                       P2_POA : access Standard.Org.Omg.PortableServer.POA.Typ'Class;
                       P3_String : access Standard.Java.Lang.String.Typ'Class;
                       P4_CookieHolder : access Standard.Org.Omg.PortableServer.ServantLocatorPackage.CookieHolder.Typ'Class)
                       return access Org.Omg.PortableServer.Servant.Typ'Class;
   --  can raise Org.Omg.PortableServer.ForwardRequest.Except

   procedure Postinvoke (This : access Typ;
                         P1_Byte_Arr : Java.Byte_Arr;
                         P2_POA : access Standard.Org.Omg.PortableServer.POA.Typ'Class;
                         P3_String : access Standard.Java.Lang.String.Typ'Class;
                         P4_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P5_Servant : access Standard.Org.Omg.PortableServer.Servant.Typ'Class);

   function U_Ids (This : access Typ)
                   return Standard.Java.Lang.Object.Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   U_OpsClass : access Java.Lang.Class.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_U_ServantLocatorStub);
   pragma Import (Java, Preinvoke, "preinvoke");
   pragma Import (Java, Postinvoke, "postinvoke");
   pragma Import (Java, U_Ids, "_ids");
   pragma Import (Java, U_OpsClass, "_opsClass");

end Org.Omg.PortableServer.U_ServantLocatorStub;
pragma Import (Java, Org.Omg.PortableServer.U_ServantLocatorStub, "org.omg.PortableServer._ServantLocatorStub");
pragma Extensions_Allowed (Off);
