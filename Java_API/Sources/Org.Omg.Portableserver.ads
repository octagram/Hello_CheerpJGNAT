pragma Extensions_Allowed (On);
package Org.Omg.PortableServer is
   pragma Preelaborate;
end Org.Omg.PortableServer;
pragma Import (Java, Org.Omg.PortableServer, "org.omg.PortableServer");
pragma Extensions_Allowed (Off);
