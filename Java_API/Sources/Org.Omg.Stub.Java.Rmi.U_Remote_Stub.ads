pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Rmi.Remote;
with Javax.Rmi.CORBA.Stub;
with Org.Omg.CORBA.Object;

package Org.Omg.Stub.Java.Rmi.U_Remote_Stub is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Standard.Java.Io.Serializable.Ref;
            Remote_I : Standard.Java.Rmi.Remote.Ref;
            Object_I : Org.Omg.CORBA.Object.Ref)
    is new Javax.Rmi.CORBA.Stub.Typ(Serializable_I,
                                    Object_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_U_Remote_Stub (This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function U_Ids (This : access Typ)
                   return Standard.Java.Lang.Object.Ref;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_U_Remote_Stub);
   pragma Import (Java, U_Ids, "_ids");

end Org.Omg.Stub.Java.Rmi.U_Remote_Stub;
pragma Import (Java, Org.Omg.Stub.Java.Rmi.U_Remote_Stub, "org.omg.stub.java.rmi._Remote_Stub");
pragma Extensions_Allowed (Off);
