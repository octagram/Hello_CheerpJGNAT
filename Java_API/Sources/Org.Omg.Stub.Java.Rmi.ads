pragma Extensions_Allowed (On);
package Org.Omg.Stub.Java.Rmi is
   pragma Preelaborate;
end Org.Omg.Stub.Java.Rmi;
pragma Import (Java, Org.Omg.Stub.Java.Rmi, "org.omg.stub.java.rmi");
pragma Extensions_Allowed (Off);
