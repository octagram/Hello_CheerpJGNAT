pragma Extensions_Allowed (On);
package Org.Omg.Stub.Java is
   pragma Preelaborate;
end Org.Omg.Stub.Java;
pragma Import (Java, Org.Omg.Stub.Java, "org.omg.stub.java");
pragma Extensions_Allowed (Off);
