pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Javax.Management.Remote.Rmi.RMIConnection;
with Java.Io.Serializable;
with Java.Lang.Object;
with Javax.Management.Remote.Rmi.RMIServer;
with Javax.Rmi.CORBA.Stub;
with Org.Omg.CORBA.Object;

package Org.Omg.Stub.Javax.Management.Remote.Rmi.U_RMIServer_Stub is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref;
            RMIServer_I : Standard.Javax.Management.Remote.Rmi.RMIServer.Ref;
            Object_I : Org.Omg.CORBA.Object.Ref)
    is new Standard.Javax.Rmi.CORBA.Stub.Typ(Serializable_I,
                                    Object_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_U_RMIServer_Stub (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function U_Ids (This : access Typ)
                   return Standard.Java.Lang.Object.Ref;

   function GetVersion (This : access Typ)
                        return access Java.Lang.String.Typ'Class;
   --  can raise Java.Rmi.RemoteException.Except

   function NewClient (This : access Typ;
                       P1_Object : access Standard.Java.Lang.Object.Typ'Class)
                       return access Standard.Javax.Management.Remote.Rmi.RMIConnection.Typ'Class;
   --  can raise Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_U_RMIServer_Stub);
   pragma Import (Java, U_Ids, "_ids");
   pragma Import (Java, GetVersion, "getVersion");
   pragma Import (Java, NewClient, "newClient");

end Org.Omg.Stub.Javax.Management.Remote.Rmi.U_RMIServer_Stub;
pragma Import (Java, Org.Omg.Stub.Javax.Management.Remote.Rmi.U_RMIServer_Stub, "org.omg.stub.javax.management.remote.rmi._RMIServer_Stub");
pragma Extensions_Allowed (Off);
