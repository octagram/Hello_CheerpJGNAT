pragma Extensions_Allowed (On);
package Org.Omg.Stub.Javax.Management.Remote.Rmi is
   pragma Preelaborate;
end Org.Omg.Stub.Javax.Management.Remote.Rmi;
pragma Import (Java, Org.Omg.Stub.Javax.Management.Remote.Rmi, "org.omg.stub.javax.management.remote.rmi");
pragma Extensions_Allowed (Off);
