pragma Extensions_Allowed (On);
package Org.Omg.Stub.Javax.Management.Remote is
   pragma Preelaborate;
end Org.Omg.Stub.Javax.Management.Remote;
pragma Import (Java, Org.Omg.Stub.Javax.Management.Remote, "org.omg.stub.javax.management.remote");
pragma Extensions_Allowed (Off);
