pragma Extensions_Allowed (On);
package Org.Omg.Stub.Javax.Management is
   pragma Preelaborate;
end Org.Omg.Stub.Javax.Management;
pragma Import (Java, Org.Omg.Stub.Javax.Management, "org.omg.stub.javax.management");
pragma Extensions_Allowed (Off);
