pragma Extensions_Allowed (On);
package Org.Omg.Stub.Javax is
   pragma Preelaborate;
end Org.Omg.Stub.Javax;
pragma Import (Java, Org.Omg.Stub.Javax, "org.omg.stub.javax");
pragma Extensions_Allowed (Off);
