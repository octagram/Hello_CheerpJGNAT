pragma Extensions_Allowed (On);
package Org.Omg.Stub is
   pragma Preelaborate;
end Org.Omg.Stub;
pragma Import (Java, Org.Omg.Stub, "org.omg.stub");
pragma Extensions_Allowed (Off);
