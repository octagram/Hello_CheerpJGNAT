pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Element;
limited with Org.W3c.Dom.TypeInfo;
with Java.Lang.Object;
with Org.W3c.Dom.Node;

package Org.W3c.Dom.Attr is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Node_I : Org.W3c.Dom.Node.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetSpecified (This : access Typ)
                          return Java.Boolean is abstract;

   function GetValue (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetValue (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetOwnerElement (This : access Typ)
                             return access Org.W3c.Dom.Element.Typ'Class is abstract;

   function GetSchemaTypeInfo (This : access Typ)
                               return access Org.W3c.Dom.TypeInfo.Typ'Class is abstract;

   function IsId (This : access Typ)
                  return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, GetSpecified, "getSpecified");
   pragma Export (Java, GetValue, "getValue");
   pragma Export (Java, SetValue, "setValue");
   pragma Export (Java, GetOwnerElement, "getOwnerElement");
   pragma Export (Java, GetSchemaTypeInfo, "getSchemaTypeInfo");
   pragma Export (Java, IsId, "isId");

end Org.W3c.Dom.Attr;
pragma Import (Java, Org.W3c.Dom.Attr, "org.w3c.dom.Attr");
pragma Extensions_Allowed (Off);
