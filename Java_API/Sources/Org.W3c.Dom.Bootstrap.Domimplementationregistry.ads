pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.DOMImplementation;
limited with Org.W3c.Dom.DOMImplementationList;
limited with Org.W3c.Dom.DOMImplementationSource;
with Java.Lang.Object;

package Org.W3c.Dom.Bootstrap.DOMImplementationRegistry is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function NewInstance return access Org.W3c.Dom.Bootstrap.DOMImplementationRegistry.Typ'Class;
   --  can raise Java.Lang.ClassNotFoundException.Except,
   --  Java.Lang.InstantiationException.Except,
   --  Java.Lang.IllegalAccessException.Except and
   --  Java.Lang.ClassCastException.Except

   function GetDOMImplementation (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Org.W3c.Dom.DOMImplementation.Typ'Class;

   function GetDOMImplementationList (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                                      return access Org.W3c.Dom.DOMImplementationList.Typ'Class;

   procedure AddSource (This : access Typ;
                        P1_DOMImplementationSource : access Standard.Org.W3c.Dom.DOMImplementationSource.Typ'Class);

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   PROPERTY : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, NewInstance, "newInstance");
   pragma Import (Java, GetDOMImplementation, "getDOMImplementation");
   pragma Import (Java, GetDOMImplementationList, "getDOMImplementationList");
   pragma Import (Java, AddSource, "addSource");
   pragma Import (Java, PROPERTY, "PROPERTY");

end Org.W3c.Dom.Bootstrap.DOMImplementationRegistry;
pragma Import (Java, Org.W3c.Dom.Bootstrap.DOMImplementationRegistry, "org.w3c.dom.bootstrap.DOMImplementationRegistry");
pragma Extensions_Allowed (Off);
