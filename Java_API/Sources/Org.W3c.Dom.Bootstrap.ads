pragma Extensions_Allowed (On);
package Org.W3c.Dom.Bootstrap is
   pragma Preelaborate;
end Org.W3c.Dom.Bootstrap;
pragma Import (Java, Org.W3c.Dom.Bootstrap, "org.w3c.dom.bootstrap");
pragma Extensions_Allowed (Off);
