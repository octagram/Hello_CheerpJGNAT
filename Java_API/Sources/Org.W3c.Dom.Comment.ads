pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.W3c.Dom.CharacterData;

package Org.W3c.Dom.Comment is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            CharacterData_I : Org.W3c.Dom.CharacterData.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   
private
   pragma Convention (Java, Typ);

end Org.W3c.Dom.Comment;
pragma Import (Java, Org.W3c.Dom.Comment, "org.w3c.dom.Comment");
pragma Extensions_Allowed (Off);
