pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Org.W3c.Dom.Css.CSS2Properties is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAzimuth (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAzimuth (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBackground (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBackground (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBackgroundAttachment (This : access Typ)
                                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBackgroundAttachment (This : access Typ;
                                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBackgroundColor (This : access Typ)
                                return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBackgroundColor (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBackgroundImage (This : access Typ)
                                return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBackgroundImage (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBackgroundPosition (This : access Typ)
                                   return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBackgroundPosition (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBackgroundRepeat (This : access Typ)
                                 return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBackgroundRepeat (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorder (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorder (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderCollapse (This : access Typ)
                               return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderCollapse (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderColor (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderColor (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderSpacing (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderSpacing (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderStyle (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderStyle (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderTop (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderTop (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderRight (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderRight (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderBottom (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderBottom (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderLeft (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderLeft (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderTopColor (This : access Typ)
                               return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderTopColor (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderRightColor (This : access Typ)
                                 return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderRightColor (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderBottomColor (This : access Typ)
                                  return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderBottomColor (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderLeftColor (This : access Typ)
                                return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderLeftColor (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderTopStyle (This : access Typ)
                               return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderTopStyle (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderRightStyle (This : access Typ)
                                 return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderRightStyle (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderBottomStyle (This : access Typ)
                                  return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderBottomStyle (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderLeftStyle (This : access Typ)
                                return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderLeftStyle (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderTopWidth (This : access Typ)
                               return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderTopWidth (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderRightWidth (This : access Typ)
                                 return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderRightWidth (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderBottomWidth (This : access Typ)
                                  return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderBottomWidth (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderLeftWidth (This : access Typ)
                                return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderLeftWidth (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBorderWidth (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorderWidth (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetBottom (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBottom (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetCaptionSide (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCaptionSide (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetClear (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetClear (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetClip (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetClip (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetColor (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetColor (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetContent (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetContent (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetCounterIncrement (This : access Typ)
                                 return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCounterIncrement (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetCounterReset (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCounterReset (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetCue (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCue (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetCueAfter (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCueAfter (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetCueBefore (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCueBefore (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetCursor (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCursor (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetDirection (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetDirection (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetDisplay (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetDisplay (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetElevation (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetElevation (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetEmptyCells (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetEmptyCells (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetCssFloat (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCssFloat (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetFont (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetFont (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetFontFamily (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetFontFamily (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetFontSize (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetFontSize (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetFontSizeAdjust (This : access Typ)
                               return access Java.Lang.String.Typ'Class is abstract;

   procedure SetFontSizeAdjust (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetFontStretch (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetFontStretch (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetFontStyle (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetFontStyle (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetFontVariant (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetFontVariant (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetFontWeight (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetFontWeight (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetHeight (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetHeight (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetLeft (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetLeft (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetLetterSpacing (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;

   procedure SetLetterSpacing (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetLineHeight (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetLineHeight (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetListStyle (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetListStyle (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetListStyleImage (This : access Typ)
                               return access Java.Lang.String.Typ'Class is abstract;

   procedure SetListStyleImage (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetListStylePosition (This : access Typ)
                                  return access Java.Lang.String.Typ'Class is abstract;

   procedure SetListStylePosition (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetListStyleType (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;

   procedure SetListStyleType (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetMargin (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetMargin (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetMarginTop (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetMarginTop (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetMarginRight (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetMarginRight (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetMarginBottom (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   procedure SetMarginBottom (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetMarginLeft (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetMarginLeft (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetMarkerOffset (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   procedure SetMarkerOffset (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetMarks (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetMarks (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetMaxHeight (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetMaxHeight (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetMaxWidth (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetMaxWidth (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetMinHeight (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetMinHeight (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetMinWidth (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetMinWidth (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetOrphans (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetOrphans (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetOutline (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetOutline (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetOutlineColor (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   procedure SetOutlineColor (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetOutlineStyle (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   procedure SetOutlineStyle (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetOutlineWidth (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   procedure SetOutlineWidth (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetOverflow (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetOverflow (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPadding (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPadding (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPaddingTop (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPaddingTop (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPaddingRight (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPaddingRight (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPaddingBottom (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPaddingBottom (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPaddingLeft (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPaddingLeft (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPage (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPage (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPageBreakAfter (This : access Typ)
                               return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPageBreakAfter (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPageBreakBefore (This : access Typ)
                                return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPageBreakBefore (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPageBreakInside (This : access Typ)
                                return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPageBreakInside (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPause (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPause (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPauseAfter (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPauseAfter (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPauseBefore (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPauseBefore (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPitch (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPitch (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPitchRange (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPitchRange (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPlayDuring (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPlayDuring (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPosition (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPosition (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetQuotes (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetQuotes (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetRichness (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetRichness (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetRight (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetRight (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetSize (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetSize (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetSpeak (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetSpeak (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetSpeakHeader (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetSpeakHeader (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetSpeakNumeral (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   procedure SetSpeakNumeral (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetSpeakPunctuation (This : access Typ)
                                 return access Java.Lang.String.Typ'Class is abstract;

   procedure SetSpeakPunctuation (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetSpeechRate (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetSpeechRate (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetStress (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetStress (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetTableLayout (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetTableLayout (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetTextAlign (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetTextAlign (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetTextDecoration (This : access Typ)
                               return access Java.Lang.String.Typ'Class is abstract;

   procedure SetTextDecoration (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetTextIndent (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetTextIndent (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetTextShadow (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetTextShadow (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetTextTransform (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;

   procedure SetTextTransform (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetTop (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   procedure SetTop (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetUnicodeBidi (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetUnicodeBidi (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetVerticalAlign (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;

   procedure SetVerticalAlign (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetVisibility (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetVisibility (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetVoiceFamily (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetVoiceFamily (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetVolume (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetVolume (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetWhiteSpace (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetWhiteSpace (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetWidows (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetWidows (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetWidth (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetWidth (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetWordSpacing (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetWordSpacing (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetZIndex (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetZIndex (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAzimuth, "getAzimuth");
   pragma Export (Java, SetAzimuth, "setAzimuth");
   pragma Export (Java, GetBackground, "getBackground");
   pragma Export (Java, SetBackground, "setBackground");
   pragma Export (Java, GetBackgroundAttachment, "getBackgroundAttachment");
   pragma Export (Java, SetBackgroundAttachment, "setBackgroundAttachment");
   pragma Export (Java, GetBackgroundColor, "getBackgroundColor");
   pragma Export (Java, SetBackgroundColor, "setBackgroundColor");
   pragma Export (Java, GetBackgroundImage, "getBackgroundImage");
   pragma Export (Java, SetBackgroundImage, "setBackgroundImage");
   pragma Export (Java, GetBackgroundPosition, "getBackgroundPosition");
   pragma Export (Java, SetBackgroundPosition, "setBackgroundPosition");
   pragma Export (Java, GetBackgroundRepeat, "getBackgroundRepeat");
   pragma Export (Java, SetBackgroundRepeat, "setBackgroundRepeat");
   pragma Export (Java, GetBorder, "getBorder");
   pragma Export (Java, SetBorder, "setBorder");
   pragma Export (Java, GetBorderCollapse, "getBorderCollapse");
   pragma Export (Java, SetBorderCollapse, "setBorderCollapse");
   pragma Export (Java, GetBorderColor, "getBorderColor");
   pragma Export (Java, SetBorderColor, "setBorderColor");
   pragma Export (Java, GetBorderSpacing, "getBorderSpacing");
   pragma Export (Java, SetBorderSpacing, "setBorderSpacing");
   pragma Export (Java, GetBorderStyle, "getBorderStyle");
   pragma Export (Java, SetBorderStyle, "setBorderStyle");
   pragma Export (Java, GetBorderTop, "getBorderTop");
   pragma Export (Java, SetBorderTop, "setBorderTop");
   pragma Export (Java, GetBorderRight, "getBorderRight");
   pragma Export (Java, SetBorderRight, "setBorderRight");
   pragma Export (Java, GetBorderBottom, "getBorderBottom");
   pragma Export (Java, SetBorderBottom, "setBorderBottom");
   pragma Export (Java, GetBorderLeft, "getBorderLeft");
   pragma Export (Java, SetBorderLeft, "setBorderLeft");
   pragma Export (Java, GetBorderTopColor, "getBorderTopColor");
   pragma Export (Java, SetBorderTopColor, "setBorderTopColor");
   pragma Export (Java, GetBorderRightColor, "getBorderRightColor");
   pragma Export (Java, SetBorderRightColor, "setBorderRightColor");
   pragma Export (Java, GetBorderBottomColor, "getBorderBottomColor");
   pragma Export (Java, SetBorderBottomColor, "setBorderBottomColor");
   pragma Export (Java, GetBorderLeftColor, "getBorderLeftColor");
   pragma Export (Java, SetBorderLeftColor, "setBorderLeftColor");
   pragma Export (Java, GetBorderTopStyle, "getBorderTopStyle");
   pragma Export (Java, SetBorderTopStyle, "setBorderTopStyle");
   pragma Export (Java, GetBorderRightStyle, "getBorderRightStyle");
   pragma Export (Java, SetBorderRightStyle, "setBorderRightStyle");
   pragma Export (Java, GetBorderBottomStyle, "getBorderBottomStyle");
   pragma Export (Java, SetBorderBottomStyle, "setBorderBottomStyle");
   pragma Export (Java, GetBorderLeftStyle, "getBorderLeftStyle");
   pragma Export (Java, SetBorderLeftStyle, "setBorderLeftStyle");
   pragma Export (Java, GetBorderTopWidth, "getBorderTopWidth");
   pragma Export (Java, SetBorderTopWidth, "setBorderTopWidth");
   pragma Export (Java, GetBorderRightWidth, "getBorderRightWidth");
   pragma Export (Java, SetBorderRightWidth, "setBorderRightWidth");
   pragma Export (Java, GetBorderBottomWidth, "getBorderBottomWidth");
   pragma Export (Java, SetBorderBottomWidth, "setBorderBottomWidth");
   pragma Export (Java, GetBorderLeftWidth, "getBorderLeftWidth");
   pragma Export (Java, SetBorderLeftWidth, "setBorderLeftWidth");
   pragma Export (Java, GetBorderWidth, "getBorderWidth");
   pragma Export (Java, SetBorderWidth, "setBorderWidth");
   pragma Export (Java, GetBottom, "getBottom");
   pragma Export (Java, SetBottom, "setBottom");
   pragma Export (Java, GetCaptionSide, "getCaptionSide");
   pragma Export (Java, SetCaptionSide, "setCaptionSide");
   pragma Export (Java, GetClear, "getClear");
   pragma Export (Java, SetClear, "setClear");
   pragma Export (Java, GetClip, "getClip");
   pragma Export (Java, SetClip, "setClip");
   pragma Export (Java, GetColor, "getColor");
   pragma Export (Java, SetColor, "setColor");
   pragma Export (Java, GetContent, "getContent");
   pragma Export (Java, SetContent, "setContent");
   pragma Export (Java, GetCounterIncrement, "getCounterIncrement");
   pragma Export (Java, SetCounterIncrement, "setCounterIncrement");
   pragma Export (Java, GetCounterReset, "getCounterReset");
   pragma Export (Java, SetCounterReset, "setCounterReset");
   pragma Export (Java, GetCue, "getCue");
   pragma Export (Java, SetCue, "setCue");
   pragma Export (Java, GetCueAfter, "getCueAfter");
   pragma Export (Java, SetCueAfter, "setCueAfter");
   pragma Export (Java, GetCueBefore, "getCueBefore");
   pragma Export (Java, SetCueBefore, "setCueBefore");
   pragma Export (Java, GetCursor, "getCursor");
   pragma Export (Java, SetCursor, "setCursor");
   pragma Export (Java, GetDirection, "getDirection");
   pragma Export (Java, SetDirection, "setDirection");
   pragma Export (Java, GetDisplay, "getDisplay");
   pragma Export (Java, SetDisplay, "setDisplay");
   pragma Export (Java, GetElevation, "getElevation");
   pragma Export (Java, SetElevation, "setElevation");
   pragma Export (Java, GetEmptyCells, "getEmptyCells");
   pragma Export (Java, SetEmptyCells, "setEmptyCells");
   pragma Export (Java, GetCssFloat, "getCssFloat");
   pragma Export (Java, SetCssFloat, "setCssFloat");
   pragma Export (Java, GetFont, "getFont");
   pragma Export (Java, SetFont, "setFont");
   pragma Export (Java, GetFontFamily, "getFontFamily");
   pragma Export (Java, SetFontFamily, "setFontFamily");
   pragma Export (Java, GetFontSize, "getFontSize");
   pragma Export (Java, SetFontSize, "setFontSize");
   pragma Export (Java, GetFontSizeAdjust, "getFontSizeAdjust");
   pragma Export (Java, SetFontSizeAdjust, "setFontSizeAdjust");
   pragma Export (Java, GetFontStretch, "getFontStretch");
   pragma Export (Java, SetFontStretch, "setFontStretch");
   pragma Export (Java, GetFontStyle, "getFontStyle");
   pragma Export (Java, SetFontStyle, "setFontStyle");
   pragma Export (Java, GetFontVariant, "getFontVariant");
   pragma Export (Java, SetFontVariant, "setFontVariant");
   pragma Export (Java, GetFontWeight, "getFontWeight");
   pragma Export (Java, SetFontWeight, "setFontWeight");
   pragma Export (Java, GetHeight, "getHeight");
   pragma Export (Java, SetHeight, "setHeight");
   pragma Export (Java, GetLeft, "getLeft");
   pragma Export (Java, SetLeft, "setLeft");
   pragma Export (Java, GetLetterSpacing, "getLetterSpacing");
   pragma Export (Java, SetLetterSpacing, "setLetterSpacing");
   pragma Export (Java, GetLineHeight, "getLineHeight");
   pragma Export (Java, SetLineHeight, "setLineHeight");
   pragma Export (Java, GetListStyle, "getListStyle");
   pragma Export (Java, SetListStyle, "setListStyle");
   pragma Export (Java, GetListStyleImage, "getListStyleImage");
   pragma Export (Java, SetListStyleImage, "setListStyleImage");
   pragma Export (Java, GetListStylePosition, "getListStylePosition");
   pragma Export (Java, SetListStylePosition, "setListStylePosition");
   pragma Export (Java, GetListStyleType, "getListStyleType");
   pragma Export (Java, SetListStyleType, "setListStyleType");
   pragma Export (Java, GetMargin, "getMargin");
   pragma Export (Java, SetMargin, "setMargin");
   pragma Export (Java, GetMarginTop, "getMarginTop");
   pragma Export (Java, SetMarginTop, "setMarginTop");
   pragma Export (Java, GetMarginRight, "getMarginRight");
   pragma Export (Java, SetMarginRight, "setMarginRight");
   pragma Export (Java, GetMarginBottom, "getMarginBottom");
   pragma Export (Java, SetMarginBottom, "setMarginBottom");
   pragma Export (Java, GetMarginLeft, "getMarginLeft");
   pragma Export (Java, SetMarginLeft, "setMarginLeft");
   pragma Export (Java, GetMarkerOffset, "getMarkerOffset");
   pragma Export (Java, SetMarkerOffset, "setMarkerOffset");
   pragma Export (Java, GetMarks, "getMarks");
   pragma Export (Java, SetMarks, "setMarks");
   pragma Export (Java, GetMaxHeight, "getMaxHeight");
   pragma Export (Java, SetMaxHeight, "setMaxHeight");
   pragma Export (Java, GetMaxWidth, "getMaxWidth");
   pragma Export (Java, SetMaxWidth, "setMaxWidth");
   pragma Export (Java, GetMinHeight, "getMinHeight");
   pragma Export (Java, SetMinHeight, "setMinHeight");
   pragma Export (Java, GetMinWidth, "getMinWidth");
   pragma Export (Java, SetMinWidth, "setMinWidth");
   pragma Export (Java, GetOrphans, "getOrphans");
   pragma Export (Java, SetOrphans, "setOrphans");
   pragma Export (Java, GetOutline, "getOutline");
   pragma Export (Java, SetOutline, "setOutline");
   pragma Export (Java, GetOutlineColor, "getOutlineColor");
   pragma Export (Java, SetOutlineColor, "setOutlineColor");
   pragma Export (Java, GetOutlineStyle, "getOutlineStyle");
   pragma Export (Java, SetOutlineStyle, "setOutlineStyle");
   pragma Export (Java, GetOutlineWidth, "getOutlineWidth");
   pragma Export (Java, SetOutlineWidth, "setOutlineWidth");
   pragma Export (Java, GetOverflow, "getOverflow");
   pragma Export (Java, SetOverflow, "setOverflow");
   pragma Export (Java, GetPadding, "getPadding");
   pragma Export (Java, SetPadding, "setPadding");
   pragma Export (Java, GetPaddingTop, "getPaddingTop");
   pragma Export (Java, SetPaddingTop, "setPaddingTop");
   pragma Export (Java, GetPaddingRight, "getPaddingRight");
   pragma Export (Java, SetPaddingRight, "setPaddingRight");
   pragma Export (Java, GetPaddingBottom, "getPaddingBottom");
   pragma Export (Java, SetPaddingBottom, "setPaddingBottom");
   pragma Export (Java, GetPaddingLeft, "getPaddingLeft");
   pragma Export (Java, SetPaddingLeft, "setPaddingLeft");
   pragma Export (Java, GetPage, "getPage");
   pragma Export (Java, SetPage, "setPage");
   pragma Export (Java, GetPageBreakAfter, "getPageBreakAfter");
   pragma Export (Java, SetPageBreakAfter, "setPageBreakAfter");
   pragma Export (Java, GetPageBreakBefore, "getPageBreakBefore");
   pragma Export (Java, SetPageBreakBefore, "setPageBreakBefore");
   pragma Export (Java, GetPageBreakInside, "getPageBreakInside");
   pragma Export (Java, SetPageBreakInside, "setPageBreakInside");
   pragma Export (Java, GetPause, "getPause");
   pragma Export (Java, SetPause, "setPause");
   pragma Export (Java, GetPauseAfter, "getPauseAfter");
   pragma Export (Java, SetPauseAfter, "setPauseAfter");
   pragma Export (Java, GetPauseBefore, "getPauseBefore");
   pragma Export (Java, SetPauseBefore, "setPauseBefore");
   pragma Export (Java, GetPitch, "getPitch");
   pragma Export (Java, SetPitch, "setPitch");
   pragma Export (Java, GetPitchRange, "getPitchRange");
   pragma Export (Java, SetPitchRange, "setPitchRange");
   pragma Export (Java, GetPlayDuring, "getPlayDuring");
   pragma Export (Java, SetPlayDuring, "setPlayDuring");
   pragma Export (Java, GetPosition, "getPosition");
   pragma Export (Java, SetPosition, "setPosition");
   pragma Export (Java, GetQuotes, "getQuotes");
   pragma Export (Java, SetQuotes, "setQuotes");
   pragma Export (Java, GetRichness, "getRichness");
   pragma Export (Java, SetRichness, "setRichness");
   pragma Export (Java, GetRight, "getRight");
   pragma Export (Java, SetRight, "setRight");
   pragma Export (Java, GetSize, "getSize");
   pragma Export (Java, SetSize, "setSize");
   pragma Export (Java, GetSpeak, "getSpeak");
   pragma Export (Java, SetSpeak, "setSpeak");
   pragma Export (Java, GetSpeakHeader, "getSpeakHeader");
   pragma Export (Java, SetSpeakHeader, "setSpeakHeader");
   pragma Export (Java, GetSpeakNumeral, "getSpeakNumeral");
   pragma Export (Java, SetSpeakNumeral, "setSpeakNumeral");
   pragma Export (Java, GetSpeakPunctuation, "getSpeakPunctuation");
   pragma Export (Java, SetSpeakPunctuation, "setSpeakPunctuation");
   pragma Export (Java, GetSpeechRate, "getSpeechRate");
   pragma Export (Java, SetSpeechRate, "setSpeechRate");
   pragma Export (Java, GetStress, "getStress");
   pragma Export (Java, SetStress, "setStress");
   pragma Export (Java, GetTableLayout, "getTableLayout");
   pragma Export (Java, SetTableLayout, "setTableLayout");
   pragma Export (Java, GetTextAlign, "getTextAlign");
   pragma Export (Java, SetTextAlign, "setTextAlign");
   pragma Export (Java, GetTextDecoration, "getTextDecoration");
   pragma Export (Java, SetTextDecoration, "setTextDecoration");
   pragma Export (Java, GetTextIndent, "getTextIndent");
   pragma Export (Java, SetTextIndent, "setTextIndent");
   pragma Export (Java, GetTextShadow, "getTextShadow");
   pragma Export (Java, SetTextShadow, "setTextShadow");
   pragma Export (Java, GetTextTransform, "getTextTransform");
   pragma Export (Java, SetTextTransform, "setTextTransform");
   pragma Export (Java, GetTop, "getTop");
   pragma Export (Java, SetTop, "setTop");
   pragma Export (Java, GetUnicodeBidi, "getUnicodeBidi");
   pragma Export (Java, SetUnicodeBidi, "setUnicodeBidi");
   pragma Export (Java, GetVerticalAlign, "getVerticalAlign");
   pragma Export (Java, SetVerticalAlign, "setVerticalAlign");
   pragma Export (Java, GetVisibility, "getVisibility");
   pragma Export (Java, SetVisibility, "setVisibility");
   pragma Export (Java, GetVoiceFamily, "getVoiceFamily");
   pragma Export (Java, SetVoiceFamily, "setVoiceFamily");
   pragma Export (Java, GetVolume, "getVolume");
   pragma Export (Java, SetVolume, "setVolume");
   pragma Export (Java, GetWhiteSpace, "getWhiteSpace");
   pragma Export (Java, SetWhiteSpace, "setWhiteSpace");
   pragma Export (Java, GetWidows, "getWidows");
   pragma Export (Java, SetWidows, "setWidows");
   pragma Export (Java, GetWidth, "getWidth");
   pragma Export (Java, SetWidth, "setWidth");
   pragma Export (Java, GetWordSpacing, "getWordSpacing");
   pragma Export (Java, SetWordSpacing, "setWordSpacing");
   pragma Export (Java, GetZIndex, "getZIndex");
   pragma Export (Java, SetZIndex, "setZIndex");

end Org.W3c.Dom.Css.CSS2Properties;
pragma Import (Java, Org.W3c.Dom.Css.CSS2Properties, "org.w3c.dom.css.CSS2Properties");
pragma Extensions_Allowed (Off);
