pragma Extensions_Allowed (On);
limited with Org.W3c.Dom.Css.CSSStyleDeclaration;
with Java.Lang.Object;
with Org.W3c.Dom.Css.CSSRule;

package Org.W3c.Dom.Css.CSSFontFaceRule is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            CSSRule_I : Org.W3c.Dom.Css.CSSRule.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetStyle (This : access Typ)
                      return access Org.W3c.Dom.Css.CSSStyleDeclaration.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetStyle, "getStyle");

end Org.W3c.Dom.Css.CSSFontFaceRule;
pragma Import (Java, Org.W3c.Dom.Css.CSSFontFaceRule, "org.w3c.dom.css.CSSFontFaceRule");
pragma Extensions_Allowed (Off);
