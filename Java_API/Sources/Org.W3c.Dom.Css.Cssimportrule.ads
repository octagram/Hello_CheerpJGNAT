pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Css.CSSStyleSheet;
limited with Org.W3c.Dom.Stylesheets.MediaList;
with Java.Lang.Object;
with Org.W3c.Dom.Css.CSSRule;

package Org.W3c.Dom.Css.CSSImportRule is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            CSSRule_I : Org.W3c.Dom.Css.CSSRule.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetHref (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetMedia (This : access Typ)
                      return access Org.W3c.Dom.Stylesheets.MediaList.Typ'Class is abstract;

   function GetStyleSheet (This : access Typ)
                           return access Org.W3c.Dom.Css.CSSStyleSheet.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetHref, "getHref");
   pragma Export (Java, GetMedia, "getMedia");
   pragma Export (Java, GetStyleSheet, "getStyleSheet");

end Org.W3c.Dom.Css.CSSImportRule;
pragma Import (Java, Org.W3c.Dom.Css.CSSImportRule, "org.w3c.dom.css.CSSImportRule");
pragma Extensions_Allowed (Off);
