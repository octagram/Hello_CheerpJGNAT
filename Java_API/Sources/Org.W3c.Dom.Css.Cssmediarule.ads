pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Css.CSSRuleList;
limited with Org.W3c.Dom.Stylesheets.MediaList;
with Java.Lang.Object;
with Org.W3c.Dom.Css.CSSRule;

package Org.W3c.Dom.Css.CSSMediaRule is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            CSSRule_I : Org.W3c.Dom.Css.CSSRule.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMedia (This : access Typ)
                      return access Org.W3c.Dom.Stylesheets.MediaList.Typ'Class is abstract;

   function GetCssRules (This : access Typ)
                         return access Org.W3c.Dom.Css.CSSRuleList.Typ'Class is abstract;

   function InsertRule (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Int : Java.Int)
                        return Java.Int is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure DeleteRule (This : access Typ;
                         P1_Int : Java.Int) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetMedia, "getMedia");
   pragma Export (Java, GetCssRules, "getCssRules");
   pragma Export (Java, InsertRule, "insertRule");
   pragma Export (Java, DeleteRule, "deleteRule");

end Org.W3c.Dom.Css.CSSMediaRule;
pragma Import (Java, Org.W3c.Dom.Css.CSSMediaRule, "org.w3c.dom.css.CSSMediaRule");
pragma Extensions_Allowed (Off);
