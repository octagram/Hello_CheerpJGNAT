pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Css.Counter;
limited with Org.W3c.Dom.Css.RGBColor;
limited with Org.W3c.Dom.Css.Rect;
with Java.Lang.Object;
with Org.W3c.Dom.Css.CSSValue;

package Org.W3c.Dom.Css.CSSPrimitiveValue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            CSSValue_I : Org.W3c.Dom.Css.CSSValue.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPrimitiveType (This : access Typ)
                              return Java.Short is abstract;

   procedure SetFloatValue (This : access Typ;
                            P1_Short : Java.Short;
                            P2_Float : Java.Float) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetFloatValue (This : access Typ;
                           P1_Short : Java.Short)
                           return Java.Float is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure SetStringValue (This : access Typ;
                             P1_Short : Java.Short;
                             P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetStringValue (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetCounterValue (This : access Typ)
                             return access Org.W3c.Dom.Css.Counter.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetRectValue (This : access Typ)
                          return access Org.W3c.Dom.Css.Rect.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetRGBColorValue (This : access Typ)
                              return access Org.W3c.Dom.Css.RGBColor.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CSS_UNKNOWN : constant Java.Short;

   --  final
   CSS_NUMBER : constant Java.Short;

   --  final
   CSS_PERCENTAGE : constant Java.Short;

   --  final
   CSS_EMS : constant Java.Short;

   --  final
   CSS_EXS : constant Java.Short;

   --  final
   CSS_PX : constant Java.Short;

   --  final
   CSS_CM : constant Java.Short;

   --  final
   CSS_MM : constant Java.Short;

   --  final
   CSS_IN : constant Java.Short;

   --  final
   CSS_PT : constant Java.Short;

   --  final
   CSS_PC : constant Java.Short;

   --  final
   CSS_DEG : constant Java.Short;

   --  final
   CSS_RAD : constant Java.Short;

   --  final
   CSS_GRAD : constant Java.Short;

   --  final
   CSS_MS : constant Java.Short;

   --  final
   CSS_S : constant Java.Short;

   --  final
   CSS_HZ : constant Java.Short;

   --  final
   CSS_KHZ : constant Java.Short;

   --  final
   CSS_DIMENSION : constant Java.Short;

   --  final
   CSS_STRING : constant Java.Short;

   --  final
   CSS_URI : constant Java.Short;

   --  final
   CSS_IDENT : constant Java.Short;

   --  final
   CSS_ATTR : constant Java.Short;

   --  final
   CSS_COUNTER : constant Java.Short;

   --  final
   CSS_RECT : constant Java.Short;

   --  final
   CSS_RGBCOLOR : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetPrimitiveType, "getPrimitiveType");
   pragma Export (Java, SetFloatValue, "setFloatValue");
   pragma Export (Java, GetFloatValue, "getFloatValue");
   pragma Export (Java, SetStringValue, "setStringValue");
   pragma Export (Java, GetStringValue, "getStringValue");
   pragma Export (Java, GetCounterValue, "getCounterValue");
   pragma Export (Java, GetRectValue, "getRectValue");
   pragma Export (Java, GetRGBColorValue, "getRGBColorValue");
   pragma Import (Java, CSS_UNKNOWN, "CSS_UNKNOWN");
   pragma Import (Java, CSS_NUMBER, "CSS_NUMBER");
   pragma Import (Java, CSS_PERCENTAGE, "CSS_PERCENTAGE");
   pragma Import (Java, CSS_EMS, "CSS_EMS");
   pragma Import (Java, CSS_EXS, "CSS_EXS");
   pragma Import (Java, CSS_PX, "CSS_PX");
   pragma Import (Java, CSS_CM, "CSS_CM");
   pragma Import (Java, CSS_MM, "CSS_MM");
   pragma Import (Java, CSS_IN, "CSS_IN");
   pragma Import (Java, CSS_PT, "CSS_PT");
   pragma Import (Java, CSS_PC, "CSS_PC");
   pragma Import (Java, CSS_DEG, "CSS_DEG");
   pragma Import (Java, CSS_RAD, "CSS_RAD");
   pragma Import (Java, CSS_GRAD, "CSS_GRAD");
   pragma Import (Java, CSS_MS, "CSS_MS");
   pragma Import (Java, CSS_S, "CSS_S");
   pragma Import (Java, CSS_HZ, "CSS_HZ");
   pragma Import (Java, CSS_KHZ, "CSS_KHZ");
   pragma Import (Java, CSS_DIMENSION, "CSS_DIMENSION");
   pragma Import (Java, CSS_STRING, "CSS_STRING");
   pragma Import (Java, CSS_URI, "CSS_URI");
   pragma Import (Java, CSS_IDENT, "CSS_IDENT");
   pragma Import (Java, CSS_ATTR, "CSS_ATTR");
   pragma Import (Java, CSS_COUNTER, "CSS_COUNTER");
   pragma Import (Java, CSS_RECT, "CSS_RECT");
   pragma Import (Java, CSS_RGBCOLOR, "CSS_RGBCOLOR");

end Org.W3c.Dom.Css.CSSPrimitiveValue;
pragma Import (Java, Org.W3c.Dom.Css.CSSPrimitiveValue, "org.w3c.dom.css.CSSPrimitiveValue");
pragma Extensions_Allowed (Off);
