pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Css.CSSStyleSheet;
with Java.Lang.Object;

package Org.W3c.Dom.Css.CSSRule is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetType (This : access Typ)
                     return Java.Short is abstract;

   function GetCssText (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCssText (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetParentStyleSheet (This : access Typ)
                                 return access Org.W3c.Dom.Css.CSSStyleSheet.Typ'Class is abstract;

   function GetParentRule (This : access Typ)
                           return access Org.W3c.Dom.Css.CSSRule.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UNKNOWN_RULE : constant Java.Short;

   --  final
   STYLE_RULE : constant Java.Short;

   --  final
   CHARSET_RULE : constant Java.Short;

   --  final
   IMPORT_RULE : constant Java.Short;

   --  final
   MEDIA_RULE : constant Java.Short;

   --  final
   FONT_FACE_RULE : constant Java.Short;

   --  final
   PAGE_RULE : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, GetCssText, "getCssText");
   pragma Export (Java, SetCssText, "setCssText");
   pragma Export (Java, GetParentStyleSheet, "getParentStyleSheet");
   pragma Export (Java, GetParentRule, "getParentRule");
   pragma Import (Java, UNKNOWN_RULE, "UNKNOWN_RULE");
   pragma Import (Java, STYLE_RULE, "STYLE_RULE");
   pragma Import (Java, CHARSET_RULE, "CHARSET_RULE");
   pragma Import (Java, IMPORT_RULE, "IMPORT_RULE");
   pragma Import (Java, MEDIA_RULE, "MEDIA_RULE");
   pragma Import (Java, FONT_FACE_RULE, "FONT_FACE_RULE");
   pragma Import (Java, PAGE_RULE, "PAGE_RULE");

end Org.W3c.Dom.Css.CSSRule;
pragma Import (Java, Org.W3c.Dom.Css.CSSRule, "org.w3c.dom.css.CSSRule");
pragma Extensions_Allowed (Off);
