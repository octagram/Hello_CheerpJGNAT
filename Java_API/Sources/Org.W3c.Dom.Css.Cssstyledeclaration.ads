pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Css.CSSRule;
limited with Org.W3c.Dom.Css.CSSValue;
with Java.Lang.Object;

package Org.W3c.Dom.Css.CSSStyleDeclaration is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCssText (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCssText (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPropertyValue (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Java.Lang.String.Typ'Class is abstract;

   function GetPropertyCSSValue (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Org.W3c.Dom.Css.CSSValue.Typ'Class is abstract;

   function RemoveProperty (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetPropertyPriority (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Java.Lang.String.Typ'Class is abstract;

   procedure SetProperty (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class;
                          P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetLength (This : access Typ)
                       return Java.Int is abstract;

   function Item (This : access Typ;
                  P1_Int : Java.Int)
                  return access Java.Lang.String.Typ'Class is abstract;

   function GetParentRule (This : access Typ)
                           return access Org.W3c.Dom.Css.CSSRule.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetCssText, "getCssText");
   pragma Export (Java, SetCssText, "setCssText");
   pragma Export (Java, GetPropertyValue, "getPropertyValue");
   pragma Export (Java, GetPropertyCSSValue, "getPropertyCSSValue");
   pragma Export (Java, RemoveProperty, "removeProperty");
   pragma Export (Java, GetPropertyPriority, "getPropertyPriority");
   pragma Export (Java, SetProperty, "setProperty");
   pragma Export (Java, GetLength, "getLength");
   pragma Export (Java, Item, "item");
   pragma Export (Java, GetParentRule, "getParentRule");

end Org.W3c.Dom.Css.CSSStyleDeclaration;
pragma Import (Java, Org.W3c.Dom.Css.CSSStyleDeclaration, "org.w3c.dom.css.CSSStyleDeclaration");
pragma Extensions_Allowed (Off);
