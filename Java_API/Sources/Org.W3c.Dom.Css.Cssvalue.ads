pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Org.W3c.Dom.Css.CSSValue is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCssText (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCssText (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetCssValueType (This : access Typ)
                             return Java.Short is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CSS_INHERIT : constant Java.Short;

   --  final
   CSS_PRIMITIVE_VALUE : constant Java.Short;

   --  final
   CSS_VALUE_LIST : constant Java.Short;

   --  final
   CSS_CUSTOM : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetCssText, "getCssText");
   pragma Export (Java, SetCssText, "setCssText");
   pragma Export (Java, GetCssValueType, "getCssValueType");
   pragma Import (Java, CSS_INHERIT, "CSS_INHERIT");
   pragma Import (Java, CSS_PRIMITIVE_VALUE, "CSS_PRIMITIVE_VALUE");
   pragma Import (Java, CSS_VALUE_LIST, "CSS_VALUE_LIST");
   pragma Import (Java, CSS_CUSTOM, "CSS_CUSTOM");

end Org.W3c.Dom.Css.CSSValue;
pragma Import (Java, Org.W3c.Dom.Css.CSSValue, "org.w3c.dom.css.CSSValue");
pragma Extensions_Allowed (Off);
