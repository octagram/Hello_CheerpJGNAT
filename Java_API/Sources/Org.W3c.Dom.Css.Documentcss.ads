pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Css.CSSStyleDeclaration;
limited with Org.W3c.Dom.Element;
with Java.Lang.Object;
with Org.W3c.Dom.Stylesheets.DocumentStyle;

package Org.W3c.Dom.Css.DocumentCSS is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DocumentStyle_I : Org.W3c.Dom.Stylesheets.DocumentStyle.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetOverrideStyle (This : access Typ;
                              P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class;
                              P2_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Org.W3c.Dom.Css.CSSStyleDeclaration.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetOverrideStyle, "getOverrideStyle");

end Org.W3c.Dom.Css.DocumentCSS;
pragma Import (Java, Org.W3c.Dom.Css.DocumentCSS, "org.w3c.dom.css.DocumentCSS");
pragma Extensions_Allowed (Off);
