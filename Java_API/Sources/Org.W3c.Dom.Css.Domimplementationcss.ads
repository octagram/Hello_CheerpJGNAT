pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Css.CSSStyleSheet;
with Java.Lang.Object;
with Org.W3c.Dom.DOMImplementation;

package Org.W3c.Dom.Css.DOMImplementationCSS is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DOMImplementation_I : Org.W3c.Dom.DOMImplementation.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateCSSStyleSheet (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class)
                                 return access Org.W3c.Dom.Css.CSSStyleSheet.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreateCSSStyleSheet, "createCSSStyleSheet");

end Org.W3c.Dom.Css.DOMImplementationCSS;
pragma Import (Java, Org.W3c.Dom.Css.DOMImplementationCSS, "org.w3c.dom.css.DOMImplementationCSS");
pragma Extensions_Allowed (Off);
