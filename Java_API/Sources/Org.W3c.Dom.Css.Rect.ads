pragma Extensions_Allowed (On);
limited with Org.W3c.Dom.Css.CSSPrimitiveValue;
with Java.Lang.Object;

package Org.W3c.Dom.Css.Rect is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTop (This : access Typ)
                    return access Org.W3c.Dom.Css.CSSPrimitiveValue.Typ'Class is abstract;

   function GetRight (This : access Typ)
                      return access Org.W3c.Dom.Css.CSSPrimitiveValue.Typ'Class is abstract;

   function GetBottom (This : access Typ)
                       return access Org.W3c.Dom.Css.CSSPrimitiveValue.Typ'Class is abstract;

   function GetLeft (This : access Typ)
                     return access Org.W3c.Dom.Css.CSSPrimitiveValue.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetTop, "getTop");
   pragma Export (Java, GetRight, "getRight");
   pragma Export (Java, GetBottom, "getBottom");
   pragma Export (Java, GetLeft, "getLeft");

end Org.W3c.Dom.Css.Rect;
pragma Import (Java, Org.W3c.Dom.Css.Rect, "org.w3c.dom.css.Rect");
pragma Extensions_Allowed (Off);
