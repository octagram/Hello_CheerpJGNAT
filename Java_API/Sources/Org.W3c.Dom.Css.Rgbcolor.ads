pragma Extensions_Allowed (On);
limited with Org.W3c.Dom.Css.CSSPrimitiveValue;
with Java.Lang.Object;

package Org.W3c.Dom.Css.RGBColor is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRed (This : access Typ)
                    return access Org.W3c.Dom.Css.CSSPrimitiveValue.Typ'Class is abstract;

   function GetGreen (This : access Typ)
                      return access Org.W3c.Dom.Css.CSSPrimitiveValue.Typ'Class is abstract;

   function GetBlue (This : access Typ)
                     return access Org.W3c.Dom.Css.CSSPrimitiveValue.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetRed, "getRed");
   pragma Export (Java, GetGreen, "getGreen");
   pragma Export (Java, GetBlue, "getBlue");

end Org.W3c.Dom.Css.RGBColor;
pragma Import (Java, Org.W3c.Dom.Css.RGBColor, "org.w3c.dom.css.RGBColor");
pragma Extensions_Allowed (Off);
