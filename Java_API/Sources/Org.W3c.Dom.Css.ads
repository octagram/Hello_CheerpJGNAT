pragma Extensions_Allowed (On);
package Org.W3c.Dom.Css is
   pragma Preelaborate;
end Org.W3c.Dom.Css;
pragma Import (Java, Org.W3c.Dom.Css, "org.w3c.dom.css");
pragma Extensions_Allowed (Off);
