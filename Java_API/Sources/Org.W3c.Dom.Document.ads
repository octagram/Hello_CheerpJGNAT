pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Attr;
limited with Org.W3c.Dom.CDATASection;
limited with Org.W3c.Dom.Comment;
limited with Org.W3c.Dom.DOMConfiguration;
limited with Org.W3c.Dom.DOMImplementation;
limited with Org.W3c.Dom.DocumentFragment;
limited with Org.W3c.Dom.DocumentType;
limited with Org.W3c.Dom.Element;
limited with Org.W3c.Dom.EntityReference;
limited with Org.W3c.Dom.NodeList;
limited with Org.W3c.Dom.ProcessingInstruction;
limited with Org.W3c.Dom.Text;
with Java.Lang.Object;
with Org.W3c.Dom.Node;

package Org.W3c.Dom.Document is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Node_I : Org.W3c.Dom.Node.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDoctype (This : access Typ)
                        return access Org.W3c.Dom.DocumentType.Typ'Class is abstract;

   function GetImplementation (This : access Typ)
                               return access Org.W3c.Dom.DOMImplementation.Typ'Class is abstract;

   function GetDocumentElement (This : access Typ)
                                return access Org.W3c.Dom.Element.Typ'Class is abstract;

   function CreateElement (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Org.W3c.Dom.Element.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function CreateDocumentFragment (This : access Typ)
                                    return access Org.W3c.Dom.DocumentFragment.Typ'Class is abstract;

   function CreateTextNode (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Org.W3c.Dom.Text.Typ'Class is abstract;

   function CreateComment (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Org.W3c.Dom.Comment.Typ'Class is abstract;

   function CreateCDATASection (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Org.W3c.Dom.CDATASection.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function CreateProcessingInstruction (This : access Typ;
                                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                                         return access Org.W3c.Dom.ProcessingInstruction.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function CreateAttribute (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Org.W3c.Dom.Attr.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function CreateEntityReference (This : access Typ;
                                   P1_String : access Standard.Java.Lang.String.Typ'Class)
                                   return access Org.W3c.Dom.EntityReference.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetElementsByTagName (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Org.W3c.Dom.NodeList.Typ'Class is abstract;

   function ImportNode (This : access Typ;
                        P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                        P2_Boolean : Java.Boolean)
                        return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function CreateElementNS (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Org.W3c.Dom.Element.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function CreateAttributeNS (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Org.W3c.Dom.Attr.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetElementsByTagNameNS (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class)
                                    return access Org.W3c.Dom.NodeList.Typ'Class is abstract;

   function GetElementById (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Org.W3c.Dom.Element.Typ'Class is abstract;

   function GetInputEncoding (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;

   function GetXmlEncoding (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   function GetXmlStandalone (This : access Typ)
                              return Java.Boolean is abstract;

   procedure SetXmlStandalone (This : access Typ;
                               P1_Boolean : Java.Boolean) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetXmlVersion (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetXmlVersion (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetStrictErrorChecking (This : access Typ)
                                    return Java.Boolean is abstract;

   procedure SetStrictErrorChecking (This : access Typ;
                                     P1_Boolean : Java.Boolean) is abstract;

   function GetDocumentURI (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetDocumentURI (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function AdoptNode (This : access Typ;
                       P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                       return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetDomConfig (This : access Typ)
                          return access Org.W3c.Dom.DOMConfiguration.Typ'Class is abstract;

   procedure NormalizeDocument (This : access Typ) is abstract;

   function RenameNode (This : access Typ;
                        P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class;
                        P3_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetDoctype, "getDoctype");
   pragma Export (Java, GetImplementation, "getImplementation");
   pragma Export (Java, GetDocumentElement, "getDocumentElement");
   pragma Export (Java, CreateElement, "createElement");
   pragma Export (Java, CreateDocumentFragment, "createDocumentFragment");
   pragma Export (Java, CreateTextNode, "createTextNode");
   pragma Export (Java, CreateComment, "createComment");
   pragma Export (Java, CreateCDATASection, "createCDATASection");
   pragma Export (Java, CreateProcessingInstruction, "createProcessingInstruction");
   pragma Export (Java, CreateAttribute, "createAttribute");
   pragma Export (Java, CreateEntityReference, "createEntityReference");
   pragma Export (Java, GetElementsByTagName, "getElementsByTagName");
   pragma Export (Java, ImportNode, "importNode");
   pragma Export (Java, CreateElementNS, "createElementNS");
   pragma Export (Java, CreateAttributeNS, "createAttributeNS");
   pragma Export (Java, GetElementsByTagNameNS, "getElementsByTagNameNS");
   pragma Export (Java, GetElementById, "getElementById");
   pragma Export (Java, GetInputEncoding, "getInputEncoding");
   pragma Export (Java, GetXmlEncoding, "getXmlEncoding");
   pragma Export (Java, GetXmlStandalone, "getXmlStandalone");
   pragma Export (Java, SetXmlStandalone, "setXmlStandalone");
   pragma Export (Java, GetXmlVersion, "getXmlVersion");
   pragma Export (Java, SetXmlVersion, "setXmlVersion");
   pragma Export (Java, GetStrictErrorChecking, "getStrictErrorChecking");
   pragma Export (Java, SetStrictErrorChecking, "setStrictErrorChecking");
   pragma Export (Java, GetDocumentURI, "getDocumentURI");
   pragma Export (Java, SetDocumentURI, "setDocumentURI");
   pragma Export (Java, AdoptNode, "adoptNode");
   pragma Export (Java, GetDomConfig, "getDomConfig");
   pragma Export (Java, NormalizeDocument, "normalizeDocument");
   pragma Export (Java, RenameNode, "renameNode");

end Org.W3c.Dom.Document;
pragma Import (Java, Org.W3c.Dom.Document, "org.w3c.dom.Document");
pragma Extensions_Allowed (Off);
