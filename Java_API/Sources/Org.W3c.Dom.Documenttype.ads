pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.NamedNodeMap;
with Java.Lang.Object;
with Org.W3c.Dom.Node;

package Org.W3c.Dom.DocumentType is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Node_I : Org.W3c.Dom.Node.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetEntities (This : access Typ)
                         return access Org.W3c.Dom.NamedNodeMap.Typ'Class is abstract;

   function GetNotations (This : access Typ)
                          return access Org.W3c.Dom.NamedNodeMap.Typ'Class is abstract;

   function GetPublicId (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function GetSystemId (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function GetInternalSubset (This : access Typ)
                               return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, GetEntities, "getEntities");
   pragma Export (Java, GetNotations, "getNotations");
   pragma Export (Java, GetPublicId, "getPublicId");
   pragma Export (Java, GetSystemId, "getSystemId");
   pragma Export (Java, GetInternalSubset, "getInternalSubset");

end Org.W3c.Dom.DocumentType;
pragma Import (Java, Org.W3c.Dom.DocumentType, "org.w3c.dom.DocumentType");
pragma Extensions_Allowed (Off);
