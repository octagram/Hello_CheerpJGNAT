pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.DOMStringList;
with Java.Lang.Object;

package Org.W3c.Dom.DOMConfiguration is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetParameter (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetParameter (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function CanSetParameter (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Object : access Standard.Java.Lang.Object.Typ'Class)
                             return Java.Boolean is abstract;

   function GetParameterNames (This : access Typ)
                               return access Org.W3c.Dom.DOMStringList.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetParameter, "setParameter");
   pragma Export (Java, GetParameter, "getParameter");
   pragma Export (Java, CanSetParameter, "canSetParameter");
   pragma Export (Java, GetParameterNames, "getParameterNames");

end Org.W3c.Dom.DOMConfiguration;
pragma Import (Java, Org.W3c.Dom.DOMConfiguration, "org.w3c.dom.DOMConfiguration");
pragma Extensions_Allowed (Off);
