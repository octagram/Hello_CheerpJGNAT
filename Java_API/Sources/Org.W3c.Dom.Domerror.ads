pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.DOMLocator;
with Java.Lang.Object;

package Org.W3c.Dom.DOMError is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetSeverity (This : access Typ)
                         return Java.Short is abstract;

   function GetMessage (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetRelatedException (This : access Typ)
                                 return access Java.Lang.Object.Typ'Class is abstract;

   function GetRelatedData (This : access Typ)
                            return access Java.Lang.Object.Typ'Class is abstract;

   function GetLocation (This : access Typ)
                         return access Org.W3c.Dom.DOMLocator.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   SEVERITY_WARNING : constant Java.Short;

   --  final
   SEVERITY_ERROR : constant Java.Short;

   --  final
   SEVERITY_FATAL_ERROR : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetSeverity, "getSeverity");
   pragma Export (Java, GetMessage, "getMessage");
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, GetRelatedException, "getRelatedException");
   pragma Export (Java, GetRelatedData, "getRelatedData");
   pragma Export (Java, GetLocation, "getLocation");
   pragma Import (Java, SEVERITY_WARNING, "SEVERITY_WARNING");
   pragma Import (Java, SEVERITY_ERROR, "SEVERITY_ERROR");
   pragma Import (Java, SEVERITY_FATAL_ERROR, "SEVERITY_FATAL_ERROR");

end Org.W3c.Dom.DOMError;
pragma Import (Java, Org.W3c.Dom.DOMError, "org.w3c.dom.DOMError");
pragma Extensions_Allowed (Off);
