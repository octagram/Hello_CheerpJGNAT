pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Lang.RuntimeException;

package Org.W3c.Dom.DOMException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.RuntimeException.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Code : Java.Short;
      pragma Import (Java, Code, "code");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DOMException (P1_Short : Java.Short;
                              P2_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   INDEX_SIZE_ERR : constant Java.Short;

   --  final
   DOMSTRING_SIZE_ERR : constant Java.Short;

   --  final
   HIERARCHY_REQUEST_ERR : constant Java.Short;

   --  final
   WRONG_DOCUMENT_ERR : constant Java.Short;

   --  final
   INVALID_CHARACTER_ERR : constant Java.Short;

   --  final
   NO_DATA_ALLOWED_ERR : constant Java.Short;

   --  final
   NO_MODIFICATION_ALLOWED_ERR : constant Java.Short;

   --  final
   NOT_FOUND_ERR : constant Java.Short;

   --  final
   NOT_SUPPORTED_ERR : constant Java.Short;

   --  final
   INUSE_ATTRIBUTE_ERR : constant Java.Short;

   --  final
   INVALID_STATE_ERR : constant Java.Short;

   --  final
   SYNTAX_ERR : constant Java.Short;

   --  final
   INVALID_MODIFICATION_ERR : constant Java.Short;

   --  final
   NAMESPACE_ERR : constant Java.Short;

   --  final
   INVALID_ACCESS_ERR : constant Java.Short;

   --  final
   VALIDATION_ERR : constant Java.Short;

   --  final
   TYPE_MISMATCH_ERR : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "org.w3c.dom.DOMException");
   pragma Java_Constructor (New_DOMException);
   pragma Import (Java, INDEX_SIZE_ERR, "INDEX_SIZE_ERR");
   pragma Import (Java, DOMSTRING_SIZE_ERR, "DOMSTRING_SIZE_ERR");
   pragma Import (Java, HIERARCHY_REQUEST_ERR, "HIERARCHY_REQUEST_ERR");
   pragma Import (Java, WRONG_DOCUMENT_ERR, "WRONG_DOCUMENT_ERR");
   pragma Import (Java, INVALID_CHARACTER_ERR, "INVALID_CHARACTER_ERR");
   pragma Import (Java, NO_DATA_ALLOWED_ERR, "NO_DATA_ALLOWED_ERR");
   pragma Import (Java, NO_MODIFICATION_ALLOWED_ERR, "NO_MODIFICATION_ALLOWED_ERR");
   pragma Import (Java, NOT_FOUND_ERR, "NOT_FOUND_ERR");
   pragma Import (Java, NOT_SUPPORTED_ERR, "NOT_SUPPORTED_ERR");
   pragma Import (Java, INUSE_ATTRIBUTE_ERR, "INUSE_ATTRIBUTE_ERR");
   pragma Import (Java, INVALID_STATE_ERR, "INVALID_STATE_ERR");
   pragma Import (Java, SYNTAX_ERR, "SYNTAX_ERR");
   pragma Import (Java, INVALID_MODIFICATION_ERR, "INVALID_MODIFICATION_ERR");
   pragma Import (Java, NAMESPACE_ERR, "NAMESPACE_ERR");
   pragma Import (Java, INVALID_ACCESS_ERR, "INVALID_ACCESS_ERR");
   pragma Import (Java, VALIDATION_ERR, "VALIDATION_ERR");
   pragma Import (Java, TYPE_MISMATCH_ERR, "TYPE_MISMATCH_ERR");

end Org.W3c.Dom.DOMException;
pragma Import (Java, Org.W3c.Dom.DOMException, "org.w3c.dom.DOMException");
pragma Extensions_Allowed (Off);
