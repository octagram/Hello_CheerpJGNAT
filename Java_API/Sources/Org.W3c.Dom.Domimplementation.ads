pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Document;
limited with Org.W3c.Dom.DocumentType;
with Java.Lang.Object;

package Org.W3c.Dom.DOMImplementation is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function HasFeature (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean is abstract;

   function CreateDocumentType (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class;
                                P3_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Org.W3c.Dom.DocumentType.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function CreateDocument (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                            P3_DocumentType : access Standard.Org.W3c.Dom.DocumentType.Typ'Class)
                            return access Org.W3c.Dom.Document.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetFeature (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Lang.Object.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, HasFeature, "hasFeature");
   pragma Export (Java, CreateDocumentType, "createDocumentType");
   pragma Export (Java, CreateDocument, "createDocument");
   pragma Export (Java, GetFeature, "getFeature");

end Org.W3c.Dom.DOMImplementation;
pragma Import (Java, Org.W3c.Dom.DOMImplementation, "org.w3c.dom.DOMImplementation");
pragma Extensions_Allowed (Off);
