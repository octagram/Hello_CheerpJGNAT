pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;

package Org.W3c.Dom.DOMLocator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLineNumber (This : access Typ)
                           return Java.Int is abstract;

   function GetColumnNumber (This : access Typ)
                             return Java.Int is abstract;

   function GetByteOffset (This : access Typ)
                           return Java.Int is abstract;

   function GetUtf16Offset (This : access Typ)
                            return Java.Int is abstract;

   function GetRelatedNode (This : access Typ)
                            return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function GetUri (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetLineNumber, "getLineNumber");
   pragma Export (Java, GetColumnNumber, "getColumnNumber");
   pragma Export (Java, GetByteOffset, "getByteOffset");
   pragma Export (Java, GetUtf16Offset, "getUtf16Offset");
   pragma Export (Java, GetRelatedNode, "getRelatedNode");
   pragma Export (Java, GetUri, "getUri");

end Org.W3c.Dom.DOMLocator;
pragma Import (Java, Org.W3c.Dom.DOMLocator, "org.w3c.dom.DOMLocator");
pragma Extensions_Allowed (Off);
