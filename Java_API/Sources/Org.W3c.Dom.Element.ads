pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Attr;
limited with Org.W3c.Dom.NodeList;
limited with Org.W3c.Dom.TypeInfo;
with Java.Lang.Object;
with Org.W3c.Dom.Node;

package Org.W3c.Dom.Element is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Node_I : Org.W3c.Dom.Node.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTagName (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   function GetAttribute (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAttribute (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure RemoveAttribute (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetAttributeNode (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Org.W3c.Dom.Attr.Typ'Class is abstract;

   function SetAttributeNode (This : access Typ;
                              P1_Attr : access Standard.Org.W3c.Dom.Attr.Typ'Class)
                              return access Org.W3c.Dom.Attr.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function RemoveAttributeNode (This : access Typ;
                                 P1_Attr : access Standard.Org.W3c.Dom.Attr.Typ'Class)
                                 return access Org.W3c.Dom.Attr.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetElementsByTagName (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class)
                                  return access Org.W3c.Dom.NodeList.Typ'Class is abstract;

   function GetAttributeNS (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure SetAttributeNS (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure RemoveAttributeNS (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetAttributeNodeNS (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Org.W3c.Dom.Attr.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function SetAttributeNodeNS (This : access Typ;
                                P1_Attr : access Standard.Org.W3c.Dom.Attr.Typ'Class)
                                return access Org.W3c.Dom.Attr.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetElementsByTagNameNS (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class)
                                    return access Org.W3c.Dom.NodeList.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function HasAttribute (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return Java.Boolean is abstract;

   function HasAttributeNS (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class)
                            return Java.Boolean is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetSchemaTypeInfo (This : access Typ)
                               return access Org.W3c.Dom.TypeInfo.Typ'Class is abstract;

   procedure SetIdAttribute (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Boolean : Java.Boolean) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure SetIdAttributeNS (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class;
                               P3_Boolean : Java.Boolean) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure SetIdAttributeNode (This : access Typ;
                                 P1_Attr : access Standard.Org.W3c.Dom.Attr.Typ'Class;
                                 P2_Boolean : Java.Boolean) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetTagName, "getTagName");
   pragma Export (Java, GetAttribute, "getAttribute");
   pragma Export (Java, SetAttribute, "setAttribute");
   pragma Export (Java, RemoveAttribute, "removeAttribute");
   pragma Export (Java, GetAttributeNode, "getAttributeNode");
   pragma Export (Java, SetAttributeNode, "setAttributeNode");
   pragma Export (Java, RemoveAttributeNode, "removeAttributeNode");
   pragma Export (Java, GetElementsByTagName, "getElementsByTagName");
   pragma Export (Java, GetAttributeNS, "getAttributeNS");
   pragma Export (Java, SetAttributeNS, "setAttributeNS");
   pragma Export (Java, RemoveAttributeNS, "removeAttributeNS");
   pragma Export (Java, GetAttributeNodeNS, "getAttributeNodeNS");
   pragma Export (Java, SetAttributeNodeNS, "setAttributeNodeNS");
   pragma Export (Java, GetElementsByTagNameNS, "getElementsByTagNameNS");
   pragma Export (Java, HasAttribute, "hasAttribute");
   pragma Export (Java, HasAttributeNS, "hasAttributeNS");
   pragma Export (Java, GetSchemaTypeInfo, "getSchemaTypeInfo");
   pragma Export (Java, SetIdAttribute, "setIdAttribute");
   pragma Export (Java, SetIdAttributeNS, "setIdAttributeNS");
   pragma Export (Java, SetIdAttributeNode, "setIdAttributeNode");

end Org.W3c.Dom.Element;
pragma Import (Java, Org.W3c.Dom.Element, "org.w3c.dom.Element");
pragma Extensions_Allowed (Off);
