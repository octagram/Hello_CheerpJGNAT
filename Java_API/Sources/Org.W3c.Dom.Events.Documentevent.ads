pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Events.Event;
with Java.Lang.Object;

package Org.W3c.Dom.Events.DocumentEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateEvent (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Org.W3c.Dom.Events.Event.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreateEvent, "createEvent");

end Org.W3c.Dom.Events.DocumentEvent;
pragma Import (Java, Org.W3c.Dom.Events.DocumentEvent, "org.w3c.dom.events.DocumentEvent");
pragma Extensions_Allowed (Off);
