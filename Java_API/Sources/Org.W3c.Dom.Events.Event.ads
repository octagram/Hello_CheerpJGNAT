pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Events.EventTarget;
with Java.Lang.Object;

package Org.W3c.Dom.Events.Event is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetTarget (This : access Typ)
                       return access Org.W3c.Dom.Events.EventTarget.Typ'Class is abstract;

   function GetCurrentTarget (This : access Typ)
                              return access Org.W3c.Dom.Events.EventTarget.Typ'Class is abstract;

   function GetEventPhase (This : access Typ)
                           return Java.Short is abstract;

   function GetBubbles (This : access Typ)
                        return Java.Boolean is abstract;

   function GetCancelable (This : access Typ)
                           return Java.Boolean is abstract;

   function GetTimeStamp (This : access Typ)
                          return Java.Long is abstract;

   procedure StopPropagation (This : access Typ) is abstract;

   procedure PreventDefault (This : access Typ) is abstract;

   procedure InitEvent (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_Boolean : Java.Boolean;
                        P3_Boolean : Java.Boolean) is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   CAPTURING_PHASE : constant Java.Short;

   --  final
   AT_TARGET : constant Java.Short;

   --  final
   BUBBLING_PHASE : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, GetTarget, "getTarget");
   pragma Export (Java, GetCurrentTarget, "getCurrentTarget");
   pragma Export (Java, GetEventPhase, "getEventPhase");
   pragma Export (Java, GetBubbles, "getBubbles");
   pragma Export (Java, GetCancelable, "getCancelable");
   pragma Export (Java, GetTimeStamp, "getTimeStamp");
   pragma Export (Java, StopPropagation, "stopPropagation");
   pragma Export (Java, PreventDefault, "preventDefault");
   pragma Export (Java, InitEvent, "initEvent");
   pragma Import (Java, CAPTURING_PHASE, "CAPTURING_PHASE");
   pragma Import (Java, AT_TARGET, "AT_TARGET");
   pragma Import (Java, BUBBLING_PHASE, "BUBBLING_PHASE");

end Org.W3c.Dom.Events.Event;
pragma Import (Java, Org.W3c.Dom.Events.Event, "org.w3c.dom.events.Event");
pragma Extensions_Allowed (Off);
