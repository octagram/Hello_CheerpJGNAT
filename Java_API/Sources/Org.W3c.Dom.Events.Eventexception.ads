pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Lang.RuntimeException;

package Org.W3c.Dom.Events.EventException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.RuntimeException.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Code : Java.Short;
      pragma Import (Java, Code, "code");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_EventException (P1_Short : Java.Short;
                                P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   UNSPECIFIED_EVENT_TYPE_ERR : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "org.w3c.dom.events.EventException");
   pragma Java_Constructor (New_EventException);
   pragma Import (Java, UNSPECIFIED_EVENT_TYPE_ERR, "UNSPECIFIED_EVENT_TYPE_ERR");

end Org.W3c.Dom.Events.EventException;
pragma Import (Java, Org.W3c.Dom.Events.EventException, "org.w3c.dom.events.EventException");
pragma Extensions_Allowed (Off);
