pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Events.Event;
limited with Org.W3c.Dom.Events.EventListener;
with Java.Lang.Object;

package Org.W3c.Dom.Events.EventTarget is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure AddEventListener (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_EventListener : access Standard.Org.W3c.Dom.Events.EventListener.Typ'Class;
                               P3_Boolean : Java.Boolean) is abstract;

   procedure RemoveEventListener (This : access Typ;
                                  P1_String : access Standard.Java.Lang.String.Typ'Class;
                                  P2_EventListener : access Standard.Org.W3c.Dom.Events.EventListener.Typ'Class;
                                  P3_Boolean : Java.Boolean) is abstract;

   function DispatchEvent (This : access Typ;
                           P1_Event : access Standard.Org.W3c.Dom.Events.Event.Typ'Class)
                           return Java.Boolean is abstract;
   --  can raise Org.W3c.Dom.Events.EventException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AddEventListener, "addEventListener");
   pragma Export (Java, RemoveEventListener, "removeEventListener");
   pragma Export (Java, DispatchEvent, "dispatchEvent");

end Org.W3c.Dom.Events.EventTarget;
pragma Import (Java, Org.W3c.Dom.Events.EventTarget, "org.w3c.dom.events.EventTarget");
pragma Extensions_Allowed (Off);
