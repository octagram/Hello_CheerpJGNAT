pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Events.EventTarget;
limited with Org.W3c.Dom.Views.AbstractView;
with Java.Lang.Object;
with Org.W3c.Dom.Events.UIEvent;

package Org.W3c.Dom.Events.MouseEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            UIEvent_I : Org.W3c.Dom.Events.UIEvent.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetScreenX (This : access Typ)
                        return Java.Int is abstract;

   function GetScreenY (This : access Typ)
                        return Java.Int is abstract;

   function GetClientX (This : access Typ)
                        return Java.Int is abstract;

   function GetClientY (This : access Typ)
                        return Java.Int is abstract;

   function GetCtrlKey (This : access Typ)
                        return Java.Boolean is abstract;

   function GetShiftKey (This : access Typ)
                         return Java.Boolean is abstract;

   function GetAltKey (This : access Typ)
                       return Java.Boolean is abstract;

   function GetMetaKey (This : access Typ)
                        return Java.Boolean is abstract;

   function GetButton (This : access Typ)
                       return Java.Short is abstract;

   function GetRelatedTarget (This : access Typ)
                              return access Org.W3c.Dom.Events.EventTarget.Typ'Class is abstract;

   procedure InitMouseEvent (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_Boolean : Java.Boolean;
                             P3_Boolean : Java.Boolean;
                             P4_AbstractView : access Standard.Org.W3c.Dom.Views.AbstractView.Typ'Class;
                             P5_Int : Java.Int;
                             P6_Int : Java.Int;
                             P7_Int : Java.Int;
                             P8_Int : Java.Int;
                             P9_Int : Java.Int;
                             P10_Boolean : Java.Boolean;
                             P11_Boolean : Java.Boolean;
                             P12_Boolean : Java.Boolean;
                             P13_Boolean : Java.Boolean;
                             P14_Short : Java.Short;
                             P15_EventTarget : access Standard.Org.W3c.Dom.Events.EventTarget.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetScreenX, "getScreenX");
   pragma Export (Java, GetScreenY, "getScreenY");
   pragma Export (Java, GetClientX, "getClientX");
   pragma Export (Java, GetClientY, "getClientY");
   pragma Export (Java, GetCtrlKey, "getCtrlKey");
   pragma Export (Java, GetShiftKey, "getShiftKey");
   pragma Export (Java, GetAltKey, "getAltKey");
   pragma Export (Java, GetMetaKey, "getMetaKey");
   pragma Export (Java, GetButton, "getButton");
   pragma Export (Java, GetRelatedTarget, "getRelatedTarget");
   pragma Export (Java, InitMouseEvent, "initMouseEvent");

end Org.W3c.Dom.Events.MouseEvent;
pragma Import (Java, Org.W3c.Dom.Events.MouseEvent, "org.w3c.dom.events.MouseEvent");
pragma Extensions_Allowed (Off);
