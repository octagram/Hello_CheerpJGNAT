pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;
with Org.W3c.Dom.Events.Event;

package Org.W3c.Dom.Events.MutationEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Event_I : Org.W3c.Dom.Events.Event.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRelatedNode (This : access Typ)
                            return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function GetPrevValue (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   function GetNewValue (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function GetAttrName (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function GetAttrChange (This : access Typ)
                           return Java.Short is abstract;

   procedure InitMutationEvent (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class;
                                P2_Boolean : Java.Boolean;
                                P3_Boolean : Java.Boolean;
                                P4_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                                P5_String : access Standard.Java.Lang.String.Typ'Class;
                                P6_String : access Standard.Java.Lang.String.Typ'Class;
                                P7_String : access Standard.Java.Lang.String.Typ'Class;
                                P8_Short : Java.Short) is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MODIFICATION : constant Java.Short;

   --  final
   ADDITION : constant Java.Short;

   --  final
   REMOVAL : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetRelatedNode, "getRelatedNode");
   pragma Export (Java, GetPrevValue, "getPrevValue");
   pragma Export (Java, GetNewValue, "getNewValue");
   pragma Export (Java, GetAttrName, "getAttrName");
   pragma Export (Java, GetAttrChange, "getAttrChange");
   pragma Export (Java, InitMutationEvent, "initMutationEvent");
   pragma Import (Java, MODIFICATION, "MODIFICATION");
   pragma Import (Java, ADDITION, "ADDITION");
   pragma Import (Java, REMOVAL, "REMOVAL");

end Org.W3c.Dom.Events.MutationEvent;
pragma Import (Java, Org.W3c.Dom.Events.MutationEvent, "org.w3c.dom.events.MutationEvent");
pragma Extensions_Allowed (Off);
