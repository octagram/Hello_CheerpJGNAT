pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Views.AbstractView;
with Java.Lang.Object;
with Org.W3c.Dom.Events.Event;

package Org.W3c.Dom.Events.UIEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Event_I : Org.W3c.Dom.Events.Event.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetView (This : access Typ)
                     return access Org.W3c.Dom.Views.AbstractView.Typ'Class is abstract;

   function GetDetail (This : access Typ)
                       return Java.Int is abstract;

   procedure InitUIEvent (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Boolean : Java.Boolean;
                          P3_Boolean : Java.Boolean;
                          P4_AbstractView : access Standard.Org.W3c.Dom.Views.AbstractView.Typ'Class;
                          P5_Int : Java.Int) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetView, "getView");
   pragma Export (Java, GetDetail, "getDetail");
   pragma Export (Java, InitUIEvent, "initUIEvent");

end Org.W3c.Dom.Events.UIEvent;
pragma Import (Java, Org.W3c.Dom.Events.UIEvent, "org.w3c.dom.events.UIEvent");
pragma Extensions_Allowed (Off);
