pragma Extensions_Allowed (On);
package Org.W3c.Dom.Events is
   pragma Preelaborate;
end Org.W3c.Dom.Events;
pragma Import (Java, Org.W3c.Dom.Events, "org.w3c.dom.events");
pragma Extensions_Allowed (Off);
