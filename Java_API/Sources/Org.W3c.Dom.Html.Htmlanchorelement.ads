pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLAnchorElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAccessKey (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAccessKey (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetCharset (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCharset (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetCoords (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCoords (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetHref (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetHref (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetHreflang (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetHreflang (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetName (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetRel (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   procedure SetRel (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetRev (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   procedure SetRev (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetShape (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetShape (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetTabIndex (This : access Typ)
                         return Java.Int is abstract;

   procedure SetTabIndex (This : access Typ;
                          P1_Int : Java.Int) is abstract;

   function GetTarget (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetTarget (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetType (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure Blur (This : access Typ) is abstract;

   procedure Focus (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAccessKey, "getAccessKey");
   pragma Export (Java, SetAccessKey, "setAccessKey");
   pragma Export (Java, GetCharset, "getCharset");
   pragma Export (Java, SetCharset, "setCharset");
   pragma Export (Java, GetCoords, "getCoords");
   pragma Export (Java, SetCoords, "setCoords");
   pragma Export (Java, GetHref, "getHref");
   pragma Export (Java, SetHref, "setHref");
   pragma Export (Java, GetHreflang, "getHreflang");
   pragma Export (Java, SetHreflang, "setHreflang");
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, SetName, "setName");
   pragma Export (Java, GetRel, "getRel");
   pragma Export (Java, SetRel, "setRel");
   pragma Export (Java, GetRev, "getRev");
   pragma Export (Java, SetRev, "setRev");
   pragma Export (Java, GetShape, "getShape");
   pragma Export (Java, SetShape, "setShape");
   pragma Export (Java, GetTabIndex, "getTabIndex");
   pragma Export (Java, SetTabIndex, "setTabIndex");
   pragma Export (Java, GetTarget, "getTarget");
   pragma Export (Java, SetTarget, "setTarget");
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, SetType, "setType");
   pragma Export (Java, Blur, "blur");
   pragma Export (Java, Focus, "focus");

end Org.W3c.Dom.Html.HTMLAnchorElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLAnchorElement, "org.w3c.dom.html.HTMLAnchorElement");
pragma Extensions_Allowed (Off);
