pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLAppletElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAlign (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAlign (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetAlt (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAlt (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetArchive (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetArchive (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetCode (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCode (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetCodeBase (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCodeBase (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetHeight (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetHeight (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetHspace (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetHspace (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetName (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetObject (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetObject (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetVspace (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetVspace (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetWidth (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetWidth (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAlign, "getAlign");
   pragma Export (Java, SetAlign, "setAlign");
   pragma Export (Java, GetAlt, "getAlt");
   pragma Export (Java, SetAlt, "setAlt");
   pragma Export (Java, GetArchive, "getArchive");
   pragma Export (Java, SetArchive, "setArchive");
   pragma Export (Java, GetCode, "getCode");
   pragma Export (Java, SetCode, "setCode");
   pragma Export (Java, GetCodeBase, "getCodeBase");
   pragma Export (Java, SetCodeBase, "setCodeBase");
   pragma Export (Java, GetHeight, "getHeight");
   pragma Export (Java, SetHeight, "setHeight");
   pragma Export (Java, GetHspace, "getHspace");
   pragma Export (Java, SetHspace, "setHspace");
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, SetName, "setName");
   pragma Export (Java, GetObject, "getObject");
   pragma Export (Java, SetObject, "setObject");
   pragma Export (Java, GetVspace, "getVspace");
   pragma Export (Java, SetVspace, "setVspace");
   pragma Export (Java, GetWidth, "getWidth");
   pragma Export (Java, SetWidth, "setWidth");

end Org.W3c.Dom.Html.HTMLAppletElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLAppletElement, "org.w3c.dom.html.HTMLAppletElement");
pragma Extensions_Allowed (Off);
