pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLAreaElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAccessKey (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAccessKey (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetAlt (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAlt (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetCoords (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCoords (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetHref (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetHref (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetNoHref (This : access Typ)
                       return Java.Boolean is abstract;

   procedure SetNoHref (This : access Typ;
                        P1_Boolean : Java.Boolean) is abstract;

   function GetShape (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetShape (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetTabIndex (This : access Typ)
                         return Java.Int is abstract;

   procedure SetTabIndex (This : access Typ;
                          P1_Int : Java.Int) is abstract;

   function GetTarget (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetTarget (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAccessKey, "getAccessKey");
   pragma Export (Java, SetAccessKey, "setAccessKey");
   pragma Export (Java, GetAlt, "getAlt");
   pragma Export (Java, SetAlt, "setAlt");
   pragma Export (Java, GetCoords, "getCoords");
   pragma Export (Java, SetCoords, "setCoords");
   pragma Export (Java, GetHref, "getHref");
   pragma Export (Java, SetHref, "setHref");
   pragma Export (Java, GetNoHref, "getNoHref");
   pragma Export (Java, SetNoHref, "setNoHref");
   pragma Export (Java, GetShape, "getShape");
   pragma Export (Java, SetShape, "setShape");
   pragma Export (Java, GetTabIndex, "getTabIndex");
   pragma Export (Java, SetTabIndex, "setTabIndex");
   pragma Export (Java, GetTarget, "getTarget");
   pragma Export (Java, SetTarget, "setTarget");

end Org.W3c.Dom.Html.HTMLAreaElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLAreaElement, "org.w3c.dom.html.HTMLAreaElement");
pragma Extensions_Allowed (Off);
