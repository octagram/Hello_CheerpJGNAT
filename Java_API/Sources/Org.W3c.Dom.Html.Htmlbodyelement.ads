pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLBodyElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetALink (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetALink (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetBackground (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBackground (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetBgColor (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBgColor (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetLink (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetLink (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetText (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetText (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetVLink (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetVLink (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetALink, "getALink");
   pragma Export (Java, SetALink, "setALink");
   pragma Export (Java, GetBackground, "getBackground");
   pragma Export (Java, SetBackground, "setBackground");
   pragma Export (Java, GetBgColor, "getBgColor");
   pragma Export (Java, SetBgColor, "setBgColor");
   pragma Export (Java, GetLink, "getLink");
   pragma Export (Java, SetLink, "setLink");
   pragma Export (Java, GetText, "getText");
   pragma Export (Java, SetText, "setText");
   pragma Export (Java, GetVLink, "getVLink");
   pragma Export (Java, SetVLink, "setVLink");

end Org.W3c.Dom.Html.HTMLBodyElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLBodyElement, "org.w3c.dom.html.HTMLBodyElement");
pragma Extensions_Allowed (Off);
