pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Html.HTMLFormElement;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLButtonElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetForm (This : access Typ)
                     return access Org.W3c.Dom.Html.HTMLFormElement.Typ'Class is abstract;

   function GetAccessKey (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAccessKey (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetDisabled (This : access Typ)
                         return Java.Boolean is abstract;

   procedure SetDisabled (This : access Typ;
                          P1_Boolean : Java.Boolean) is abstract;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetName (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetTabIndex (This : access Typ)
                         return Java.Int is abstract;

   procedure SetTabIndex (This : access Typ;
                          P1_Int : Java.Int) is abstract;

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetValue (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetValue (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetForm, "getForm");
   pragma Export (Java, GetAccessKey, "getAccessKey");
   pragma Export (Java, SetAccessKey, "setAccessKey");
   pragma Export (Java, GetDisabled, "getDisabled");
   pragma Export (Java, SetDisabled, "setDisabled");
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, SetName, "setName");
   pragma Export (Java, GetTabIndex, "getTabIndex");
   pragma Export (Java, SetTabIndex, "setTabIndex");
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, GetValue, "getValue");
   pragma Export (Java, SetValue, "setValue");

end Org.W3c.Dom.Html.HTMLButtonElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLButtonElement, "org.w3c.dom.html.HTMLButtonElement");
pragma Extensions_Allowed (Off);
