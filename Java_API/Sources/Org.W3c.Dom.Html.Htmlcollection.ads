pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;

package Org.W3c.Dom.Html.HTMLCollection is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLength (This : access Typ)
                       return Java.Int is abstract;

   function Item (This : access Typ;
                  P1_Int : Java.Int)
                  return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function NamedItem (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Org.W3c.Dom.Node.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetLength, "getLength");
   pragma Export (Java, Item, "item");
   pragma Export (Java, NamedItem, "namedItem");

end Org.W3c.Dom.Html.HTMLCollection;
pragma Import (Java, Org.W3c.Dom.Html.HTMLCollection, "org.w3c.dom.html.HTMLCollection");
pragma Extensions_Allowed (Off);
