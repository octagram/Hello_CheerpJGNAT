pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Html.HTMLCollection;
limited with Org.W3c.Dom.Html.HTMLElement;
limited with Org.W3c.Dom.NodeList;
with Java.Lang.Object;
with Org.W3c.Dom.Document;

package Org.W3c.Dom.Html.HTMLDocument is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Document_I : Org.W3c.Dom.Document.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTitle (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetTitle (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetReferrer (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function GetDomain (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   function GetURL (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   function GetBody (This : access Typ)
                     return access Org.W3c.Dom.Html.HTMLElement.Typ'Class is abstract;

   procedure SetBody (This : access Typ;
                      P1_HTMLElement : access Standard.Org.W3c.Dom.Html.HTMLElement.Typ'Class) is abstract;

   function GetImages (This : access Typ)
                       return access Org.W3c.Dom.Html.HTMLCollection.Typ'Class is abstract;

   function GetApplets (This : access Typ)
                        return access Org.W3c.Dom.Html.HTMLCollection.Typ'Class is abstract;

   function GetLinks (This : access Typ)
                      return access Org.W3c.Dom.Html.HTMLCollection.Typ'Class is abstract;

   function GetForms (This : access Typ)
                      return access Org.W3c.Dom.Html.HTMLCollection.Typ'Class is abstract;

   function GetAnchors (This : access Typ)
                        return access Org.W3c.Dom.Html.HTMLCollection.Typ'Class is abstract;

   function GetCookie (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCookie (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure Open (This : access Typ) is abstract;

   procedure Close (This : access Typ) is abstract;

   procedure Write (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure Writeln (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetElementsByName (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Org.W3c.Dom.NodeList.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetTitle, "getTitle");
   pragma Export (Java, SetTitle, "setTitle");
   pragma Export (Java, GetReferrer, "getReferrer");
   pragma Export (Java, GetDomain, "getDomain");
   pragma Export (Java, GetURL, "getURL");
   pragma Export (Java, GetBody, "getBody");
   pragma Export (Java, SetBody, "setBody");
   pragma Export (Java, GetImages, "getImages");
   pragma Export (Java, GetApplets, "getApplets");
   pragma Export (Java, GetLinks, "getLinks");
   pragma Export (Java, GetForms, "getForms");
   pragma Export (Java, GetAnchors, "getAnchors");
   pragma Export (Java, GetCookie, "getCookie");
   pragma Export (Java, SetCookie, "setCookie");
   pragma Export (Java, Open, "open");
   pragma Export (Java, Close, "close");
   pragma Export (Java, Write, "write");
   pragma Export (Java, Writeln, "writeln");
   pragma Export (Java, GetElementsByName, "getElementsByName");

end Org.W3c.Dom.Html.HTMLDocument;
pragma Import (Java, Org.W3c.Dom.Html.HTMLDocument, "org.w3c.dom.html.HTMLDocument");
pragma Extensions_Allowed (Off);
