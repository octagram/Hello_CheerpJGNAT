pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Html.HTMLDocument;
with Java.Lang.Object;
with Org.W3c.Dom.DOMImplementation;

package Org.W3c.Dom.Html.HTMLDOMImplementation is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            DOMImplementation_I : Org.W3c.Dom.DOMImplementation.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateHTMLDocument (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Org.W3c.Dom.Html.HTMLDocument.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreateHTMLDocument, "createHTMLDocument");

end Org.W3c.Dom.Html.HTMLDOMImplementation;
pragma Import (Java, Org.W3c.Dom.Html.HTMLDOMImplementation, "org.w3c.dom.html.HTMLDOMImplementation");
pragma Extensions_Allowed (Off);
