pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.W3c.Dom.Element;

package Org.W3c.Dom.Html.HTMLElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Element_I : Org.W3c.Dom.Element.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetId (This : access Typ)
                   return access Java.Lang.String.Typ'Class is abstract;

   procedure SetId (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetTitle (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetTitle (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetLang (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetLang (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetDir (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   procedure SetDir (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetClassName (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetClassName (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetId, "getId");
   pragma Export (Java, SetId, "setId");
   pragma Export (Java, GetTitle, "getTitle");
   pragma Export (Java, SetTitle, "setTitle");
   pragma Export (Java, GetLang, "getLang");
   pragma Export (Java, SetLang, "setLang");
   pragma Export (Java, GetDir, "getDir");
   pragma Export (Java, SetDir, "setDir");
   pragma Export (Java, GetClassName, "getClassName");
   pragma Export (Java, SetClassName, "setClassName");

end Org.W3c.Dom.Html.HTMLElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLElement, "org.w3c.dom.html.HTMLElement");
pragma Extensions_Allowed (Off);
