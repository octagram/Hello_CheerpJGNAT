pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Html.HTMLCollection;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLFormElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetElements (This : access Typ)
                         return access Org.W3c.Dom.Html.HTMLCollection.Typ'Class is abstract;

   function GetLength (This : access Typ)
                       return Java.Int is abstract;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetName (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetAcceptCharset (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAcceptCharset (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetAction (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAction (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetEnctype (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetEnctype (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetMethod (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetMethod (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetTarget (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetTarget (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure Submit (This : access Typ) is abstract;

   procedure Reset (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetElements, "getElements");
   pragma Export (Java, GetLength, "getLength");
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, SetName, "setName");
   pragma Export (Java, GetAcceptCharset, "getAcceptCharset");
   pragma Export (Java, SetAcceptCharset, "setAcceptCharset");
   pragma Export (Java, GetAction, "getAction");
   pragma Export (Java, SetAction, "setAction");
   pragma Export (Java, GetEnctype, "getEnctype");
   pragma Export (Java, SetEnctype, "setEnctype");
   pragma Export (Java, GetMethod, "getMethod");
   pragma Export (Java, SetMethod, "setMethod");
   pragma Export (Java, GetTarget, "getTarget");
   pragma Export (Java, SetTarget, "setTarget");
   pragma Export (Java, Submit, "submit");
   pragma Export (Java, Reset, "reset");

end Org.W3c.Dom.Html.HTMLFormElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLFormElement, "org.w3c.dom.html.HTMLFormElement");
pragma Extensions_Allowed (Off);
