pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Document;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLFrameElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFrameBorder (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetFrameBorder (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetLongDesc (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetLongDesc (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetMarginHeight (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   procedure SetMarginHeight (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetMarginWidth (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetMarginWidth (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetName (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetNoResize (This : access Typ)
                         return Java.Boolean is abstract;

   procedure SetNoResize (This : access Typ;
                          P1_Boolean : Java.Boolean) is abstract;

   function GetScrolling (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetScrolling (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetSrc (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   procedure SetSrc (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetContentDocument (This : access Typ)
                                return access Org.W3c.Dom.Document.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetFrameBorder, "getFrameBorder");
   pragma Export (Java, SetFrameBorder, "setFrameBorder");
   pragma Export (Java, GetLongDesc, "getLongDesc");
   pragma Export (Java, SetLongDesc, "setLongDesc");
   pragma Export (Java, GetMarginHeight, "getMarginHeight");
   pragma Export (Java, SetMarginHeight, "setMarginHeight");
   pragma Export (Java, GetMarginWidth, "getMarginWidth");
   pragma Export (Java, SetMarginWidth, "setMarginWidth");
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, SetName, "setName");
   pragma Export (Java, GetNoResize, "getNoResize");
   pragma Export (Java, SetNoResize, "setNoResize");
   pragma Export (Java, GetScrolling, "getScrolling");
   pragma Export (Java, SetScrolling, "setScrolling");
   pragma Export (Java, GetSrc, "getSrc");
   pragma Export (Java, SetSrc, "setSrc");
   pragma Export (Java, GetContentDocument, "getContentDocument");

end Org.W3c.Dom.Html.HTMLFrameElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLFrameElement, "org.w3c.dom.html.HTMLFrameElement");
pragma Extensions_Allowed (Off);
