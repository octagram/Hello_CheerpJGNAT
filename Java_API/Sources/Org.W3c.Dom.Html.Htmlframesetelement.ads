pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLFrameSetElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCols (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCols (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetRows (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetRows (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetCols, "getCols");
   pragma Export (Java, SetCols, "setCols");
   pragma Export (Java, GetRows, "getRows");
   pragma Export (Java, SetRows, "setRows");

end Org.W3c.Dom.Html.HTMLFrameSetElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLFrameSetElement, "org.w3c.dom.html.HTMLFrameSetElement");
pragma Extensions_Allowed (Off);
