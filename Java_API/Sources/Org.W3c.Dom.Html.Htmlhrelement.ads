pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLHRElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAlign (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAlign (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetNoShade (This : access Typ)
                        return Java.Boolean is abstract;

   procedure SetNoShade (This : access Typ;
                         P1_Boolean : Java.Boolean) is abstract;

   function GetSize (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetSize (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetWidth (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetWidth (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAlign, "getAlign");
   pragma Export (Java, SetAlign, "setAlign");
   pragma Export (Java, GetNoShade, "getNoShade");
   pragma Export (Java, SetNoShade, "setNoShade");
   pragma Export (Java, GetSize, "getSize");
   pragma Export (Java, SetSize, "setSize");
   pragma Export (Java, GetWidth, "getWidth");
   pragma Export (Java, SetWidth, "setWidth");

end Org.W3c.Dom.Html.HTMLHRElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLHRElement, "org.w3c.dom.html.HTMLHRElement");
pragma Extensions_Allowed (Off);
