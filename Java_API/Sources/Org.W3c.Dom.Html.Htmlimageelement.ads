pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLImageElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLowSrc (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetLowSrc (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetName (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetAlign (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAlign (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetAlt (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAlt (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetBorder (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorder (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetHeight (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetHeight (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetHspace (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetHspace (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetIsMap (This : access Typ)
                      return Java.Boolean is abstract;

   procedure SetIsMap (This : access Typ;
                       P1_Boolean : Java.Boolean) is abstract;

   function GetLongDesc (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetLongDesc (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetSrc (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   procedure SetSrc (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetUseMap (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetUseMap (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetVspace (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetVspace (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetWidth (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetWidth (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetLowSrc, "getLowSrc");
   pragma Export (Java, SetLowSrc, "setLowSrc");
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, SetName, "setName");
   pragma Export (Java, GetAlign, "getAlign");
   pragma Export (Java, SetAlign, "setAlign");
   pragma Export (Java, GetAlt, "getAlt");
   pragma Export (Java, SetAlt, "setAlt");
   pragma Export (Java, GetBorder, "getBorder");
   pragma Export (Java, SetBorder, "setBorder");
   pragma Export (Java, GetHeight, "getHeight");
   pragma Export (Java, SetHeight, "setHeight");
   pragma Export (Java, GetHspace, "getHspace");
   pragma Export (Java, SetHspace, "setHspace");
   pragma Export (Java, GetIsMap, "getIsMap");
   pragma Export (Java, SetIsMap, "setIsMap");
   pragma Export (Java, GetLongDesc, "getLongDesc");
   pragma Export (Java, SetLongDesc, "setLongDesc");
   pragma Export (Java, GetSrc, "getSrc");
   pragma Export (Java, SetSrc, "setSrc");
   pragma Export (Java, GetUseMap, "getUseMap");
   pragma Export (Java, SetUseMap, "setUseMap");
   pragma Export (Java, GetVspace, "getVspace");
   pragma Export (Java, SetVspace, "setVspace");
   pragma Export (Java, GetWidth, "getWidth");
   pragma Export (Java, SetWidth, "setWidth");

end Org.W3c.Dom.Html.HTMLImageElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLImageElement, "org.w3c.dom.html.HTMLImageElement");
pragma Extensions_Allowed (Off);
