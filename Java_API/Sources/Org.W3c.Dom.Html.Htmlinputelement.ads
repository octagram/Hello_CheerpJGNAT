pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Html.HTMLFormElement;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLInputElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDefaultValue (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   procedure SetDefaultValue (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetDefaultChecked (This : access Typ)
                               return Java.Boolean is abstract;

   procedure SetDefaultChecked (This : access Typ;
                                P1_Boolean : Java.Boolean) is abstract;

   function GetForm (This : access Typ)
                     return access Org.W3c.Dom.Html.HTMLFormElement.Typ'Class is abstract;

   function GetAccept (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAccept (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetAccessKey (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAccessKey (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetAlign (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAlign (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetAlt (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAlt (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetChecked (This : access Typ)
                        return Java.Boolean is abstract;

   procedure SetChecked (This : access Typ;
                         P1_Boolean : Java.Boolean) is abstract;

   function GetDisabled (This : access Typ)
                         return Java.Boolean is abstract;

   procedure SetDisabled (This : access Typ;
                          P1_Boolean : Java.Boolean) is abstract;

   function GetMaxLength (This : access Typ)
                          return Java.Int is abstract;

   procedure SetMaxLength (This : access Typ;
                           P1_Int : Java.Int) is abstract;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetName (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetReadOnly (This : access Typ)
                         return Java.Boolean is abstract;

   procedure SetReadOnly (This : access Typ;
                          P1_Boolean : Java.Boolean) is abstract;

   function GetSize (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetSize (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetSrc (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   procedure SetSrc (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetTabIndex (This : access Typ)
                         return Java.Int is abstract;

   procedure SetTabIndex (This : access Typ;
                          P1_Int : Java.Int) is abstract;

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetUseMap (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetUseMap (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetValue (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetValue (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   procedure Blur (This : access Typ) is abstract;

   procedure Focus (This : access Typ) is abstract;

   procedure select_K (This : access Typ) is abstract;

   procedure Click (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetDefaultValue, "getDefaultValue");
   pragma Export (Java, SetDefaultValue, "setDefaultValue");
   pragma Export (Java, GetDefaultChecked, "getDefaultChecked");
   pragma Export (Java, SetDefaultChecked, "setDefaultChecked");
   pragma Export (Java, GetForm, "getForm");
   pragma Export (Java, GetAccept, "getAccept");
   pragma Export (Java, SetAccept, "setAccept");
   pragma Export (Java, GetAccessKey, "getAccessKey");
   pragma Export (Java, SetAccessKey, "setAccessKey");
   pragma Export (Java, GetAlign, "getAlign");
   pragma Export (Java, SetAlign, "setAlign");
   pragma Export (Java, GetAlt, "getAlt");
   pragma Export (Java, SetAlt, "setAlt");
   pragma Export (Java, GetChecked, "getChecked");
   pragma Export (Java, SetChecked, "setChecked");
   pragma Export (Java, GetDisabled, "getDisabled");
   pragma Export (Java, SetDisabled, "setDisabled");
   pragma Export (Java, GetMaxLength, "getMaxLength");
   pragma Export (Java, SetMaxLength, "setMaxLength");
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, SetName, "setName");
   pragma Export (Java, GetReadOnly, "getReadOnly");
   pragma Export (Java, SetReadOnly, "setReadOnly");
   pragma Export (Java, GetSize, "getSize");
   pragma Export (Java, SetSize, "setSize");
   pragma Export (Java, GetSrc, "getSrc");
   pragma Export (Java, SetSrc, "setSrc");
   pragma Export (Java, GetTabIndex, "getTabIndex");
   pragma Export (Java, SetTabIndex, "setTabIndex");
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, GetUseMap, "getUseMap");
   pragma Export (Java, SetUseMap, "setUseMap");
   pragma Export (Java, GetValue, "getValue");
   pragma Export (Java, SetValue, "setValue");
   pragma Export (Java, Blur, "blur");
   pragma Export (Java, Focus, "focus");
   pragma Export (Java, select_K, "select");
   pragma Export (Java, Click, "click");

end Org.W3c.Dom.Html.HTMLInputElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLInputElement, "org.w3c.dom.html.HTMLInputElement");
pragma Extensions_Allowed (Off);
