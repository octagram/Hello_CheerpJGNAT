pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Html.HTMLFormElement;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLLegendElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetForm (This : access Typ)
                     return access Org.W3c.Dom.Html.HTMLFormElement.Typ'Class is abstract;

   function GetAccessKey (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAccessKey (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetAlign (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAlign (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetForm, "getForm");
   pragma Export (Java, GetAccessKey, "getAccessKey");
   pragma Export (Java, SetAccessKey, "setAccessKey");
   pragma Export (Java, GetAlign, "getAlign");
   pragma Export (Java, SetAlign, "setAlign");

end Org.W3c.Dom.Html.HTMLLegendElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLLegendElement, "org.w3c.dom.html.HTMLLegendElement");
pragma Extensions_Allowed (Off);
