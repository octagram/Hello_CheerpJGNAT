pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLLIElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetType (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetValue (This : access Typ)
                      return Java.Int is abstract;

   procedure SetValue (This : access Typ;
                       P1_Int : Java.Int) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, SetType, "setType");
   pragma Export (Java, GetValue, "getValue");
   pragma Export (Java, SetValue, "setValue");

end Org.W3c.Dom.Html.HTMLLIElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLLIElement, "org.w3c.dom.html.HTMLLIElement");
pragma Extensions_Allowed (Off);
