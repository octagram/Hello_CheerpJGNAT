pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Document;
limited with Org.W3c.Dom.Html.HTMLFormElement;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLObjectElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetForm (This : access Typ)
                     return access Org.W3c.Dom.Html.HTMLFormElement.Typ'Class is abstract;

   function GetCode (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCode (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetAlign (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAlign (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetArchive (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetArchive (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetBorder (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorder (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetCodeBase (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCodeBase (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetCodeType (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCodeType (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetData (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetData (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetDeclare (This : access Typ)
                        return Java.Boolean is abstract;

   procedure SetDeclare (This : access Typ;
                         P1_Boolean : Java.Boolean) is abstract;

   function GetHeight (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetHeight (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetHspace (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetHspace (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetName (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetStandby (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetStandby (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetTabIndex (This : access Typ)
                         return Java.Int is abstract;

   procedure SetTabIndex (This : access Typ;
                          P1_Int : Java.Int) is abstract;

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetType (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetUseMap (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetUseMap (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetVspace (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetVspace (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetWidth (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetWidth (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetContentDocument (This : access Typ)
                                return access Org.W3c.Dom.Document.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetForm, "getForm");
   pragma Export (Java, GetCode, "getCode");
   pragma Export (Java, SetCode, "setCode");
   pragma Export (Java, GetAlign, "getAlign");
   pragma Export (Java, SetAlign, "setAlign");
   pragma Export (Java, GetArchive, "getArchive");
   pragma Export (Java, SetArchive, "setArchive");
   pragma Export (Java, GetBorder, "getBorder");
   pragma Export (Java, SetBorder, "setBorder");
   pragma Export (Java, GetCodeBase, "getCodeBase");
   pragma Export (Java, SetCodeBase, "setCodeBase");
   pragma Export (Java, GetCodeType, "getCodeType");
   pragma Export (Java, SetCodeType, "setCodeType");
   pragma Export (Java, GetData, "getData");
   pragma Export (Java, SetData, "setData");
   pragma Export (Java, GetDeclare, "getDeclare");
   pragma Export (Java, SetDeclare, "setDeclare");
   pragma Export (Java, GetHeight, "getHeight");
   pragma Export (Java, SetHeight, "setHeight");
   pragma Export (Java, GetHspace, "getHspace");
   pragma Export (Java, SetHspace, "setHspace");
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, SetName, "setName");
   pragma Export (Java, GetStandby, "getStandby");
   pragma Export (Java, SetStandby, "setStandby");
   pragma Export (Java, GetTabIndex, "getTabIndex");
   pragma Export (Java, SetTabIndex, "setTabIndex");
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, SetType, "setType");
   pragma Export (Java, GetUseMap, "getUseMap");
   pragma Export (Java, SetUseMap, "setUseMap");
   pragma Export (Java, GetVspace, "getVspace");
   pragma Export (Java, SetVspace, "setVspace");
   pragma Export (Java, GetWidth, "getWidth");
   pragma Export (Java, SetWidth, "setWidth");
   pragma Export (Java, GetContentDocument, "getContentDocument");

end Org.W3c.Dom.Html.HTMLObjectElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLObjectElement, "org.w3c.dom.html.HTMLObjectElement");
pragma Extensions_Allowed (Off);
