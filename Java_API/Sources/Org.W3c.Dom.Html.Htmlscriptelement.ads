pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLScriptElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetText (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetText (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetHtmlFor (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetHtmlFor (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetEvent (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetEvent (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetCharset (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCharset (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetDefer (This : access Typ)
                      return Java.Boolean is abstract;

   procedure SetDefer (This : access Typ;
                       P1_Boolean : Java.Boolean) is abstract;

   function GetSrc (This : access Typ)
                    return access Java.Lang.String.Typ'Class is abstract;

   procedure SetSrc (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetType (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetText, "getText");
   pragma Export (Java, SetText, "setText");
   pragma Export (Java, GetHtmlFor, "getHtmlFor");
   pragma Export (Java, SetHtmlFor, "setHtmlFor");
   pragma Export (Java, GetEvent, "getEvent");
   pragma Export (Java, SetEvent, "setEvent");
   pragma Export (Java, GetCharset, "getCharset");
   pragma Export (Java, SetCharset, "setCharset");
   pragma Export (Java, GetDefer, "getDefer");
   pragma Export (Java, SetDefer, "setDefer");
   pragma Export (Java, GetSrc, "getSrc");
   pragma Export (Java, SetSrc, "setSrc");
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, SetType, "setType");

end Org.W3c.Dom.Html.HTMLScriptElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLScriptElement, "org.w3c.dom.html.HTMLScriptElement");
pragma Extensions_Allowed (Off);
