pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Html.HTMLCollection;
limited with Org.W3c.Dom.Html.HTMLFormElement;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLSelectElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetSelectedIndex (This : access Typ)
                              return Java.Int is abstract;

   procedure SetSelectedIndex (This : access Typ;
                               P1_Int : Java.Int) is abstract;

   function GetValue (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetValue (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetLength (This : access Typ)
                       return Java.Int is abstract;

   function GetForm (This : access Typ)
                     return access Org.W3c.Dom.Html.HTMLFormElement.Typ'Class is abstract;

   function GetOptions (This : access Typ)
                        return access Org.W3c.Dom.Html.HTMLCollection.Typ'Class is abstract;

   function GetDisabled (This : access Typ)
                         return Java.Boolean is abstract;

   procedure SetDisabled (This : access Typ;
                          P1_Boolean : Java.Boolean) is abstract;

   function GetMultiple (This : access Typ)
                         return Java.Boolean is abstract;

   procedure SetMultiple (This : access Typ;
                          P1_Boolean : Java.Boolean) is abstract;

   function GetName (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetName (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetSize (This : access Typ)
                     return Java.Int is abstract;

   procedure SetSize (This : access Typ;
                      P1_Int : Java.Int) is abstract;

   function GetTabIndex (This : access Typ)
                         return Java.Int is abstract;

   procedure SetTabIndex (This : access Typ;
                          P1_Int : Java.Int) is abstract;

   procedure Add (This : access Typ;
                  P1_HTMLElement : access Standard.Org.W3c.Dom.Html.HTMLElement.Typ'Class;
                  P2_HTMLElement : access Standard.Org.W3c.Dom.Html.HTMLElement.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure Remove (This : access Typ;
                     P1_Int : Java.Int) is abstract;

   procedure Blur (This : access Typ) is abstract;

   procedure Focus (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, GetSelectedIndex, "getSelectedIndex");
   pragma Export (Java, SetSelectedIndex, "setSelectedIndex");
   pragma Export (Java, GetValue, "getValue");
   pragma Export (Java, SetValue, "setValue");
   pragma Export (Java, GetLength, "getLength");
   pragma Export (Java, GetForm, "getForm");
   pragma Export (Java, GetOptions, "getOptions");
   pragma Export (Java, GetDisabled, "getDisabled");
   pragma Export (Java, SetDisabled, "setDisabled");
   pragma Export (Java, GetMultiple, "getMultiple");
   pragma Export (Java, SetMultiple, "setMultiple");
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, SetName, "setName");
   pragma Export (Java, GetSize, "getSize");
   pragma Export (Java, SetSize, "setSize");
   pragma Export (Java, GetTabIndex, "getTabIndex");
   pragma Export (Java, SetTabIndex, "setTabIndex");
   pragma Export (Java, Add, "add");
   pragma Export (Java, Remove, "remove");
   pragma Export (Java, Blur, "blur");
   pragma Export (Java, Focus, "focus");

end Org.W3c.Dom.Html.HTMLSelectElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLSelectElement, "org.w3c.dom.html.HTMLSelectElement");
pragma Extensions_Allowed (Off);
