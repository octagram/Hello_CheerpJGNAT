pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLTableCellElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCellIndex (This : access Typ)
                          return Java.Int is abstract;

   function GetAbbr (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAbbr (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetAlign (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAlign (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetAxis (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAxis (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetBgColor (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBgColor (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetCh (This : access Typ)
                   return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCh (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetChOff (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetChOff (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetColSpan (This : access Typ)
                        return Java.Int is abstract;

   procedure SetColSpan (This : access Typ;
                         P1_Int : Java.Int) is abstract;

   function GetHeaders (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetHeaders (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetHeight (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetHeight (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetNoWrap (This : access Typ)
                       return Java.Boolean is abstract;

   procedure SetNoWrap (This : access Typ;
                        P1_Boolean : Java.Boolean) is abstract;

   function GetRowSpan (This : access Typ)
                        return Java.Int is abstract;

   procedure SetRowSpan (This : access Typ;
                         P1_Int : Java.Int) is abstract;

   function GetScope (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetScope (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetVAlign (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetVAlign (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetWidth (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetWidth (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetCellIndex, "getCellIndex");
   pragma Export (Java, GetAbbr, "getAbbr");
   pragma Export (Java, SetAbbr, "setAbbr");
   pragma Export (Java, GetAlign, "getAlign");
   pragma Export (Java, SetAlign, "setAlign");
   pragma Export (Java, GetAxis, "getAxis");
   pragma Export (Java, SetAxis, "setAxis");
   pragma Export (Java, GetBgColor, "getBgColor");
   pragma Export (Java, SetBgColor, "setBgColor");
   pragma Export (Java, GetCh, "getCh");
   pragma Export (Java, SetCh, "setCh");
   pragma Export (Java, GetChOff, "getChOff");
   pragma Export (Java, SetChOff, "setChOff");
   pragma Export (Java, GetColSpan, "getColSpan");
   pragma Export (Java, SetColSpan, "setColSpan");
   pragma Export (Java, GetHeaders, "getHeaders");
   pragma Export (Java, SetHeaders, "setHeaders");
   pragma Export (Java, GetHeight, "getHeight");
   pragma Export (Java, SetHeight, "setHeight");
   pragma Export (Java, GetNoWrap, "getNoWrap");
   pragma Export (Java, SetNoWrap, "setNoWrap");
   pragma Export (Java, GetRowSpan, "getRowSpan");
   pragma Export (Java, SetRowSpan, "setRowSpan");
   pragma Export (Java, GetScope, "getScope");
   pragma Export (Java, SetScope, "setScope");
   pragma Export (Java, GetVAlign, "getVAlign");
   pragma Export (Java, SetVAlign, "setVAlign");
   pragma Export (Java, GetWidth, "getWidth");
   pragma Export (Java, SetWidth, "setWidth");

end Org.W3c.Dom.Html.HTMLTableCellElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLTableCellElement, "org.w3c.dom.html.HTMLTableCellElement");
pragma Extensions_Allowed (Off);
