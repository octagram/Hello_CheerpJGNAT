pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Html.HTMLCollection;
limited with Org.W3c.Dom.Html.HTMLTableCaptionElement;
limited with Org.W3c.Dom.Html.HTMLTableSectionElement;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLTableElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCaption (This : access Typ)
                        return access Org.W3c.Dom.Html.HTMLTableCaptionElement.Typ'Class is abstract;

   procedure SetCaption (This : access Typ;
                         P1_HTMLTableCaptionElement : access Standard.Org.W3c.Dom.Html.HTMLTableCaptionElement.Typ'Class) is abstract;

   function GetTHead (This : access Typ)
                      return access Org.W3c.Dom.Html.HTMLTableSectionElement.Typ'Class is abstract;

   procedure SetTHead (This : access Typ;
                       P1_HTMLTableSectionElement : access Standard.Org.W3c.Dom.Html.HTMLTableSectionElement.Typ'Class) is abstract;

   function GetTFoot (This : access Typ)
                      return access Org.W3c.Dom.Html.HTMLTableSectionElement.Typ'Class is abstract;

   procedure SetTFoot (This : access Typ;
                       P1_HTMLTableSectionElement : access Standard.Org.W3c.Dom.Html.HTMLTableSectionElement.Typ'Class) is abstract;

   function GetRows (This : access Typ)
                     return access Org.W3c.Dom.Html.HTMLCollection.Typ'Class is abstract;

   function GetTBodies (This : access Typ)
                        return access Org.W3c.Dom.Html.HTMLCollection.Typ'Class is abstract;

   function GetAlign (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAlign (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetBgColor (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBgColor (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetBorder (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBorder (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetCellPadding (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCellPadding (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetCellSpacing (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCellSpacing (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetFrame (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetFrame (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetRules (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetRules (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetSummary (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetSummary (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetWidth (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetWidth (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function CreateTHead (This : access Typ)
                         return access Org.W3c.Dom.Html.HTMLElement.Typ'Class is abstract;

   procedure DeleteTHead (This : access Typ) is abstract;

   function CreateTFoot (This : access Typ)
                         return access Org.W3c.Dom.Html.HTMLElement.Typ'Class is abstract;

   procedure DeleteTFoot (This : access Typ) is abstract;

   function CreateCaption (This : access Typ)
                           return access Org.W3c.Dom.Html.HTMLElement.Typ'Class is abstract;

   procedure DeleteCaption (This : access Typ) is abstract;

   function InsertRow (This : access Typ;
                       P1_Int : Java.Int)
                       return access Org.W3c.Dom.Html.HTMLElement.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure DeleteRow (This : access Typ;
                        P1_Int : Java.Int) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetCaption, "getCaption");
   pragma Export (Java, SetCaption, "setCaption");
   pragma Export (Java, GetTHead, "getTHead");
   pragma Export (Java, SetTHead, "setTHead");
   pragma Export (Java, GetTFoot, "getTFoot");
   pragma Export (Java, SetTFoot, "setTFoot");
   pragma Export (Java, GetRows, "getRows");
   pragma Export (Java, GetTBodies, "getTBodies");
   pragma Export (Java, GetAlign, "getAlign");
   pragma Export (Java, SetAlign, "setAlign");
   pragma Export (Java, GetBgColor, "getBgColor");
   pragma Export (Java, SetBgColor, "setBgColor");
   pragma Export (Java, GetBorder, "getBorder");
   pragma Export (Java, SetBorder, "setBorder");
   pragma Export (Java, GetCellPadding, "getCellPadding");
   pragma Export (Java, SetCellPadding, "setCellPadding");
   pragma Export (Java, GetCellSpacing, "getCellSpacing");
   pragma Export (Java, SetCellSpacing, "setCellSpacing");
   pragma Export (Java, GetFrame, "getFrame");
   pragma Export (Java, SetFrame, "setFrame");
   pragma Export (Java, GetRules, "getRules");
   pragma Export (Java, SetRules, "setRules");
   pragma Export (Java, GetSummary, "getSummary");
   pragma Export (Java, SetSummary, "setSummary");
   pragma Export (Java, GetWidth, "getWidth");
   pragma Export (Java, SetWidth, "setWidth");
   pragma Export (Java, CreateTHead, "createTHead");
   pragma Export (Java, DeleteTHead, "deleteTHead");
   pragma Export (Java, CreateTFoot, "createTFoot");
   pragma Export (Java, DeleteTFoot, "deleteTFoot");
   pragma Export (Java, CreateCaption, "createCaption");
   pragma Export (Java, DeleteCaption, "deleteCaption");
   pragma Export (Java, InsertRow, "insertRow");
   pragma Export (Java, DeleteRow, "deleteRow");

end Org.W3c.Dom.Html.HTMLTableElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLTableElement, "org.w3c.dom.html.HTMLTableElement");
pragma Extensions_Allowed (Off);
