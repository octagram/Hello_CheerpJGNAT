pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Html.HTMLCollection;
with Java.Lang.Object;
with Org.W3c.Dom.Html.HTMLElement;

package Org.W3c.Dom.Html.HTMLTableSectionElement is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            HTMLElement_I : Org.W3c.Dom.Html.HTMLElement.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetAlign (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetAlign (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetCh (This : access Typ)
                   return access Java.Lang.String.Typ'Class is abstract;

   procedure SetCh (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetChOff (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   procedure SetChOff (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetVAlign (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetVAlign (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetRows (This : access Typ)
                     return access Org.W3c.Dom.Html.HTMLCollection.Typ'Class is abstract;

   function InsertRow (This : access Typ;
                       P1_Int : Java.Int)
                       return access Org.W3c.Dom.Html.HTMLElement.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure DeleteRow (This : access Typ;
                        P1_Int : Java.Int) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetAlign, "getAlign");
   pragma Export (Java, SetAlign, "setAlign");
   pragma Export (Java, GetCh, "getCh");
   pragma Export (Java, SetCh, "setCh");
   pragma Export (Java, GetChOff, "getChOff");
   pragma Export (Java, SetChOff, "setChOff");
   pragma Export (Java, GetVAlign, "getVAlign");
   pragma Export (Java, SetVAlign, "setVAlign");
   pragma Export (Java, GetRows, "getRows");
   pragma Export (Java, InsertRow, "insertRow");
   pragma Export (Java, DeleteRow, "deleteRow");

end Org.W3c.Dom.Html.HTMLTableSectionElement;
pragma Import (Java, Org.W3c.Dom.Html.HTMLTableSectionElement, "org.w3c.dom.html.HTMLTableSectionElement");
pragma Extensions_Allowed (Off);
