pragma Extensions_Allowed (On);
package Org.W3c.Dom.Html is
   pragma Preelaborate;
end Org.W3c.Dom.Html;
pragma Import (Java, Org.W3c.Dom.Html, "org.w3c.dom.html");
pragma Extensions_Allowed (Off);
