pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Ls.LSInput;
limited with Org.W3c.Dom.Ls.LSOutput;
limited with Org.W3c.Dom.Ls.LSParser;
limited with Org.W3c.Dom.Ls.LSSerializer;
with Java.Lang.Object;

package Org.W3c.Dom.Ls.DOMImplementationLS is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateLSParser (This : access Typ;
                            P1_Short : Java.Short;
                            P2_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Org.W3c.Dom.Ls.LSParser.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function CreateLSSerializer (This : access Typ)
                                return access Org.W3c.Dom.Ls.LSSerializer.Typ'Class is abstract;

   function CreateLSInput (This : access Typ)
                           return access Org.W3c.Dom.Ls.LSInput.Typ'Class is abstract;

   function CreateLSOutput (This : access Typ)
                            return access Org.W3c.Dom.Ls.LSOutput.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   MODE_SYNCHRONOUS : constant Java.Short;

   --  final
   MODE_ASYNCHRONOUS : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreateLSParser, "createLSParser");
   pragma Export (Java, CreateLSSerializer, "createLSSerializer");
   pragma Export (Java, CreateLSInput, "createLSInput");
   pragma Export (Java, CreateLSOutput, "createLSOutput");
   pragma Import (Java, MODE_SYNCHRONOUS, "MODE_SYNCHRONOUS");
   pragma Import (Java, MODE_ASYNCHRONOUS, "MODE_ASYNCHRONOUS");

end Org.W3c.Dom.Ls.DOMImplementationLS;
pragma Import (Java, Org.W3c.Dom.Ls.DOMImplementationLS, "org.w3c.dom.ls.DOMImplementationLS");
pragma Extensions_Allowed (Off);
