pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.Reader;
limited with Java.Lang.String;
with Java.Lang.Object;

package Org.W3c.Dom.Ls.LSInput is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetCharacterStream (This : access Typ)
                                return access Java.Io.Reader.Typ'Class is abstract;

   procedure SetCharacterStream (This : access Typ;
                                 P1_Reader : access Standard.Java.Io.Reader.Typ'Class) is abstract;

   function GetByteStream (This : access Typ)
                           return access Java.Io.InputStream.Typ'Class is abstract;

   procedure SetByteStream (This : access Typ;
                            P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class) is abstract;

   function GetStringData (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   procedure SetStringData (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetSystemId (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetSystemId (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetPublicId (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPublicId (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetBaseURI (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetBaseURI (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetEncoding (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   procedure SetEncoding (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetCertifiedText (This : access Typ)
                              return Java.Boolean is abstract;

   procedure SetCertifiedText (This : access Typ;
                               P1_Boolean : Java.Boolean) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetCharacterStream, "getCharacterStream");
   pragma Export (Java, SetCharacterStream, "setCharacterStream");
   pragma Export (Java, GetByteStream, "getByteStream");
   pragma Export (Java, SetByteStream, "setByteStream");
   pragma Export (Java, GetStringData, "getStringData");
   pragma Export (Java, SetStringData, "setStringData");
   pragma Export (Java, GetSystemId, "getSystemId");
   pragma Export (Java, SetSystemId, "setSystemId");
   pragma Export (Java, GetPublicId, "getPublicId");
   pragma Export (Java, SetPublicId, "setPublicId");
   pragma Export (Java, GetBaseURI, "getBaseURI");
   pragma Export (Java, SetBaseURI, "setBaseURI");
   pragma Export (Java, GetEncoding, "getEncoding");
   pragma Export (Java, SetEncoding, "setEncoding");
   pragma Export (Java, GetCertifiedText, "getCertifiedText");
   pragma Export (Java, SetCertifiedText, "setCertifiedText");

end Org.W3c.Dom.Ls.LSInput;
pragma Import (Java, Org.W3c.Dom.Ls.LSInput, "org.w3c.dom.ls.LSInput");
pragma Extensions_Allowed (Off);
