pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.DOMConfiguration;
limited with Org.W3c.Dom.Document;
limited with Org.W3c.Dom.Ls.LSInput;
limited with Org.W3c.Dom.Ls.LSParserFilter;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;

package Org.W3c.Dom.Ls.LSParser is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDomConfig (This : access Typ)
                          return access Org.W3c.Dom.DOMConfiguration.Typ'Class is abstract;

   function GetFilter (This : access Typ)
                       return access Org.W3c.Dom.Ls.LSParserFilter.Typ'Class is abstract;

   procedure SetFilter (This : access Typ;
                        P1_LSParserFilter : access Standard.Org.W3c.Dom.Ls.LSParserFilter.Typ'Class) is abstract;

   function GetAsync (This : access Typ)
                      return Java.Boolean is abstract;

   function GetBusy (This : access Typ)
                     return Java.Boolean is abstract;

   function Parse (This : access Typ;
                   P1_LSInput : access Standard.Org.W3c.Dom.Ls.LSInput.Typ'Class)
                   return access Org.W3c.Dom.Document.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except and
   --  Org.W3c.Dom.Ls.LSException.Except

   function ParseURI (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Org.W3c.Dom.Document.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except and
   --  Org.W3c.Dom.Ls.LSException.Except

   function ParseWithContext (This : access Typ;
                              P1_LSInput : access Standard.Org.W3c.Dom.Ls.LSInput.Typ'Class;
                              P2_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                              P3_Short : Java.Short)
                              return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except and
   --  Org.W3c.Dom.Ls.LSException.Except

   procedure abort_K (This : access Typ) is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ACTION_APPEND_AS_CHILDREN : constant Java.Short;

   --  final
   ACTION_REPLACE_CHILDREN : constant Java.Short;

   --  final
   ACTION_INSERT_BEFORE : constant Java.Short;

   --  final
   ACTION_INSERT_AFTER : constant Java.Short;

   --  final
   ACTION_REPLACE : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetDomConfig, "getDomConfig");
   pragma Export (Java, GetFilter, "getFilter");
   pragma Export (Java, SetFilter, "setFilter");
   pragma Export (Java, GetAsync, "getAsync");
   pragma Export (Java, GetBusy, "getBusy");
   pragma Export (Java, Parse, "parse");
   pragma Export (Java, ParseURI, "parseURI");
   pragma Export (Java, ParseWithContext, "parseWithContext");
   pragma Export (Java, abort_K, "abort");
   pragma Import (Java, ACTION_APPEND_AS_CHILDREN, "ACTION_APPEND_AS_CHILDREN");
   pragma Import (Java, ACTION_REPLACE_CHILDREN, "ACTION_REPLACE_CHILDREN");
   pragma Import (Java, ACTION_INSERT_BEFORE, "ACTION_INSERT_BEFORE");
   pragma Import (Java, ACTION_INSERT_AFTER, "ACTION_INSERT_AFTER");
   pragma Import (Java, ACTION_REPLACE, "ACTION_REPLACE");

end Org.W3c.Dom.Ls.LSParser;
pragma Import (Java, Org.W3c.Dom.Ls.LSParser, "org.w3c.dom.ls.LSParser");
pragma Extensions_Allowed (Off);
