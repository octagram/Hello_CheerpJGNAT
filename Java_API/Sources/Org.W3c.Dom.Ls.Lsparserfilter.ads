pragma Extensions_Allowed (On);
limited with Org.W3c.Dom.Element;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;

package Org.W3c.Dom.Ls.LSParserFilter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function StartElement (This : access Typ;
                          P1_Element : access Standard.Org.W3c.Dom.Element.Typ'Class)
                          return Java.Short is abstract;

   function AcceptNode (This : access Typ;
                        P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                        return Java.Short is abstract;

   function GetWhatToShow (This : access Typ)
                           return Java.Int is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FILTER_ACCEPT : constant Java.Short;

   --  final
   FILTER_REJECT : constant Java.Short;

   --  final
   FILTER_SKIP : constant Java.Short;

   --  final
   FILTER_INTERRUPT : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, StartElement, "startElement");
   pragma Export (Java, AcceptNode, "acceptNode");
   pragma Export (Java, GetWhatToShow, "getWhatToShow");
   pragma Import (Java, FILTER_ACCEPT, "FILTER_ACCEPT");
   pragma Import (Java, FILTER_REJECT, "FILTER_REJECT");
   pragma Import (Java, FILTER_SKIP, "FILTER_SKIP");
   pragma Import (Java, FILTER_INTERRUPT, "FILTER_INTERRUPT");

end Org.W3c.Dom.Ls.LSParserFilter;
pragma Import (Java, Org.W3c.Dom.Ls.LSParserFilter, "org.w3c.dom.ls.LSParserFilter");
pragma Extensions_Allowed (Off);
