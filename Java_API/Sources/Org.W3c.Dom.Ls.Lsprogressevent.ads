pragma Extensions_Allowed (On);
limited with Org.W3c.Dom.Ls.LSInput;
with Java.Lang.Object;
with Org.W3c.Dom.Events.Event;

package Org.W3c.Dom.Ls.LSProgressEvent is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Event_I : Org.W3c.Dom.Events.Event.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetInput (This : access Typ)
                      return access Org.W3c.Dom.Ls.LSInput.Typ'Class is abstract;

   function GetPosition (This : access Typ)
                         return Java.Int is abstract;

   function GetTotalSize (This : access Typ)
                          return Java.Int is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetInput, "getInput");
   pragma Export (Java, GetPosition, "getPosition");
   pragma Export (Java, GetTotalSize, "getTotalSize");

end Org.W3c.Dom.Ls.LSProgressEvent;
pragma Import (Java, Org.W3c.Dom.Ls.LSProgressEvent, "org.w3c.dom.ls.LSProgressEvent");
pragma Extensions_Allowed (Off);
