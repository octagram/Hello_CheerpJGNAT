pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Ls.LSInput;
with Java.Lang.Object;

package Org.W3c.Dom.Ls.LSResourceResolver is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function ResolveResource (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class;
                             P2_String : access Standard.Java.Lang.String.Typ'Class;
                             P3_String : access Standard.Java.Lang.String.Typ'Class;
                             P4_String : access Standard.Java.Lang.String.Typ'Class;
                             P5_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Org.W3c.Dom.Ls.LSInput.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ResolveResource, "resolveResource");

end Org.W3c.Dom.Ls.LSResourceResolver;
pragma Import (Java, Org.W3c.Dom.Ls.LSResourceResolver, "org.w3c.dom.ls.LSResourceResolver");
pragma Extensions_Allowed (Off);
