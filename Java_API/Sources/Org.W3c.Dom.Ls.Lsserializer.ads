pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.DOMConfiguration;
limited with Org.W3c.Dom.Ls.LSOutput;
limited with Org.W3c.Dom.Ls.LSSerializerFilter;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;

package Org.W3c.Dom.Ls.LSSerializer is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetDomConfig (This : access Typ)
                          return access Org.W3c.Dom.DOMConfiguration.Typ'Class is abstract;

   function GetNewLine (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   procedure SetNewLine (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;

   function GetFilter (This : access Typ)
                       return access Org.W3c.Dom.Ls.LSSerializerFilter.Typ'Class is abstract;

   procedure SetFilter (This : access Typ;
                        P1_LSSerializerFilter : access Standard.Org.W3c.Dom.Ls.LSSerializerFilter.Typ'Class) is abstract;

   function Write (This : access Typ;
                   P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                   P2_LSOutput : access Standard.Org.W3c.Dom.Ls.LSOutput.Typ'Class)
                   return Java.Boolean is abstract;
   --  can raise Org.W3c.Dom.Ls.LSException.Except

   function WriteToURI (This : access Typ;
                        P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean is abstract;
   --  can raise Org.W3c.Dom.Ls.LSException.Except

   function WriteToString (This : access Typ;
                           P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                           return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except and
   --  Org.W3c.Dom.Ls.LSException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetDomConfig, "getDomConfig");
   pragma Export (Java, GetNewLine, "getNewLine");
   pragma Export (Java, SetNewLine, "setNewLine");
   pragma Export (Java, GetFilter, "getFilter");
   pragma Export (Java, SetFilter, "setFilter");
   pragma Export (Java, Write, "write");
   pragma Export (Java, WriteToURI, "writeToURI");
   pragma Export (Java, WriteToString, "writeToString");

end Org.W3c.Dom.Ls.LSSerializer;
pragma Import (Java, Org.W3c.Dom.Ls.LSSerializer, "org.w3c.dom.ls.LSSerializer");
pragma Extensions_Allowed (Off);
