pragma Extensions_Allowed (On);
package Org.W3c.Dom.Ls is
   pragma Preelaborate;
end Org.W3c.Dom.Ls;
pragma Import (Java, Org.W3c.Dom.Ls, "org.w3c.dom.ls");
pragma Extensions_Allowed (Off);
