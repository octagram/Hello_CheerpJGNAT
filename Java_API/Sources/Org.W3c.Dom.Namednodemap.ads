pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;

package Org.W3c.Dom.NamedNodeMap is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNamedItem (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function SetNamedItem (This : access Typ;
                          P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                          return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function RemoveNamedItem (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function Item (This : access Typ;
                  P1_Int : Java.Int)
                  return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function GetLength (This : access Typ)
                       return Java.Int is abstract;

   function GetNamedItemNS (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class)
                            return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function SetNamedItemNS (This : access Typ;
                            P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                            return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function RemoveNamedItemNS (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetNamedItem, "getNamedItem");
   pragma Export (Java, SetNamedItem, "setNamedItem");
   pragma Export (Java, RemoveNamedItem, "removeNamedItem");
   pragma Export (Java, Item, "item");
   pragma Export (Java, GetLength, "getLength");
   pragma Export (Java, GetNamedItemNS, "getNamedItemNS");
   pragma Export (Java, SetNamedItemNS, "setNamedItemNS");
   pragma Export (Java, RemoveNamedItemNS, "removeNamedItemNS");

end Org.W3c.Dom.NamedNodeMap;
pragma Import (Java, Org.W3c.Dom.NamedNodeMap, "org.w3c.dom.NamedNodeMap");
pragma Extensions_Allowed (Off);
