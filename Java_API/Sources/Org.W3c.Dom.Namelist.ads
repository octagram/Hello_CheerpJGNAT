pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Org.W3c.Dom.NameList is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetName (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetNamespaceURI (This : access Typ;
                             P1_Int : Java.Int)
                             return access Java.Lang.String.Typ'Class is abstract;

   function GetLength (This : access Typ)
                       return Java.Int is abstract;

   function Contains (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Boolean is abstract;

   function ContainsNS (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetName, "getName");
   pragma Export (Java, GetNamespaceURI, "getNamespaceURI");
   pragma Export (Java, GetLength, "getLength");
   pragma Export (Java, Contains, "contains");
   pragma Export (Java, ContainsNS, "containsNS");

end Org.W3c.Dom.NameList;
pragma Import (Java, Org.W3c.Dom.NameList, "org.w3c.dom.NameList");
pragma Extensions_Allowed (Off);
