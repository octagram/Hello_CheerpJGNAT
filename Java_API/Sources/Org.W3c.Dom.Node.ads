pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Document;
limited with Org.W3c.Dom.NamedNodeMap;
limited with Org.W3c.Dom.NodeList;
limited with Org.W3c.Dom.UserDataHandler;
with Java.Lang.Object;

package Org.W3c.Dom.Node is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetNodeName (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function GetNodeValue (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure SetNodeValue (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetNodeType (This : access Typ)
                         return Java.Short is abstract;

   function GetParentNode (This : access Typ)
                           return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function GetChildNodes (This : access Typ)
                           return access Org.W3c.Dom.NodeList.Typ'Class is abstract;

   function GetFirstChild (This : access Typ)
                           return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function GetLastChild (This : access Typ)
                          return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function GetPreviousSibling (This : access Typ)
                                return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function GetNextSibling (This : access Typ)
                            return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function GetAttributes (This : access Typ)
                           return access Org.W3c.Dom.NamedNodeMap.Typ'Class is abstract;

   function GetOwnerDocument (This : access Typ)
                              return access Org.W3c.Dom.Document.Typ'Class is abstract;

   function InsertBefore (This : access Typ;
                          P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                          P2_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                          return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function ReplaceChild (This : access Typ;
                          P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                          P2_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                          return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function RemoveChild (This : access Typ;
                         P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                         return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function AppendChild (This : access Typ;
                         P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                         return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function HasChildNodes (This : access Typ)
                           return Java.Boolean is abstract;

   function CloneNode (This : access Typ;
                       P1_Boolean : Java.Boolean)
                       return access Org.W3c.Dom.Node.Typ'Class is abstract;

   procedure Normalize (This : access Typ) is abstract;

   function IsSupported (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Boolean is abstract;

   function GetNamespaceURI (This : access Typ)
                             return access Java.Lang.String.Typ'Class is abstract;

   function GetPrefix (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   procedure SetPrefix (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetLocalName (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   function HasAttributes (This : access Typ)
                           return Java.Boolean is abstract;

   function GetBaseURI (This : access Typ)
                        return access Java.Lang.String.Typ'Class is abstract;

   function CompareDocumentPosition (This : access Typ;
                                     P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                                     return Java.Short is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetTextContent (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure SetTextContent (This : access Typ;
                             P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function IsSameNode (This : access Typ;
                        P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                        return Java.Boolean is abstract;

   function LookupPrefix (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class)
                          return access Java.Lang.String.Typ'Class is abstract;

   function IsDefaultNamespace (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return Java.Boolean is abstract;

   function LookupNamespaceURI (This : access Typ;
                                P1_String : access Standard.Java.Lang.String.Typ'Class)
                                return access Java.Lang.String.Typ'Class is abstract;

   function IsEqualNode (This : access Typ;
                         P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                         return Java.Boolean is abstract;

   function GetFeature (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Java.Lang.Object.Typ'Class is abstract;

   function SetUserData (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Object : access Standard.Java.Lang.Object.Typ'Class;
                         P3_UserDataHandler : access Standard.Org.W3c.Dom.UserDataHandler.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;

   function GetUserData (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ELEMENT_NODE : constant Java.Short;

   --  final
   ATTRIBUTE_NODE : constant Java.Short;

   --  final
   TEXT_NODE : constant Java.Short;

   --  final
   CDATA_SECTION_NODE : constant Java.Short;

   --  final
   ENTITY_REFERENCE_NODE : constant Java.Short;

   --  final
   ENTITY_NODE : constant Java.Short;

   --  final
   PROCESSING_INSTRUCTION_NODE : constant Java.Short;

   --  final
   COMMENT_NODE : constant Java.Short;

   --  final
   DOCUMENT_NODE : constant Java.Short;

   --  final
   DOCUMENT_TYPE_NODE : constant Java.Short;

   --  final
   DOCUMENT_FRAGMENT_NODE : constant Java.Short;

   --  final
   NOTATION_NODE : constant Java.Short;

   --  final
   DOCUMENT_POSITION_DISCONNECTED : constant Java.Short;

   --  final
   DOCUMENT_POSITION_PRECEDING : constant Java.Short;

   --  final
   DOCUMENT_POSITION_FOLLOWING : constant Java.Short;

   --  final
   DOCUMENT_POSITION_CONTAINS : constant Java.Short;

   --  final
   DOCUMENT_POSITION_CONTAINED_BY : constant Java.Short;

   --  final
   DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetNodeName, "getNodeName");
   pragma Export (Java, GetNodeValue, "getNodeValue");
   pragma Export (Java, SetNodeValue, "setNodeValue");
   pragma Export (Java, GetNodeType, "getNodeType");
   pragma Export (Java, GetParentNode, "getParentNode");
   pragma Export (Java, GetChildNodes, "getChildNodes");
   pragma Export (Java, GetFirstChild, "getFirstChild");
   pragma Export (Java, GetLastChild, "getLastChild");
   pragma Export (Java, GetPreviousSibling, "getPreviousSibling");
   pragma Export (Java, GetNextSibling, "getNextSibling");
   pragma Export (Java, GetAttributes, "getAttributes");
   pragma Export (Java, GetOwnerDocument, "getOwnerDocument");
   pragma Export (Java, InsertBefore, "insertBefore");
   pragma Export (Java, ReplaceChild, "replaceChild");
   pragma Export (Java, RemoveChild, "removeChild");
   pragma Export (Java, AppendChild, "appendChild");
   pragma Export (Java, HasChildNodes, "hasChildNodes");
   pragma Export (Java, CloneNode, "cloneNode");
   pragma Export (Java, Normalize, "normalize");
   pragma Export (Java, IsSupported, "isSupported");
   pragma Export (Java, GetNamespaceURI, "getNamespaceURI");
   pragma Export (Java, GetPrefix, "getPrefix");
   pragma Export (Java, SetPrefix, "setPrefix");
   pragma Export (Java, GetLocalName, "getLocalName");
   pragma Export (Java, HasAttributes, "hasAttributes");
   pragma Export (Java, GetBaseURI, "getBaseURI");
   pragma Export (Java, CompareDocumentPosition, "compareDocumentPosition");
   pragma Export (Java, GetTextContent, "getTextContent");
   pragma Export (Java, SetTextContent, "setTextContent");
   pragma Export (Java, IsSameNode, "isSameNode");
   pragma Export (Java, LookupPrefix, "lookupPrefix");
   pragma Export (Java, IsDefaultNamespace, "isDefaultNamespace");
   pragma Export (Java, LookupNamespaceURI, "lookupNamespaceURI");
   pragma Export (Java, IsEqualNode, "isEqualNode");
   pragma Export (Java, GetFeature, "getFeature");
   pragma Export (Java, SetUserData, "setUserData");
   pragma Export (Java, GetUserData, "getUserData");
   pragma Import (Java, ELEMENT_NODE, "ELEMENT_NODE");
   pragma Import (Java, ATTRIBUTE_NODE, "ATTRIBUTE_NODE");
   pragma Import (Java, TEXT_NODE, "TEXT_NODE");
   pragma Import (Java, CDATA_SECTION_NODE, "CDATA_SECTION_NODE");
   pragma Import (Java, ENTITY_REFERENCE_NODE, "ENTITY_REFERENCE_NODE");
   pragma Import (Java, ENTITY_NODE, "ENTITY_NODE");
   pragma Import (Java, PROCESSING_INSTRUCTION_NODE, "PROCESSING_INSTRUCTION_NODE");
   pragma Import (Java, COMMENT_NODE, "COMMENT_NODE");
   pragma Import (Java, DOCUMENT_NODE, "DOCUMENT_NODE");
   pragma Import (Java, DOCUMENT_TYPE_NODE, "DOCUMENT_TYPE_NODE");
   pragma Import (Java, DOCUMENT_FRAGMENT_NODE, "DOCUMENT_FRAGMENT_NODE");
   pragma Import (Java, NOTATION_NODE, "NOTATION_NODE");
   pragma Import (Java, DOCUMENT_POSITION_DISCONNECTED, "DOCUMENT_POSITION_DISCONNECTED");
   pragma Import (Java, DOCUMENT_POSITION_PRECEDING, "DOCUMENT_POSITION_PRECEDING");
   pragma Import (Java, DOCUMENT_POSITION_FOLLOWING, "DOCUMENT_POSITION_FOLLOWING");
   pragma Import (Java, DOCUMENT_POSITION_CONTAINS, "DOCUMENT_POSITION_CONTAINS");
   pragma Import (Java, DOCUMENT_POSITION_CONTAINED_BY, "DOCUMENT_POSITION_CONTAINED_BY");
   pragma Import (Java, DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC, "DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC");

end Org.W3c.Dom.Node;
pragma Import (Java, Org.W3c.Dom.Node, "org.w3c.dom.Node");
pragma Extensions_Allowed (Off);
