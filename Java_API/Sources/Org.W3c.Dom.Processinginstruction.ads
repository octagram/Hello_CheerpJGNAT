pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.W3c.Dom.Node;

package Org.W3c.Dom.ProcessingInstruction is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Node_I : Org.W3c.Dom.Node.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTarget (This : access Typ)
                       return access Java.Lang.String.Typ'Class is abstract;

   function GetData (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   procedure SetData (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetTarget, "getTarget");
   pragma Export (Java, GetData, "getData");
   pragma Export (Java, SetData, "setData");

end Org.W3c.Dom.ProcessingInstruction;
pragma Import (Java, Org.W3c.Dom.ProcessingInstruction, "org.w3c.dom.ProcessingInstruction");
pragma Extensions_Allowed (Off);
