pragma Extensions_Allowed (On);
limited with Org.W3c.Dom.Ranges.Range_K;
with Java.Lang.Object;

package Org.W3c.Dom.Ranges.DocumentRange is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateRange (This : access Typ)
                         return access Org.W3c.Dom.Ranges.Range_K.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreateRange, "createRange");

end Org.W3c.Dom.Ranges.DocumentRange;
pragma Import (Java, Org.W3c.Dom.Ranges.DocumentRange, "org.w3c.dom.ranges.DocumentRange");
pragma Extensions_Allowed (Off);
