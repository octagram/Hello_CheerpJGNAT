pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.DocumentFragment;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;

package Org.W3c.Dom.Ranges.Range_K is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetStartContainer (This : access Typ)
                               return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetStartOffset (This : access Typ)
                            return Java.Int is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetEndContainer (This : access Typ)
                             return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetEndOffset (This : access Typ)
                          return Java.Int is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetCollapsed (This : access Typ)
                          return Java.Boolean is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetCommonAncestorContainer (This : access Typ)
                                        return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure SetStart (This : access Typ;
                       P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                       P2_Int : Java.Int) is abstract;
   --  can raise Org.W3c.Dom.Ranges.RangeException.Except and
   --  Org.W3c.Dom.DOMException.Except

   procedure SetEnd (This : access Typ;
                     P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                     P2_Int : Java.Int) is abstract;
   --  can raise Org.W3c.Dom.Ranges.RangeException.Except and
   --  Org.W3c.Dom.DOMException.Except

   procedure SetStartBefore (This : access Typ;
                             P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.Ranges.RangeException.Except and
   --  Org.W3c.Dom.DOMException.Except

   procedure SetStartAfter (This : access Typ;
                            P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.Ranges.RangeException.Except and
   --  Org.W3c.Dom.DOMException.Except

   procedure SetEndBefore (This : access Typ;
                           P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.Ranges.RangeException.Except and
   --  Org.W3c.Dom.DOMException.Except

   procedure SetEndAfter (This : access Typ;
                          P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.Ranges.RangeException.Except and
   --  Org.W3c.Dom.DOMException.Except

   procedure Collapse (This : access Typ;
                       P1_Boolean : Java.Boolean) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure SelectNode (This : access Typ;
                         P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.Ranges.RangeException.Except and
   --  Org.W3c.Dom.DOMException.Except

   procedure SelectNodeContents (This : access Typ;
                                 P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.Ranges.RangeException.Except and
   --  Org.W3c.Dom.DOMException.Except

   function CompareBoundaryPoints (This : access Typ;
                                   P1_Short : Java.Short;
                                   P2_Range_K : access Standard.Org.W3c.Dom.Ranges.Range_K.Typ'Class)
                                   return Java.Short is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure DeleteContents (This : access Typ) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function ExtractContents (This : access Typ)
                             return access Org.W3c.Dom.DocumentFragment.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function CloneContents (This : access Typ)
                           return access Org.W3c.Dom.DocumentFragment.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure InsertNode (This : access Typ;
                         P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except and
   --  Org.W3c.Dom.Ranges.RangeException.Except

   procedure SurroundContents (This : access Typ;
                               P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except and
   --  Org.W3c.Dom.Ranges.RangeException.Except

   function CloneRange (This : access Typ)
                        return access Org.W3c.Dom.Ranges.Range_K.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure Detach (This : access Typ) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   START_TO_START : constant Java.Short;

   --  final
   START_TO_END : constant Java.Short;

   --  final
   END_TO_END : constant Java.Short;

   --  final
   END_TO_START : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetStartContainer, "getStartContainer");
   pragma Export (Java, GetStartOffset, "getStartOffset");
   pragma Export (Java, GetEndContainer, "getEndContainer");
   pragma Export (Java, GetEndOffset, "getEndOffset");
   pragma Export (Java, GetCollapsed, "getCollapsed");
   pragma Export (Java, GetCommonAncestorContainer, "getCommonAncestorContainer");
   pragma Export (Java, SetStart, "setStart");
   pragma Export (Java, SetEnd, "setEnd");
   pragma Export (Java, SetStartBefore, "setStartBefore");
   pragma Export (Java, SetStartAfter, "setStartAfter");
   pragma Export (Java, SetEndBefore, "setEndBefore");
   pragma Export (Java, SetEndAfter, "setEndAfter");
   pragma Export (Java, Collapse, "collapse");
   pragma Export (Java, SelectNode, "selectNode");
   pragma Export (Java, SelectNodeContents, "selectNodeContents");
   pragma Export (Java, CompareBoundaryPoints, "compareBoundaryPoints");
   pragma Export (Java, DeleteContents, "deleteContents");
   pragma Export (Java, ExtractContents, "extractContents");
   pragma Export (Java, CloneContents, "cloneContents");
   pragma Export (Java, InsertNode, "insertNode");
   pragma Export (Java, SurroundContents, "surroundContents");
   pragma Export (Java, CloneRange, "cloneRange");
   pragma Export (Java, ToString, "toString");
   pragma Export (Java, Detach, "detach");
   pragma Import (Java, START_TO_START, "START_TO_START");
   pragma Import (Java, START_TO_END, "START_TO_END");
   pragma Import (Java, END_TO_END, "END_TO_END");
   pragma Import (Java, END_TO_START, "END_TO_START");

end Org.W3c.Dom.Ranges.Range_K;
pragma Import (Java, Org.W3c.Dom.Ranges.Range_K, "org.w3c.dom.ranges.Range");
pragma Extensions_Allowed (Off);
