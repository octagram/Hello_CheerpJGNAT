pragma Extensions_Allowed (On);
package Org.W3c.Dom.Ranges is
   pragma Preelaborate;
end Org.W3c.Dom.Ranges;
pragma Import (Java, Org.W3c.Dom.Ranges, "org.w3c.dom.ranges");
pragma Extensions_Allowed (Off);
