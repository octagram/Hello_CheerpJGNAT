pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Org.W3c.Dom.Stylesheets.MediaList is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMediaText (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   procedure SetMediaText (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function GetLength (This : access Typ)
                       return Java.Int is abstract;

   function Item (This : access Typ;
                  P1_Int : Java.Int)
                  return access Java.Lang.String.Typ'Class is abstract;

   procedure DeleteMedium (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure AppendMedium (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetMediaText, "getMediaText");
   pragma Export (Java, SetMediaText, "setMediaText");
   pragma Export (Java, GetLength, "getLength");
   pragma Export (Java, Item, "item");
   pragma Export (Java, DeleteMedium, "deleteMedium");
   pragma Export (Java, AppendMedium, "appendMedium");

end Org.W3c.Dom.Stylesheets.MediaList;
pragma Import (Java, Org.W3c.Dom.Stylesheets.MediaList, "org.w3c.dom.stylesheets.MediaList");
pragma Extensions_Allowed (Off);
