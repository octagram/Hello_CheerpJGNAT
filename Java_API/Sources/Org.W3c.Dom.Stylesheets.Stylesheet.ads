pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Node;
limited with Org.W3c.Dom.Stylesheets.MediaList;
with Java.Lang.Object;

package Org.W3c.Dom.Stylesheets.StyleSheet is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetType (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetDisabled (This : access Typ)
                         return Java.Boolean is abstract;

   procedure SetDisabled (This : access Typ;
                          P1_Boolean : Java.Boolean) is abstract;

   function GetOwnerNode (This : access Typ)
                          return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function GetParentStyleSheet (This : access Typ)
                                 return access Org.W3c.Dom.Stylesheets.StyleSheet.Typ'Class is abstract;

   function GetHref (This : access Typ)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetTitle (This : access Typ)
                      return access Java.Lang.String.Typ'Class is abstract;

   function GetMedia (This : access Typ)
                      return access Org.W3c.Dom.Stylesheets.MediaList.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, GetDisabled, "getDisabled");
   pragma Export (Java, SetDisabled, "setDisabled");
   pragma Export (Java, GetOwnerNode, "getOwnerNode");
   pragma Export (Java, GetParentStyleSheet, "getParentStyleSheet");
   pragma Export (Java, GetHref, "getHref");
   pragma Export (Java, GetTitle, "getTitle");
   pragma Export (Java, GetMedia, "getMedia");

end Org.W3c.Dom.Stylesheets.StyleSheet;
pragma Import (Java, Org.W3c.Dom.Stylesheets.StyleSheet, "org.w3c.dom.stylesheets.StyleSheet");
pragma Extensions_Allowed (Off);
