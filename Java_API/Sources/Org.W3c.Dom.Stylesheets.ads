pragma Extensions_Allowed (On);
package Org.W3c.Dom.Stylesheets is
   pragma Preelaborate;
end Org.W3c.Dom.Stylesheets;
pragma Import (Java, Org.W3c.Dom.Stylesheets, "org.w3c.dom.stylesheets");
pragma Extensions_Allowed (Off);
