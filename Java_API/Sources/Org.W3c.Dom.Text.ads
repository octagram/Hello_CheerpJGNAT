pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.W3c.Dom.CharacterData;

package Org.W3c.Dom.Text is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            CharacterData_I : Org.W3c.Dom.CharacterData.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function SplitText (This : access Typ;
                       P1_Int : Java.Int)
                       return access Org.W3c.Dom.Text.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function IsElementContentWhitespace (This : access Typ)
                                        return Java.Boolean is abstract;

   function GetWholeText (This : access Typ)
                          return access Java.Lang.String.Typ'Class is abstract;

   function ReplaceWholeText (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class)
                              return access Org.W3c.Dom.Text.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SplitText, "splitText");
   pragma Export (Java, IsElementContentWhitespace, "isElementContentWhitespace");
   pragma Export (Java, GetWholeText, "getWholeText");
   pragma Export (Java, ReplaceWholeText, "replaceWholeText");

end Org.W3c.Dom.Text;
pragma Import (Java, Org.W3c.Dom.Text, "org.w3c.dom.Text");
pragma Extensions_Allowed (Off);
