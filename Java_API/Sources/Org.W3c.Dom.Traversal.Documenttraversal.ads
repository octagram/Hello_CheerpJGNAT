pragma Extensions_Allowed (On);
limited with Org.W3c.Dom.Node;
limited with Org.W3c.Dom.Traversal.NodeFilter;
limited with Org.W3c.Dom.Traversal.NodeIterator;
limited with Org.W3c.Dom.Traversal.TreeWalker;
with Java.Lang.Object;

package Org.W3c.Dom.Traversal.DocumentTraversal is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateNodeIterator (This : access Typ;
                                P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                                P2_Int : Java.Int;
                                P3_NodeFilter : access Standard.Org.W3c.Dom.Traversal.NodeFilter.Typ'Class;
                                P4_Boolean : Java.Boolean)
                                return access Org.W3c.Dom.Traversal.NodeIterator.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function CreateTreeWalker (This : access Typ;
                              P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                              P2_Int : Java.Int;
                              P3_NodeFilter : access Standard.Org.W3c.Dom.Traversal.NodeFilter.Typ'Class;
                              P4_Boolean : Java.Boolean)
                              return access Org.W3c.Dom.Traversal.TreeWalker.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreateNodeIterator, "createNodeIterator");
   pragma Export (Java, CreateTreeWalker, "createTreeWalker");

end Org.W3c.Dom.Traversal.DocumentTraversal;
pragma Import (Java, Org.W3c.Dom.Traversal.DocumentTraversal, "org.w3c.dom.traversal.DocumentTraversal");
pragma Extensions_Allowed (Off);
