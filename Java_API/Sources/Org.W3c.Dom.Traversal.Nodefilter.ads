pragma Extensions_Allowed (On);
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;

package Org.W3c.Dom.Traversal.NodeFilter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function AcceptNode (This : access Typ;
                        P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                        return Java.Short is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   FILTER_ACCEPT : constant Java.Short;

   --  final
   FILTER_REJECT : constant Java.Short;

   --  final
   FILTER_SKIP : constant Java.Short;

   --  final
   SHOW_ALL : constant Java.Int;

   --  final
   SHOW_ELEMENT : constant Java.Int;

   --  final
   SHOW_ATTRIBUTE : constant Java.Int;

   --  final
   SHOW_TEXT : constant Java.Int;

   --  final
   SHOW_CDATA_SECTION : constant Java.Int;

   --  final
   SHOW_ENTITY_REFERENCE : constant Java.Int;

   --  final
   SHOW_ENTITY : constant Java.Int;

   --  final
   SHOW_PROCESSING_INSTRUCTION : constant Java.Int;

   --  final
   SHOW_COMMENT : constant Java.Int;

   --  final
   SHOW_DOCUMENT : constant Java.Int;

   --  final
   SHOW_DOCUMENT_TYPE : constant Java.Int;

   --  final
   SHOW_DOCUMENT_FRAGMENT : constant Java.Int;

   --  final
   SHOW_NOTATION : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, AcceptNode, "acceptNode");
   pragma Import (Java, FILTER_ACCEPT, "FILTER_ACCEPT");
   pragma Import (Java, FILTER_REJECT, "FILTER_REJECT");
   pragma Import (Java, FILTER_SKIP, "FILTER_SKIP");
   pragma Import (Java, SHOW_ALL, "SHOW_ALL");
   pragma Import (Java, SHOW_ELEMENT, "SHOW_ELEMENT");
   pragma Import (Java, SHOW_ATTRIBUTE, "SHOW_ATTRIBUTE");
   pragma Import (Java, SHOW_TEXT, "SHOW_TEXT");
   pragma Import (Java, SHOW_CDATA_SECTION, "SHOW_CDATA_SECTION");
   pragma Import (Java, SHOW_ENTITY_REFERENCE, "SHOW_ENTITY_REFERENCE");
   pragma Import (Java, SHOW_ENTITY, "SHOW_ENTITY");
   pragma Import (Java, SHOW_PROCESSING_INSTRUCTION, "SHOW_PROCESSING_INSTRUCTION");
   pragma Import (Java, SHOW_COMMENT, "SHOW_COMMENT");
   pragma Import (Java, SHOW_DOCUMENT, "SHOW_DOCUMENT");
   pragma Import (Java, SHOW_DOCUMENT_TYPE, "SHOW_DOCUMENT_TYPE");
   pragma Import (Java, SHOW_DOCUMENT_FRAGMENT, "SHOW_DOCUMENT_FRAGMENT");
   pragma Import (Java, SHOW_NOTATION, "SHOW_NOTATION");

end Org.W3c.Dom.Traversal.NodeFilter;
pragma Import (Java, Org.W3c.Dom.Traversal.NodeFilter, "org.w3c.dom.traversal.NodeFilter");
pragma Extensions_Allowed (Off);
