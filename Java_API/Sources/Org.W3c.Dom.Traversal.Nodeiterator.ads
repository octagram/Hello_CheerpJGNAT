pragma Extensions_Allowed (On);
limited with Org.W3c.Dom.Node;
limited with Org.W3c.Dom.Traversal.NodeFilter;
with Java.Lang.Object;

package Org.W3c.Dom.Traversal.NodeIterator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRoot (This : access Typ)
                     return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function GetWhatToShow (This : access Typ)
                           return Java.Int is abstract;

   function GetFilter (This : access Typ)
                       return access Org.W3c.Dom.Traversal.NodeFilter.Typ'Class is abstract;

   function GetExpandEntityReferences (This : access Typ)
                                       return Java.Boolean is abstract;

   function NextNode (This : access Typ)
                      return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function PreviousNode (This : access Typ)
                          return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   procedure Detach (This : access Typ) is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetRoot, "getRoot");
   pragma Export (Java, GetWhatToShow, "getWhatToShow");
   pragma Export (Java, GetFilter, "getFilter");
   pragma Export (Java, GetExpandEntityReferences, "getExpandEntityReferences");
   pragma Export (Java, NextNode, "nextNode");
   pragma Export (Java, PreviousNode, "previousNode");
   pragma Export (Java, Detach, "detach");

end Org.W3c.Dom.Traversal.NodeIterator;
pragma Import (Java, Org.W3c.Dom.Traversal.NodeIterator, "org.w3c.dom.traversal.NodeIterator");
pragma Extensions_Allowed (Off);
