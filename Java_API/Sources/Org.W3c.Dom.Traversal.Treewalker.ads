pragma Extensions_Allowed (On);
limited with Org.W3c.Dom.Node;
limited with Org.W3c.Dom.Traversal.NodeFilter;
with Java.Lang.Object;

package Org.W3c.Dom.Traversal.TreeWalker is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetRoot (This : access Typ)
                     return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function GetWhatToShow (This : access Typ)
                           return Java.Int is abstract;

   function GetFilter (This : access Typ)
                       return access Org.W3c.Dom.Traversal.NodeFilter.Typ'Class is abstract;

   function GetExpandEntityReferences (This : access Typ)
                                       return Java.Boolean is abstract;

   function GetCurrentNode (This : access Typ)
                            return access Org.W3c.Dom.Node.Typ'Class is abstract;

   procedure SetCurrentNode (This : access Typ;
                             P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class) is abstract;
   --  can raise Org.W3c.Dom.DOMException.Except

   function ParentNode (This : access Typ)
                        return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function FirstChild (This : access Typ)
                        return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function LastChild (This : access Typ)
                       return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function PreviousSibling (This : access Typ)
                             return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function NextSibling (This : access Typ)
                         return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function PreviousNode (This : access Typ)
                          return access Org.W3c.Dom.Node.Typ'Class is abstract;

   function NextNode (This : access Typ)
                      return access Org.W3c.Dom.Node.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetRoot, "getRoot");
   pragma Export (Java, GetWhatToShow, "getWhatToShow");
   pragma Export (Java, GetFilter, "getFilter");
   pragma Export (Java, GetExpandEntityReferences, "getExpandEntityReferences");
   pragma Export (Java, GetCurrentNode, "getCurrentNode");
   pragma Export (Java, SetCurrentNode, "setCurrentNode");
   pragma Export (Java, ParentNode, "parentNode");
   pragma Export (Java, FirstChild, "firstChild");
   pragma Export (Java, LastChild, "lastChild");
   pragma Export (Java, PreviousSibling, "previousSibling");
   pragma Export (Java, NextSibling, "nextSibling");
   pragma Export (Java, PreviousNode, "previousNode");
   pragma Export (Java, NextNode, "nextNode");

end Org.W3c.Dom.Traversal.TreeWalker;
pragma Import (Java, Org.W3c.Dom.Traversal.TreeWalker, "org.w3c.dom.traversal.TreeWalker");
pragma Extensions_Allowed (Off);
