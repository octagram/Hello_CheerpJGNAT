pragma Extensions_Allowed (On);
package Org.W3c.Dom.Traversal is
   pragma Preelaborate;
end Org.W3c.Dom.Traversal;
pragma Import (Java, Org.W3c.Dom.Traversal, "org.w3c.dom.traversal");
pragma Extensions_Allowed (Off);
