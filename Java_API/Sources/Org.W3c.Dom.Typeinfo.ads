pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Org.W3c.Dom.TypeInfo is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetTypeName (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;

   function GetTypeNamespace (This : access Typ)
                              return access Java.Lang.String.Typ'Class is abstract;

   function IsDerivedFrom (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_Int : Java.Int)
                           return Java.Boolean is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   DERIVATION_RESTRICTION : constant Java.Int;

   --  final
   DERIVATION_EXTENSION : constant Java.Int;

   --  final
   DERIVATION_UNION : constant Java.Int;

   --  final
   DERIVATION_LIST : constant Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetTypeName, "getTypeName");
   pragma Export (Java, GetTypeNamespace, "getTypeNamespace");
   pragma Export (Java, IsDerivedFrom, "isDerivedFrom");
   pragma Import (Java, DERIVATION_RESTRICTION, "DERIVATION_RESTRICTION");
   pragma Import (Java, DERIVATION_EXTENSION, "DERIVATION_EXTENSION");
   pragma Import (Java, DERIVATION_UNION, "DERIVATION_UNION");
   pragma Import (Java, DERIVATION_LIST, "DERIVATION_LIST");

end Org.W3c.Dom.TypeInfo;
pragma Import (Java, Org.W3c.Dom.TypeInfo, "org.w3c.dom.TypeInfo");
pragma Extensions_Allowed (Off);
