pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;

package Org.W3c.Dom.UserDataHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Handle (This : access Typ;
                     P1_Short : Java.Short;
                     P2_String : access Standard.Java.Lang.String.Typ'Class;
                     P3_Object : access Standard.Java.Lang.Object.Typ'Class;
                     P4_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                     P5_Node : access Standard.Org.W3c.Dom.Node.Typ'Class) is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   NODE_CLONED : constant Java.Short;

   --  final
   NODE_IMPORTED : constant Java.Short;

   --  final
   NODE_DELETED : constant Java.Short;

   --  final
   NODE_RENAMED : constant Java.Short;

   --  final
   NODE_ADOPTED : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Handle, "handle");
   pragma Import (Java, NODE_CLONED, "NODE_CLONED");
   pragma Import (Java, NODE_IMPORTED, "NODE_IMPORTED");
   pragma Import (Java, NODE_DELETED, "NODE_DELETED");
   pragma Import (Java, NODE_RENAMED, "NODE_RENAMED");
   pragma Import (Java, NODE_ADOPTED, "NODE_ADOPTED");

end Org.W3c.Dom.UserDataHandler;
pragma Import (Java, Org.W3c.Dom.UserDataHandler, "org.w3c.dom.UserDataHandler");
pragma Extensions_Allowed (Off);
