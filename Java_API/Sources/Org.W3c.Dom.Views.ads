pragma Extensions_Allowed (On);
package Org.W3c.Dom.Views is
   pragma Preelaborate;
end Org.W3c.Dom.Views;
pragma Import (Java, Org.W3c.Dom.Views, "org.w3c.dom.views");
pragma Extensions_Allowed (Off);
