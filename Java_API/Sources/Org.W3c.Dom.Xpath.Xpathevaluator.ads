pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Node;
limited with Org.W3c.Dom.Xpath.XPathExpression;
limited with Org.W3c.Dom.Xpath.XPathNSResolver;
with Java.Lang.Object;

package Org.W3c.Dom.Xpath.XPathEvaluator is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateExpression (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_XPathNSResolver : access Standard.Org.W3c.Dom.Xpath.XPathNSResolver.Typ'Class)
                              return access Org.W3c.Dom.Xpath.XPathExpression.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.Xpath.XPathException.Except and
   --  Org.W3c.Dom.DOMException.Except

   function CreateNSResolver (This : access Typ;
                              P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class)
                              return access Org.W3c.Dom.Xpath.XPathNSResolver.Typ'Class is abstract;

   function Evaluate (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                      P3_XPathNSResolver : access Standard.Org.W3c.Dom.Xpath.XPathNSResolver.Typ'Class;
                      P4_Short : Java.Short;
                      P5_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.Xpath.XPathException.Except and
   --  Org.W3c.Dom.DOMException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, CreateExpression, "createExpression");
   pragma Export (Java, CreateNSResolver, "createNSResolver");
   pragma Export (Java, Evaluate, "evaluate");

end Org.W3c.Dom.Xpath.XPathEvaluator;
pragma Import (Java, Org.W3c.Dom.Xpath.XPathEvaluator, "org.w3c.dom.xpath.XPathEvaluator");
pragma Extensions_Allowed (Off);
