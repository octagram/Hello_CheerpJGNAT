pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Io.Serializable;
with Java.Lang.Object;
with Java.Lang.RuntimeException;

package Org.W3c.Dom.Xpath.XPathException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.RuntimeException.Typ(Serializable_I) with record
      
      ------------------------
      -- Field Declarations --
      ------------------------

      Code : Java.Short;
      pragma Import (Java, Code, "code");

   end record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_XPathException (P1_Short : Java.Short;
                                P2_String : access Standard.Java.Lang.String.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   INVALID_EXPRESSION_ERR : constant Java.Short;

   --  final
   TYPE_ERR : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "org.w3c.dom.xpath.XPathException");
   pragma Java_Constructor (New_XPathException);
   pragma Import (Java, INVALID_EXPRESSION_ERR, "INVALID_EXPRESSION_ERR");
   pragma Import (Java, TYPE_ERR, "TYPE_ERR");

end Org.W3c.Dom.Xpath.XPathException;
pragma Import (Java, Org.W3c.Dom.Xpath.XPathException, "org.w3c.dom.xpath.XPathException");
pragma Extensions_Allowed (Off);
