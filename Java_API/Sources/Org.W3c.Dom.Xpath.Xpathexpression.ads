pragma Extensions_Allowed (On);
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;

package Org.W3c.Dom.Xpath.XPathExpression is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function Evaluate (This : access Typ;
                      P1_Node : access Standard.Org.W3c.Dom.Node.Typ'Class;
                      P2_Short : Java.Short;
                      P3_Object : access Standard.Java.Lang.Object.Typ'Class)
                      return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.Xpath.XPathException.Except and
   --  Org.W3c.Dom.DOMException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Evaluate, "evaluate");

end Org.W3c.Dom.Xpath.XPathExpression;
pragma Import (Java, Org.W3c.Dom.Xpath.XPathExpression, "org.w3c.dom.xpath.XPathExpression");
pragma Extensions_Allowed (Off);
