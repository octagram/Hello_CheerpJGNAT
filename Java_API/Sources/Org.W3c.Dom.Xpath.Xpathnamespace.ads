pragma Extensions_Allowed (On);
limited with Org.W3c.Dom.Element;
with Java.Lang.Object;
with Org.W3c.Dom.Node;

package Org.W3c.Dom.Xpath.XPathNamespace is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Node_I : Org.W3c.Dom.Node.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetOwnerElement (This : access Typ)
                             return access Org.W3c.Dom.Element.Typ'Class is abstract;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   XPATH_NAMESPACE_NODE : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetOwnerElement, "getOwnerElement");
   pragma Import (Java, XPATH_NAMESPACE_NODE, "XPATH_NAMESPACE_NODE");

end Org.W3c.Dom.Xpath.XPathNamespace;
pragma Import (Java, Org.W3c.Dom.Xpath.XPathNamespace, "org.w3c.dom.xpath.XPathNamespace");
pragma Extensions_Allowed (Off);
