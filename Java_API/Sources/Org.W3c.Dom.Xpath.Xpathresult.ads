pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.W3c.Dom.Node;
with Java.Lang.Object;

package Org.W3c.Dom.Xpath.XPathResult is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetResultType (This : access Typ)
                           return Java.Short is abstract;

   function GetNumberValue (This : access Typ)
                            return Java.Double is abstract;
   --  can raise Org.W3c.Dom.Xpath.XPathException.Except

   function GetStringValue (This : access Typ)
                            return access Java.Lang.String.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.Xpath.XPathException.Except

   function GetBooleanValue (This : access Typ)
                             return Java.Boolean is abstract;
   --  can raise Org.W3c.Dom.Xpath.XPathException.Except

   function GetSingleNodeValue (This : access Typ)
                                return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.Xpath.XPathException.Except

   function GetInvalidIteratorState (This : access Typ)
                                     return Java.Boolean is abstract;

   function GetSnapshotLength (This : access Typ)
                               return Java.Int is abstract;
   --  can raise Org.W3c.Dom.Xpath.XPathException.Except

   function IterateNext (This : access Typ)
                         return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.Xpath.XPathException.Except and
   --  Org.W3c.Dom.DOMException.Except

   function SnapshotItem (This : access Typ;
                          P1_Int : Java.Int)
                          return access Org.W3c.Dom.Node.Typ'Class is abstract;
   --  can raise Org.W3c.Dom.Xpath.XPathException.Except

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   ANY_TYPE : constant Java.Short;

   --  final
   NUMBER_TYPE : constant Java.Short;

   --  final
   STRING_TYPE : constant Java.Short;

   --  final
   BOOLEAN_TYPE : constant Java.Short;

   --  final
   UNORDERED_NODE_ITERATOR_TYPE : constant Java.Short;

   --  final
   ORDERED_NODE_ITERATOR_TYPE : constant Java.Short;

   --  final
   UNORDERED_NODE_SNAPSHOT_TYPE : constant Java.Short;

   --  final
   ORDERED_NODE_SNAPSHOT_TYPE : constant Java.Short;

   --  final
   ANY_UNORDERED_NODE_TYPE : constant Java.Short;

   --  final
   FIRST_ORDERED_NODE_TYPE : constant Java.Short;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetResultType, "getResultType");
   pragma Export (Java, GetNumberValue, "getNumberValue");
   pragma Export (Java, GetStringValue, "getStringValue");
   pragma Export (Java, GetBooleanValue, "getBooleanValue");
   pragma Export (Java, GetSingleNodeValue, "getSingleNodeValue");
   pragma Export (Java, GetInvalidIteratorState, "getInvalidIteratorState");
   pragma Export (Java, GetSnapshotLength, "getSnapshotLength");
   pragma Export (Java, IterateNext, "iterateNext");
   pragma Export (Java, SnapshotItem, "snapshotItem");
   pragma Import (Java, ANY_TYPE, "ANY_TYPE");
   pragma Import (Java, NUMBER_TYPE, "NUMBER_TYPE");
   pragma Import (Java, STRING_TYPE, "STRING_TYPE");
   pragma Import (Java, BOOLEAN_TYPE, "BOOLEAN_TYPE");
   pragma Import (Java, UNORDERED_NODE_ITERATOR_TYPE, "UNORDERED_NODE_ITERATOR_TYPE");
   pragma Import (Java, ORDERED_NODE_ITERATOR_TYPE, "ORDERED_NODE_ITERATOR_TYPE");
   pragma Import (Java, UNORDERED_NODE_SNAPSHOT_TYPE, "UNORDERED_NODE_SNAPSHOT_TYPE");
   pragma Import (Java, ORDERED_NODE_SNAPSHOT_TYPE, "ORDERED_NODE_SNAPSHOT_TYPE");
   pragma Import (Java, ANY_UNORDERED_NODE_TYPE, "ANY_UNORDERED_NODE_TYPE");
   pragma Import (Java, FIRST_ORDERED_NODE_TYPE, "FIRST_ORDERED_NODE_TYPE");

end Org.W3c.Dom.Xpath.XPathResult;
pragma Import (Java, Org.W3c.Dom.Xpath.XPathResult, "org.w3c.dom.xpath.XPathResult");
pragma Extensions_Allowed (Off);
