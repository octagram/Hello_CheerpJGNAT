pragma Extensions_Allowed (On);
package Org.W3c.Dom.Xpath is
   pragma Preelaborate;
end Org.W3c.Dom.Xpath;
pragma Import (Java, Org.W3c.Dom.Xpath, "org.w3c.dom.xpath");
pragma Extensions_Allowed (Off);
