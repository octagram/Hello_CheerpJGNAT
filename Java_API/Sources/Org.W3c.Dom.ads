pragma Extensions_Allowed (On);
package Org.W3c.Dom is
   pragma Preelaborate;
end Org.W3c.Dom;
pragma Import (Java, Org.W3c.Dom, "org.w3c.dom");
pragma Extensions_Allowed (Off);
