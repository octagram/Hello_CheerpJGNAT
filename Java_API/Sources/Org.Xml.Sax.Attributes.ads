pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Org.Xml.Sax.Attributes is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLength (This : access Typ)
                       return Java.Int is abstract;

   function GetURI (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Lang.String.Typ'Class is abstract;

   function GetLocalName (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Lang.String.Typ'Class is abstract;

   function GetQName (This : access Typ;
                      P1_Int : Java.Int)
                      return access Java.Lang.String.Typ'Class is abstract;

   function GetType (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetValue (This : access Typ;
                      P1_Int : Java.Int)
                      return access Java.Lang.String.Typ'Class is abstract;

   function GetIndex (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Int is abstract;

   function GetIndex (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Int is abstract;

   function GetType (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetType (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.String.Typ'Class is abstract;

   function GetValue (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Lang.String.Typ'Class is abstract;

   function GetValue (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetLength, "getLength");
   pragma Export (Java, GetURI, "getURI");
   pragma Export (Java, GetLocalName, "getLocalName");
   pragma Export (Java, GetQName, "getQName");
   pragma Export (Java, GetType, "getType");
   pragma Export (Java, GetValue, "getValue");
   pragma Export (Java, GetIndex, "getIndex");

end Org.Xml.Sax.Attributes;
pragma Import (Java, Org.Xml.Sax.Attributes, "org.xml.sax.Attributes");
pragma Extensions_Allowed (Off);
