pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Xml.Sax.Attributes;
limited with Org.Xml.Sax.Locator;
with Java.Lang.Object;

package Org.Xml.Sax.ContentHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetDocumentLocator (This : access Typ;
                                 P1_Locator : access Standard.Org.Xml.Sax.Locator.Typ'Class) is abstract;

   procedure StartDocument (This : access Typ) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure EndDocument (This : access Typ) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure StartPrefixMapping (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure EndPrefixMapping (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure StartElement (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class;
                           P4_Attributes : access Standard.Org.Xml.Sax.Attributes.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure EndElement (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure Characters (This : access Typ;
                         P1_Char_Arr : Java.Char_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure IgnorableWhitespace (This : access Typ;
                                  P1_Char_Arr : Java.Char_Arr;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure ProcessingInstruction (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure SkippedEntity (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetDocumentLocator, "setDocumentLocator");
   pragma Export (Java, StartDocument, "startDocument");
   pragma Export (Java, EndDocument, "endDocument");
   pragma Export (Java, StartPrefixMapping, "startPrefixMapping");
   pragma Export (Java, EndPrefixMapping, "endPrefixMapping");
   pragma Export (Java, StartElement, "startElement");
   pragma Export (Java, EndElement, "endElement");
   pragma Export (Java, Characters, "characters");
   pragma Export (Java, IgnorableWhitespace, "ignorableWhitespace");
   pragma Export (Java, ProcessingInstruction, "processingInstruction");
   pragma Export (Java, SkippedEntity, "skippedEntity");

end Org.Xml.Sax.ContentHandler;
pragma Import (Java, Org.Xml.Sax.ContentHandler, "org.xml.sax.ContentHandler");
pragma Extensions_Allowed (Off);
