pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Org.Xml.Sax.DTDHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure NotationDecl (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure UnparsedEntityDecl (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                 P3_String : access Standard.Java.Lang.String.Typ'Class;
                                 P4_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, NotationDecl, "notationDecl");
   pragma Export (Java, UnparsedEntityDecl, "unparsedEntityDecl");

end Org.Xml.Sax.DTDHandler;
pragma Import (Java, Org.Xml.Sax.DTDHandler, "org.xml.sax.DTDHandler");
pragma Extensions_Allowed (Off);
