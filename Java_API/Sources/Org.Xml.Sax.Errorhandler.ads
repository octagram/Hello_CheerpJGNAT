pragma Extensions_Allowed (On);
limited with Org.Xml.Sax.SAXParseException;
with Java.Lang.Object;

package Org.Xml.Sax.ErrorHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Warning (This : access Typ;
                      P1_SAXParseException : access Standard.Org.Xml.Sax.SAXParseException.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure Error (This : access Typ;
                    P1_SAXParseException : access Standard.Org.Xml.Sax.SAXParseException.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure FatalError (This : access Typ;
                         P1_SAXParseException : access Standard.Org.Xml.Sax.SAXParseException.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, Warning, "warning");
   pragma Export (Java, Error, "error");
   pragma Export (Java, FatalError, "fatalError");

end Org.Xml.Sax.ErrorHandler;
pragma Import (Java, Org.Xml.Sax.ErrorHandler, "org.xml.sax.ErrorHandler");
pragma Extensions_Allowed (Off);
