pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.Xml.Sax.Attributes;

package Org.Xml.Sax.Ext.Attributes2 is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Attributes_I : Org.Xml.Sax.Attributes.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsDeclared (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Boolean is abstract;

   function IsDeclared (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean is abstract;

   function IsDeclared (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean is abstract;

   function IsSpecified (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Boolean is abstract;

   function IsSpecified (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Boolean is abstract;

   function IsSpecified (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Boolean is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, IsDeclared, "isDeclared");
   pragma Export (Java, IsSpecified, "isSpecified");

end Org.Xml.Sax.Ext.Attributes2;
pragma Import (Java, Org.Xml.Sax.Ext.Attributes2, "org.xml.sax.ext.Attributes2");
pragma Extensions_Allowed (Off);
