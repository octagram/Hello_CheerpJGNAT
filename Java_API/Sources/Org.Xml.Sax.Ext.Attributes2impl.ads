pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.Xml.Sax.Attributes;
with Org.Xml.Sax.Ext.Attributes2;
with Org.Xml.Sax.Helpers.AttributesImpl;

package Org.Xml.Sax.Ext.Attributes2Impl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Attributes_I : Org.Xml.Sax.Attributes.Ref;
            Attributes2_I : Org.Xml.Sax.Ext.Attributes2.Ref)
    is new Org.Xml.Sax.Helpers.AttributesImpl.Typ(Attributes_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Attributes2Impl (This : Ref := null)
                                 return Ref;

   function New_Attributes2Impl (P1_Attributes : access Standard.Org.Xml.Sax.Attributes.Typ'Class; 
                                 This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function IsDeclared (This : access Typ;
                        P1_Int : Java.Int)
                        return Java.Boolean;

   function IsDeclared (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class;
                        P2_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean;

   function IsDeclared (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean;

   function IsSpecified (This : access Typ;
                         P1_Int : Java.Int)
                         return Java.Boolean;

   function IsSpecified (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Boolean;

   function IsSpecified (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return Java.Boolean;

   procedure SetAttributes (This : access Typ;
                            P1_Attributes : access Standard.Org.Xml.Sax.Attributes.Typ'Class);

   procedure AddAttribute (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class;
                           P4_String : access Standard.Java.Lang.String.Typ'Class;
                           P5_String : access Standard.Java.Lang.String.Typ'Class);

   procedure RemoveAttribute (This : access Typ;
                              P1_Int : Java.Int);

   procedure SetDeclared (This : access Typ;
                          P1_Int : Java.Int;
                          P2_Boolean : Java.Boolean);

   procedure SetSpecified (This : access Typ;
                           P1_Int : Java.Int;
                           P2_Boolean : Java.Boolean);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Attributes2Impl);
   pragma Import (Java, IsDeclared, "isDeclared");
   pragma Import (Java, IsSpecified, "isSpecified");
   pragma Import (Java, SetAttributes, "setAttributes");
   pragma Import (Java, AddAttribute, "addAttribute");
   pragma Import (Java, RemoveAttribute, "removeAttribute");
   pragma Import (Java, SetDeclared, "setDeclared");
   pragma Import (Java, SetSpecified, "setSpecified");

end Org.Xml.Sax.Ext.Attributes2Impl;
pragma Import (Java, Org.Xml.Sax.Ext.Attributes2Impl, "org.xml.sax.ext.Attributes2Impl");
pragma Extensions_Allowed (Off);
