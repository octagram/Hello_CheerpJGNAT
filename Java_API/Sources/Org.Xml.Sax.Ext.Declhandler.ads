pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Org.Xml.Sax.Ext.DeclHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure ElementDecl (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure AttributeDecl (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                            P3_String : access Standard.Java.Lang.String.Typ'Class;
                            P4_String : access Standard.Java.Lang.String.Typ'Class;
                            P5_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure InternalEntityDecl (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure ExternalEntityDecl (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                 P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, ElementDecl, "elementDecl");
   pragma Export (Java, AttributeDecl, "attributeDecl");
   pragma Export (Java, InternalEntityDecl, "internalEntityDecl");
   pragma Export (Java, ExternalEntityDecl, "externalEntityDecl");

end Org.Xml.Sax.Ext.DeclHandler;
pragma Import (Java, Org.Xml.Sax.Ext.DeclHandler, "org.xml.sax.ext.DeclHandler");
pragma Extensions_Allowed (Off);
