pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Xml.Sax.InputSource;
with Java.Lang.Object;
with Org.Xml.Sax.ContentHandler;
with Org.Xml.Sax.DTDHandler;
with Org.Xml.Sax.EntityResolver;
with Org.Xml.Sax.ErrorHandler;
with Org.Xml.Sax.Ext.DeclHandler;
with Org.Xml.Sax.Ext.EntityResolver2;
with Org.Xml.Sax.Ext.LexicalHandler;
with Org.Xml.Sax.Helpers.DefaultHandler;

package Org.Xml.Sax.Ext.DefaultHandler2 is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ContentHandler_I : Org.Xml.Sax.ContentHandler.Ref;
            DTDHandler_I : Org.Xml.Sax.DTDHandler.Ref;
            EntityResolver_I : Org.Xml.Sax.EntityResolver.Ref;
            ErrorHandler_I : Org.Xml.Sax.ErrorHandler.Ref;
            DeclHandler_I : Org.Xml.Sax.Ext.DeclHandler.Ref;
            EntityResolver2_I : Org.Xml.Sax.Ext.EntityResolver2.Ref;
            LexicalHandler_I : Org.Xml.Sax.Ext.LexicalHandler.Ref)
    is new Org.Xml.Sax.Helpers.DefaultHandler.Typ(ContentHandler_I,
                                                  DTDHandler_I,
                                                  EntityResolver_I,
                                                  ErrorHandler_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_DefaultHandler2 (This : Ref := null)
                                 return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure StartCDATA (This : access Typ);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure EndCDATA (This : access Typ);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure StartDTD (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class;
                       P3_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure EndDTD (This : access Typ);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure StartEntity (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure EndEntity (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure Comment (This : access Typ;
                      P1_Char_Arr : Java.Char_Arr;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure AttributeDecl (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class;
                            P2_String : access Standard.Java.Lang.String.Typ'Class;
                            P3_String : access Standard.Java.Lang.String.Typ'Class;
                            P4_String : access Standard.Java.Lang.String.Typ'Class;
                            P5_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure ElementDecl (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure ExternalEntityDecl (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                 P3_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure InternalEntityDecl (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   function GetExternalSubset (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Org.Xml.Sax.InputSource.Typ'Class;
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   function ResolveEntity (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class;
                           P4_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Org.Xml.Sax.InputSource.Typ'Class;
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   function ResolveEntity (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Org.Xml.Sax.InputSource.Typ'Class;
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_DefaultHandler2);
   pragma Import (Java, StartCDATA, "startCDATA");
   pragma Import (Java, EndCDATA, "endCDATA");
   pragma Import (Java, StartDTD, "startDTD");
   pragma Import (Java, EndDTD, "endDTD");
   pragma Import (Java, StartEntity, "startEntity");
   pragma Import (Java, EndEntity, "endEntity");
   pragma Import (Java, Comment, "comment");
   pragma Import (Java, AttributeDecl, "attributeDecl");
   pragma Import (Java, ElementDecl, "elementDecl");
   pragma Import (Java, ExternalEntityDecl, "externalEntityDecl");
   pragma Import (Java, InternalEntityDecl, "internalEntityDecl");
   pragma Import (Java, GetExternalSubset, "getExternalSubset");
   pragma Import (Java, ResolveEntity, "resolveEntity");

end Org.Xml.Sax.Ext.DefaultHandler2;
pragma Import (Java, Org.Xml.Sax.Ext.DefaultHandler2, "org.xml.sax.ext.DefaultHandler2");
pragma Extensions_Allowed (Off);
