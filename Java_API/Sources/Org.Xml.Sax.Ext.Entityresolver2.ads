pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Xml.Sax.InputSource;
with Java.Lang.Object;
with Org.Xml.Sax.EntityResolver;

package Org.Xml.Sax.Ext.EntityResolver2 is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            EntityResolver_I : Org.Xml.Sax.EntityResolver.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetExternalSubset (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class;
                               P2_String : access Standard.Java.Lang.String.Typ'Class)
                               return access Org.Xml.Sax.InputSource.Typ'Class is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   function ResolveEntity (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class;
                           P4_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Org.Xml.Sax.InputSource.Typ'Class is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetExternalSubset, "getExternalSubset");
   pragma Export (Java, ResolveEntity, "resolveEntity");

end Org.Xml.Sax.Ext.EntityResolver2;
pragma Import (Java, Org.Xml.Sax.Ext.EntityResolver2, "org.xml.sax.ext.EntityResolver2");
pragma Extensions_Allowed (Off);
