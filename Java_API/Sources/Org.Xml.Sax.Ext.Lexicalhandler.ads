pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;

package Org.Xml.Sax.Ext.LexicalHandler is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure StartDTD (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class;
                       P2_String : access Standard.Java.Lang.String.Typ'Class;
                       P3_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure EndDTD (This : access Typ) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure StartEntity (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure EndEntity (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure StartCDATA (This : access Typ) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure EndCDATA (This : access Typ) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure Comment (This : access Typ;
                      P1_Char_Arr : Java.Char_Arr;
                      P2_Int : Java.Int;
                      P3_Int : Java.Int) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, StartDTD, "startDTD");
   pragma Export (Java, EndDTD, "endDTD");
   pragma Export (Java, StartEntity, "startEntity");
   pragma Export (Java, EndEntity, "endEntity");
   pragma Export (Java, StartCDATA, "startCDATA");
   pragma Export (Java, EndCDATA, "endCDATA");
   pragma Export (Java, Comment, "comment");

end Org.Xml.Sax.Ext.LexicalHandler;
pragma Import (Java, Org.Xml.Sax.Ext.LexicalHandler, "org.xml.sax.ext.LexicalHandler");
pragma Extensions_Allowed (Off);
