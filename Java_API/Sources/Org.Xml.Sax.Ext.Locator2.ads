pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.Xml.Sax.Locator;

package Org.Xml.Sax.Ext.Locator2 is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            Locator_I : Org.Xml.Sax.Locator.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetXMLVersion (This : access Typ)
                           return access Java.Lang.String.Typ'Class is abstract;

   function GetEncoding (This : access Typ)
                         return access Java.Lang.String.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetXMLVersion, "getXMLVersion");
   pragma Export (Java, GetEncoding, "getEncoding");

end Org.Xml.Sax.Ext.Locator2;
pragma Import (Java, Org.Xml.Sax.Ext.Locator2, "org.xml.sax.ext.Locator2");
pragma Extensions_Allowed (Off);
