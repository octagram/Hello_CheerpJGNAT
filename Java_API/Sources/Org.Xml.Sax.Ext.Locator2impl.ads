pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.Xml.Sax.Ext.Locator2;
with Org.Xml.Sax.Helpers.LocatorImpl;
with Org.Xml.Sax.Locator;

package Org.Xml.Sax.Ext.Locator2Impl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Locator_I : Org.Xml.Sax.Locator.Ref;
            Locator2_I : Org.Xml.Sax.Ext.Locator2.Ref)
    is new Org.Xml.Sax.Helpers.LocatorImpl.Typ(Locator_I)
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_Locator2Impl (This : Ref := null)
                              return Ref;

   function New_Locator2Impl (P1_Locator : access Standard.Org.Xml.Sax.Locator.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetXMLVersion (This : access Typ)
                           return access Java.Lang.String.Typ'Class;

   function GetEncoding (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   procedure SetXMLVersion (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetEncoding (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_Locator2Impl);
   pragma Import (Java, GetXMLVersion, "getXMLVersion");
   pragma Import (Java, GetEncoding, "getEncoding");
   pragma Import (Java, SetXMLVersion, "setXMLVersion");
   pragma Import (Java, SetEncoding, "setEncoding");

end Org.Xml.Sax.Ext.Locator2Impl;
pragma Import (Java, Org.Xml.Sax.Ext.Locator2Impl, "org.xml.sax.ext.Locator2Impl");
pragma Extensions_Allowed (Off);
