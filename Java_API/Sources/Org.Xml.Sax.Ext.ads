pragma Extensions_Allowed (On);
package Org.Xml.Sax.Ext is
   pragma Preelaborate;
end Org.Xml.Sax.Ext;
pragma Import (Java, Org.Xml.Sax.Ext, "org.xml.sax.ext");
pragma Extensions_Allowed (Off);
