pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Xml.Sax.AttributeList;
limited with Org.Xml.Sax.InputSource;
limited with Org.Xml.Sax.Locator;
limited with Org.Xml.Sax.SAXParseException;
with Java.Lang.Object;
with Org.Xml.Sax.DTDHandler;
with Org.Xml.Sax.DocumentHandler;
with Org.Xml.Sax.EntityResolver;
with Org.Xml.Sax.ErrorHandler;

package Org.Xml.Sax.HandlerBase is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DTDHandler_I : Org.Xml.Sax.DTDHandler.Ref;
            DocumentHandler_I : Org.Xml.Sax.DocumentHandler.Ref;
            EntityResolver_I : Org.Xml.Sax.EntityResolver.Ref;
            ErrorHandler_I : Org.Xml.Sax.ErrorHandler.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_HandlerBase (This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function ResolveEntity (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Org.Xml.Sax.InputSource.Typ'Class;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure NotationDecl (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class);

   procedure UnparsedEntityDecl (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                 P3_String : access Standard.Java.Lang.String.Typ'Class;
                                 P4_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetDocumentLocator (This : access Typ;
                                 P1_Locator : access Standard.Org.Xml.Sax.Locator.Typ'Class);

   procedure StartDocument (This : access Typ);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure EndDocument (This : access Typ);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure StartElement (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_AttributeList : access Standard.Org.Xml.Sax.AttributeList.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure EndElement (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure Characters (This : access Typ;
                         P1_Char_Arr : Java.Char_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure IgnorableWhitespace (This : access Typ;
                                  P1_Char_Arr : Java.Char_Arr;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure ProcessingInstruction (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure Warning (This : access Typ;
                      P1_SAXParseException : access Standard.Org.Xml.Sax.SAXParseException.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure Error (This : access Typ;
                    P1_SAXParseException : access Standard.Org.Xml.Sax.SAXParseException.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure FatalError (This : access Typ;
                         P1_SAXParseException : access Standard.Org.Xml.Sax.SAXParseException.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_HandlerBase);
   pragma Import (Java, ResolveEntity, "resolveEntity");
   pragma Import (Java, NotationDecl, "notationDecl");
   pragma Import (Java, UnparsedEntityDecl, "unparsedEntityDecl");
   pragma Import (Java, SetDocumentLocator, "setDocumentLocator");
   pragma Import (Java, StartDocument, "startDocument");
   pragma Import (Java, EndDocument, "endDocument");
   pragma Import (Java, StartElement, "startElement");
   pragma Import (Java, EndElement, "endElement");
   pragma Import (Java, Characters, "characters");
   pragma Import (Java, IgnorableWhitespace, "ignorableWhitespace");
   pragma Import (Java, ProcessingInstruction, "processingInstruction");
   pragma Import (Java, Warning, "warning");
   pragma Import (Java, Error, "error");
   pragma Import (Java, FatalError, "fatalError");

end Org.Xml.Sax.HandlerBase;
pragma Import (Java, Org.Xml.Sax.HandlerBase, "org.xml.sax.HandlerBase");
pragma Extensions_Allowed (Off);
