pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.Xml.Sax.AttributeList;

package Org.Xml.Sax.Helpers.AttributeListImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(AttributeList_I : Org.Xml.Sax.AttributeList.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AttributeListImpl (This : Ref := null)
                                   return Ref;

   function New_AttributeListImpl (P1_AttributeList : access Standard.Org.Xml.Sax.AttributeList.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetAttributeList (This : access Typ;
                               P1_AttributeList : access Standard.Org.Xml.Sax.AttributeList.Typ'Class);

   procedure AddAttribute (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class);

   procedure RemoveAttribute (This : access Typ;
                              P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure Clear (This : access Typ);

   function GetLength (This : access Typ)
                       return Java.Int;

   function GetName (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Lang.String.Typ'Class;

   function GetType (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Lang.String.Typ'Class;

   function GetValue (This : access Typ;
                      P1_Int : Java.Int)
                      return access Java.Lang.String.Typ'Class;

   function GetType (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.String.Typ'Class;

   function GetValue (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AttributeListImpl);
   pragma Import (Java, SetAttributeList, "setAttributeList");
   pragma Import (Java, AddAttribute, "addAttribute");
   pragma Import (Java, RemoveAttribute, "removeAttribute");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, GetLength, "getLength");
   pragma Import (Java, GetName, "getName");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetValue, "getValue");

end Org.Xml.Sax.Helpers.AttributeListImpl;
pragma Import (Java, Org.Xml.Sax.Helpers.AttributeListImpl, "org.xml.sax.helpers.AttributeListImpl");
pragma Extensions_Allowed (Off);
