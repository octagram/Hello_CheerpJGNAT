pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.Xml.Sax.Attributes;

package Org.Xml.Sax.Helpers.AttributesImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Attributes_I : Org.Xml.Sax.Attributes.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_AttributesImpl (This : Ref := null)
                                return Ref;

   function New_AttributesImpl (P1_Attributes : access Standard.Org.Xml.Sax.Attributes.Typ'Class; 
                                This : Ref := null)
                                return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetLength (This : access Typ)
                       return Java.Int;

   function GetURI (This : access Typ;
                    P1_Int : Java.Int)
                    return access Java.Lang.String.Typ'Class;

   function GetLocalName (This : access Typ;
                          P1_Int : Java.Int)
                          return access Java.Lang.String.Typ'Class;

   function GetQName (This : access Typ;
                      P1_Int : Java.Int)
                      return access Java.Lang.String.Typ'Class;

   function GetType (This : access Typ;
                     P1_Int : Java.Int)
                     return access Java.Lang.String.Typ'Class;

   function GetValue (This : access Typ;
                      P1_Int : Java.Int)
                      return access Java.Lang.String.Typ'Class;

   function GetIndex (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Int;

   function GetIndex (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return Java.Int;

   function GetType (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class;
                     P2_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.String.Typ'Class;

   function GetType (This : access Typ;
                     P1_String : access Standard.Java.Lang.String.Typ'Class)
                     return access Java.Lang.String.Typ'Class;

   function GetValue (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class;
                      P2_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Lang.String.Typ'Class;

   function GetValue (This : access Typ;
                      P1_String : access Standard.Java.Lang.String.Typ'Class)
                      return access Java.Lang.String.Typ'Class;

   procedure Clear (This : access Typ);

   procedure SetAttributes (This : access Typ;
                            P1_Attributes : access Standard.Org.Xml.Sax.Attributes.Typ'Class);

   procedure AddAttribute (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class;
                           P4_String : access Standard.Java.Lang.String.Typ'Class;
                           P5_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetAttribute (This : access Typ;
                           P1_Int : Java.Int;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class;
                           P4_String : access Standard.Java.Lang.String.Typ'Class;
                           P5_String : access Standard.Java.Lang.String.Typ'Class;
                           P6_String : access Standard.Java.Lang.String.Typ'Class);

   procedure RemoveAttribute (This : access Typ;
                              P1_Int : Java.Int);

   procedure SetURI (This : access Typ;
                     P1_Int : Java.Int;
                     P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetLocalName (This : access Typ;
                           P1_Int : Java.Int;
                           P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetQName (This : access Typ;
                       P1_Int : Java.Int;
                       P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetType (This : access Typ;
                      P1_Int : Java.Int;
                      P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetValue (This : access Typ;
                       P1_Int : Java.Int;
                       P2_String : access Standard.Java.Lang.String.Typ'Class);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_AttributesImpl);
   pragma Import (Java, GetLength, "getLength");
   pragma Import (Java, GetURI, "getURI");
   pragma Import (Java, GetLocalName, "getLocalName");
   pragma Import (Java, GetQName, "getQName");
   pragma Import (Java, GetType, "getType");
   pragma Import (Java, GetValue, "getValue");
   pragma Import (Java, GetIndex, "getIndex");
   pragma Import (Java, Clear, "clear");
   pragma Import (Java, SetAttributes, "setAttributes");
   pragma Import (Java, AddAttribute, "addAttribute");
   pragma Import (Java, SetAttribute, "setAttribute");
   pragma Import (Java, RemoveAttribute, "removeAttribute");
   pragma Import (Java, SetURI, "setURI");
   pragma Import (Java, SetLocalName, "setLocalName");
   pragma Import (Java, SetQName, "setQName");
   pragma Import (Java, SetType, "setType");
   pragma Import (Java, SetValue, "setValue");

end Org.Xml.Sax.Helpers.AttributesImpl;
pragma Import (Java, Org.Xml.Sax.Helpers.AttributesImpl, "org.xml.sax.helpers.AttributesImpl");
pragma Extensions_Allowed (Off);
