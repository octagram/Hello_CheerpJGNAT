pragma Extensions_Allowed (On);
limited with Java.Lang.String;
with Java.Lang.Object;
with Org.Xml.Sax.Locator;

package Org.Xml.Sax.Helpers.LocatorImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Locator_I : Org.Xml.Sax.Locator.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_LocatorImpl (This : Ref := null)
                             return Ref;

   function New_LocatorImpl (P1_Locator : access Standard.Org.Xml.Sax.Locator.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPublicId (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetSystemId (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetLineNumber (This : access Typ)
                           return Java.Int;

   function GetColumnNumber (This : access Typ)
                             return Java.Int;

   procedure SetPublicId (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetSystemId (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure SetLineNumber (This : access Typ;
                            P1_Int : Java.Int);

   procedure SetColumnNumber (This : access Typ;
                              P1_Int : Java.Int);
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_LocatorImpl);
   pragma Import (Java, GetPublicId, "getPublicId");
   pragma Import (Java, GetSystemId, "getSystemId");
   pragma Import (Java, GetLineNumber, "getLineNumber");
   pragma Import (Java, GetColumnNumber, "getColumnNumber");
   pragma Import (Java, SetPublicId, "setPublicId");
   pragma Import (Java, SetSystemId, "setSystemId");
   pragma Import (Java, SetLineNumber, "setLineNumber");
   pragma Import (Java, SetColumnNumber, "setColumnNumber");

end Org.Xml.Sax.Helpers.LocatorImpl;
pragma Import (Java, Org.Xml.Sax.Helpers.LocatorImpl, "org.xml.sax.helpers.LocatorImpl");
pragma Extensions_Allowed (Off);
