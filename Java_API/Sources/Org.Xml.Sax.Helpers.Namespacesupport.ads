pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Enumeration;
with Java.Lang.Object;

package Org.Xml.Sax.Helpers.NamespaceSupport is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_NamespaceSupport (This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure Reset (This : access Typ);

   procedure PushContext (This : access Typ);

   procedure PopContext (This : access Typ);

   function DeclarePrefix (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class)
                           return Java.Boolean;

   function ProcessName (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String_Arr : access Java.Lang.String.Arr_Obj;
                         P3_Boolean : Java.Boolean)
                         return Standard.Java.Lang.Object.Ref;

   function GetURI (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class)
                    return access Java.Lang.String.Typ'Class;

   function GetPrefixes (This : access Typ)
                         return access Java.Util.Enumeration.Typ'Class;

   function GetPrefix (This : access Typ;
                       P1_String : access Standard.Java.Lang.String.Typ'Class)
                       return access Java.Lang.String.Typ'Class;

   function GetPrefixes (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Util.Enumeration.Typ'Class;

   function GetDeclaredPrefixes (This : access Typ)
                                 return access Java.Util.Enumeration.Typ'Class;

   procedure SetNamespaceDeclUris (This : access Typ;
                                   P1_Boolean : Java.Boolean);

   function IsNamespaceDeclUris (This : access Typ)
                                 return Java.Boolean;

   ---------------------------
   -- Variable Declarations --
   ---------------------------

   --  final
   XMLNS : constant access Java.Lang.String.Typ'Class;

   --  final
   NSDECL : constant access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_NamespaceSupport);
   pragma Import (Java, Reset, "reset");
   pragma Import (Java, PushContext, "pushContext");
   pragma Import (Java, PopContext, "popContext");
   pragma Import (Java, DeclarePrefix, "declarePrefix");
   pragma Import (Java, ProcessName, "processName");
   pragma Import (Java, GetURI, "getURI");
   pragma Import (Java, GetPrefixes, "getPrefixes");
   pragma Import (Java, GetPrefix, "getPrefix");
   pragma Import (Java, GetDeclaredPrefixes, "getDeclaredPrefixes");
   pragma Import (Java, SetNamespaceDeclUris, "setNamespaceDeclUris");
   pragma Import (Java, IsNamespaceDeclUris, "isNamespaceDeclUris");
   pragma Import (Java, XMLNS, "XMLNS");
   pragma Import (Java, NSDECL, "NSDECL");

end Org.Xml.Sax.Helpers.NamespaceSupport;
pragma Import (Java, Org.Xml.Sax.Helpers.NamespaceSupport, "org.xml.sax.helpers.NamespaceSupport");
pragma Extensions_Allowed (Off);
