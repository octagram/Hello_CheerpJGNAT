pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Xml.Sax.AttributeList;
limited with Org.Xml.Sax.ContentHandler;
limited with Org.Xml.Sax.DTDHandler;
limited with Org.Xml.Sax.EntityResolver;
limited with Org.Xml.Sax.ErrorHandler;
limited with Org.Xml.Sax.InputSource;
limited with Org.Xml.Sax.Locator;
limited with Org.Xml.Sax.Parser;
with Java.Lang.Object;
with Org.Xml.Sax.DocumentHandler;
with Org.Xml.Sax.XMLReader;

package Org.Xml.Sax.Helpers.ParserAdapter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(DocumentHandler_I : Org.Xml.Sax.DocumentHandler.Ref;
            XMLReader_I : Org.Xml.Sax.XMLReader.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_ParserAdapter (This : Ref := null)
                               return Ref;
   --  can raise Org.Xml.Sax.SAXException.Except

   function New_ParserAdapter (P1_Parser : access Standard.Org.Xml.Sax.Parser.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetFeature (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Boolean : Java.Boolean);
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   function GetFeature (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean;
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   procedure SetProperty (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   procedure SetEntityResolver (This : access Typ;
                                P1_EntityResolver : access Standard.Org.Xml.Sax.EntityResolver.Typ'Class);

   function GetEntityResolver (This : access Typ)
                               return access Org.Xml.Sax.EntityResolver.Typ'Class;

   procedure SetDTDHandler (This : access Typ;
                            P1_DTDHandler : access Standard.Org.Xml.Sax.DTDHandler.Typ'Class);

   function GetDTDHandler (This : access Typ)
                           return access Org.Xml.Sax.DTDHandler.Typ'Class;

   procedure SetContentHandler (This : access Typ;
                                P1_ContentHandler : access Standard.Org.Xml.Sax.ContentHandler.Typ'Class);

   function GetContentHandler (This : access Typ)
                               return access Org.Xml.Sax.ContentHandler.Typ'Class;

   procedure SetErrorHandler (This : access Typ;
                              P1_ErrorHandler : access Standard.Org.Xml.Sax.ErrorHandler.Typ'Class);

   function GetErrorHandler (This : access Typ)
                             return access Org.Xml.Sax.ErrorHandler.Typ'Class;

   procedure Parse (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Org.Xml.Sax.SAXException.Except

   procedure Parse (This : access Typ;
                    P1_InputSource : access Standard.Org.Xml.Sax.InputSource.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Org.Xml.Sax.SAXException.Except

   procedure SetDocumentLocator (This : access Typ;
                                 P1_Locator : access Standard.Org.Xml.Sax.Locator.Typ'Class);

   procedure StartDocument (This : access Typ);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure EndDocument (This : access Typ);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure StartElement (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_AttributeList : access Standard.Org.Xml.Sax.AttributeList.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure EndElement (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure Characters (This : access Typ;
                         P1_Char_Arr : Java.Char_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure IgnorableWhitespace (This : access Typ;
                                  P1_Char_Arr : Java.Char_Arr;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure ProcessingInstruction (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_ParserAdapter);
   pragma Import (Java, SetFeature, "setFeature");
   pragma Import (Java, GetFeature, "getFeature");
   pragma Import (Java, SetProperty, "setProperty");
   pragma Import (Java, GetProperty, "getProperty");
   pragma Import (Java, SetEntityResolver, "setEntityResolver");
   pragma Import (Java, GetEntityResolver, "getEntityResolver");
   pragma Import (Java, SetDTDHandler, "setDTDHandler");
   pragma Import (Java, GetDTDHandler, "getDTDHandler");
   pragma Import (Java, SetContentHandler, "setContentHandler");
   pragma Import (Java, GetContentHandler, "getContentHandler");
   pragma Import (Java, SetErrorHandler, "setErrorHandler");
   pragma Import (Java, GetErrorHandler, "getErrorHandler");
   pragma Import (Java, Parse, "parse");
   pragma Import (Java, SetDocumentLocator, "setDocumentLocator");
   pragma Import (Java, StartDocument, "startDocument");
   pragma Import (Java, EndDocument, "endDocument");
   pragma Import (Java, StartElement, "startElement");
   pragma Import (Java, EndElement, "endElement");
   pragma Import (Java, Characters, "characters");
   pragma Import (Java, IgnorableWhitespace, "ignorableWhitespace");
   pragma Import (Java, ProcessingInstruction, "processingInstruction");

end Org.Xml.Sax.Helpers.ParserAdapter;
pragma Import (Java, Org.Xml.Sax.Helpers.ParserAdapter, "org.xml.sax.helpers.ParserAdapter");
pragma Extensions_Allowed (Off);
