pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Xml.Sax.Parser;
with Java.Lang.Object;

package Org.Xml.Sax.Helpers.ParserFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function MakeParser return access Org.Xml.Sax.Parser.Typ'Class;
   --  can raise Java.Lang.ClassNotFoundException.Except,
   --  Java.Lang.IllegalAccessException.Except,
   --  Java.Lang.InstantiationException.Except,
   --  Java.Lang.NullPointerException.Except and
   --  Java.Lang.ClassCastException.Except

   function MakeParser (P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return access Org.Xml.Sax.Parser.Typ'Class;
   --  can raise Java.Lang.ClassNotFoundException.Except,
   --  Java.Lang.IllegalAccessException.Except,
   --  Java.Lang.InstantiationException.Except and
   --  Java.Lang.ClassCastException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, MakeParser, "makeParser");

end Org.Xml.Sax.Helpers.ParserFactory;
pragma Import (Java, Org.Xml.Sax.Helpers.ParserFactory, "org.xml.sax.helpers.ParserFactory");
pragma Extensions_Allowed (Off);
