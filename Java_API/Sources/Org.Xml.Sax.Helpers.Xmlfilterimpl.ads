pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Xml.Sax.Attributes;
limited with Org.Xml.Sax.InputSource;
limited with Org.Xml.Sax.Locator;
limited with Org.Xml.Sax.SAXParseException;
limited with Org.Xml.Sax.XMLReader;
with Java.Lang.Object;
with Org.Xml.Sax.ContentHandler;
with Org.Xml.Sax.DTDHandler;
with Org.Xml.Sax.EntityResolver;
with Org.Xml.Sax.ErrorHandler;
with Org.Xml.Sax.XMLFilter;

package Org.Xml.Sax.Helpers.XMLFilterImpl is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ContentHandler_I : Org.Xml.Sax.ContentHandler.Ref;
            DTDHandler_I : Org.Xml.Sax.DTDHandler.Ref;
            EntityResolver_I : Org.Xml.Sax.EntityResolver.Ref;
            ErrorHandler_I : Org.Xml.Sax.ErrorHandler.Ref;
            XMLFilter_I : Org.Xml.Sax.XMLFilter.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_XMLFilterImpl (This : Ref := null)
                               return Ref;

   function New_XMLFilterImpl (P1_XMLReader : access Standard.Org.Xml.Sax.XMLReader.Typ'Class; 
                               This : Ref := null)
                               return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetParent (This : access Typ;
                        P1_XMLReader : access Standard.Org.Xml.Sax.XMLReader.Typ'Class);

   function GetParent (This : access Typ)
                       return access Org.Xml.Sax.XMLReader.Typ'Class;

   procedure SetFeature (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Boolean : Java.Boolean);
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   function GetFeature (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean;
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   procedure SetProperty (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class);
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class;
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   procedure SetEntityResolver (This : access Typ;
                                P1_EntityResolver : access Standard.Org.Xml.Sax.EntityResolver.Typ'Class);

   function GetEntityResolver (This : access Typ)
                               return access Org.Xml.Sax.EntityResolver.Typ'Class;

   procedure SetDTDHandler (This : access Typ;
                            P1_DTDHandler : access Standard.Org.Xml.Sax.DTDHandler.Typ'Class);

   function GetDTDHandler (This : access Typ)
                           return access Org.Xml.Sax.DTDHandler.Typ'Class;

   procedure SetContentHandler (This : access Typ;
                                P1_ContentHandler : access Standard.Org.Xml.Sax.ContentHandler.Typ'Class);

   function GetContentHandler (This : access Typ)
                               return access Org.Xml.Sax.ContentHandler.Typ'Class;

   procedure SetErrorHandler (This : access Typ;
                              P1_ErrorHandler : access Standard.Org.Xml.Sax.ErrorHandler.Typ'Class);

   function GetErrorHandler (This : access Typ)
                             return access Org.Xml.Sax.ErrorHandler.Typ'Class;

   procedure Parse (This : access Typ;
                    P1_InputSource : access Standard.Org.Xml.Sax.InputSource.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   procedure Parse (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   function ResolveEntity (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class)
                           return access Org.Xml.Sax.InputSource.Typ'Class;
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   procedure NotationDecl (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure UnparsedEntityDecl (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class;
                                 P3_String : access Standard.Java.Lang.String.Typ'Class;
                                 P4_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure SetDocumentLocator (This : access Typ;
                                 P1_Locator : access Standard.Org.Xml.Sax.Locator.Typ'Class);

   procedure StartDocument (This : access Typ);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure EndDocument (This : access Typ);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure StartPrefixMapping (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure EndPrefixMapping (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure StartElement (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class;
                           P4_Attributes : access Standard.Org.Xml.Sax.Attributes.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure EndElement (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure Characters (This : access Typ;
                         P1_Char_Arr : Java.Char_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure IgnorableWhitespace (This : access Typ;
                                  P1_Char_Arr : Java.Char_Arr;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure ProcessingInstruction (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure SkippedEntity (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure Warning (This : access Typ;
                      P1_SAXParseException : access Standard.Org.Xml.Sax.SAXParseException.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure Error (This : access Typ;
                    P1_SAXParseException : access Standard.Org.Xml.Sax.SAXParseException.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure FatalError (This : access Typ;
                         P1_SAXParseException : access Standard.Org.Xml.Sax.SAXParseException.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_XMLFilterImpl);
   pragma Import (Java, SetParent, "setParent");
   pragma Import (Java, GetParent, "getParent");
   pragma Import (Java, SetFeature, "setFeature");
   pragma Import (Java, GetFeature, "getFeature");
   pragma Import (Java, SetProperty, "setProperty");
   pragma Import (Java, GetProperty, "getProperty");
   pragma Import (Java, SetEntityResolver, "setEntityResolver");
   pragma Import (Java, GetEntityResolver, "getEntityResolver");
   pragma Import (Java, SetDTDHandler, "setDTDHandler");
   pragma Import (Java, GetDTDHandler, "getDTDHandler");
   pragma Import (Java, SetContentHandler, "setContentHandler");
   pragma Import (Java, GetContentHandler, "getContentHandler");
   pragma Import (Java, SetErrorHandler, "setErrorHandler");
   pragma Import (Java, GetErrorHandler, "getErrorHandler");
   pragma Import (Java, Parse, "parse");
   pragma Import (Java, ResolveEntity, "resolveEntity");
   pragma Import (Java, NotationDecl, "notationDecl");
   pragma Import (Java, UnparsedEntityDecl, "unparsedEntityDecl");
   pragma Import (Java, SetDocumentLocator, "setDocumentLocator");
   pragma Import (Java, StartDocument, "startDocument");
   pragma Import (Java, EndDocument, "endDocument");
   pragma Import (Java, StartPrefixMapping, "startPrefixMapping");
   pragma Import (Java, EndPrefixMapping, "endPrefixMapping");
   pragma Import (Java, StartElement, "startElement");
   pragma Import (Java, EndElement, "endElement");
   pragma Import (Java, Characters, "characters");
   pragma Import (Java, IgnorableWhitespace, "ignorableWhitespace");
   pragma Import (Java, ProcessingInstruction, "processingInstruction");
   pragma Import (Java, SkippedEntity, "skippedEntity");
   pragma Import (Java, Warning, "warning");
   pragma Import (Java, Error, "error");
   pragma Import (Java, FatalError, "fatalError");

end Org.Xml.Sax.Helpers.XMLFilterImpl;
pragma Import (Java, Org.Xml.Sax.Helpers.XMLFilterImpl, "org.xml.sax.helpers.XMLFilterImpl");
pragma Extensions_Allowed (Off);
