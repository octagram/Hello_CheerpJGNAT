pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Locale;
limited with Org.Xml.Sax.Attributes;
limited with Org.Xml.Sax.DTDHandler;
limited with Org.Xml.Sax.DocumentHandler;
limited with Org.Xml.Sax.EntityResolver;
limited with Org.Xml.Sax.ErrorHandler;
limited with Org.Xml.Sax.InputSource;
limited with Org.Xml.Sax.Locator;
limited with Org.Xml.Sax.XMLReader;
with Java.Lang.Object;
with Org.Xml.Sax.ContentHandler;
with Org.Xml.Sax.Parser;

package Org.Xml.Sax.Helpers.XMLReaderAdapter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(ContentHandler_I : Org.Xml.Sax.ContentHandler.Ref;
            Parser_I : Org.Xml.Sax.Parser.Ref)
    is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_XMLReaderAdapter (This : Ref := null)
                                  return Ref;
   --  can raise Org.Xml.Sax.SAXException.Except

   function New_XMLReaderAdapter (P1_XMLReader : access Standard.Org.Xml.Sax.XMLReader.Typ'Class; 
                                  This : Ref := null)
                                  return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetLocale (This : access Typ;
                        P1_Locale : access Standard.Java.Util.Locale.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure SetEntityResolver (This : access Typ;
                                P1_EntityResolver : access Standard.Org.Xml.Sax.EntityResolver.Typ'Class);

   procedure SetDTDHandler (This : access Typ;
                            P1_DTDHandler : access Standard.Org.Xml.Sax.DTDHandler.Typ'Class);

   procedure SetDocumentHandler (This : access Typ;
                                 P1_DocumentHandler : access Standard.Org.Xml.Sax.DocumentHandler.Typ'Class);

   procedure SetErrorHandler (This : access Typ;
                              P1_ErrorHandler : access Standard.Org.Xml.Sax.ErrorHandler.Typ'Class);

   procedure Parse (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Org.Xml.Sax.SAXException.Except

   procedure Parse (This : access Typ;
                    P1_InputSource : access Standard.Org.Xml.Sax.InputSource.Typ'Class);
   --  can raise Java.Io.IOException.Except and
   --  Org.Xml.Sax.SAXException.Except

   procedure SetDocumentLocator (This : access Typ;
                                 P1_Locator : access Standard.Org.Xml.Sax.Locator.Typ'Class);

   procedure StartDocument (This : access Typ);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure EndDocument (This : access Typ);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure StartPrefixMapping (This : access Typ;
                                 P1_String : access Standard.Java.Lang.String.Typ'Class;
                                 P2_String : access Standard.Java.Lang.String.Typ'Class);

   procedure EndPrefixMapping (This : access Typ;
                               P1_String : access Standard.Java.Lang.String.Typ'Class);

   procedure StartElement (This : access Typ;
                           P1_String : access Standard.Java.Lang.String.Typ'Class;
                           P2_String : access Standard.Java.Lang.String.Typ'Class;
                           P3_String : access Standard.Java.Lang.String.Typ'Class;
                           P4_Attributes : access Standard.Org.Xml.Sax.Attributes.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure EndElement (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_String : access Standard.Java.Lang.String.Typ'Class;
                         P3_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure Characters (This : access Typ;
                         P1_Char_Arr : Java.Char_Arr;
                         P2_Int : Java.Int;
                         P3_Int : Java.Int);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure IgnorableWhitespace (This : access Typ;
                                  P1_Char_Arr : Java.Char_Arr;
                                  P2_Int : Java.Int;
                                  P3_Int : Java.Int);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure ProcessingInstruction (This : access Typ;
                                    P1_String : access Standard.Java.Lang.String.Typ'Class;
                                    P2_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure SkippedEntity (This : access Typ;
                            P1_String : access Standard.Java.Lang.String.Typ'Class);
   --  can raise Org.Xml.Sax.SAXException.Except
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_XMLReaderAdapter);
   pragma Import (Java, SetLocale, "setLocale");
   pragma Import (Java, SetEntityResolver, "setEntityResolver");
   pragma Import (Java, SetDTDHandler, "setDTDHandler");
   pragma Import (Java, SetDocumentHandler, "setDocumentHandler");
   pragma Import (Java, SetErrorHandler, "setErrorHandler");
   pragma Import (Java, Parse, "parse");
   pragma Import (Java, SetDocumentLocator, "setDocumentLocator");
   pragma Import (Java, StartDocument, "startDocument");
   pragma Import (Java, EndDocument, "endDocument");
   pragma Import (Java, StartPrefixMapping, "startPrefixMapping");
   pragma Import (Java, EndPrefixMapping, "endPrefixMapping");
   pragma Import (Java, StartElement, "startElement");
   pragma Import (Java, EndElement, "endElement");
   pragma Import (Java, Characters, "characters");
   pragma Import (Java, IgnorableWhitespace, "ignorableWhitespace");
   pragma Import (Java, ProcessingInstruction, "processingInstruction");
   pragma Import (Java, SkippedEntity, "skippedEntity");

end Org.Xml.Sax.Helpers.XMLReaderAdapter;
pragma Import (Java, Org.Xml.Sax.Helpers.XMLReaderAdapter, "org.xml.sax.helpers.XMLReaderAdapter");
pragma Extensions_Allowed (Off);
