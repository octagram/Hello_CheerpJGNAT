pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Xml.Sax.XMLReader;
with Java.Lang.Object;

package Org.Xml.Sax.Helpers.XMLReaderFactory is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   --  final class
   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   -------------------------
   -- Method Declarations --
   -------------------------

   function CreateXMLReader return access Org.Xml.Sax.XMLReader.Typ'Class;
   --  can raise Org.Xml.Sax.SAXException.Except

   function CreateXMLReader (P1_String : access Standard.Java.Lang.String.Typ'Class)
                             return access Org.Xml.Sax.XMLReader.Typ'Class;
   --  can raise Org.Xml.Sax.SAXException.Except
private
   pragma Convention (Java, Typ);
   pragma Import (Java, CreateXMLReader, "createXMLReader");

end Org.Xml.Sax.Helpers.XMLReaderFactory;
pragma Import (Java, Org.Xml.Sax.Helpers.XMLReaderFactory, "org.xml.sax.helpers.XMLReaderFactory");
pragma Extensions_Allowed (Off);
