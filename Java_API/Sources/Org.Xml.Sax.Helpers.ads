pragma Extensions_Allowed (On);
package Org.Xml.Sax.Helpers is
   pragma Preelaborate;
end Org.Xml.Sax.Helpers;
pragma Import (Java, Org.Xml.Sax.Helpers, "org.xml.sax.helpers");
pragma Extensions_Allowed (Off);
