pragma Extensions_Allowed (On);
limited with Java.Io.InputStream;
limited with Java.Io.Reader;
limited with Java.Lang.String;
with Java.Lang.Object;

package Org.Xml.Sax.InputSource is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ is new Java.Lang.Object.Typ
      with null record;

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_InputSource (This : Ref := null)
                             return Ref;

   function New_InputSource (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_InputSource (P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   function New_InputSource (P1_Reader : access Standard.Java.Io.Reader.Typ'Class; 
                             This : Ref := null)
                             return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetPublicId (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetPublicId (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   procedure SetSystemId (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetSystemId (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   procedure SetByteStream (This : access Typ;
                            P1_InputStream : access Standard.Java.Io.InputStream.Typ'Class);

   function GetByteStream (This : access Typ)
                           return access Java.Io.InputStream.Typ'Class;

   procedure SetEncoding (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class);

   function GetEncoding (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   procedure SetCharacterStream (This : access Typ;
                                 P1_Reader : access Standard.Java.Io.Reader.Typ'Class);

   function GetCharacterStream (This : access Typ)
                                return access Java.Io.Reader.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Java_Constructor (New_InputSource);
   pragma Import (Java, SetPublicId, "setPublicId");
   pragma Import (Java, GetPublicId, "getPublicId");
   pragma Import (Java, SetSystemId, "setSystemId");
   pragma Import (Java, GetSystemId, "getSystemId");
   pragma Import (Java, SetByteStream, "setByteStream");
   pragma Import (Java, GetByteStream, "getByteStream");
   pragma Import (Java, SetEncoding, "setEncoding");
   pragma Import (Java, GetEncoding, "getEncoding");
   pragma Import (Java, SetCharacterStream, "setCharacterStream");
   pragma Import (Java, GetCharacterStream, "getCharacterStream");

end Org.Xml.Sax.InputSource;
pragma Import (Java, Org.Xml.Sax.InputSource, "org.xml.sax.InputSource");
pragma Extensions_Allowed (Off);
