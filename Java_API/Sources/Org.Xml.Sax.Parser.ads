pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Util.Locale;
limited with Org.Xml.Sax.DTDHandler;
limited with Org.Xml.Sax.DocumentHandler;
limited with Org.Xml.Sax.EntityResolver;
limited with Org.Xml.Sax.ErrorHandler;
limited with Org.Xml.Sax.InputSource;
with Java.Lang.Object;

package Org.Xml.Sax.Parser is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetLocale (This : access Typ;
                        P1_Locale : access Standard.Java.Util.Locale.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except

   procedure SetEntityResolver (This : access Typ;
                                P1_EntityResolver : access Standard.Org.Xml.Sax.EntityResolver.Typ'Class) is abstract;

   procedure SetDTDHandler (This : access Typ;
                            P1_DTDHandler : access Standard.Org.Xml.Sax.DTDHandler.Typ'Class) is abstract;

   procedure SetDocumentHandler (This : access Typ;
                                 P1_DocumentHandler : access Standard.Org.Xml.Sax.DocumentHandler.Typ'Class) is abstract;

   procedure SetErrorHandler (This : access Typ;
                              P1_ErrorHandler : access Standard.Org.Xml.Sax.ErrorHandler.Typ'Class) is abstract;

   procedure Parse (This : access Typ;
                    P1_InputSource : access Standard.Org.Xml.Sax.InputSource.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except

   procedure Parse (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXException.Except and
   --  Java.Io.IOException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetLocale, "setLocale");
   pragma Export (Java, SetEntityResolver, "setEntityResolver");
   pragma Export (Java, SetDTDHandler, "setDTDHandler");
   pragma Export (Java, SetDocumentHandler, "setDocumentHandler");
   pragma Export (Java, SetErrorHandler, "setErrorHandler");
   pragma Export (Java, Parse, "parse");

end Org.Xml.Sax.Parser;
pragma Import (Java, Org.Xml.Sax.Parser, "org.xml.sax.Parser");
pragma Extensions_Allowed (Off);
