pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Java.Lang.Throwable;
with Java.Io.Serializable;
with Java.Lang.Exception_K;
with Java.Lang.Object;

package Org.Xml.Sax.SAXException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Java.Lang.Exception_K.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SAXException (This : Ref := null)
                              return Ref;

   function New_SAXException (P1_String : access Standard.Java.Lang.String.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_SAXException (P1_Exception_K : access Standard.Java.Lang.Exception_K.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   function New_SAXException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                              P2_Exception_K : access Standard.Java.Lang.Exception_K.Typ'Class; 
                              This : Ref := null)
                              return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetMessage (This : access Typ)
                        return access Java.Lang.String.Typ'Class;

   function GetException (This : access Typ)
                          return access Java.Lang.Exception_K.Typ'Class;

   function GetCause (This : access Typ)
                      return access Java.Lang.Throwable.Typ'Class;

   function ToString (This : access Typ)
                      return access Java.Lang.String.Typ'Class;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "org.xml.sax.SAXException");
   pragma Java_Constructor (New_SAXException);
   pragma Import (Java, GetMessage, "getMessage");
   pragma Import (Java, GetException, "getException");
   pragma Import (Java, GetCause, "getCause");
   pragma Import (Java, ToString, "toString");

end Org.Xml.Sax.SAXException;
pragma Import (Java, Org.Xml.Sax.SAXException, "org.xml.sax.SAXException");
pragma Extensions_Allowed (Off);
