pragma Extensions_Allowed (On);
limited with Java.Lang.Exception_K;
limited with Java.Lang.String;
limited with Org.Xml.Sax.Locator;
with Java.Io.Serializable;
with Java.Lang.Object;
with Org.Xml.Sax.SAXException;

package Org.Xml.Sax.SAXParseException is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Serializable_I : Java.Io.Serializable.Ref)
    is new Org.Xml.Sax.SAXException.Typ(Serializable_I)
      with null record;
---------------------------
   -- Exception Declaration --
   ---------------------------
Except : Exception;
   

   ------------------------------
   -- Constructor Declarations --
   ------------------------------

   function New_SAXParseException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_Locator : access Standard.Org.Xml.Sax.Locator.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_SAXParseException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_Locator : access Standard.Org.Xml.Sax.Locator.Typ'Class;
                                   P3_Exception_K : access Standard.Java.Lang.Exception_K.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   function New_SAXParseException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_String : access Standard.Java.Lang.String.Typ'Class;
                                   P3_String : access Standard.Java.Lang.String.Typ'Class;
                                   P4_Int : Java.Int;
                                   P5_Int : Java.Int; 
                                   This : Ref := null)
                                   return Ref;

   function New_SAXParseException (P1_String : access Standard.Java.Lang.String.Typ'Class;
                                   P2_String : access Standard.Java.Lang.String.Typ'Class;
                                   P3_String : access Standard.Java.Lang.String.Typ'Class;
                                   P4_Int : Java.Int;
                                   P5_Int : Java.Int;
                                   P6_Exception_K : access Standard.Java.Lang.Exception_K.Typ'Class; 
                                   This : Ref := null)
                                   return Ref;

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetPublicId (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetSystemId (This : access Typ)
                         return access Java.Lang.String.Typ'Class;

   function GetLineNumber (This : access Typ)
                           return Java.Int;

   function GetColumnNumber (This : access Typ)
                             return Java.Int;
private
   pragma Convention (Java, Typ);
   pragma Import (Java, Except, "org.xml.sax.SAXParseException");
   pragma Java_Constructor (New_SAXParseException);
   pragma Import (Java, GetPublicId, "getPublicId");
   pragma Import (Java, GetSystemId, "getSystemId");
   pragma Import (Java, GetLineNumber, "getLineNumber");
   pragma Import (Java, GetColumnNumber, "getColumnNumber");

end Org.Xml.Sax.SAXParseException;
pragma Import (Java, Org.Xml.Sax.SAXParseException, "org.xml.sax.SAXParseException");
pragma Extensions_Allowed (Off);
