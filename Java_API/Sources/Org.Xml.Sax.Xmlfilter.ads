pragma Extensions_Allowed (On);
with Java.Lang.Object;
with Org.Xml.Sax.XMLReader;

package Org.Xml.Sax.XMLFilter is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class;
            XMLReader_I : Org.Xml.Sax.XMLReader.Ref)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   procedure SetParent (This : access Typ;
                        P1_XMLReader : access Standard.Org.Xml.Sax.XMLReader.Typ'Class) is abstract;

   function GetParent (This : access Typ)
                       return access Org.Xml.Sax.XMLReader.Typ'Class is abstract;
private
   pragma Convention (Java, Typ);
   pragma Export (Java, SetParent, "setParent");
   pragma Export (Java, GetParent, "getParent");

end Org.Xml.Sax.XMLFilter;
pragma Import (Java, Org.Xml.Sax.XMLFilter, "org.xml.sax.XMLFilter");
pragma Extensions_Allowed (Off);
