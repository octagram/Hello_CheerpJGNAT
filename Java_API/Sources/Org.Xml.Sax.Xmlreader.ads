pragma Extensions_Allowed (On);
limited with Java.Lang.String;
limited with Org.Xml.Sax.ContentHandler;
limited with Org.Xml.Sax.DTDHandler;
limited with Org.Xml.Sax.EntityResolver;
limited with Org.Xml.Sax.ErrorHandler;
limited with Org.Xml.Sax.InputSource;
with Java.Lang.Object;

package Org.Xml.Sax.XMLReader is
pragma Preelaborate;

   -----------------------
   -- Type Declarations --
   -----------------------

   type Typ;
   type Ref is access all Typ'Class;
   
   ------------------------
   -- Array Declarations --
   ------------------------

   type Arr_Obj is array (Natural range <>) of Ref;
   type Arr     is access all Arr_Obj;
   type Arr_2_Obj is array (Natural range <>) of Arr;
   type Arr_2     is access all Arr_2_Obj;
   type Arr_3_Obj is array (Natural range <>) of Arr_2;
   type Arr_3     is access all Arr_3_Obj;

   type Typ(Self : access Standard.Java.Lang.Object.Typ'Class)
    is abstract new Java.Lang.Object.Typ
      with null record;
pragma Java_Interface (Typ);
   

   -------------------------
   -- Method Declarations --
   -------------------------

   function GetFeature (This : access Typ;
                        P1_String : access Standard.Java.Lang.String.Typ'Class)
                        return Java.Boolean is abstract;
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   procedure SetFeature (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class;
                         P2_Boolean : Java.Boolean) is abstract;
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   function GetProperty (This : access Typ;
                         P1_String : access Standard.Java.Lang.String.Typ'Class)
                         return access Java.Lang.Object.Typ'Class is abstract;
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   procedure SetProperty (This : access Typ;
                          P1_String : access Standard.Java.Lang.String.Typ'Class;
                          P2_Object : access Standard.Java.Lang.Object.Typ'Class) is abstract;
   --  can raise Org.Xml.Sax.SAXNotRecognizedException.Except and
   --  Org.Xml.Sax.SAXNotSupportedException.Except

   procedure SetEntityResolver (This : access Typ;
                                P1_EntityResolver : access Standard.Org.Xml.Sax.EntityResolver.Typ'Class) is abstract;

   function GetEntityResolver (This : access Typ)
                               return access Org.Xml.Sax.EntityResolver.Typ'Class is abstract;

   procedure SetDTDHandler (This : access Typ;
                            P1_DTDHandler : access Standard.Org.Xml.Sax.DTDHandler.Typ'Class) is abstract;

   function GetDTDHandler (This : access Typ)
                           return access Org.Xml.Sax.DTDHandler.Typ'Class is abstract;

   procedure SetContentHandler (This : access Typ;
                                P1_ContentHandler : access Standard.Org.Xml.Sax.ContentHandler.Typ'Class) is abstract;

   function GetContentHandler (This : access Typ)
                               return access Org.Xml.Sax.ContentHandler.Typ'Class is abstract;

   procedure SetErrorHandler (This : access Typ;
                              P1_ErrorHandler : access Standard.Org.Xml.Sax.ErrorHandler.Typ'Class) is abstract;

   function GetErrorHandler (This : access Typ)
                             return access Org.Xml.Sax.ErrorHandler.Typ'Class is abstract;

   procedure Parse (This : access Typ;
                    P1_InputSource : access Standard.Org.Xml.Sax.InputSource.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except and
   --  Org.Xml.Sax.SAXException.Except

   procedure Parse (This : access Typ;
                    P1_String : access Standard.Java.Lang.String.Typ'Class) is abstract;
   --  can raise Java.Io.IOException.Except and
   --  Org.Xml.Sax.SAXException.Except
private
   pragma Convention (Java, Typ);
   pragma Export (Java, GetFeature, "getFeature");
   pragma Export (Java, SetFeature, "setFeature");
   pragma Export (Java, GetProperty, "getProperty");
   pragma Export (Java, SetProperty, "setProperty");
   pragma Export (Java, SetEntityResolver, "setEntityResolver");
   pragma Export (Java, GetEntityResolver, "getEntityResolver");
   pragma Export (Java, SetDTDHandler, "setDTDHandler");
   pragma Export (Java, GetDTDHandler, "getDTDHandler");
   pragma Export (Java, SetContentHandler, "setContentHandler");
   pragma Export (Java, GetContentHandler, "getContentHandler");
   pragma Export (Java, SetErrorHandler, "setErrorHandler");
   pragma Export (Java, GetErrorHandler, "getErrorHandler");
   pragma Export (Java, Parse, "parse");

end Org.Xml.Sax.XMLReader;
pragma Import (Java, Org.Xml.Sax.XMLReader, "org.xml.sax.XMLReader");
pragma Extensions_Allowed (Off);
