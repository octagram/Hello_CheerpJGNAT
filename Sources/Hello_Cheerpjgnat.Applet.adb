with Java.Lang.String;

with Java.Awt.Component;
with Java.Awt.Event.ActionEvent;

with Javax.Swing.JApplet;
with Javax.Swing.JButton;
with Javax.Swing.JOptionPane;

with Hello_CheerpJGNAT.Life_Cycle;

package body Hello_CheerpJGNAT.Applet is

   function "+" (S : Java.Lang.String.Ref) return Java.Lang.String.String_Access renames Java.Lang.String."+";
   function "+" (S : String) return Java.Lang.String.Ref renames Java.Lang.String."+";

   -----------------------------------
   -- Hello_CheerpJGNAT.Applet.Init --
   -----------------------------------

   procedure Init (This : access Typ) is
      Handler_Installed : Integer;
      pragma Import (C, Handler_Installed, "__gnat_handler_installed");
   begin
      Handler_Installed := 1; -- workaround for permission denied
      Life_Cycle.Adainit;

      --  other initializations go here, after the call to Adainit
      declare
         Button : constant Javax.Swing.JButton.Ref := Javax.Swing.JButton.New_JButton (+"Click Me!");
      begin
         Button.AddActionListener (This.ActionListener_I);
         declare
            Added_Component : constant Java.Awt.Component.Ref := This.GetContentPane.Add (Button) with Unreferenced;
         begin
            null;
         end;
      end;
   end Init;

   --------------------------------------
   -- Hello_CheerpJGNAT.Applet.Destroy --
   --------------------------------------

   procedure Destroy (This : access Typ) is
   begin
      --  other finalizations go here, before the call to Adafinal

      Life_Cycle.Adafinal;
   end Destroy;

   ----------------------------------------------
   -- Hello_CheerpJGNAT.Applet.ActionPerformed --
   ----------------------------------------------

   procedure ActionPerformed
     (This : access Typ; Event : access Java.Awt.Event.ActionEvent.Typ'Class)
   is
      Title : constant Java.Lang.String.Ref := +"Greetings";  -- Shown in title bar of dialog box.
      Message : constant Java.Lang.String.Ref := +"Hello from the Swing User Interface Library.";
   begin
      Javax.Swing.JOptionPane.ShowMessageDialog
        (null, Message, Title,
         Javax.Swing.JOptionPane.INFORMATION_MESSAGE);
   end ActionPerformed;

end Hello_CheerpJGNAT.Applet;

