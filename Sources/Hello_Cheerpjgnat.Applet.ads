-- An applet that appears on the page as a button that says
-- "Click Me!".  When the button is clicked, an informational
-- dialog box appears to say Hello from Swing.

with Java.Io.Serializable;

with Java.Awt.Event.ActionEvent;
with Java.Awt.MenuContainer;
with Java.Awt.Image.ImageObserver;
with Java.Awt.Event.ActionListener;

with Javax.Swing.JApplet;
with Javax.Swing.RootPaneContainer;
with Javax.Accessibility.Accessible;

package Hello_CheerpJGNAT.Applet is

   type Typ;
   type Ref is access all Typ'Class;

   type Typ
     (ActionListener_I : Java.Awt.Event.ActionListener.Ref;
      MenuContainer_I : Java.Awt.MenuContainer.Ref;
      ImageObserver_I : Java.Awt.Image.ImageObserver.Ref;
      Serializable_I : Java.Io.Serializable.Ref;
      Accessible_I : Javax.Accessibility.Accessible.Ref;
      RootPaneContainer_I : Javax.Swing.RootPaneContainer.Ref)
   is
      new Javax.Swing.JApplet.Typ
     (MenuContainer_I,
      ImageObserver_I,
      Serializable_I,
      Accessible_I,
      RootPaneContainer_I)
   with record
      null;
   end record;

   procedure Destroy (This : access Typ);

   -- This method is called by the system before the applet
   -- appears.  It is used here to create the button and add
   -- it to the "content pane" of the JApplet.  The applet
   -- is also registered as an ActionListener for the button.
   procedure Init (This : access Typ);

   -- This method is called when an action event occurs.
   -- In this case, the only possible source of the event
   -- is the button.  So, when this method is called, we know
   -- that the button has been clicked.  Respond by showing
   -- an informational dialog box.  The dialog box will
   -- contain an "OK" button which the user must click to
   -- dismiss the dialog box.
   procedure ActionPerformed
     (This : access Typ; Event : access Java.Awt.Event.ActionEvent.Typ'Class);

end Hello_CheerpJGNAT.Applet;

