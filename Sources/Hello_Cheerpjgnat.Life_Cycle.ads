pragma Extensions_Allowed (On);
package Hello_CheerpJGNAT.Life_Cycle is
   pragma Preelaborate;

   procedure Adainit;
   procedure Adafinal;

private
   pragma Import (Java, Adainit, "adainit");
   pragma Import (Java, Adafinal, "adafinal");

end Hello_CheerpJGNAT.Life_Cycle;
pragma Import (Java, Hello_CheerpJGNAT.Life_Cycle, "ada_hello_cheerpjgnat");
pragma Extensions_Allowed (Off);
